<!DOCTYPE html>

<html>

  <head>

    <title>RIO</title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>  

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    

    <!-- CSS Files comes here -->

    <link href="css/bootstrap.css" rel="stylesheet" media="screen">

    <link href="css/style.css" rel="stylesheet" media="screen">

    <link href="css/animate.css" rel="stylesheet" media="screen">

    <link href="css/owl.carousel.css" rel="stylesheet" media="screen">

    <link href="css/owl.theme.css" rel="stylesheet" media="screen">

    <link href="css/nivo-lightbox.css" rel="stylesheet" media="screen">

    <link href="css/nivo_lightbox_themes/default/default.css" rel="stylesheet" media="screen">

    <link href="css/colors/blue.css" rel="stylesheet" media="screen">

    <link href="css/responsive.css" rel="stylesheet" media="screen">

    

    <!-- Google fonts -->

    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900,200italic,300italic,400italic,600italic,700italic,900italic" rel="stylesheet" type="text/css">

    

    <!-- Modernizer and IE specyfic files -->  

    <script src="js/modernizr.custom.js"></script>

      

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!--[if lt IE 9]>

      <script src="js/html5shiv.js"></script>

      <script src="js/respond.min.js"></script>

    <![endif]-->

    

    <!--[if IE 9]>

     <link href="css/ie.css" rel="stylesheet" media="screen">

    <![endif]-->

    

    

    

    <!-- Switcher Only -->

    <link rel="stylesheet" id="switcher-css" type="text/css" href="css/switcher.css" media="all" />

    <link href="http://fonts.googleapis.com/css?family=Oxygen:400,300,700|Open+Sans:300italic,400italic,600italic,700italic,800italic,400,600,700,300,800" rel="stylesheet" type="text/css">

    <!-- END Switcher Styles -->

    

    <!-- Demo Examples -->

    <link rel="alternate stylesheet" type="text/css" href="css/colors/blue.css" title="green" media="all" />

    <!-- END Demo Examples -->





    

    

  </head>

  

  <body>

  

    <!--###############################-->

    <!--PRELOADER #####################-->

    <!--###############################-->

    

    <div id="preloader">

        <div id="status">

            <div class="spinner">

                <div class="bounce1"></div>

                <div class="bounce2"></div>

                <div class="bounce3"></div>

            </div>

        </div>

    </div>





    <!--###############################-->

    <!--HOME ##########################-->

    <!--###############################-->



    <section id="home" style="height: 32rem;">

    <div class="container">

        <div class="row">

            <div id="homescreen" class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">

                 <div id="logo"style="background: f2f2f2;"><a href="#home"><img src="images/liveduel.png" width="200px"  /></a></div>

                 <div id="logo_header"><h5></h5></div>

                 <div id="slogan">

					<h1>Welcome to RIO</h1>

				</div>

				<p style="color: #fff;">

				Bring the Samba excitement to your fans.

				</p>

                 <a class="cta2" href="#features_2" id="button_getstarted" style="background: #0B5A9C !important;">Get Started</a>

                 <a class="cta2" href="http://www.liveduel.com/wcsim" style="background: #0B5A9C !important;">Play for Yourself</a>
		<a class="cta2" href="http://www.liveduel.tumblr.com" style="background: #0B5A9C !important;">Blog</a>

                 

            </div> <div class="clearfix"></div>

        </div>

    </div>

    <div id="video_pattern" style="height: 620px;"></div>

    <div id="video-fallback" style="height: 620px;"></div>

    <div id="video-container">

        <video autoplay loop class="fillWidth" width="896" height="604">

            <source src="video/video1.mp4" type="video/mp4"/>

            <source src="video/video1.ogg" type="video/ogg"/>

            <source src="video/video1.webm" type="video/webm"/>

            Your browser does not support the video tag. I suggest you upgrade your browser.

        </video>

    </div><!-- end video-container -->

    </section>

    

	

	

	<!--###############################-->

    <!--Newsletter and Footer #########-->

    <!--###############################-->

    

    <section id="footer">

        <div class="container">

            <div class="row" id="newsletter">

                <div class="col-sm-12" >

                    

                    <h1 style="width: 100%; color: #fff;">Engage your users by adding our World Cup game to your site</h1>

                    <br>

					<p>Fun and engaging games made easy. Sign up now.

                    </p>

                    

                    <div id="newsletter_form">

                        
                 
						<form class="subscribe-form" id="subscribe-form">

                            

							<input type="email" name="signup_email" id="signup_email" class="subscribe-input" placeholder="Enter your e-mail address..."  required>

							<input type="button" class="subscribe-submit" value="Signup Now" id="signup_submit" style="background: #3C7BB0;">

                        </form>

						

						

                    </div>

                    <div id="preview"></div>

					<br>

					<div id="signup_done"></div>

					

                </div>

            </div>

          

        </div>

    </section>

	

    

	

	<!--###############################-->

    <!--ABOUT #########################-->

    <!--###############################-->

    

    <section id="about" style="">

        <div class="container">

            

            <div class="row" id="about_intro">

                <div class="col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3" >

                    

                    <h2>LiveDuel</h2>

                    <p>

					Engaging your users is the key to building a community and building your user base.

					</p>

                    <br><br><br>

                </div>

            </div>

            

            <div class="row" >

                <!-- style="background: #333; color: #f2f2f2; padding: 10px;" -->

                <div class="col-sm-3 col-md-3 col-lg-3" id="service_1" >

					<h3>ABOUT US</h3>

					<img src="images/ld_logo.png" />

                    <p>

					LiveDuel is an early stage startup based in Cork, Ireland. We are passionate about sports and games.

					</p>

                </div>

                

                <div class="col-sm-3 col-md-3 col-lg-3" id="service_2">

                    <h3>SOFTWARE</h3>

					<img src="images/img2.png" width="250px" height="250px" />

                    <p>

					Our products are built using modern flexible technologies that work with your stack.

					</p>

                </div>

                

                <div class="col-sm-3 col-md-3 col-lg-3" id="service_3">

                    <h3>PRODUCTS</h3>

					<img src="images/img3.png" width="250px" height="250px" />

                    <p>

					Our first product, World Cup Simulator, allows users to predict what they think will happen at this years World Cup and play against other users.

					</p>

                </div>

				

				<div class="col-sm-3 col-md-3 col-lg-3" id="service_4" style="background: #f2f2f2; padding: 10px; box-shadow: 10px 10px 5px #888888;">

                    <h3>CONTACT</h3>

                    <form action="">

						<input type="text" id="user_name" class="form-input" placeholder="Enter Your Name">

						<input type="text" id="email" class="form-input" placeholder="Enter Email Address">

						<textarea id="message" class="form-textarea" placeholder="Your Message Here"></textarea>

						<div class="cta3" id="send_message" style="background: #0B5A9C !important; cursor: pointer;">Send Message</div>

					</form>
					
					<br>
					
					<div id="email_sent"></div>

                </div>

                

            </div>

    

        </div>

    </section>

    

	

	

	<!--###############################-->

    <!--Features 2 ####################-->

    <!--###############################-->

    <section id="features_2" style="background: #0B5A9C !important;">

        <div class="container">

            <div class="row">

                

                <div class="col-sm-6 col-md-6 col-lg-6" id="features_2_content" style="color: #fff;">

                    <h2 style="color: #f2f2f2;">Product Demo</h2>

                    <p>

					Here is a brief over view of our World Cup Simulator product.

					</p>

                    <div class="feature">

                        <div>

                            <h4>Engage your users</h4>

                            <p>Play games with your users to build brand loyalty.</p>

                        </div>

                    </div>

                    

                    <div class="feature">

                        <div>

                            <h4>Build your community</h4>

                            <p>Allow users to play against each other and make your site their online home.</p>

                        </div>

                    </div>

                    

                </div>

                

                

                <div class="col-sm-6 col-md-6  col-lg-6" >

					<iframe class="video-iframe" style=" padding: 0px;" src="http://www.youtube.com/embed/SBrwv6jvK9o" frameborder="0" allowfullscreen></iframe>

					

				</div>



            </div>

            

        </div>

        

    </section>

	

	

	<!--###############################-->

    <!--Gallery #######################-->

    <!--###############################-->



    <section id="gallery">

        <div class="container">

            <div class="row" id="gallery_intro">

                <div class="col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3" >

                    

                    <h2>Product Screenshots</h2>

                    <p>

					Our first product, World Cup Simulator, allows users to predict what they 

					think will happen at this years World Cup and play against other users.

					</p>

					

                </div>

            </div>

            

            <div class="row" id="gallery_carousel">

                <div class="col-sm-12 col-md-12 col-lg-12" >

                    <div id="owl-gallery" class="owl-carousel">

                        <div class="item"><a href="images/screen1.jpg" data-lightbox-gallery="gallery1" data-lightbox-hidpi="images/screen1.jpg"><img src="images/screen1.jpg" class="img-responsive img-rounded" alt="img"></a></div>

                        <div class="item"><a href="images/screen2.jpg" data-lightbox-gallery="gallery1" data-lightbox-hidpi="images/screen2.jpg"><img src="images/screen2.jpg" class="img-responsive img-rounded" alt="img"></a></div>

                        <div class="item"><a href="images/screen3.jpg" data-lightbox-gallery="gallery1" data-lightbox-hidpi="images/screen3.jpg"><img src="images/screen3.jpg" class="img-responsive img-rounded" alt="img"></a></div>

                        <div class="item"><a href="images/screen4.jpg" data-lightbox-gallery="gallery1" data-lightbox-hidpi="images/screen4.jpg"><img src="images/screen4.jpg" class="img-responsive img-rounded" alt="img"></a></div>

                        <div class="item"><a href="images/screen5.jpg" data-lightbox-gallery="gallery1" data-lightbox-hidpi="images/screen5.jpg"><img src="images/screen5.jpg" class="img-responsive img-rounded" alt="img"></a></div>

                        

					</div>

                </div>

            </div>

            

        </div>

    </section>

	

    



    <!-- JavaScript plugins comes here -->

    <script src="js/jquery-2.0.3.min.js"></script>

    <script src="js/jquery.easing.min.js"></script>

    <script src="js/jquery.scrollTo.js"></script>

    <script src="js/jquery.form.js"></script>

    <script src="js/main.js"></script>

    <script src="js/retina.js"></script>

    <script src="js/waypoints.min.js"></script>

    <script src="js/owl.carousel.min.js"></script>

    <script src="js/nivo-lightbox.min.js"></script>

    <script type="text/javascript">

        $('document').ready(function(){

		

                $('#subscribe-form').ajaxForm( {

                target: '#preview',

                success: function() { 

                      $('#subscribe-form').slideUp('slow');

                      $('#preview').css({"opacity":"1"});

                    }

                });
				
				
				$('#send_message').click(function() {
					var user_name = $('#user_name').val();
					var email = $('#email').val();
					var message = $('#message').val();
					
					var dataString = 'user_name='+ user_name + '&email=' + email + '&message=' + message;
					
					if(user_name=='' || email=='' || message=='') {
						$('#service_4').css({'border': '1px solid #AE0707'});
					}
					else
					{
						$('#email_sent').html('<img src = images/loader.gif />');
					
						$.ajax({
						type: "POST",
						url: "sendemail.php",
						data: dataString,
						
						success: function(){
							$('#email_sent').html("<b>Thanks, we will respond to you shortly.</b>");
						}
						})
					}
				});

				

				$('#signup_submit').click(function() {

					var signup_email = $('#signup_email').val();

					var dataString = 'email='+ signup_email;

					$('#signup_done').html('<img src = images/loader.gif />');

					

					$.ajax({

						type: "POST",

						url: "signupemail.php",

						data: dataString,

						

						success: function(){

							$('#signup_done').html("<font color=#fff><b>Thanks for signing up.</b></font>");

						}

						})

					

				});
		

            });

    </script>

    <script>

        var url ='images/icons.svg';

        var c=new XMLHttpRequest(); c.open('GET.html', url, false); c.setRequestHeader('Content-Type', 'text/xml'); c.send();

        document.body.insertBefore(c.responseXML.firstChild, document.body.firstChild)

    </script>



    <!-- Demo Switcher JS -->

    <script type="text/javascript" src="js/fswit.js"></script>

        

  </body>

</html>