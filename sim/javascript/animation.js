Parse.initialize("d3CUxjaXZ9SzF4XPrG1ricTlxNm1RVlyKOXVzfzv", "VHtkzXx5Wo71WoXtJBVP86r9L4Hrrb4kemwD8cEK");
var subscriber,
	hasSubscribed;
hasSubscribed=false;




var RANKINGS={BR:"10",AR:"3",CO:"4",UY:"6",ES:"1",DE:"2",BE:"11",CH:"8",JP:"48",AU:"59",IR:"45",KR:"54",US:"14",CR:"31",HN:"41",MX:"20",CL:"15",EC:"23",GH:"24",CM:"51",CI:"17",NG:"36",DZ:"26",BF:"58",RU:"22",NL:"9",IT:"7",BA:"21",_england:"13",HR:"16",FR:"19",UA:"18*",GR:"12",PT:"5"};
var C={BR:"Brazil",AR:"Argentina",CO:"Colombia",UY:"Uruguay",ES:"Spain",DE:"Germany",BE:"Belgium",CH:"Switzerland",JP:"Japan",AU:"Australia",IR:"Iran",KR:"Korea Republic",US:"USA",CR:"Costa Rica",HN:"Honduras",MX:"Mexico",CL:"Chile",EC:"Ecuador",GH:"Ghana",CM:"Cameroon",CI:"C\u00f4te d'Ivoire",NG:"Nigeria",DZ:"Algeria",BF:"Burkina Faso*",RU:"Russia",NL:"Netherlands",IT:"Italy",BA:"Bosnia-Herzegovina",_england:"England",HR:"Croatia",FR:"France",UA:"Ukraine*",GR:"Greece",PT:"Portugal"},
pot=["BR AR CO UY ES DE BE CH".split(" "),"CL EC CI GH DZ NG CM FR".split(" "),"JP IR KR AU US MX CR HN".split(" "),"NL IT _england PT GR BA HR RU".split(" ")],
pot_a=[pot[0][0],pot[0][1],pot[0][2],pot[0][3],pot[0][4],pot[0][5],pot[0][6],pot[0][7]],
pot_b=[pot[1][0],pot[1][1],pot[1][2],pot[1][3],pot[1][4],pot[1][5],pot[1][6],pot[1][7]],
pot_c1=[pot[2][0],pot[2][1]],
pot_c2=[pot[2][2],pot[2][3],pot[2][4],pot[2][5],pot[2][6]],
pot_c3=[pot[2][7]],
pot_c4=[],
pot_c5=[],
pot_d=[pot[3][0],pot[3][1],pot[3][2],pot[3][3],pot[3][4],pot[3][5],pot[3][6],pot[3][7]],
group=[{n:"A",m:Array(4)},{n:"B",m:Array(4)},{n:"C",m:Array(4)},{n:"D",m:Array(4)},{n:"E",m:Array(4)},{n:"F",m:Array(4)},{n:"G",m:Array(4)},{n:"H",m:Array(4)}];

var CURSOR1,CURSOR2,RAWG=Array(8),T=!1;
var teamA1;
var teamA2;
var teamB1;
var teamB2;
var teamC1;
var teamC2;
var teamD1;
var teamD2;
var teamE1;
var teamE2;
var teamF1;
var teamF2;
var teamG1;
var teamG2;
var teamH1;
var teamH2;

var team16_1; 
var team16_2;
var team16_3;
var team16_4;
var team16_5;
var team16_6;
var team16_7;
var team16_8;

var team8_1;
var team8_2;
var team8_3;
var team8_4;

var team4_1;
var team4_2;

var winner;

var firstForm=true;


$(function(){
	init();
});

function init(){
	$("#standings").slideUp(200);
	i=0;
	for(iLen=pot.length;i<iLen;i++){
		var a=[];
		$("#pot-"+(i+1)).empty();j=0;
		for(jLen=pot[i].length;j<jLen;j++)
			a.push("<li class='"+pot[i][j]+"'><div class='flag-24 flag-24-"+pot[i][j]+"'></div>"+C[pot[i][j]]+"");
		$("#pot-"+(i+1)).append(a.join(""));
	}
	$("#groups").empty();
	i=0;
	for(iLen=group.length;i<iLen;i++){
		a=[];
		a.push("<div class='groupW'>");
		a.push("<h3>Group "+group[i].n+"</h3>");
		a.push("<ul id='group-"+group[i].n+"'>");
		j=0;
		for(jLen=group[i].m.length;j<jLen;j++)
			a.push("<li>\u3000");
		a.push("</ul>");
		a.push("</div>");
		$("#groups").append(a.join(""));
	}
}


function draw(){
	document.getElementById("drawKnockoutDiv").style.display='none';
	document.getElementById("knockouts").style.display='none';
	$("#message").html("");
	if(!T){
		init();
		aShuffle(pot_b);
		aShuffle(pot_c2);
		pot_c4=aShuffle([pot_c1[0],pot_c1[1],pot_c2[0],pot_c2[1]]);
		pot_c5=aShuffle([pot_c3[0],pot_c2[2],pot_c2[3],pot_c2[4]]);
		aShuffle(pot_d);
		RAWG=[["BR",pot_b[0],pot_c5[0],pot_d[0]],["AR",pot_b[1],pot_c5[1],pot_d[1]],["CO",pot_b[2],pot_c5[2],pot_d[2]],["UY",pot_b[3],pot_c5[3],pot_d[3]],["ES",pot_b[4],pot_c4[0],pot_d[4]],["DE",pot_b[5],pot_c4[1],pot_d[5]],["BE",pot_b[6],pot_c4[2],pot_d[6]],["CH",pot_b[7],pot_c4[3],pot_d[7]]];
		RAWG_TEMP=aShuffle([RAWG[1],RAWG[2],RAWG[3],RAWG[4],RAWG[5],RAWG[6],RAWG[7]]);RAWG=[RAWG[0],RAWG_TEMP[0],RAWG_TEMP[1],RAWG_TEMP[2],RAWG_TEMP[3],RAWG_TEMP[4],RAWG_TEMP[5],RAWG_TEMP[6]];
		var a=[];
		for(i=0;i<RAWG.length;i++){
			var b=[];0==i?(b.push(RAWG[0][0]),b=b.concat(aShuffle([RAWG[0][1],RAWG[0][2],RAWG[0][3]]))):(b=$.extend(!0,[],RAWG[i]),aShuffle(b));
			for(j=0;j<b.length;j++)
				a.push(group[i].n+(j+1)+"="+b[j]);
		}
		$("#standings").attr("href","2014-FIFA-World-Cup-Group-Stage#!"+a.join("&"));
		CURSOR2=CURSOR1=0;
		setTimeout("setGroup()",100);
	}
}

function setGroup(){
	var a=RAWG[CURSOR2][CURSOR1];
	$("#drawexec").html("Draw... "+(8*CURSOR1+(CURSOR2+1))+" / 32");
	from_o=$("#pots ."+a);
	from_top=from_o.position().top;
	from_left=from_o.position().left;
	to_o=$("#groups ul:eq("+CURSOR2+") li:eq("+CURSOR1+")");
	to_top=to_o.position().top;
	to_left=to_o.position().left;
	from_o.css("opacity",".3");
	$("<div class='c' style='top:"+from_top+"px;left:"+from_left+"px;'><div class='flag-24 flag-24-"+a+"'></div>"+C[a]+"</div>").appendTo("#groups").animate({top:to_top+"px",left:to_left+"px"},500,"swing");
	if(8<=++CURSOR2&&(CURSOR2=0,4<=++CURSOR1)){
		clearTimeout(T);
		T=!1;
		$("#drawexec").html("Simulate draw again");
		setQualifiers();
		
		setTimeout('displayPopup();',1000);
		
		$("#standings").slideDown(200);
		return;
	}
	T=0==CURSOR2||$("#bulk"+CURSOR1).prop("checked")?setTimeout("setGroup()",2E3):setTimeout("setGroup()",10);
}

function displayPopup(){
	$("#myForm1 .btnSubmit").show();
	$('#popup1 .myFormResponse').hide();
	firstForm=true;
	document.getElementById("drawKnockoutDiv").style.display='block'; 
	$("#pointsLbl").html(""+getRandomPoints()+""); 
	$( "#popup1" ).dialog({modal: true,width:550,show: 'blind', hide: 'explode'});
}
	
function aShuffle(a){
	for(var b=a.length;b;){
		var c=Math.floor(Math.random()*b),d=a[--b];
		a[b]=a[c];
		a[c]=d
	}
	return a;
}

function setQualifiers(){
	teamA1 = RAWG[0][0];
	teamA2 = getWinner(RAWG[0][1],RAWG[0][2]);
	teamA2 = getWinner(teamA2,RAWG[0][3]);
	
	teamB1 = RAWG[1][0];
	teamB2 = getWinner(RAWG[1][1],RAWG[1][2]);
	teamB2 = getWinner(teamB2,RAWG[1][3]);
	
	teamC1 = RAWG[2][0];
	teamC2 = getWinner(RAWG[2][1],RAWG[2][2]);
	teamC2 = getWinner(teamC2,RAWG[2][3]);
	
	teamD1 = RAWG[3][0];
	teamD2 = getWinner(RAWG[3][1],RAWG[3][2]);
	teamD2 = getWinner(teamD2,RAWG[3][3]);
	
	teamE1 = RAWG[4][0];
	teamE2 = getWinner(RAWG[4][1],RAWG[4][2]);
	teamE2 = getWinner(teamE2,RAWG[4][3]);
	
	teamF1 = RAWG[5][0];
	teamF2 = getWinner(RAWG[5][1],RAWG[5][2]);
	teamF2 = getWinner(teamF2,RAWG[5][3]);
	
	teamG1 = RAWG[6][0];
	teamG2 = getWinner(RAWG[6][1],RAWG[6][2]);
	teamG2 = getWinner(teamG2,RAWG[6][3]);
	
	teamH1 = RAWG[7][0];
	teamH2 = getWinner(RAWG[7][1],RAWG[7][2]);
	teamH2 = getWinner(teamH2,RAWG[7][3]);
}

function drawKnockout(){
	setQualifiers();
	$('#popup1').dialog('close');
	document.getElementById("knockouts").style.display='block';
	$("#message").html("");
	$("#matches_16").html("");
	$("#matches_8").html("");
	$("#matches_4").html("");
	$("#matches_2").html("");
	// draw the round of 16 
	team16_1 = getWinner(teamA1,teamB2);
	if(team16_1 == teamA1){
		$("#matches_16").append("<div class='match'><div class='team finalswinner team0'><div class='flag-24 flag-24-"+teamA1+"' style='float:right;'></div><div class='teamInfo'><div class='teamName'>"+C[teamA1]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Winner of Group A</div></div><div class='clearboth'></div></div><div class='teamSeparator0'></div><div class='team finals team1'><div class='flag-24 flag-24-"+teamB2+"' style='float:left;'></div><div class='teamInfo'><div class='teamName'>"+C[teamB2]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Runner-up Group B</div></div><div class='clearboth'></div></div><div class='teamSeparator1'></div>");
	}
	else{
		$("#matches_16").append("<div class='match'><div class='team team0'><div class='flag-24 flag-24-"+teamA1+"' style='float:right;'></div><div class='teamInfo'><div class='teamName'>"+C[teamA1]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Winner of Group A</div></div><div class='clearboth'></div></div><div class='teamSeparator0'></div><div class='team finalswinner team1'><div class='flag-24 flag-24-"+teamB2+"' style='float:left;'></div><div class='teamInfo'><div class='teamName'>"+C[teamB2]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Runner-up Group B</div></div><div class='clearboth'></div></div><div class='teamSeparator1'></div>");
	}
	team16_2 = getWinner(teamA2,teamB1);
	if(team16_2 == teamA2){
		$("#matches_16").append("<div class='match'><div class='team finalswinner team0'><div class='flag-24 flag-24-"+teamA2+"' style='float:right;'></div><div class='teamInfo'><div class='teamName'>"+C[teamA2]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Runner-up Group A</div></div><div class='clearboth'></div></div><div class='teamSeparator0'></div><div class='team finals team1'><div class='flag-24 flag-24-"+teamB1+"' style='float:left;'></div><div class='teamInfo'><div class='teamName'>"+C[teamB1]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Winner of Group B</div></div><div class='clearboth'></div></div><div class='teamSeparator1'></div>");
	}
	else{
		$("#matches_16").append("<div class='match'><div class='team team0'><div class='flag-24 flag-24-"+teamA2+"' style='float:right;'></div><div class='teamInfo'><div class='teamName'>"+C[teamA2]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Runner-up Group A</div></div><div class='clearboth'></div></div><div class='teamSeparator0'></div><div class='team finalswinner team1'><div class='flag-24 flag-24-"+teamB1+"' style='float:left;'></div><div class='teamInfo'><div class='teamName'>"+C[teamB1]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Winner of Group B</div></div><div class='clearboth'></div></div><div class='teamSeparator1'></div>");
	}
	team16_3 = getWinner(teamC1,teamD2);
	if(team16_3 == teamC1){
		$("#matches_16").append("<div class='match'><div class='team finalswinner team0'><div class='flag-24 flag-24-"+teamC1+"' style='float:right;'></div><div class='teamInfo'><div class='teamName'>"+C[teamC1]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Winner of Group C</div></div><div class='clearboth'></div></div><div class='teamSeparator0'></div><div class='team finals team1'><div class='flag-24 flag-24-"+teamD2+"' style='float:left;'></div><div class='teamInfo'><div class='teamName'>"+C[teamD2]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Runner-up Group D</div></div><div class='clearboth'></div></div><div class='teamSeparator1'></div>");
	}
	else{
		$("#matches_16").append("<div class='match'><div class='team team0'><div class='flag-24 flag-24-"+teamC1+"' style='float:right;'></div><div class='teamInfo'><div class='teamName'>"+C[teamC1]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Winner of Group C</div></div><div class='clearboth'></div></div><div class='teamSeparator0'></div><div class='team finalswinner team1'><div class='flag-24 flag-24-"+teamD2+"' style='float:left;'></div><div class='teamInfo'><div class='teamName'>"+C[teamD2]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Runner-up Group D</div></div><div class='clearboth'></div></div><div class='teamSeparator1'></div>");
	}
	team16_4 = getWinner(teamC2,teamD1);
	if(team16_4 == teamC2){
		$("#matches_16").append("<div class='match'><div class='team finalswinner team0'><div class='flag-24 flag-24-"+teamC2+"' style='float:right;'></div><div class='teamInfo'><div class='teamName'>"+C[teamC2]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Runner-up Group C</div></div><div class='clearboth'></div></div><div class='teamSeparator0'></div><div class='team finals team1'><div class='flag-24 flag-24-"+teamD1+"' style='float:left;'></div><div class='teamInfo'><div class='teamName'>"+C[teamD1]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Winner of Group D</div></div><div class='clearboth'></div></div><div class='teamSeparator1'></div>");
	}
	else{
		$("#matches_16").append("<div class='match'><div class='team team0'><div class='flag-24 flag-24-"+teamC2+"' style='float:right;'></div><div class='teamInfo'><div class='teamName'>"+C[teamC2]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Runner-up Group C</div></div><div class='clearboth'></div></div><div class='teamSeparator0'></div><div class='team finalswinner team1'><div class='flag-24 flag-24-"+teamD1+"' style='float:left;'></div><div class='teamInfo'><div class='teamName'>"+C[teamD1]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Winner of Group D</div></div><div class='clearboth'></div></div><div class='teamSeparator1'></div>");
	}
	team16_5 = getWinner(teamE1,teamF2);
	if(team16_5 == teamE1){
		team16_5 = teamE1;
		$("#matches_16").append("<div class='match'><div class='team finalswinner team0'><div class='flag-24 flag-24-"+teamE1+"' style='float:right;'></div><div class='teamInfo'><div class='teamName'>"+C[teamE1]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Winner of Group E</div></div><div class='clearboth'></div></div><div class='teamSeparator0'></div><div class='team finals team1'><div class='flag-24 flag-24-"+teamF2+"' style='float:left;'></div><div class='teamInfo'><div class='teamName'>"+C[teamF2]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Runner-up Group F</div></div><div class='clearboth'></div></div><div class='teamSeparator1'></div>");
	}
	else{
		$("#matches_16").append("<div class='match'><div class='team team0'><div class='flag-24 flag-24-"+teamE1+"' style='float:right;'></div><div class='teamInfo'><div class='teamName'>"+C[teamE1]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Winner of Group E</div></div><div class='clearboth'></div></div><div class='teamSeparator0'></div><div class='team finalswinner team1'><div class='flag-24 flag-24-"+teamF2+"' style='float:left;'></div><div class='teamInfo'><div class='teamName'>"+C[teamF2]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Runner-up Group F</div></div><div class='clearboth'></div></div><div class='teamSeparator1'></div>");
	}
	team16_6 = getWinner(teamE2,teamF1);
	if(team16_6 == teamE2){
		$("#matches_16").append("<div class='match'><div class='team finalswinner team0'><div class='flag-24 flag-24-"+teamE2+"' style='float:right;'></div><div class='teamInfo'><div class='teamName'>"+C[teamE2]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Runner-up Group E</div></div><div class='clearboth'></div></div><div class='teamSeparator0'></div><div class='team finals team1'><div class='flag-24 flag-24-"+teamF1+"' style='float:left;'></div><div class='teamInfo'><div class='teamName'>"+C[teamF1]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Winner of Group F</div></div><div class='clearboth'></div></div><div class='teamSeparator1'></div>");
	}
	else{
		$("#matches_16").append("<div class='match'><div class='team team0'><div class='flag-24 flag-24-"+teamE2+"' style='float:right;'></div><div class='teamInfo'><div class='teamName'>"+C[teamE2]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Runner-up Group E</div></div><div class='clearboth'></div></div><div class='teamSeparator0'></div><div class='team finalswinner team1'><div class='flag-24 flag-24-"+teamF1+"' style='float:left;'></div><div class='teamInfo'><div class='teamName'>"+C[teamF1]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Winner of Group F</div></div><div class='clearboth'></div></div><div class='teamSeparator1'></div>");
	}
	team16_7 = getWinner(teamG1,teamH2);
	if(team16_7 == teamG1){
		$("#matches_16").append("<div class='match'><div class='team finalswinner team0'><div class='flag-24 flag-24-"+teamG1+"' style='float:right;'></div><div class='teamInfo'><div class='teamName'>"+C[teamG1]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Winner of Group G</div></div><div class='clearboth'></div></div><div class='teamSeparator0'></div><div class='team finals team1'><div class='flag-24 flag-24-"+teamH2+"' style='float:left;'></div><div class='teamInfo'><div class='teamName'>"+C[teamH2]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Runner-up Group H</div></div><div class='clearboth'></div></div><div class='teamSeparator1'></div>");
	}
	else{
		$("#matches_16").append("<div class='match'><div class='team team0'><div class='flag-24 flag-24-"+teamG1+"' style='float:right;'></div><div class='teamInfo'><div class='teamName'>"+C[teamG1]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Winner of Group G</div></div><div class='clearboth'></div></div><div class='teamSeparator0'></div><div class='team finalswinner team1'><div class='flag-24 flag-24-"+teamH2+"' style='float:left;'></div><div class='teamInfo'><div class='teamName'>"+C[teamH2]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Runner-up Group H</div></div><div class='clearboth'></div></div><div class='teamSeparator1'></div>");
	}
	team16_8 = getWinner(teamG2,teamH1);
	if(team16_8 == teamG2){
		$("#matches_16").append("<div class='match'><div class='team finalswinner team0'><div class='flag-24 flag-24-"+teamG2+"' style='float:right;'></div><div class='teamInfo'><div class='teamName'>"+C[teamG2]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Runner-up Group G</div></div><div class='clearboth'></div></div><div class='teamSeparator0'></div><div class='team finals team1'><div class='flag-24 flag-24-"+teamH1+"' style='float:left;'></div><div class='teamInfo'><div class='teamName'>"+C[teamH1]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Winner of Group H</div></div><div class='clearboth'></div></div><div class='teamSeparator1'></div>");
	}
	else{
		$("#matches_16").append("<div class='match'><div class='team team0'><div class='flag-24 flag-24-"+teamG2+"' style='float:right;'></div><div class='teamInfo'><div class='teamName'>"+C[teamG2]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Runner-up Group G</div></div><div class='clearboth'></div></div><div class='teamSeparator0'></div><div class='team finalswinner team1'><div class='flag-24 flag-24-"+teamH1+"' style='float:left;'></div><div class='teamInfo'><div class='teamName'>"+C[teamH1]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Winner of Group H</div></div><div class='clearboth'></div></div><div class='teamSeparator1'></div>");
	}

	// draw the round of 8
	team8_1 = getWinner(team16_1,team16_3);
	if(team8_1 == team16_1){
		$("#matches_8").append("<div class='match'><div class='team finalswinner team0'><div class='flag-24 flag-24-"+team16_1+"' style='float:right;'></div><div class='teamInfo'><div class='teamName'>"+C[team16_1]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Won round of 16: match 1</div></div><div class='clearboth'></div></div><div class='teamSeparator0'></div><div class='team finals team1'><div class='flag-24 flag-24-"+team16_3+"' style='float:left;'></div><div class='teamInfo'><div class='teamName'>"+C[team16_3]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Won round of 16: match 3</div></div><div class='clearboth'></div></div><div class='teamSeparator1'></div>");
	}
	else{
		$("#matches_8").append("<div class='match'><div class='team team0'><div class='flag-24 flag-24-"+team16_1+"' style='float:right;'></div><div class='teamInfo'><div class='teamName'>"+C[team16_1]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Won round of 16: match 1</div></div><div class='clearboth'></div></div><div class='teamSeparator0'></div><div class='team finalswinner team1'><div class='flag-24 flag-24-"+team16_3+"' style='float:left;'></div><div class='teamInfo'><div class='teamName'>"+C[team16_3]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Won round of 16: match 3</div></div><div class='clearboth'></div></div><div class='teamSeparator1'></div>");
	}
	team8_2 = getWinner(team16_2,team16_4);
	if(team8_2 == team16_2){
		$("#matches_8").append("<div class='match'><div class='team finalswinner team0'><div class='flag-24 flag-24-"+team16_2+"' style='float:right;'></div><div class='teamInfo'><div class='teamName'>"+C[team16_2]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Won round of 16: match 2</div></div><div class='clearboth'></div></div><div class='teamSeparator0'></div><div class='team finals team1'><div class='flag-24 flag-24-"+team16_4+"' style='float:left;'></div><div class='teamInfo'><div class='teamName'>"+C[team16_4]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Won round of 16: match 4</div></div><div class='clearboth'></div></div><div class='teamSeparator1'></div>");
	}
	else{
		$("#matches_8").append("<div class='match'><div class='team team0'><div class='flag-24 flag-24-"+team16_2+"' style='float:right;'></div><div class='teamInfo'><div class='teamName'>"+C[team16_2]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Won round of 16: match 2</div></div><div class='clearboth'></div></div><div class='teamSeparator0'></div><div class='team finalswinner team1'><div class='flag-24 flag-24-"+team16_4+"' style='float:left;'></div><div class='teamInfo'><div class='teamName'>"+C[team16_4]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Won round of 16: match 4</div></div><div class='clearboth'></div></div><div class='teamSeparator1'></div>");
	}
	team8_3 = getWinner(team16_5,team16_7);
	if(team8_3 == team16_5){
		$("#matches_8").append("<div class='match'><div class='team finalswinner team0'><div class='flag-24 flag-24-"+team16_5+"' style='float:right;'></div><div class='teamInfo'><div class='teamName'>"+C[team16_5]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Won round of 16: match 5</div></div><div class='clearboth'></div></div><div class='teamSeparator0'></div><div class='team finals team1'><div class='flag-24 flag-24-"+team16_7+"' style='float:left;'></div><div class='teamInfo'><div class='teamName'>"+C[team16_7]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Won round of 16: match 7</div></div><div class='clearboth'></div></div><div class='teamSeparator1'></div>");
	}
	else{
		$("#matches_8").append("<div class='match'><div class='team team0'><div class='flag-24 flag-24-"+team16_5+"' style='float:right;'></div><div class='teamInfo'><div class='teamName'>"+C[team16_5]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Won round of 16: match 5</div></div><div class='clearboth'></div></div><div class='teamSeparator0'></div><div class='team finalswinner team1'><div class='flag-24 flag-24-"+team16_7+"' style='float:left;'></div><div class='teamInfo'><div class='teamName'>"+C[team16_7]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Won round of 16: match 7</div></div><div class='clearboth'></div></div><div class='teamSeparator1'></div>");
	}
	team8_4 = getWinner(team16_6,team16_8);
	if(team8_4 == team16_6){
		;
		$("#matches_8").append("<div class='match'><div class='team finalswinner team0'><div class='flag-24 flag-24-"+team16_6+"' style='float:right;'></div><div class='teamInfo'><div class='teamName'>"+C[team16_6]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Won round of 16: match 6</div></div><div class='clearboth'></div></div><div class='teamSeparator0'></div><div class='team finals team1'><div class='flag-24 flag-24-"+team16_8+"' style='float:left;'></div><div class='teamInfo'><div class='teamName'>"+C[team16_8]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Won round of 16: match 8</div></div><div class='clearboth'></div></div><div class='teamSeparator1'></div>");
	}
	else{
		$("#matches_8").append("<div class='match'><div class='team team0'><div class='flag-24 flag-24-"+team16_6+"' style='float:right;'></div><div class='teamInfo'><div class='teamName'>"+C[team16_6]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Won round of 16: match 6</div></div><div class='clearboth'></div></div><div class='teamSeparator0'></div><div class='team finalswinner team1'><div class='flag-24 flag-24-"+team16_8+"' style='float:left;'></div><div class='teamInfo'><div class='teamName'>"+C[team16_8]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Won round of 16: match 8</div></div><div class='clearboth'></div></div><div class='teamSeparator1'></div>");
	}
	
	// draw the round of 4
	team4_1 = getWinner(team8_1,team8_3);
	if(team4_1 == team8_1){
		$("#matches_4").append("<div class='match'><div class='team finalswinner team0'><div class='flag-24 flag-24-"+team8_1+"' style='float:right;'></div><div class='teamInfo'><div class='teamName'>"+C[team8_1]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Winner of quarter-final 1</div></div><div class='clearboth'></div></div><div class='teamSeparator0'></div><div class='team finals team1'><div class='flag-24 flag-24-"+team8_3+"' style='float:left;'></div><div class='teamInfo'><div class='teamName'>"+C[team8_3]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Winner of quarter-final 3</div></div><div class='clearboth'></div></div><div class='teamSeparator1'></div>");
	}
	else{
		$("#matches_4").append("<div class='match'><div class='team team0'><div class='flag-24 flag-24-"+team8_1+"' style='float:right;'></div><div class='teamInfo'><div class='teamName'>"+C[team8_1]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Winner of quarter-final 1</div></div><div class='clearboth'></div></div><div class='teamSeparator0'></div><div class='team finalswinner team1'><div class='flag-24 flag-24-"+team8_3+"' style='float:left;'></div><div class='teamInfo'><div class='teamName'>"+C[team8_3]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Winner of quarter-final 3</div></div><div class='clearboth'></div></div><div class='teamSeparator1'></div>");
	}
	team4_2 = getWinner(team8_2,team8_4);
	if(team4_2 == team8_2){
		$("#matches_4").append("<div class='match'><div class='team finalswinner team0'><div class='flag-24 flag-24-"+team8_2+"' style='float:right;'></div><div class='teamInfo'><div class='teamName'>"+C[team8_2]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Winner of quarter-final 2</div></div><div class='clearboth'></div></div><div class='teamSeparator0'></div><div class='team finals team1'><div class='flag-24 flag-24-"+team8_4+"' style='float:left;'></div><div class='teamInfo'><div class='teamName'>"+C[team8_4]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Winner of quarter-final 4</div></div><div class='clearboth'></div></div><div class='teamSeparator1'></div>");
	}
	else{
		$("#matches_4").append("<div class='match'><div class='team team0'><div class='flag-24 flag-24-"+team8_2+"' style='float:right;'></div><div class='teamInfo'><div class='teamName'>"+C[team8_2]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Winner of quarter-final 2</div></div><div class='clearboth'></div></div><div class='teamSeparator0'></div><div class='team finalswinner team1'><div class='flag-24 flag-24-"+team8_4+"' style='float:left;'></div><div class='teamInfo'><div class='teamName'>"+C[team8_4]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div><div class='teamPot'>Winner of quarter-final 4</div></div><div class='clearboth'></div></div><div class='teamSeparator1'></div>");
	}
	
	// draw the round of 2
	winner = getWinner(team4_1,team4_2);
	if(winner == team4_1){
		$("#matches_2").append("<div class='match'><div class='team finalswinner team0'><div class='flag-24 flag-24-"+team4_1+"' style='float:right;'></div><div class='teamInfo'><div class='teamName'>"+C[team4_1]+"</div><div class='championshipwinner'></div><div class='clearboth'></div></div><div class='clearboth'></div></div><div class='teamSeparator0'></div><div class='team finals team1'><div class='flag-24 flag-24-"+team4_2+"' style='float:left;'></div><div class='teamInfo'><div class='teamName'>"+C[team4_2]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div></div><div class='clearboth'></div></div><div class='teamSeparator1'></div>");
	}
	else{
		$("#matches_2").append("<div class='match'><div class='team team0'><div class='flag-24 flag-24-"+team4_1+"' style='float:right;'></div><div class='teamInfo'><div class='teamName'>"+C[team4_1]+"</div><div style='opacity: 1;' class='winner'></div><div class='clearboth'></div></div><div class='clearboth'></div></div><div class='teamSeparator0'></div><div class='team finalswinner team1'><div class='flag-24 flag-24-"+team4_2+"' style='float:left;'></div><div class='teamInfo'><div class='teamName'>"+C[team4_2]+"</div><div class='championshipwinner'></div><div class='clearboth'></div></div><div class='clearboth'></div></div><div class='teamSeparator1'></div>");
	}
	$("html, body").animate({ scrollTop: $(document).height() }, "slow");
	$("#winnerTeam").html(C[winner]);
	setTimeout('displayPopup2();',1000);
	
}

function displayPopup2(){
	firstForm=false;
	$("#myForm2 .btnSubmit").show();
	//get the jersey name to display
	var jerseyName = "./images/jerseys/"+C[winner].toLowerCase().split("-").join("").split(" ").join("").replace("\u00f4","o")+"1.png";
	$(".jerseypic").css("background","url('"+jerseyName+"')");
	$("").hide();
	$('#popup2 .myFormResponse').hide();
	//$(".myform").css("display","inline");
	$( "#popup2" ).dialog({
		modal: true,
		width:425,
		position: [450,240],
		show: 'blind', hide: 'explode',
		close: function(){
				$("html, body").animate({ scrollTop: $(document).height() }, "fast");
		}
	});
}

function getWinner(team1, team2){
	if(Math.abs (parseInt(RANKINGS[team1]) - parseInt(RANKINGS[team2])) < 15){
		if(getRandomNumber() == 0){
			return team1;
		}
		else{
			return team2;
		}
	}
	else{
		if(parseInt(RANKINGS[team1]) < parseInt(RANKINGS[team2])){
			return team1;
		}
		else{
			return team2;
		}
	}
}

function getRandomNumber() {
    return Math.floor( Math.random() * ( 1 + 1 - 0 ) ) + 0;
}

function drawKnockout2(){
	$('#popup2').dialog('close');
	drawKnockout();
}

function drawKnockout3(){
	$('#popup1').dialog('close');
	drawKnockout();
	return false;
}

function getRandomPoints() {
    return Math.floor( Math.random() * ( 2250 + 1 - 2000 ) ) + 2000;
}

function draw2(){
	$('#popup1').dialog('close');
	draw();
	return false;
}

function draw3(){
	$('#popup2').dialog('close');
	draw();
	return false;
}

function submitForm(){
	console.log(this);
	var form = (firstForm?"#myForm1":"#myForm2")
	$("#btnSubmit").val("Loading..");

	 var fname = $(form+" .fname").val();
	 var lname = $(form+" .lname").val();
	 var email = $(form+" .email").val();
	 var sub;
	 if(!hasSubscribed)
	 {
	 	var Subscriber = Parse.Object.extend("Subscriber");
	 	sub = new Subscriber();
	 	sub.set("fname",fname);
	 	sub.set("lname",lname);
	 	sub.set("email",email);
	 }
	 else
	 {
	 	sub=subscriber;
	 }
	 var Score;
	 if(firstForm)
	 {
	 	Score = Parse.Object.extend("WorldCupDrawBet");
	 }
	 else
	 {
	 	Score = Parse.Object.extend("WorldCupBet");
	 }
	 score=new Score();
	 score.set("points",(firstForm ? parseInt($("#pointsLbl").html()) :250));
	 score.set("country",(firstForm ? $(".countrySupported").html() : C[winner]));
	 score.set("subscriber",sub);
	 
	 sub.save(null, {
	  success: function(sub) {
	  	subscriber=sub;
	  	score.save();
	  	$("#myForm1 .fname").prop('disabled', true);
	  	$("#myForm2 .fname").prop('disabled', true);
	  	$("#myForm1 .lname").prop('disabled', true);
	  	$("#myForm2 .lname").prop('disabled', true);
	  	$("#myForm1 .email").prop('disabled', true);
	  	$("#myForm2 .email").prop('disabled', true);
	  	$("#myForm1 .fname").val(sub.get("fname"));
	  	$("#myForm2 .fname").val(sub.get("fname"));
	  	$("#myForm1 .lname").val(sub.get("lname"));
	  	$("#myForm2 .lname").val(sub.get("lname"));
	  	$("#myForm1 .email").val(sub.get("email"));
	  	$("#myForm2 .email").val(sub.get("email"));
	  	console.log("saved");
	  	hasSubscribed=true;
	  	$(form+" .btnSubmit").hide();
	  	$("#popup"+(firstForm?"1":"2")+' .myFormResponse').show();
	    

	  },
	  error: function(gameScore, error) {
	    // Execute any logic that should take place if the save fails.
	    // error is a Parse.Error with an error code and description.
	    alert('Failed to create new object, with error code: ' + error.description);
	  }
	});

}



$(document).ready(function(){ 
				 $("#myForm1 .btnSubmit").click(submitForm);
				 $("#myForm2 .btnSubmit").click(submitForm);

				 $('.flexslider').flexslider({
			animation: "slide"
		  });

	});	 

