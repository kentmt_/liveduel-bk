<?php

define ("HOST_IP",
        getenv('HTTP_CLIENT_IP')?:
        getenv('HTTP_X_FORWARDED_FOR')?:
        getenv('HTTP_X_FORWARDED')?:
        getenv('HTTP_FORWARDED_FOR')?:
        getenv('HTTP_FORWARDED')?:
        getenv('REMOTE_ADDR')
);

define("FILE_PATH", "php/iplog.txt");

$file = fopen(FILE_PATH,"r");

$found = FALSE;

if ($file) {

    while(!feof($file) && !$found) {

        $curLine = explode("|",fgets($file));

        if ($curLine[0] == HOST_IP) {

            $found = TRUE;

        }

    }

    fclose($file);

} else {

    $found = FALSE;

}


?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>LiveDuel</title>
		<link rel="stylesheet" type="text/css" href="style/index.css" />
<?php

	if ($found == FALSE) {

?>
		<link rel="stylesheet" type="text/css" href="style/fonts.css" />
		<link rel="stylesheet" type="text/css" href="style/colorbox/colorbox.css" />
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script src="script/jquery.colorbox-min.js"></script>
		<script src="script/index.js"></script>
	</head>
	<body>
	
		<div id="container">
			<p>Thank you for taking part to this test. For a better result, we need you to follow some simple guidelines.
			Please read them carefully before starting.</p>
			<ul>
				<li>
					Before testing our Web prototype, we need you to take a pre-test questionnaire.
					To start it, click on the first button in the list below. A popup will open.
					Close it <strong>only after you have completed it</strong>.
				</li>
				<li>
					Once closed the questionnaire window, you will be able to take both the main test (<i>HTML test</i>) and the post-test questionnaire:
					<strong>please take the HTML test before</strong>, which will be opened in a new window/tab.
				</li>
				<li>
					Do not close the HTML test window/tab before you can see the message &ldquo;<i>You can now close this window</i>&rdquo;. If you close
					it by mistake, just	click the HTML test link to start it again.
				</li>
				<li>Take the post-test questionnaire <strong>once completed the HTML test.</strong></li>
				<li>Most of all, <strong>do not think too much!</strong></li>
				
			</ul>
			<p>Enjoy!</p>
			<div id="links">
				<div class="link enabled" id="pre-test-link">Pre-test questionnaire</div>
				<div class="link" id="html-test-link">HTML Test</div>
				<div class="link" id="post-test-link">Post-test questionnaire</div>
			</div>
			<p><strong>Please note:</strong> both pre-test and post-test questionnaires are hosted on Google Drive. Please make sure you have access
			to Google Drive before starting this test.</p>
			<p id="thanks">Thank you for your time.<br/>You can now close this window.</p>

<?php	} else {	?>

	</head>
	<body>
		<div id="container">
			<p style="text-align: center; font-size: 11pt">You have already taken this test.<br/>Thank you for your time.</p>

<?php	}	?>

		</div>
	</body>
</html>