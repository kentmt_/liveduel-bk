var isLocalTimeSet = true;
var currentGroup = 1; // Group A selected by default
var letterArray = ["A","B","C","D","E","F","G","H"];
var isWidgetView = true;
var firstTwoTeams = new Array(2);

$(document).ready(function() {


	// Initialization settings
	$("#group-1").removeClass('hover');
	$("#group-1").next().addClass('connected');

	// Timezone radio buttons

	// set local time
	$("#localTimeRadio").click(function(){
		
		if (!isLocalTimeSet)
			setTime(true);
		
	});
	
	
	// set browser time
	$("#timeZoneRadio").click(function(){
		
		if (isLocalTimeSet)
			setTime(false);
		
	});


	// click on group tab
	$(".group-btn").click(function(event) {
	
		var groupNum;
	
		// if the clicked event has no id, the current div is a flag
		// this first braked is a workaround to the issue that occurs
		// if the user clicks the small flag (no reaction)
		if (event.target.id == "") {

			$(event.target).parent().removeClass('hover');
			$(event.target).parent().next().addClass('connected');

			groupNum = parseInt($(event.target).parent().attr('id').slice(-1));

		} else {

			$(event.target).removeClass('hover');
			$(event.target).next().addClass('connected');

			groupNum = parseInt(event.target.id.slice(-1));

		}

		groupTabClicked(groupNum);
	
	});
	
	
	// click on view selection menu
	$("#view-select").click(function(event) {
		
		$("#view-options").toggle();
		
	});
	

	// click on widget view option
	$("#widget-view-option").click(function() {
						
		if (!isWidgetView)
			showWidgetView();
		
		isWidgetView = true;
			
		$("#view-options").hide();
		
	});
	
	// click on list view select option
	$("#list-view-option").click(function() {
		
		if (isWidgetView)
			showListView();
		
		isWidgetView = false;
		
		$("#view-options").hide();
		
	});

	// click on '?' button
	$("#help-btn").click(function() {
	
		// show the tutorial
		showTutorial();
	
	});
	
	
	// click on random scores button
	$("#random-scores").click(function() {
		fillEmptyScores();
	});
	
	// click on clear button
	$("#clear").click(function() {
		clearAllFields();
	});

				
	// shows the next group
	$("#next-group").click(function() {
		groupTabClicked(currentGroup+1);
	});

	// set the autotab for the score fields
	$(".team-result").autotab({ maxlength: 1, format: 'number' });

	// update the score information displayed at every key press
	$(".team-result").keyup(function(){
		checkScores();
	});

	// opens the knockout page if clicked
	$("#knockout-btn-big").click(function() {
		
		goKnockout();
		
	});
	
	// opens the knockout page if clicked
	$("#go-knockout").click(function() {
		
		goKnockout();
		
	});
	
	// show the group A as default
	setMatchInformation(1);
	
	// show the tutorial
	initTutorial();
	
});

function groupTabClicked(groupNum) {

	// save current Group status
	saveGroupStatus();

	// 'disconnect' all tabs
	for (var i=1 ; i<=8; i++) {

		$("#group-"+i).addClass('hover');
		$("#group-"+i).next().removeClass('connected');

	}

	$("#group-"+groupNum).removeClass('hover');
	$("#group-"+groupNum).next().addClass('connected');

	setMatchInformation(groupNum);

}

function showListView() {
	
	$("#view-select").removeClass("widgetSelect").addClass("listSelect");
	
	$('link[href="style/group-stage-widget-view.css"]').attr('href','style/group-stage-list-view.css');
	
}

function showWidgetView() {

	$("#view-select").removeClass("listSelect").addClass("widgetSelect");
	
	$('link[href="style/group-stage-list-view.css"]').attr('href','style/group-stage-widget-view.css');
	
}

/* Given a time string it adds the integer value hourNum
 * and returns the result string.
 * @input	dateString	the date string (e.g. "12:00")
 * @input	hourNum		integer value to add to the time
 * @return	the result string
 */
function sumHours(dateString, hourNum) {

	var dateTimeArr = dateString.split(":");

	var newTime = (parseInt(dateTimeArr[0]) + hourNum)%24

	dateTimeArr[0] = newTime.toString();

	dateString = dateTimeArr.join(":");
	
	return dateString;

}

function setTime(local) {	

	isLocalTimeSet = local;

	var letterGroup = letterArray[currentGroup-1];

	for (var i=1; i<=6; ++i) {

		// currentMatch = eval("match"+letterGroup+i);
		currentMatch = matches[letterGroup][i-1];

		var dateTimeLocationArr = $("#location"+i).html().split(" - ");

		if (local) {
	
			dateTimeLocationArr[1] = sumHours(dateTimeLocationArr[1],currentMatch.timezone);
			$("#timezone-box").removeClass("timeZone").addClass("localTime");
			
		} else {
			
			dateTimeLocationArr[1] = sumHours(dateTimeLocationArr[1],-currentMatch.timezone);
			$("#timezone-box").removeClass("localTime").addClass("timeZone");
			
		}

		$("#location"+i).html(dateTimeLocationArr.join(" - "))
	
	}
		
	
}

/* input: number of fields filled (1-12) */
function updateProgressBar(filledFieldsNum) {

	
	for (var i=1; i<=12; i++) {
		
		if (i < filledFieldsNum) {
			
			$("#prog"+i).addClass("filled");
			$("#prog"+i).removeClass("last-filled");
		
		} else if (i == filledFieldsNum) {
			
			$("#prog"+i).addClass("filled");
			$("#prog"+i).addClass("last-filled");
				
		} else {
			
			$("#prog"+i).removeClass();
			
		}
		
	}
	
	$("#prog-perc").html(Math.floor(filledFieldsNum*100/12)+"%");

}

/* updates the score, renders them, updates the progress bar
 * and, if all matches have been played, shows the selected
 * teams in the knockout bracket on the right of the page
 */
function checkScores() {
	
	var emptyFieldsNum = 0;
	
	// count the number of empty fields for this group
	for (var i=1; i<=6; ++i) {
		
		if ($("#score"+i+"1").val() == "")
			emptyFieldsNum++;
		
		if ($("#score"+i+"2").val() == "")
			emptyFieldsNum++;
			
	}
	
	// update score objects
	updateScores();
	
	// render the scores in the table
	renderScores();
	
	// completed is defined in team-scores.js
	completed[currentGroup-1] = 12-emptyFieldsNum;

	var letter = letterArray[currentGroup-1];

	// remove the team flag background
	$("#1"+letter).removeClass().addClass("flag-small");
	$("#2"+letter).removeClass().addClass("flag-small");
	
	// add team flag background only if all scores are filled, leave empty otherwise
	if (emptyFieldsNum == 0) {		

		// get the first two teams of this group
		firstTwoTeams = getFirstByGroup(currentGroup);

		// all scores of this group filled in - we already know 1st and 2nd places
		$("#1"+letter).addClass("bg-img " + firstTwoTeams[0] + "-s");
		$("#2"+letter).addClass("bg-img " + firstTwoTeams[1] + "-s");

	}
	
	// check if 
	if (areAllFilled()) {
		
		// all scores filled in, show the 'knockout' button
		$("#go-knockout").show();
		$("#knockout-btn-big").show();
		$("#knockout-btn-big").focus();
		
	} else {
	
		// hide the knockout button, if visible
		$("#go-knockout").hide();
		$("#knockout-btn-big").hide();
	
		if (emptyFieldsNum == 0 && currentGroup < 8) {

			// don't show 'next group' if group H is the current group

			$("#next-group").show();	// show the 'next group' button
			$("#next-group").focus();	// focus the shown button

		} else {

			$("#next-group").hide();

		}
		
	}

	updateProgressBar(12-emptyFieldsNum);
	
}

/* Random score range: [0,4], based on
http://en.wikipedia.org/wiki/FIFA_World_Cup_records#Team
*/
function fillEmptyScores() {
	
	var score = Math.floor(Math.random()*5);
	
	for (var i=1; i<=6; ++i) {
		
		if ($("#score"+i+"1").val() == "") {
			$("#score"+i+"1").val(Math.floor(Math.random()*5));
		}
		
		if ($("#score"+i+"2").val() == "") {
			$("#score"+i+"2").val(Math.floor(Math.random()*5));
		}
			
	}
	
	checkScores();
	
}


function clearAllFields() {

	for (var i=1; i<=6; ++i) {
		
		$("#score"+i+"1").val("");
		$("#score"+i+"2").val("");
			
	}
	
	checkScores();

}

/*	This function saves the status of the visible groupLetter
 *	in the relative json object
 */
function saveGroupStatus(callback) {
	
	var group = letterArray[currentGroup-1]; 
	
	for (var i=0; i<6; ++i) {
		
		var score1val = $("#score"+(i+1)+"1").val();
		var score2val = $("#score"+(i+1)+"2").val();
		
		var score1;
		var score2;
		
		score1 = score1val != "" ? parseInt(score1val) : undefined;
		score2 = score2val != "" ? parseInt(score2val) : undefined;
		
		matches[group][i].score1 = score1;
		matches[group][i].score2 = score2;
		
		// object to send to the knockout page
		groupInfo[currentGroup-1].scores[i][0] = score1;			
		groupInfo[currentGroup-1].scores[i][1] = score2;
		
	}
	
	groupInfo[currentGroup-1].chart = firstTwoTeams;
	
	if (callback && typeof(callback) === "function") {
		callback();
	}
	
}

/*	This function updates all the match-related fields.
 *	@input	group (int)
 *			integer value that identifies the group to be showed (1=A, 8=H)
 */
function setMatchInformation(group) {
	
	currentGroup = group;

	// reset the first-two-teams array
	firstTwoTeams = ["",""];

	var groupLetter = letterArray[group-1];	

	// Remove all xx-big flags css from .team1 and .team2
	$(".team1").removeClass().addClass("match-flag").addClass("team1");
	$(".team2").removeClass().addClass("match-flag").addClass("team2");

	// render flags in the score table
	renderFlags();

	// Update match information for every match
	for (var i=1; i<=6; ++i) {
		
		// var currentMatch = eval("match"+groupLetter+i);
		var currentMatch = matches[groupLetter][i-1];
		
		var dateTimeLocation = currentMatch.date + " - ";
		
		dateTimeLocation += currentMatch.time;
			
		dateTimeLocation += " - " + currentMatch.location;
		
		// Set date, time and location to the related div
		$("#match-" + i + " > .match-location").html(dateTimeLocation);
		
		// set team names
		$("#match-" + i + " > .name1").html(currentMatch.team1);
		$("#match-" + i + " > .name2").html(currentMatch.team2);
		
		// set team flags
		$("#match-" + i + " > .team1").addClass(currentMatch.flag1+"-big");
		$("#match-" + i + " > .team2").addClass(currentMatch.flag2+"-big");

		// set team score (if already set in this session)
		
		$("#score"+i+"1").val(currentMatch.score1 != undefined ? currentMatch.score1 : "");
		$("#score"+i+"2").val(currentMatch.score2 != undefined ? currentMatch.score2 : "");
		
	}
	
	focusOnFirstEmptyField();
	
	// set the time format, according to the variable isLocalTimeSet
	if (!isLocalTimeSet)
		setTime(false);
		
	checkScores();


}

function updateScores() {
	
	var group = eval("group"+currentGroup);
		
	// reset the score of all team, as the are going to be re-calculated
	for (var i=0; i< group.length; ++i) {
				
		var teamScore = scores[group[i]];
		
		// reset all values in the object
		scores[group[i]].pts = [0,0,0,0];
		scores[group[i]].mp = 0;
    	scores[group[i]].w  = 0;
		scores[group[i]].d  = 0;
		scores[group[i]].l  = 0;
		scores[group[i]].gf = 0;
		scores[group[i]].ga = 0;
		
	}
	
	// scan every match score
	for (var i=1; i<=6; i++) {
		
		// retrieve the first team short tag
		var team1 = $("#match-"+i+" > div.team1").removeClass("match-flag").removeClass("team1").attr("class").substr(0,2);
		$("#match-"+i+" > div."+team1+"-big").addClass("match-flag").addClass("team1");
				
		// retrieve the second team short tag
		var team2 = $("#match-"+i+" > div.team2").removeClass("match-flag").removeClass("team2").attr("class").substr(0,2);
		$("#match-"+i+" > div."+team2+"-big").addClass("match-flag").addClass("team2");

		// retrieve the score object of both teams of the match
		var scoreObj1 = scores[team1];
		var scoreObj2 = scores[team2];

		// get the value of the score fields		
		var score1 = parseInt($("#score"+i+"1").val());
		var score2 = parseInt($("#score"+i+"2").val());

		// update the information only if the scores are not empty		
		if ( !isNaN(score1) && !isNaN(score2) ) {
			
			// update the number of matches played
			scoreObj1.mp++;			
			scoreObj2.mp++;
		
			if (score1 > score2) {	// team1 won
			
				scoreObj1.w++;	// increase won matches for team 1
				scoreObj2.l++; // increase lost matches for team 2

				// update direct confrontation info
				scoreObj1.pts[group.indexOf(team2)] = 3;
				scoreObj2.pts[group.indexOf(team1)] = 0;
	
			} else if (score1 < score2) { // team2 won
	
				scoreObj1.l++; // increase lost matches for team 1
				scoreObj2.w++; // increase won matches for team 2			
				
				// update direct confrontation info
				scoreObj1.pts[group.indexOf(team2.substr(0,2))] = 0;
				scoreObj2.pts[group.indexOf(team1)] = 3;

			} else { 		// draw result
			
				// increase draw matches for both team
				scoreObj1.d++;
				scoreObj2.d++;

				// update direct confrontation info
				scoreObj1.pts[group.indexOf(team2)] = 1;
				scoreObj2.pts[group.indexOf(team1)] = 1;
			
			}
			
			// update goals for and goals against number for team 1
			scoreObj1.gf += score1;
			scoreObj1.ga += score2;
			
			// update goals for and goals against number for team 2
			scoreObj2.gf += score2;
			scoreObj2.ga += score1;
			
			// update the score object of both teams of the match
			scores[team1] = scoreObj1;
			scores[team2] = scoreObj2;
			
		}
		
	}
	
	
	
}

/* Renders the scores of the object scores (team-scores.js)
 * in the score table.
 */
function renderScores() {
	
	var groupArray = eval("group"+currentGroup);
	
	for (var i=0; i<groupArray.length; ++i) {
		
		// retrieve scores
		var teamObj = scores[groupArray[i]];
		
		$("#team"+(i+1)+" > td.pts").html(teamObj.w*3+teamObj.d);
		$("#team"+(i+1)+" > td.mp").html(teamObj.mp);
		$("#team"+(i+1)+" > td.won").html(teamObj.w);
		$("#team"+(i+1)+" > td.draw").html(teamObj.d);
		$("#team"+(i+1)+" > td.lost").html(teamObj.l);
		$("#team"+(i+1)+" > td.gf").html(teamObj.gf);
		$("#team"+(i+1)+" > td.ga").html(teamObj.ga);
		$("#team"+(i+1)+" > td.gd").html(teamObj.gf-teamObj.ga);
		
		
	}
	
	
}

// renders the flags of the current group
function renderFlags() {
	
	var groupArray = eval("group"+currentGroup);
	
	for (var i=0; i<groupArray.length; ++i) {
	
		$("#team"+(i+1)+" > td.pts-flag > img").attr("src","images/flags-50x30/"+groupArray[i]+".png");
		
	}
	
}


// shows the Knockout page
function goKnockout() {
	
	// save current Group status
	saveGroupStatus(function() {
		
		$("#group-info").val(JSON.stringify(groupInfo));
		
		$("#resultsForm").submit();
		
	});
	
}


function focusOnFirstEmptyField() {
	
	
	/*
	 * This script is wrapped in an anonymous function
	 * the loop interrupts when the first empty fields
	 * is found.
	 */
	(function() {

		for (var i=1; i<=8; ++i) {

			for (var j=1; j<=2; ++j) {

				if ($("#score"+i+j).val() == "") {

					$("#score"+i+j).focus();
					return;

				}

			}


		}

	})();
	
}