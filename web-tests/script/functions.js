function areAllFilled() {
    
    var allFilled = true;
    
    for (var i=0; i<completed.length; ++i) {
        
        if (completed[i] != 12) {
            allFilled = false;
            break;
        }
        
    }
    
    return allFilled;
    
}

/** Given a group number, returns the two teams
 *  with the highest score.
 *  @input  groupNumber 1=A, 8=H
 *  @return placeIndexes
 */
function getFirstByGroup(groupNumber) {
    
    var group = eval("group"+groupNumber);  // = array of short tags
    var ptsArray = new Array(group.length); // = points per team
    var gdArray = new Array(group.length);  // = goal difference per team
    
    for (var i=0; i<group.length; ++i) {
        
        // get the current team object
        var curTeam = scores[group[i]];
        
        // calculate points and goal difference for the current team
        ptsArray[i] = curTeam.w*3 + curTeam.d;
        gdArray[i] = curTeam.gf - curTeam.ga;
        
    }
    
    // this array will contain the index of the first two teams (0 = 1st place, 1 = 2nd place)
    var placeIndexes = new Array(2);

    for (i=0; i<2; ++i) {
    
        var maxPts = 0;
        var maxIndex;
        
        for (var j=0; j<group.length; ++j) {

            if (ptsArray[j] > maxPts) {

                maxPts = ptsArray[j];
                maxIndex = j;
                
            } else if (ptsArray[j] == maxPts) {

                // we need to check goal difference array
                if (gdArray[j] > gdArray[maxIndex]) {

                    maxPts = ptsArray[j];
                    maxIndex = j;
                    
                } else if ( gdArray[j] == gdArray[maxIndex]) {

                    // check the direct confrontation                   
                    var directConfrResult = scores[group[maxIndex]][j];
                    
                    /* 
                     * 3 = group[maxIndex] won against group[j]
                     * 1 = group[maxIndex] drew against group[j]
                     * 0 = group[maxIndex] lost against group[j]
                     */
                     
                    if (directConfrResult == 0) {

                        maxPts = ptsArray[j];
                        maxIndex = j;
                        
                    } else if (directConfrResult == 1) {

                        // draw
                        var draw = [true,false][Math.round(Math.random())];

                        maxPts = draw ? ptsArray[j] : maxPts;
                        maxIndex = draw ? j : maxIndex;
                        
                    } // if directConfrResult == 3, leave everything as it is

                } // if gdArray[j] < gdArray[maxIndex], leave everything as it is


            } // ig ptsArray[j] < maxPts, leave everything as it is
            
        }
        

        placeIndexes[i] = maxIndex;
        

        // reset the points, in order to get the 2nd higher score
        ptsArray[maxIndex] = 0;
        gdArray[maxIndex] = 0;

    }
    

    console.log("return: " + group[placeIndexes[0]] + ", " + group[placeIndexes[1]]);

    // return the array with the two teams' short name
    return new Array(group[placeIndexes[0]], group[placeIndexes[1]]);

}