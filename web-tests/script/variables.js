var teams = {
    
    'br': 'Brazil',
    'hr': 'Croatia',
    'mx': 'Mexico',
    'cm': 'Cameroon',
    'es': 'Spain',
    'nl': 'Netherlands',
    'cl': 'Chile',
    'au': 'Australia',
    'co': 'Colombia',
    'gr': 'Greece',
    'ci': 'Côte d\'Ivoire',
    'jp': 'Japan',
    'uy': 'Uruguay',
    'cr': 'Costa Rica',
    'en': 'England',
    'it': 'Italy',
    'ch': 'Switzerland',
    'ec': 'Ecuador',
    'fr': 'France',
    'hn': 'Honduras',
    'ar': 'Argentina',
    'ba': 'Bosnia-Herz.',
    'ir': 'Iran',
    'ng': 'Nigeria',
    'de': 'Germany',
    'pt': 'Portugal',
    'gh': 'Ghana',
    'us': 'USA',
    'be': 'Belgium',
    'dz': 'Algeria',
    'ru': 'Russia',
    'kr': 'Korea Rep.'
    
};

/* JSON - Match information.
 * Structure:
 * matches["GROUP_LETTER"][MATCH_INDEX]["parameter"], where
 * GROUP_LETTER = A...H;
 * MATCH_INDEX = 0...5;
 * parameter = date|time|location|timezone|team1|flag1|score1|team2|flag2|score2;
 */

var matches = {
	
	'A': [
		
		{

			date: "June 12",
			time: "17:00",
			location: "S&#227;o Paulo",
			timezone: -3,
			team1: "Brazil",
			flag1: "br",
			score1: "",
			team2: "Croatia",
			flag2: "hr",
			score2: ""

		},

		{

			date: "June 16",
			time: "13:00",
			location: "Natal",
			timezone: -3,
			team1: "Mexico",
			flag1: "mx",
			score1: "",
			team2: "Cameroon",
			flag2: "cm",
			score2: ""

		},

		{

			date: "June 17",
			time: "16:00",
			location: "Fortaleza",
			timezone: -3,
			team1: "Brazil",
			flag1: "br",
			score1: "",
			team2: "Mexico",
			flag2: "mx",
			score2: ""

		},

		{

			date: "June 18",
			time: "18:00",
			location: "Manaus",
			timezone: -4,
			team1: "Cameroon",
			flag1: "cm",
			score1: "",
			team2: "Croatia",
			flag2: "hr",
			score2: ""

		},

		{

			date: "June 23",
			time: "17:00",
			location: "Bras&#237;lia",
			timezone: -3,
			team1: "Cameroon",
			flag1: "cm",
			score1: "",
			team2: "Brazil",
			flag2: "br",
			score2: ""

		},

		{

			date: "June 23",
			time: "17:00",
			location: "Bras&#237;lia",
			timezone: -3,
			team1: "Croatia",
			flag1: "hr",
			score1: "",
			team2: "Mexico",
			flag2: "mx",
			score2: ""

		}
	
	],
	
	'B': [
		{
		
			date: "June 13",
			time: "16:00",
			location: "Salvador",
			timezone: -3,
			team1: "Spain",
			flag1: "es",
			score1: "",
			team2: "Netherlands",
			flag2: "nl",
			score2: ""
		
		},
		
		{
		
			date: "June 13",
			time: "18:00",
			location: "Cuiab&#225;",
			timezone: -4,
			team1: "Chile",
			flag1: "cl",
			score1: "",
			team2: "Australia",
			flag2: "au",
			score2: ""
		
		},
		
		{
		
			date: "June 18",
			time: "16:00",
			location: "Rio De Janeiro",
			timezone: -3,
			team1: "Spain",
			flag1: "es",
			score1: "",
			team2: "Chile",
			flag2: "cl",
			score2: ""
		
		},
		
		{
		
			date: "June 18",
			time: "13:00",
			location: "Porto Alegre",
			timezone: -3,
			team1: "Australia",
			flag1: "au",
			score1: "",
			team2: "Netherlands",
			flag2: "nl",
			score2: ""
		
		},
		
		{
		
			date: "June 23",
			time: "13:00",
			location: "Curitiba",
			timezone: -3,
			team1: "Australia",
			flag1: "au",
			score1: "",
			team2: "Spain",
			flag2: "es",
			score2: ""
		
		},
		
		{
		
			date: "June 23",
			time: "13:00",
			location: "S&#227;o Paulo",
			timezone: -3,
			team1: "Netherlands",
			flag1: "nl",
			score1: "",
			team2: "Chile",
			flag2: "cl",
			score2: ""
		
		}	
	],
	
	'C': [
		{
			
			date: "Jun 14",
			time: "13:00",
			location: "Belo Horizonte",
			timezone: -3,
			team1: "Colombia",
			flag1: "co",
			score1: "",
			team2: "Greece",
			flag2: "gr",
			score2: ""
		},
		
		{
		
			date: "Jun 14",
			time: "22:00",
			location: "Recife",
			timezone: -4,
			team1: "Côte d'Ivoire",
			flag1: "ci",
			score1: "",
			team2: "Japan",
			flag2: "jp",
			score2: ""
			
		},
		
		{
		
			date: "Jun 19",
			time: "13:00",
			location: "Bras&#237;lia",
			timezone: -3,
			team1: "Colombia",
			flag1: "co",
			score1: "",
			team2: "Côte d'Ivoire",
			flag2: "ci",
			score2: ""
			
		},
		
		{
		
			date: "Jun 19",
			time: "19:00",
			location: "Natal",
			timezone: -3,
			team1: "Japan",
			flag1: "jp",
			score1: "",
			team2: "Greece",
			flag2: "gr",
			score2: ""
			
		},
		
		{
			
			date: "Jun 24",
			time: "16:00",
			location: "Cuiab&#225;",
			timezone: -4,
			team1: "Japan",
			flag1: "jp",
			score1: "",
			team2: "Colombia",
			flag2: "co",
			score2: ""
		
		},
		
		{
		
			date: "Jun 24",
			time: "17:00",
			location: "Fortaleza",
			timezone: -3,
			team1: "Greece",
			flag1: "gr",
			score1: "",
			team2: "Côte d'Ivoire",
			flag2: "ci",
			score2: ""
		
		}	
	],
	
	'D': [
		{
			
			date: "June 14",
			time: "16:00",
			location: "Fortaleza",
			timezone: -3,
			team1: "Uruguay",
			flag1: "uy",
			score1: "",
			team2: "Costa Rica",
			flag2: "cr",
			score2: ""
			
		},
		
		{
			
			date: "June 14",
			time: "18:00",
			location: "Manaus",
			timezone: -4,
			team1: "England",
			flag1: "en",
			score1: "",
			team2: "Italy",
			flag2: "it",
			score2: ""
			
		},
		
		{
			
			date: "June 19",
			time: "16:00",
			location: "S&#227;o Paulo",
			timezone: -3,
			team1: "Uruguay",
			flag1: "uy",
			score1: "",
			team2: "England",
			flag2: "en",
			score2: ""
			
		},
		
		{
			
			date: "June 20",
			time: "13:00",
			location: "Recife",
			timezone: -4,
			team1: "Italy",
			flag1: "it",
			score1: "",
			team2: "Costa Rica",
			flag2: "cr",
			score2: ""
			
		},
		
		{
			
			date: "June 24",
			time: "13:00",
			location: "Natal",
			timezone: -3,
			team1: "Italy",
			flag1: "it",
			score1: "",
			team2: "Uruguay",
			flag2: "uy",
			score2: ""
			
		},
		
		{
			
			date: "June 24",
			time: "13:00",
			location: "Belo Horizonte",
			timezone: -3,
			team1: "Costa Rica",
			flag1: "cr",
			score1: "",
			team2: "England",
			flag2: "en",
			score2: ""
			
		}
	],
	
	'E': [
		{
		
			date: "June 15",
			time: "13:00",
			location: "Bras&#237;lia",
			timezone: -3,
			team1: "Switzerland",
			flag1: "ch",
			score1: "",
			team2: "Ecuador",
			flag2: "ec",
			score2: ""
		},
		
		{
		
			date: "June 15",
			time: "16:00",
			location: "Porto Alegre",
			timezone: -3,
			team1: "France",
			flag1: "fr",
			score1: "",
			team2: "Honduras",
			flag2: "hn",
			score2: ""
		},
		
		{
		
			date: "June 20",
			time: "16:00",
			location: "Salvador",
			timezone: -3,
			team1: "Switzerland",
			flag1: "ch",
			score1: "",
			team2: "France",
			flag2: "fr",
			score2: ""
		},
		
		{
		
			date: "June 20",
			time: "19:00",
			location: "Curitiba",
			timezone: -3,
			team1: "Honduras",
			flag1: "hn",
			score1: "",
			team2: "Ecuador",
			flag2: "ec",
			score2: ""
		},
		
		{
		
			date: "June 24",
			time: "16:00",
			location: "Manaus",
			timezone: -4,
			team1: "Honduras",
			flag1: "hn",
			score1: "",
			team2: "Switzerland",
			flag2: "ch",
			score2: ""
		},
		
		{
		
			date: "June 24",
			time: "17:00",
			location: "Rio De Janeiro",
			timezone: -3,
			team1: "Ecuador",
			flag1: "ec",
			score1: "",
			team2: "France",
			flag2: "fr",
			score2: ""
		}	
	],
	
	'F': [
		{
		
			date: "June 15",
			time: "19:00",
			location: "Rio De Janeiro",
			timezone: -3,
			team1: "Argentina",
			flag1: "ar",
			score1: "",
			team2: "Bosnia-Herz.",
			flag2: "ba",
			score2: ""
		},
		
		{
		
			date: "June 16",
			time: "16:00",
			location: "Curitiba",
			timezone: -3,
			team1: "Iran",
			flag1: "ir",
			score1: "",
			team2: "Nigeria",
			flag2: "ng",
			score2: ""
		},
		
		{
		
			date: "June 21",
			time: "13:00",
			location: "Belo Horizonte",
			timezone: -3,
			team1: "Argentina",
			flag1: "ar",
			score1: "",
			team2: "Iran",
			flag2: "ir",
			score2: ""
		},
		
		{
		
			date: "June 21",
			time: "18:00",
			location: "Cuiab&#225;",
			timezone: -4,
			team1: "Nigeria",
			flag1: "ng",
			score1: "",
			team2: "Bosnia-Herz.",
			flag2: "ba",
			score2: ""
		},
		
		{
		
			date: "June 25",
			time: "13:00",
			location: "Porto Alegre",
			timezone: -3,
			team1: "Nigeria",
			flag1: "ng",
			score1: "",
			team2: "Argentina",
			flag2: "ar",
			score2: ""
		},
		
		{
		
			date: "June 25",
			time: "13:00",
			location: "Salvador",
			timezone: -3,
			team1: "Bosnia-Herz.",
			flag1: "ba",
			score1: "",
			team2: "Iran",
			flag2: "ir",
			score2: ""
		}	
	],
	
	'G': [
		{
		
			date: "June 16",
			time: "13:00",
			location: "Salvador",
			timezone: -3,
			team1: "Germany",
			flag1: "de",
			score1: "",
			team2: "Portugal",
			flag2: "pt",
			score2: ""
		},
		
		{
		
			date: "June 16",
			time: "19:00",
			location: "Natal",
			timezone: -3,
			team1: "Ghana",
			flag1: "gh",
			score1: "",
			team2: "USA",
			flag2: "us",
			score2: ""
		},
		
		{
		
			date: "June 21",
			time: "16:00",
			location: "Fortaleza",
			timezone: -3,
			team1: "Germany",
			flag1: "de",
			score1: "",
			team2: "Ghana",
			flag2: "gh",
			score2: ""
		},
		
		{
		
			date: "June 22",
			time: "18:00",
			location: "Manaus",
			timezone: -4,
			team1: "USA",
			flag1: "us",
			score1: "",
			team2: "Portugal",
			flag2: "pt",
			score2: ""
		},
		
		{
		
			date: "June 25",
			time: "13:00",
			location: "Recife",
			timezone: -4,
			team1: "USA",
			flag1: "us",
			score1: "",
			team2: "Germany",
			flag2: "de",
			score2: ""
		},
		
		{
		
			date: "June 25",
			time: "13:00",
			location: "Bras&#237;lia",
			timezone: -3,
			team1: "Portugal",
			flag1: "pt",
			score1: "",
			team2: "Ghana",
			flag2: "gh",
			score2: ""
		}
	],
	
	'H': [
		{
		
			date: "June 17",
			time: "13:00",
			location: "Belo Horizonte",
			timezone: -3,
			team1: "Belgium",
			flag1: "be",
			score1: "",
			team2: "Algeria",
			flag2: "dz",
			score2: ""
		},
		
		{
		
			date: "June 17",
			time: "18:00",
			location: "Cuiab&#225;",
			timezone: -4,
			team1: "Russia",
			flag1: "ru",
			score1: "",
			team2: "Korea Rep.",
			flag2: "kr",
			score2: ""
		},
		
		{
		
			date: "June 22",
			time: "13:00",
			location: "Rio De Janeiro",
			timezone: -3,
			team1: "Belgium",
			flag1: "be",
			score1: "",
			team2: "Russia",
			flag2: "ru",
			score2: ""
		},
		
		{
		
			date: "June 22",
			time: "16:00",
			location: "Porto Alegre",
			timezone: -3,
			team1: "Korea Rep.",
			flag1: "kr",
			score1: "",
			team2: "Algeria",
			flag2: "dz",
			score2: ""
		},
		
		{
		
			date: "June 26",
			time: "17:00",
			location: "S&#227;o Paulo",
			timezone: -3,
			team1: "Korea Rep.",
			flag1: "kr",
			score1: "",
			team2: "Belgium",
			flag2: "be",
			score2: ""
		},
		
		{
		
			date: "June 26",
			time: "17:00",
			location: "Curitiba",
			timezone: -3,
			team1: "Algeria",
			flag1: "dz",
			score1: "",
			team2: "Russia",
			flag2: "ru",
			score2: ""
		}	
	]
	
};


// ------------------------- //


// 0-12, the number of filled scores per group (completed[0] = group A)
var completed = [0,0,0,0,0,0,0,0];

// short tag array per group
var group1 = ['br','hr','mx','cm'];
var group2 = ['es','nl','cl','au'];
var group3 = ['co','gr','ci','jp'];
var group4 = ['uy','cr','en','it'];
var group5 = ['ch','ec','fr','hn'];
var group6 = ['ar','ba','ir','ng'];
var group7 = ['de','pt','gh','us'];
var group8 = ['be','dz','ru','kr'];

var scores = {

    'br': {
        pts: [0,0,0,0],
        mp: 0,
        w: 0,
        d: 0,
        l: 0,
        gf: 0,
        ga: 0
    },
    
    'hr': {
        pts: [0,0,0,0],
        mp: 0,
        w: 0,
        d: 0,
        l: 0,
        gf: 0,
        ga: 0
    },
    
    'mx': {
       pts: [0,0,0,0],
        mp: 0,
        w: 0,
        d: 0,
        l: 0,
        gf: 0,
        ga: 0
    },
    
    'cm': {
        pts: [0,0,0,0],
        mp: 0,
        w: 0,
        d: 0,
        l: 0,
        gf: 0,
        ga: 0
    },
    
    'es': {
        pts: [0,0,0,0],
        mp: 0,
        w: 0,
        d: 0,
        l: 0,
        gf: 0,
        ga: 0
    },
    
    'nl': {
        pts: [0,0,0,0],
        mp: 0,
        w: 0,
        d: 0,
        l: 0,
        gf: 0,
        ga: 0
    },
    
    'cl': {
        pts: [0,0,0,0],
        mp: 0,
        w: 0,
        d: 0,
        l: 0,
        gf: 0,
        ga: 0
    },
    
    'au': {
        pts: [0,0,0,0],
        mp: 0,
        w: 0,
        d: 0,
        l: 0,
        gf: 0,
        ga: 0
    },
    
    'co': {
        pts: [0,0,0,0],
        mp: 0,
        w: 0,
        d: 0,
        l: 0,
        gf: 0,
        ga: 0
    },
    
    'gr': {
        pts: [0,0,0,0],
        mp: 0,
        w: 0,
        d: 0,
        l: 0,
        gf: 0,
        ga: 0
    },
    
    'ci': {
        pts: [0,0,0,0],
        mp: 0,
        w: 0,
        d: 0,
        l: 0,
        gf: 0,
        ga: 0
    },
    
    'jp': {
        pts: [0,0,0,0],
        mp: 0,
        w: 0,
        d: 0,
        l: 0,
        gf: 0,
        ga: 0
    },
    
    'uy': {
        pts: [0,0,0,0],
        mp: 0,
        w: 0,
        d: 0,
        l: 0,
        gf: 0,
        ga: 0
    },
    
    'cr': {
        pts: [0,0,0,0],
        mp: 0,
        w: 0,
        d: 0,
        l: 0,
        gf: 0,
        ga: 0
    },
    
    'en': {
        pts: [0,0,0,0],
        mp: 0,
        w: 0,
        d: 0,
        l: 0,
        gf: 0,
        ga: 0
    },
    
    'it': {
        pts: [0,0,0,0],
        mp: 0,
        w: 0,
        d: 0,
        l: 0,
        gf: 0,
        ga: 0
    },
    
    
    'ch': {
        pts: [0,0,0,0],
        mp: 0,
        w: 0,
        d: 0,
        l: 0,
        gf: 0,
        ga: 0
    },
    
    'ec': {
        pts: [0,0,0,0],
        mp: 0,
        w: 0,
        d: 0,
        l: 0,
        gf: 0,
        ga: 0
    },
    
    'fr': {
        pts: [0,0,0,0],
        mp: 0,
        w: 0,
        d: 0,
        l: 0,
        gf: 0,
        ga: 0
    },
    
    'hn': {
        pts: [0,0,0,0],
        mp: 0,
        w: 0,
        d: 0,
        l: 0,
        gf: 0,
        ga: 0
    },
    
    'ar': {
        pts: [0,0,0,0],
        mp: 0,
        w: 0,
        d: 0,
        l: 0,
        gf: 0,
        ga: 0
    },
    
    'ba': {
        pts: [0,0,0,0],
        mp: 0,
        w: 0,
        d: 0,
        l: 0,
        gf: 0,
        ga: 0
    },
    
    'ir': {
        pts: [0,0,0,0],
        mp: 0,
        w: 0,
        d: 0,
        l: 0,
        gf: 0,
        ga: 0
    },
    
    'ng': {
        pts: [0,0,0,0],
        mp: 0,
        w: 0,
        d: 0,
        l: 0,
        gf: 0,
        ga: 0
    },
    
    'de': {
        pts: [0,0,0,0],
        mp: 0,
        w: 0,
        d: 0,
        l: 0,
        gf: 0,
        ga: 0
    },
    
    'pt': {
        pts: [0,0,0,0],
        mp: 0,
        w: 0,
        d: 0,
        l: 0,
        gf: 0,
        ga: 0
    },
    
    'gh': {
        pts: [0,0,0,0],
        mp: 0,
        w: 0,
        d: 0,
        l: 0,
        gf: 0,
        ga: 0
    },
    
    'us': {
        pts: [0,0,0,0],
        mp: 0,
        w: 0,
        d: 0,
        l: 0,
        gf: 0,
        ga: 0
    },
    
    'be': {
        pts: [0,0,0,0],
        mp: 0,
        w: 0,
        d: 0,
        l: 0,
        gf: 0,
        ga: 0
    },
    
    'dz': {
        pts: [0,0,0,0],
        mp: 0,
        w: 0,
        d: 0,
        l: 0,
        gf: 0,
        ga: 0
    },
    
    'ru': {
        pts: [0,0,0,0],
        mp: 0,
        w: 0,
        d: 0,
        l: 0,
        gf: 0,
        ga: 0
    },
    
    'kr': {
        pts: [0,0,0,0],
        mp: 0,
        w: 0,
        d: 0,
        l: 0,
        gf: 0,
        ga: 0
    }
   
};



var groupInfo = [

    {

        "scores": [
    
            [0,0],
            [0,0],
            [0,0],
            [0,0],
            [0,0],
            [0,0]
    
        ],
        
        "chart": ["",""]

    },

    {

        "scores": [
    
            [0,0],
            [0,0],
            [0,0],
            [0,0],
            [0,0],
            [0,0]
    
        ],
        
        "chart": ["",""]

    },

    {

        "scores": [
    
            [0,0],
            [0,0],
            [0,0],
            [0,0],
            [0,0],
            [0,0]
    
        ],
        
        "chart": ["",""]

    },

    {

        "scores": [
    
            [0,0],
            [0,0],
            [0,0],
            [0,0],
            [0,0],
            [0,0]
    
        ],
        
        "chart": ["",""]

    },

    {

        "scores": [
    
            [0,0],
            [0,0],
            [0,0],
            [0,0],
            [0,0],
            [0,0]
    
        ],
        
        "chart": ["",""]

    },

    {

        "scores": [
    
            [0,0],
            [0,0],
            [0,0],
            [0,0],
            [0,0],
            [0,0]
    
        ],
        
        "chart": ["",""]

    },
    {

        "scores": [
    
            [0,0],
            [0,0],
            [0,0],
            [0,0],
            [0,0],
            [0,0]
    
        ],
        
        "chart": ["",""]

    },
    {

        "scores": [
    
            [0,0],
            [0,0],
            [0,0],
            [0,0],
            [0,0],
            [0,0]
    
        ],
        
        "chart": ["",""]

    }

];