function updateKnockout() {
    
    // first knockout level
    for (var i=1; i<=8; ++i) {
        
        var score1 = $("#score1"+i+"-1").val();
        var score2 = $("#score1"+i+"-2").val();
     
        if (score1 != "" && score2 != "") {
            
            var winnerTeam;
            
            if (score1 > score2) {
                
                // team 1 won
                winnerTeam = 1;
                
            } else if ( score1 < score2 ) {
                
                // team 2 won
                winnerTeam = 2;
                
            } else {
                
                // draw TODO winner set as team1 for the moment
                winnerTeam = 1;
                
            }
            
            // retrieve and set the team name
            var winnerName = $("#match-1"+i+" #name" + winnerTeam).html();
            $("#name2"+i).html(winnerName);

            // retrieve the winner flag tag
            var winnerFlag = $("#match-1"+i+" .team" + winnerTeam + " .match-flag").removeClass("match-flag").attr("class");
            $("."+winnerFlag).addClass("match-flag");

            // set the winner flag tag
            $("#flag2"+i).removeClass().addClass("match-flag "+winnerFlag);
            
        } else {
            
            // reset
            $("#name2"+i).html("");
            $("#flag2"+i).removeClass().addClass("match-flag no-flag");
            
        }
        
        
    }

 
 
    // second knockout level
    for (var i=1; i<=4; ++i) {
        
        var score1 = $("#score2"+i+"-1").val();
        var score2 = $("#score2"+i+"-2").val();
     
        if (score1 != "" && score2 != "") {
            
            var winnerTeam;
            
            if (score1 > score2) {
                
                // team 1 won
                winnerTeam = 1;
                
            } else if ( score1 < score2 ) {
                
                // team 2 won
                winnerTeam = 2;
                
            } else {
                
                // draw TODO winner set as team1 for the moment
                winnerTeam = 1;
                
            }
            
            
            // retrieve and set the team name
            var winnerName = $("#match-2"+i+" #name2" + ((i-1)*2+winnerTeam) ).html();
            $("#name3"+i).html(winnerName);

            // retrieve the winner flag tag
            var winnerFlag = $("#match-2"+i+" .team" + winnerTeam + " .match-flag").removeClass("match-flag").attr("class");
            $("."+winnerFlag).addClass("match-flag");

            // set the winner flag tag
            $("#flag3"+i).removeClass().addClass("match-flag "+winnerFlag);
            
        } else {
            
            // reset
            $("#name3"+i).html("");
            $("#flag3"+i).removeClass().addClass("match-flag no-flag");
            
        }
        
        
    }
   
   
    // third knockout level
    for (var i=1; i<=2; ++i) {
        
        var score1 = $("#score3"+i+"-1").val();
        var score2 = $("#score3"+i+"-2").val();
     
        if (score1 != "" && score2 != "") {
            
            var winnerTeam;
            var loserTeam;
            
            if (score1 > score2) {
                
                // team 1 won
                winnerTeam = 1;
                loserTeam = 2;
                
            } else if ( score1 < score2 ) {
                
                // team 2 won
                winnerTeam = 2;
                loserTeam = 1;
                
            } else {
                
                // draw TODO winner set as team1 for the moment
                winnerTeam = 1;
                loserTeam = 2;
                
            }
            
            // retrieve and set the team name
            var winnerName = $("#match-3"+i+" #name3" + ((i-1)*2+winnerTeam) ).html();
            var loserName  =  $("#match-3"+i+" #name3" + ((i-1)*2+loserTeam) ).html();
            
            $("#name-final1-"+i).html(winnerName);
            $("#name-final2-"+i).html(loserName);

            // retrieve the winner and loser flag tag
            var winnerFlag = $("#match-3"+i+" .team" + winnerTeam + " .match-flag").removeClass("match-flag").attr("class");
            $("."+winnerFlag).addClass("match-flag");
            
            var loserFlag = $("#match-3"+i+" .team" + loserTeam + " .match-flag").removeClass("match-flag").attr("class");
            $("."+loserFlag).addClass("match-flag");
            

            // set the winner and loser flag tag
            $("#flag-final1-"+i).removeClass().addClass("match-flag "+winnerFlag);
            $("#flag-final2-"+i).removeClass().addClass("match-flag "+loserFlag);
            
        } else {
            
            // reset
            $("#name-final1-"+i).html("");
            $("#name-final2-"+i).html("");
            $("#flag-final1-"+i).removeClass().addClass("match-flag no-flag");
            $("#flag-final2-"+i).removeClass().addClass("match-flag no-flag");
            
        }
        
        
    }
  
        
    var final1score1 = $("#score-final1-1").val();
    var final2score2 = $("#score-final1-2").val();
	var final2score1 = $("#score-final2-1").val();
	var final2score2 = $("#score-final2-2").val();        
        
     
	if (final1score1 != "" && final1score2 != "" && final2score1 != "" && final2score2 != "") {

		var chart = new Array(3);



		// retrieve and set the team name
		var winnerName = $("#match-3"+i+" #name3" + ((i-1)*2+winnerTeam) ).html();
		var loserName  =  $("#match-3"+i+" #name3" + ((i-1)*2+loserTeam) ).html();

		$("#name-final1-"+i).html(winnerName);
		$("#name-final2-"+i).html(loserName);

		// retrieve the winner and loser flag tag
		var winnerFlag = $("#match-3"+i+" .team" + winnerTeam + " .match-flag").removeClass("match-flag").attr("class");
		$("."+winnerFlag).addClass("match-flag");

		var loserFlag = $("#match-3"+i+" .team" + loserTeam + " .match-flag").removeClass("match-flag").attr("class");
		$("."+loserFlag).addClass("match-flag");


		// set the winner and loser flag tag
		$("#flag-final1-"+i).removeClass().addClass("match-flag "+winnerFlag);
		$("#flag-final2-"+i).removeClass().addClass("match-flag "+loserFlag);

	} else {

		// reset
		$("#name-final1-"+i).html("");
		$("#name-final2-"+i).html("");
		$("#flag-final1-"+i).removeClass().addClass("match-flag no-flag");
		$("#flag-final2-"+i).removeClass().addClass("match-flag no-flag");

	}
    
}



$(document).ready(function() {
    
    // 8 = number of matches
    for (var i=0; i<8; ++i) {
        
        $("#match-1" + (i+1) + " #name1").html(teams[knockout[i][0]]);
        $("#match-1" + (i+1) + " #name2").html(teams[knockout[i][1]]);
    
        $("#match-1" + (i+1) + " > .team1 > .match-flag").removeClass("no-flag").addClass(knockout[i][0]);
        $("#match-1" + (i+1) + " > .team2 > .match-flag").removeClass("no-flag").addClass(knockout[i][1]);
        
    }
    
    // setting button, visible only in small browser windows
    $("#settings-btn").click(function() {
        
        $("#settings-box").toggle();
        
    });
    
    
    // focus the first score field
    $("#score11-1").focus();
    
    // set the autotab for the score fields
	$(".team-result").autotab({ maxlength: 1, format: 'number' });
    
    // update the score information displayed at every key press
	$(".team-result").keyup(function(){
		updateKnockout();
	});
    
});