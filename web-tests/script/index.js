var preTestURL = "https://docs.google.com/forms/d/1F7NWWNsxpR584ePUEQVrUUKVucJzfdzCytu88WB8IDY/viewform";
var htmlTestURL = "group-stage-norecord.php";
var postTestURL = "https://docs.google.com/forms/d/1YJgNmFhOd8X-qs-4I21M_7MHQHZdMW0042lf-sAU5ug/viewform";

$(document).ready(function() {

    $("#pre-test-link").click(function() {

        if ($(this).hasClass("enabled")) {

            $.colorbox({

                href: preTestURL,
                iframe: true,
                innerWidth: "650px",
                innerHeight: "75%",
                overlayClose: false,
                onClosed: function() {

                    $("#pre-test-link").removeClass("enabled");
                    $("#html-test-link").addClass("enabled");
                    $("#post-test-link").addClass("enabled");

                }

            });

        }

    });

    $("#html-test-link").click(function() {

        if ($(this).hasClass("enabled")) {

            window.open(htmlTestURL, '_blank');

        }

    });

    $("#post-test-link").click(function() {

            $("#html-test-link").removeClass("enabled");

            $.colorbox({

                href: postTestURL,
                iframe: true,
                innerWidth: "75%",
                innerHeight: "75%",
                overlayClose: false,
                onClosed: function() {

                    $("#post-test-link").removeClass("enabled");

                    $('p').hide();
                    $('ul').hide();
                    $("#links").hide();
                    $("#thanks").show();

                    $.post("php/log_ip.php");


                }

            });

    });

});