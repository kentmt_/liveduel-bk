<?php

define ("HOST_IP",

            getenv('HTTP_CLIENT_IP')?:
            getenv('HTTP_X_FORWARDED_FOR')?:
            getenv('HTTP_X_FORWARDED')?:
            getenv('HTTP_FORWARDED_FOR')?:
            getenv('HTTP_FORWARDED')?:
            getenv('REMOTE_ADDR')
            
);

define("FILE_PATH", "iplog.txt");

$date = date("YmdHis");

$file = fopen(FILE_PATH,"a+");

if (flock ($file, LOCK_EX)) {

    fwrite($file, HOST_IP."|$date\r\n");    

    flock($file, LOCK_UN);

}

fclose($file);


?>