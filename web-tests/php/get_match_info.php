<?php

    $round_of_16 = array(

        array(

            // 1A - 2B
            "date" => "June 28",
            "time" => "13:00",
            "location" => "Belo Horizonte",
            "timezone" => -3

        ),

        array(
            // 1C - 2D
            "date" => "June 28",
            "time" => "17:00",
            "location" => "Rio de Janeiro",
            "timezone" => -3

        ),

        array(
            // 1E - 2F    
            "date" => "June 30",
            "time" => "13:00",
            "location" => "Brasilia",
            "timezone" => -3

        ),

        array(
            // 1G - 2H
            "date" => "June 30",
            "time" => "17:00",
            "location" => "Porto Alegre",
            "timezone" => -3

        ),

        array(
            // 1B - 2A
            "date" => "June 29",
            "time" => "13:00",
            "location" => "Fortaleza",
            "timezone" => -3

        ),

        array(
            // 1D - 2C
            "date" => "June 29",
            "time" => "17:00",
            "location" => "Recife",
            "timezone" => -3

        ),

        array(
            // 1F - 2E
            "date" => "July 01",
            "time" => "13:00",
            "location" => "Sao Paulo",
            "timezone" => -3

        ),

        array(
            // 1H - 2G
            "date" => "July 01",
            "time" => "17:00",
            "location" => "Salvador",
            "timezone" => -3

        )
        
    );


    $quarter_finals = array(
    
        array(
            // W49 - W50
            "date" => "July 04",
            "time" => "17:00",
            "location" => "Fortaleza"
            "timezone" => -3
        ),

        array(
            // W53 - W54
            "date" => "July 04",
            "time" => "13:00",
            "location" => "Rio de Janeiro"
            "timezone" => -3
        ),

        array(
            // W51 - W52
            "date" => "July 05",
            "time" => "17:00",
            "location" => "Salvador"
            "timezone" => -3
        
        ),
        array(
            // W55 - W56
            "date" => "July 05",
            "time" => "13:00",
            "location" => "Brasilia"
            "timezone" => -3
        )
    
    
    );
    
    
    $semi_finals = array(
    
        array(
            // W57 - W58
            "date" => "July 08",
            "time" => "17:00",
            "location" => "Belo Horizonte",
            "timezone" => -3
        ),
        
        array(
            // W59 - W60
            "date" => "July 09",
            "time" => "17:00",
            "location" => "Sao Paulo",
            "timezone" => -3
        )
    
    );
    
    $third_place_final = array(
        "date" => "July 12",
        "time" => "17:00",
        "location" => "Brasilia",
        "timezone" => -3
    );
    
    $first_place_final = array(
        "date" => "July 13",
        "time" => "16:00",
        "location" => "Rio de Janeiro",
        "timezone" => -3
    );    

?>