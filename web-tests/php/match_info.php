<?php

/* Matches array */
$matches = array(
	
	'A' => array(
		
		array(

			'date' =>'June 12',
			'time' =>'17:00',
			'location' =>'S&#227;o Paulo',
			'timezone' => -3,
			'team1' => 'Brazil',
			'flag1' => 'br',
			'score1' => '',
			'team2' => 'Croatia',
			'flag2' => 'hr',
			'score2' => ''

		),

		array(

			'date' =>'June 16',
			'time' =>'13:00',
			'location' =>'Natal',
			'timezone' => -3,
			'team1' => 'Mexico',
			'flag1' => 'mx',
			'score1' => '',
			'team2' => 'Cameroon',
			'flag2' => 'cm',
			'score2' => ''

		),

		array(

			'date' =>'June 17',
			'time' =>'16:00',
			'location' =>'Fortaleza',
			'timezone' => -3,
			'team1' => 'Brazil',
			'flag1' => 'br',
			'score1' => '',
			'team2' => 'Mexico',
			'flag2' => 'mx',
			'score2' => ''

		),

		array(

			'date' =>'June 18',
			'time' =>'18:00',
			'location' =>'Manaus',
			'timezone' => -4,
			'team1' => 'Cameroon',
			'flag1' => 'cm',
			'score1' => '',
			'team2' => 'Croatia',
			'flag2' => 'hr',
			'score2' => ''

		),

		array(

			'date' =>'June 23',
			'time' =>'17:00',
			'location' =>'Bras&#237;lia',
			'timezone' => -3,
			'team1' => 'Cameroon',
			'flag1' => 'cm',
			'score1' => '',
			'team2' => 'Brazil',
			'flag2' => 'br',
			'score2' => ''

		),

		array(

			'date' =>'June 23',
			'time' =>'17:00',
			'location' =>'Bras&#237;lia',
			'timezone' => -3,
			'team1' => 'Croatia',
			'flag1' => 'hr',
			'score1' => '',
			'team2' => 'Mexico',
			'flag2' => 'mx',
			'score2' => ''

		)
	
	),
	
	'B' => array(
		array(
		
			'date' =>'June 13',
			'time' =>'16:00',
			'location' =>'Salvador',
			'timezone' => -3,
			'team1' => 'Spain',
			'flag1' => 'es',
			'score1' => '',
			'team2' => 'Netherlands',
			'flag2' => 'nl',
			'score2' => ''
		
		),
		
		array(
		
			'date' =>'June 13',
			'time' =>'18:00',
			'location' =>'Cuiab&#225;',
			'timezone' => -4,
			'team1' => 'Chile',
			'flag1' => 'cl',
			'score1' => '',
			'team2' => 'Australia',
			'flag2' => 'au',
			'score2' => ''
		
		),
		
		array(
		
			'date' =>'June 18',
			'time' =>'16:00',
			'location' =>'Rio De Janeiro',
			'timezone' => -3,
			'team1' => 'Spain',
			'flag1' => 'es',
			'score1' => '',
			'team2' => 'Chile',
			'flag2' => 'cl',
			'score2' => ''
		
		),
		
		array(
		
			'date' =>'June 18',
			'time' =>'13:00',
			'location' =>'Porto Alegre',
			'timezone' => -3,
			'team1' => 'Australia',
			'flag1' => 'au',
			'score1' => '',
			'team2' => 'Netherlands',
			'flag2' => 'nl',
			'score2' => ''
		
		),
		
		array(
		
			'date' =>'June 23',
			'time' =>'13:00',
			'location' =>'Curitiba',
			'timezone' => -3,
			'team1' => 'Australia',
			'flag1' => 'au',
			'score1' => '',
			'team2' => 'Spain',
			'flag2' => 'es',
			'score2' => ''
		
		),
		
		array(
		
			'date' =>'June 23',
			'time' =>'13:00',
			'location' =>'S&#227;o Paulo',
			'timezone' => -3,
			'team1' => 'Netherlands',
			'flag1' => 'nl',
			'score1' => '',
			'team2' => 'Chile',
			'flag2' => 'cl',
			'score2' => ''
		
		)
	),
	
	'C' => array(
		array(
			
			'date' =>'June 14',
			'time' =>'13:00',
			'location' =>'Belo Horizonte',
			'timezone' => -3,
			'team1' => 'Colombia',
			'flag1' => 'co',
			'score1' => '',
			'team2' => 'Greece',
			'flag2' => 'gr',
			'score2' => ''
		),
		
		array(
		
			'date' =>'June 14',
			'time' =>'22:00',
			'location' =>'Recife',
			'timezone' => -4,
			'team1' => 'C&#244;te d\'Ivoire',
			'flag1' => 'ci',
			'score1' => '',
			'team2' => 'Japan',
			'flag2' => 'jp',
			'score2' => ''
			
		),
		
		array(
		
			'date' =>'June 19',
			'time' =>'13:00',
			'location' =>'Bras&#237;lia',
			'timezone' => -3,
			'team1' => 'Colombia',
			'flag1' => 'co',
			'score1' => '',
			'team2' => 'C&#244;te d\'Ivoire',
			'flag2' => 'ci',
			'score2' => ''
			
		),
		
		array(
		
			'date' =>'June 19',
			'time' =>'19:00',
			'location' =>'Natal',
			'timezone' => -3,
			'team1' => 'Japan',
			'flag1' => 'jp',
			'score1' => '',
			'team2' => 'Greece',
			'flag2' => 'gr',
			'score2' => ''
			
		),
		
		array(
			
			'date' =>'June 24',
			'time' =>'16:00',
			'location' =>'Cuiab&#225;',
			'timezone' => -4,
			'team1' => 'Japan',
			'flag1' => 'jp',
			'score1' => '',
			'team2' => 'Colombia',
			'flag2' => 'co',
			'score2' => ''
		
		),
		
		array(
		
			'date' =>'June 24',
			'time' =>'17:00',
			'location' =>'Fortaleza',
			'timezone' => -3,
			'team1' => 'Greece',
			'flag1' => 'gr',
			'score1' => '',
			'team2' => 'C&#244;te d\'Ivoire',
			'flag2' => 'ci',
			'score2' => ''
		
		)
	),
	
	'D' => array(
		array(
			
			'date' =>'June 14',
			'time' =>'16:00',
			'location' =>'Fortaleza',
			'timezone' => -3,
			'team1' => 'Uruguay',
			'flag1' => 'uy',
			'score1' => '',
			'team2' => 'Costa Rica',
			'flag2' => 'cr',
			'score2' => ''
			
		),
		
		array(
			
			'date' =>'June 14',
			'time' =>'18:00',
			'location' =>'Manaus',
			'timezone' => -4,
			'team1' => 'England',
			'flag1' => 'en',
			'score1' => '',
			'team2' => 'Italy',
			'flag2' => 'it',
			'score2' => ''
			
		),
		
		array(
			
			'date' =>'June 19',
			'time' =>'16:00',
			'location' =>'S&#227;o Paulo',
			'timezone' => -3,
			'team1' => 'Uruguay',
			'flag1' => 'uy',
			'score1' => '',
			'team2' => 'England',
			'flag2' => 'en',
			'score2' => ''
			
		),
		
		array(
			
			'date' =>'June 20',
			'time' =>'13:00',
			'location' =>'Recife',
			'timezone' => -4,
			'team1' => 'Italy',
			'flag1' => 'it',
			'score1' => '',
			'team2' => 'Costa Rica',
			'flag2' => 'cr',
			'score2' => ''
			
		),
		
		array(
			
			'date' =>'June 24',
			'time' =>'13:00',
			'location' =>'Natal',
			'timezone' => -3,
			'team1' => 'Italy',
			'flag1' => 'it',
			'score1' => '',
			'team2' => 'Uruguay',
			'flag2' => 'uy',
			'score2' => ''
			
		),
		
		array(
			
			'date' =>'June 24',
			'time' =>'13:00',
			'location' =>'Belo Horizonte',
			'timezone' => -3,
			'team1' => 'Costa Rica',
			'flag1' => 'cr',
			'score1' => '',
			'team2' => 'England',
			'flag2' => 'en',
			'score2' => ''
			
		)
	),
	
	'E' => array(
		array(
		
			'date' =>'June 15',
			'time' =>'13:00',
			'location' =>'Bras&#237;lia',
			'timezone' => -3,
			'team1' => 'Switzerland',
			'flag1' => 'ch',
			'score1' => '',
			'team2' => 'Ecuador',
			'flag2' => 'ec',
			'score2' => ''
		),
		
		array(
		
			'date' =>'June 15',
			'time' =>'16:00',
			'location' =>'Porto Alegre',
			'timezone' => -3,
			'team1' => 'France',
			'flag1' => 'fr',
			'score1' => '',
			'team2' => 'Honduras',
			'flag2' => 'hn',
			'score2' => ''
		),
		
		array(
		
			'date' =>'June 20',
			'time' =>'16:00',
			'location' =>'Salvador',
			'timezone' => -3,
			'team1' => 'Switzerland',
			'flag1' => 'ch',
			'score1' => '',
			'team2' => 'France',
			'flag2' => 'fr',
			'score2' => ''
		),
		
		array(
		
			'date' =>'June 20',
			'time' =>'19:00',
			'location' =>'Curitiba',
			'timezone' => -3,
			'team1' => 'Honduras',
			'flag1' => 'hn',
			'score1' => '',
			'team2' => 'Ecuador',
			'flag2' => 'ec',
			'score2' => ''
		),
		
		array(
		
			'date' =>'June 24',
			'time' =>'16:00',
			'location' =>'Manaus',
			'timezone' => -4,
			'team1' => 'Honduras',
			'flag1' => 'hn',
			'score1' => '',
			'team2' => 'Switzerland',
			'flag2' => 'ch',
			'score2' => ''
		),
		
		array(
		
			'date' =>'June 24',
			'time' =>'17:00',
			'location' =>'Rio De Janeiro',
			'timezone' => -3,
			'team1' => 'Ecuador',
			'flag1' => 'ec',
			'score1' => '',
			'team2' => 'France',
			'flag2' => 'fr',
			'score2' => ''
		)
	),
	
	'F' => array(
		array(
		
			'date' =>'June 15',
			'time' =>'19:00',
			'location' =>'Rio De Janeiro',
			'timezone' => -3,
			'team1' => 'Argentina',
			'flag1' => 'ar',
			'score1' => '',
			'team2' => 'Bosnia-Herzegovina',
			'flag2' => 'ba',
			'score2' => ''
		),
		
		array(
		
			'date' =>'June 16',
			'time' =>'16:00',
			'location' =>'Curitiba',
			'timezone' => -3,
			'team1' => 'Iran',
			'flag1' => 'ir',
			'score1' => '',
			'team2' => 'Nigeria',
			'flag2' => 'ng',
			'score2' => ''
		),
		
		array(
		
			'date' =>'June 21',
			'time' =>'13:00',
			'location' =>'Belo Horizonte',
			'timezone' => -3,
			'team1' => 'Argentina',
			'flag1' => 'ar',
			'score1' => '',
			'team2' => 'Iran',
			'flag2' => 'ir',
			'score2' => ''
		),
		
		array(
		
			'date' =>'June 21',
			'time' =>'18:00',
			'location' =>'Cuiab&#225;',
			'timezone' => -4,
			'team1' => 'Nigeria',
			'flag1' => 'ng',
			'score1' => '',
			'team2' => 'Bosnia-Herzegovina',
			'flag2' => 'ba',
			'score2' => ''
		),
		
		array(
		
			'date' =>'June 25',
			'time' =>'13:00',
			'location' =>'Porto Alegre',
			'timezone' => -3,
			'team1' => 'Nigeria',
			'flag1' => 'ng',
			'score1' => '',
			'team2' => 'Argentina',
			'flag2' => 'ar',
			'score2' => ''
		),
		
		array(
		
			'date' =>'June 25',
			'time' =>'13:00',
			'location' =>'Salvador',
			'timezone' => -3,
			'team1' => 'Bosnia-Herzegovina',
			'flag1' => 'ba',
			'score1' => '',
			'team2' => 'Iran',
			'flag2' => 'ir',
			'score2' => ''

		)
	),
	
	'G' => array(
		array(
		
			'date' =>'June 16',
			'time' =>'13:00',
			'location' =>'Salvador',
			'timezone' => -3,
			'team1' => 'Germany',
			'flag1' => 'de',
			'score1' => '',
			'team2' => 'Portugal',
			'flag2' => 'pt',
			'score2' => ''
		),
		
		array(
		
			'date' =>'June 16',
			'time' =>'19:00',
			'location' =>'Natal',
			'timezone' => -3,
			'team1' => 'Ghana',
			'flag1' => 'gh',
			'score1' => '',
			'team2' => 'USA',
			'flag2' => 'us',
			'score2' => ''
		),
		
		array(
		
			'date' =>'June 21',
			'time' =>'16:00',
			'location' =>'Fortaleza',
			'timezone' => -3,
			'team1' => 'Germany',
			'flag1' => 'de',
			'score1' => '',
			'team2' => 'Ghana',
			'flag2' => 'gh',
			'score2' => ''
		),
		
		array(
		
			'date' =>'June 22',
			'time' =>'18:00',
			'location' =>'Manaus',
			'timezone' => -4,
			'team1' => 'USA',
			'flag1' => 'us',
			'score1' => '',
			'team2' => 'Portugal',
			'flag2' => 'pt',
			'score2' => ''
		),
		
		array(
		
			'date' =>'June 25',
			'time' =>'13:00',
			'location' =>'Recife',
			'timezone' => -4,
			'team1' => 'USA',
			'flag1' => 'us',
			'score1' => '',
			'team2' => 'Germany',
			'flag2' => 'de',
			'score2' => ''
		),
		
		array(
		
			'date' =>'June 25',
			'time' =>'13:00',
			'location' =>'Bras&#237;lia',
			'timezone' => -3,
			'team1' => 'Portugal',
			'flag1' => 'pt',
			'score1' => '',
			'team2' => 'Ghana',
			'flag2' => 'gh',
			'score2' => ''
		)
	),
	
	'H' => array(
		array(
		
			'date' =>'June 17',
			'time' =>'13:00',
			'location' =>'Belo Horizonte',
			'timezone' => -3,
			'team1' => 'Belgium',
			'flag1' => 'be',
			'score1' => '',
			'team2' => 'Algeria',
			'flag2' => 'dz',
			'score2' => ''
		),
		
		array(
		
			'date' =>'June 17',
			'time' =>'18:00',
			'location' =>'Cuiab&#225;',
			'timezone' => -4,
			'team1' => 'Russia',
			'flag1' => 'ru',
			'score1' => '',
			'team2' => 'Korea Republic',
			'flag2' => 'kr',
			'score2' => ''
		),
		
		array(
		
			'date' =>'June 22',
			'time' =>'13:00',
			'location' =>'Rio De Janeiro',
			'timezone' => -3,
			'team1' => 'Belgium',
			'flag1' => 'be',
			'score1' => '',
			'team2' => 'Russia',
			'flag2' => 'ru',
			'score2' => ''
		),
		
		array(
		
			'date' =>'June 22',
			'time' =>'16:00',
			'location' =>'Porto Alegre',
			'timezone' => -3,
			'team1' => 'Korea Republic',
			'flag1' => 'kr',
			'score1' => '',
			'team2' => 'Algeria',
			'flag2' => 'dz',
			'score2' => ''
		),
		
		array(
		
			'date' =>'June 26',
			'time' =>'17:00',
			'location' =>'S&#227;o Paulo',
			'timezone' => -3,
			'team1' => 'Korea Republic',
			'flag1' => 'kr',
			'score1' => '',
			'team2' => 'Belgium',
			'flag2' => 'be',
			'score2' => ''
		),
		
		array(
		
			'date' =>'June 26',
			'time' =>'17:00',
			'location' =>'Curitiba',
			'timezone' => -3,
			'team1' => 'Algeria',
			'flag1' => 'dz',
			'score1' => '',
			'team2' => 'Russia',
			'flag2' => 'ru',
			'score2' => ''
		)	
	)
	
);


$letterGroup = array('A','B','C','D','E','F','G','H');

?>