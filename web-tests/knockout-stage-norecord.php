<?php

	/*
	 *  Structure of $_POST["group_info"]
	 *  ---------------------------------
	 *  A group object per array index (8 in total).
	 *	Each group has the following objects:
	 *	- "score"  Array that contains the scores of
	 *	           each match (i.e. scores[2][0] = score
	 *			   of the 1st team in the 3th match
	 *	- "chart"  First two qualified teams (0 = 1st place, 1 = 2nd place)
	 *
	 *	[
     *		{
     *  		"scores": [[1,0],[3,3],[1,3],[0,4],[3,0],[3,3]],
     *  		"chart":["mx","hr"]
     *		},
     *		{
	 *			"scores":[[0,1],[3,1],[1,4],[1,2],[2,3],[4,0]],
	 *			"chart":["nl","cl"]
     *		},
     *		.
	 *		.
	 *		.
	 *	]
	 *
	 */

	$groupInfo = json_decode($_POST["group_info"]);
	
	$teams = array(
    
		'br' => 'Brazil',
		'hr' => 'Croatia',
		'mx' => 'Mexico',
		'cm' => 'Cameroon',
		'es' => 'Spain',
		'nl' => 'Netherlands',
		'cl' => 'Chile',
		'au' => 'Australia',
		'co' => 'Colombia',
		'gr' => 'Greece',
		'ci' => 'C&#244;te d\'Ivoire',
		'jp' => 'Japan',
		'uy' => 'Uruguay',
		'cr' => 'Costa Rica',
		'en' => 'England',
		'it' => 'Italy',
		'ch' => 'Switzerland',
		'ec' => 'Ecuador',
		'fr' => 'France',
		'hn' => 'Honduras',
		'ar' => 'Argentina',
		'ba' => 'Bosnia-Herz.',
		'ir' => 'Iran',
		'ng' => 'Nigeria',
		'de' => 'Germany',
		'pt' => 'Portugal',
		'gh' => 'Ghana',
		'us' => 'USA',
		'be' => 'Belgium',
		'dz' => 'Algeria',
		'ru' => 'Russia',
		'kr' => 'Korea Rep.'
    
	);
	
	
	
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>LiveDuel</title>

		<link rel="stylesheet" type="text/css" href="style/common.css" />
		<link rel="stylesheet" type="text/css" href="style/knockout-stage.css" />
		<link rel="stylesheet" type="text/css" href="style/fonts.css" />
		<link rel="stylesheet" type="text/css" href="style/flags.css" />

		<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" />
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>

		<script src="script/knockout-stage.js"></script>
		<script src="script/jquery.autotab.min.js"></script>
		<script src="script/variables.js"></script>		
		<script src="script/functions.js"></script>
		
		<!-- <?php echo $groupInfo[0]->chart[0]; ?> -->
		
		<script type="text/javascript"><?php
		
			echo	"var knockout = [".
					"['".$groupInfo[0]->chart[0]."','".$groupInfo[1]->chart[1]."'],".
					"['".$groupInfo[2]->chart[0]."','".$groupInfo[3]->chart[1]."'],".
					"['".$groupInfo[4]->chart[0]."','".$groupInfo[5]->chart[1]."'],".
					"['".$groupInfo[6]->chart[0]."','".$groupInfo[7]->chart[1]."'],".
					"['".$groupInfo[1]->chart[0]."','".$groupInfo[0]->chart[1]."'],".
					"['".$groupInfo[3]->chart[0]."','".$groupInfo[2]->chart[1]."'],".
					"['".$groupInfo[5]->chart[0]."','".$groupInfo[4]->chart[1]."'],".
					"['".$groupInfo[7]->chart[0]."','".$groupInfo[6]->chart[1]."'],".
					"];";
		
		?></script>

	</head>
	<body>
		<div id="container">
			
			<!-- Header -->
			<div id="header">
				<div id="title">
					<img src="images/knockout-title.png" alt="Live Duel. Knockout stage simulator" />
				</div>
				<div id="settings-btn" title="Click for settings"></div>
				<div id="settings-box">
					<div id="timezone-box" class="localTime">
						<div id="localTimeRadio"></div>
						<div id="timeZoneRadio"></div>
					</div>
					<div id="help-btn"></div>
				</div>
			</div>
			<!-- Content -->
			<div id="content" class="knockout-area">
			

				<!-- knockout - stage1 - left area -->
				<div class="match-box col1 row1" id="match-11">
					<div class="team team1">
						<div id="flag11-1" class="match-flag no-flag"></div>
						<div class="team-name" id="name1">1A</div>
						<input id="score11-1" type="text" pattern="\d+" class="team-result res1" maxlength="1" tabindex="1" />
					</div>
					<div class="team team2">
						<div id="flag11-2" class="match-flag no-flag"></div>
						<div class="team-name" id="name2">2B</div>
						<input id="score11-2" type="text" pattern="\d+" class="team-result res2" maxlength="1" tabindex="2" />
					</div>
				</div>
				
				<div class="match-box col1 row2" id="match-12">
					<div class="team team1">
						<div id="flag12-1" class="match-flag no-flag"></div>
						<div class="team-name" id="name1">1C</div>
						<input id="score12-1" type="text" pattern="\d+" class="team-result res1" maxlength="1" tabindex="3" />
					</div>
					<div class="team team2">
						<div id="flag12-2" class="match-flag no-flag"></div>
						<div class="team-name" id="name2">2D</div>
						<input id="score12-2" type="text" pattern="\d+" class="team-result res2" maxlength="1" tabindex="4" />
					</div>
				</div>
				
				<div class="match-box col1 row3" id="match-13">
					<div class="team team1">
						<div id="flag13-1" class="match-flag no-flag"></div>
						<div class="team-name" id="name1">1E</div>
						<input id="score13-1" type="text" pattern="\d+" class="team-result res1" maxlength="1" tabindex="5" />
					</div>
					<div class="team team2">
						<div id="flag13-2" class="match-flag no-flag"></div>
						<div class="team-name" id="name2">2F</div>
						<input id="score13-2" type="text" pattern="\d+" class="team-result res2" maxlength="1" tabindex="6" />
					</div>
				</div>
				
				<div class="match-box col1 row4" id="match-14">
					<div class="team team1">
						<div id="flag14-1" class="match-flag no-flag"></div>
						<div class="team-name" id="name1">1G</div>
						<input id="score14-1" type="text" pattern="\d+" class="team-result res1" maxlength="1" tabindex="7" />
					</div>
					<div class="team team2">
						<div id="flag14-2" class="match-flag no-flag"></div>
						<div class="team-name" id="name2">2H</div>
						<input id="score14-2" type="text" pattern="\d+" class="team-result res2" maxlength="1" tabindex="8" />
					</div>
				</div>


				<!-- knockout - stage 1 - right area -->
				<div class="match-box col7 row1" id="match-15">
					<div class="team team1">		
						<input id="score15-1" type="text" pattern="\d+" class="team-result res1" maxlength="1" tabindex="9" />
						<div class="team-name" id="name1">1B</div>
						<div id="flag15-1" class="match-flag no-flag"></div>
					</div>
					<div class="team team2">
						<input id="score15-2" type="text" pattern="\d+" class="team-result res2" maxlength="1" tabindex="10" />
						<div class="team-name" id="name2">2A</div>
						<div id="flag15-2" class="match-flag no-flag"></div>
					</div>
				</div>
				
				<div class="match-box col7 row2" id="match-16">
					<div class="team team1">		
						<input id="score16-1" type="text" pattern="\d+" class="team-result res1" maxlength="1" tabindex="11" />
						<div class="team-name" id="name1">1D</div>
						<div id="flag16-1" class="match-flag no-flag"></div>
					</div>
					<div class="team team2">
						<input id="score16-2" type="text" pattern="\d+" class="team-result res2" maxlength="1" tabindex="12" />
						<div class="team-name" id="name2">2C</div>
						<div id="flag16-2" class="match-flag no-flag"></div>
					</div>
				</div>
				
				<div class="match-box col7 row3" id="match-17">
					<div class="team team1">						
						<input id="score17-1" type="text" pattern="\d+" class="team-result res1" maxlength="1" tabindex="13" />
						<div class="team-name" id="name1">1F</div>
						<div id="flag17-1" class="match-flag no-flag"></div>
					</div>
					<div class="team team2">
						<input id="score17-2" type="text" pattern="\d+" class="team-result res2" maxlength="1" tabindex="14" />
						<div class="team-name" id="name2">2E</div>
						<div id="flag17-2" class="match-flag no-flag"></div>
					</div>
				</div>
				
				<div class="match-box col7 row4" id="match-18">
					<div class="team team1">						
						<input id="score18-1" type="text" pattern="\d+" class="team-result res1" maxlength="1" tabindex="15" />
						<div class="team-name" id="name1">1H</div>
						<div id="flag18-1" class="match-flag no-flag"></div>
					</div>
					<div class="team team2">
						<input id="score18-2" type="text" pattern="\d+" class="team-result res2" maxlength="1" tabindex="16" />
						<div class="team-name" id="name2">2G</div>
						<div id="flag18-2" class="match-flag no-flag"></div>
					</div>
				</div>
				
				
				<!-- knockout - stage 2 - left area -->
				<div class="match-box col2 row21" id="match-21">
					<div class="team team1">
						<div id="flag21" class="match-flag no-flag"></div>
						<div class="team-name" id="name21">Winner LS1</div>
						<input id="score21-1" type="text" pattern="\d+" class="team-result res1" maxlength="1" tabindex="17" />
					</div>
					<div class="team team2">
						<div id="flag22" class="match-flag no-flag"></div>
						<div class="team-name" id="name22">Winner LS2</div>
						<input id="score21-2" type="text" pattern="\d+" class="team-result res2" maxlength="1" tabindex="18" />
					</div>
				</div>
				
				<div class="match-box col2 row22" id="match-22">
					<div class="team team1">
						<div id="flag23" class="match-flag no-flag"></div>
						<div class="team-name" id="name23">Winner LS3</div>
						<input id="score22-1" type="text" pattern="\d+" class="team-result res1" maxlength="1" tabindex="19" />
					</div>
					<div class="team team2">
						<div id="flag24" class="match-flag no-flag"></div>
						<div class="team-name" id="name24">Winner LS4</div>
						<input id="score22-2" type="text" pattern="\d+" class="team-result res2" maxlength="1" tabindex="20" />
					</div>
				</div>

				<!-- knockout - stage 2 - right area -->
				<div class="match-box col6 row21" id="match-23">
					<div class="team team1">
						<input id="score23-1" type="text" pattern="\d+" class="team-result res1" maxlength="1" tabindex="21" />
						<div class="team-name" id="name25">Winner LS5</div>
						<div id="flag25" class="match-flag no-flag"></div>
					</div>
					<div class="team team2">
						<input id="score23-2" type="text" pattern="\d+" class="team-result res2" maxlength="1" tabindex="22" />
						<div class="team-name" id="name26">Winner LS6</div>
						<div id="flag26" class="match-flag no-flag"></div>
					</div>
				</div>
				
				<div class="match-box col6 row22" id="match-24">
					<div class="team team1">
						<input id="score24-1" type="text" pattern="\d+" class="team-result res1" maxlength="1" tabindex="23" />
						<div class="team-name" id="name27">Winner LS7</div>
						<div id="flag27" class="match-flag no-flag"></div>
					</div>
					<div class="team team2">
						<input id="score24-2" type="text" pattern="\d+" class="team-result res2" maxlength="1" tabindex="24" />
						<div class="team-name" id="name28">Winner LS8</div>
						<div id="flag28" class="match-flag no-flag"></div>
					</div>
				</div>

			
				<!-- knockout - stage 3 - left area -->
				<div class="match-box col3" id="match-31">
					<div class="team team1">
						<div id="flag31" class="match-flag no-flag"></div>
						<div class="team-name" id="name31">Winner QF1</div>
						<input id="score31-1" type="text" pattern="\d+" class="team-result res1" maxlength="1" tabindex="25" />
					</div>
					<div class="team team2">
						<div id="flag32" class="match-flag no-flag"></div>
						<div class="team-name" id="name32">Winner QF2</div>
						<input id="score31-2" type="text" pattern="\d+" class="team-result res2" maxlength="1" tabindex="26" />
					</div>
				</div>

				<!-- knockout - stage 3 - right area -->
				<div class="match-box col5" id="match-32">
					<div class="team team1">
						<input id="score32-1" type="text" pattern="\d+" class="team-result res1" maxlength="1" tabindex="27" />					
						<div class="team-name" id="name33">Winner QF3</div>
						<div id="flag33" class="match-flag no-flag"></div>
					</div>
					<div class="team team2">
						<input id="score32-2" type="text" pattern="\d+" class="team-result res2" maxlength="1" tabindex="28" />
						<div class="team-name" id="name34">Winner QF4</div>
						<div id="flag34" class="match-flag no-flag"></div>
					</div>
				</div>

				<!-- knockout - finals -->
				<div class="match-box final1" id="match-41">
					<div class="team team1">
						<div id="flag-final1-1" class="match-flag no-flag"></div>
						<div id="name-final1-1" class="team-name">Winner SF1</div>
						<input id="score-final1-1" type="text" pattern="\d+" class="team-result res1" maxlength="1" tabindex="29" />					
					</div>
					<div class="team team2">
						<div id="flag-final1-2" class="match-flag no-flag"></div>
						<div id="name-final1-2" class="team-name">Winner SF2</div>
						<input id="score-final1-2" type="text" pattern="\d+" class="team-result res2" maxlength="1" tabindex="30" />
					</div>
				</div>

				<div class="match-box final2" id="match-42">
					<div class="team team1">
						<div id="flag-final2-1" class="match-flag no-flag"></div>
						<div id="name-final2-1" class="team-name">Loser SF1</div>
						<input id="score-final2-1" type="text" pattern="\d+" class="team-result res1" maxlength="1" tabindex="1" />					
					</div>
					<div class="team team2">
						<div id="flag-final2-2" class="match-flag no-flag"></div>
						<div id="name-final2-2" class="team-name">Loser SF2</div>
						<input id="score-final2-2" type="text" pattern="\d+" class="team-result res2" maxlength="1" tabindex="2" />
					</div>
				</div>

				<div id="bracket11" class="bracket top right"></div>
				<div id="bracket12" class="bracket bottom right"></div>
				<div id="bracket13" class="bracket top right"></div>
				<div id="bracket14" class="bracket bottom right"></div>
				<div id="bracket15" class="bracket top left"></div>
				<div id="bracket16" class="bracket bottom left"></div>
				<div id="bracket17" class="bracket top left"></div>
				<div id="bracket18" class="bracket bottom left"></div>
				
				<div id="bracket21" class="bracket top right"></div>
				<div id="bracket22" class="bracket bottom right"></div>
				<div id="bracket23" class="bracket top left"></div>
				<div id="bracket24" class="bracket bottom left"></div>

				<!-- match details -->
				<div id="match-details">
					<div id="match-location"></div>
					<div id="match-date"></div>
					<div id="match-time"></div>
					<div class="team-name name1"></div>
					<div class="match-flag hr-big team1"></div>
					<div class="match-flag mx-big team2"></div>
					<div class="team-name name2"></div>
				</div>

				<input type="button" id="random-scores" value="Fill with random scores" />
				<input type="button" id="clear" value="Clear"/>
				
			</div>
			<!-- div#content -->
		
		</div>
		<!-- div#container -->

		<div id="overlay"></div>
		
	</body>
</html>