<?php

define ("HOST_IP",
        getenv('HTTP_CLIENT_IP')?:
        getenv('HTTP_X_FORWARDED_FOR')?:
        getenv('HTTP_X_FORWARDED')?:
        getenv('HTTP_FORWARDED_FOR')?:
        getenv('HTTP_FORWARDED')?:
        getenv('REMOTE_ADDR')
);

define("FILE_PATH", "php/iplog.txt");

$file = fopen(FILE_PATH,"r");

$found = FALSE;

if ($file) {

    while(!feof($file) && !$found) {

        $curLine = explode("|",fgets($file));

        if ($curLine[0] == HOST_IP) {

            $found = TRUE;

        }

    }

    fclose($file);

} else {

    $found = FALSE;

}


?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>LiveDuel</title>
		<link rel="stylesheet" type="text/css" href="style/common.css" />
<?php

	if ($found == FALSE) {

?>		
		<!-- css files -->
		<link rel="stylesheet" type="text/css" href="style/group-stage.css" />
		<link rel="stylesheet" type="text/css" href="style/group-stage-widget-view.css" />
		<link rel="stylesheet" type="text/css" href="style/group-stage-help.css" />
		<link rel="stylesheet" type="text/css" href="style/fonts.css" />
		<link rel="stylesheet" type="text/css" href="style/flags.css" />
		
		<!-- js files -->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script src="script/jquery.autotab.min.js"></script>
		<script src="script/group-stage.js"></script>
		<script src="script/group-stage-help.js"></script>
		<script src="script/variables.js"></script>
		<script src="script/functions.js"></script>

	</head>
	<body>
		<div id="container">
			
			<!-- Header -->
			<div id="header">
				<div id="title"></div>
				<div id="timezone-box" class="localTime">
					<div id="localTimeRadio"></div>
					<div id="timeZoneRadio"></div>
				</div>
			</div>
			
			<!-- Group Tabs -->
			<div id="group-buttons">
			
				<div class="group-btn hover" id="group-1">
					<div class="group-text">GROUP A</div>
					<div class="flag br"></div>
					<div class="flag right-f hr"></div>
					<div class="flag mx"></div>
					<div class="flag right-f cr"></div>
				</div>
				<div class="group-connector"></div>
				<div class="vert-separator"></div>
				
				<div class="group-btn hover" id="group-2">
					<div class="group-text">GROUP B</div>
					<div class="flag es"></div>
					<div class="flag right-f nl"></div>
					<div class="flag cl"></div>
					<div class="flag right-f au"></div>
				</div>
				<div class="group-connector"></div>
				<div class="vert-separator"></div>
				
				<div class="group-btn hover" id="group-3">
					<div class="group-text">GROUP C</div>
					<div class="flag co"></div>
					<div class="flag right-f gr"></div>
					<div class="flag ci"></div>
					<div class="flag right-f jp"></div>
				</div>
				<div class="group-connector"></div>
				<div class="vert-separator"></div>
				
				<div class="group-btn hover" id="group-4">
					<div class="group-text">GROUP D</div>
					<div class="flag uy"></div>
					<div class="flag right-f cr"></div>
					<div class="flag en"></div>
					<div class="flag right-f it"></div>
				</div>
				<div class="group-connector"></div>
				<div class="vert-separator"></div>
				
				<div class="group-btn hover" id="group-5">
					<div class="group-text">GROUP E</div>
					<div class="flag ch"></div>
					<div class="flag right-f ec"></div>
					<div class="flag fr"></div>
					<div class="flag right-f hn"></div>
				</div>
				<div class="group-connector"></div>
				<div class="vert-separator"></div>
				
				<div class="group-btn hover" id="group-6">
					<div class="group-text">GROUP F</div>
					<div class="flag ar"></div>
					<div class="flag right-f ba"></div>
					<div class="flag ir"></div>
					<div class="flag right-f ng"></div>
				</div>
				<div class="group-connector"></div>
				<div class="vert-separator"></div>
				
				<div class="group-btn hover" id="group-7">
					<div class="group-text">GROUP G</div>
					<div class="flag de"></div>
					<div class="flag right-f pt"></div>
					<div class="flag gh"></div>
					<div class="flag right-f us"></div>
				</div>
				<div class="group-connector"></div>
				<div class="vert-separator"></div>
				
				<div class="group-btn hover" id="group-8">
					<div class="group-text">GROUP H</div>
					<div class="flag be"></div>
					<div class="flag right-f dz"></div>
					<div class="flag ru"></div>
					<div class="flag right-f kr"></div>
				</div>
				<div class="group-connector"></div>

			</div>
			<!-- div#group-buttons -->
			
			<!-- Content -->
			<div id="content">

				<!-- progress bar, display selection, help button -->
				<div id="content-top">
					
					<div id="progress-bar">
						<div id="prog-perc">0%</div>
						<div id="prog-boxes">
							<span id="prog1"></span>
							<span id="prog2"></span>
							<span id="prog3"></span>
							<span id="prog4"></span>
							<span id="prog5"></span>
							<span id="prog6"></span>
							<span id="prog7"></span>
							<span id="prog8"></span>
							<span id="prog9"></span>
							<span id="prog10"></span>
							<span id="prog11"></span>
							<span id="prog12"></span>
						</div>
					</div>
					<div id="settings">
						<div id="view-text">Display:</div>
						<div id="view-select" class="widgetSelect tooltip"></div>
						<div id="view-options">
							<div id="widget-view-option"></div>
							<div id="list-view-option"></div>
						</div>
						<div id="help-btn"></div>
					</div>
				</div>
				<!-- div#content-top -->
			
				<div class="match-box" id="match-1">
					<div class="match-location" id="location1">June 12 - 20:00 - Sao Paulo</div>
					<div class="team-name name1">Brazil</div>
					<div class="match-flag br-big team1"></div>
					<input id="score11" type="text" pattern="\d+" class="team-result res1" maxlength="1" tabindex="1" />
					<div class="dash hidden">-</div>
					<input id="score12" type="text" pattern="\d+" class="team-result res2" maxlength="1" tabindex="2" />
					<div class="match-flag hr-big team2"></div>
					<div class="team-name name2">Croatia</div>
				</div>

				<div class="match-box" id="match-2">
					<div class="match-location" id="location2">June 16 - 16:00 - Natal</div>
					<div class="team-name name1">Mexico</div>
					<div class="match-flag mx-big team1"></div>
					<input id="score21" type="text" pattern="\d+" class="team-result res1" maxlength="1" tabindex="3" />
					<div class="dash hidden">-</div>
					<input id="score22" type="text" pattern="\d+" class="team-result res2" maxlength="1" tabindex="4" />
					<div class="match-flag cr-big team2"></div>
					<div class="team-name name2">Cameroon</div>
				</div>
				
				<div class="match-box" id="match-3">
					<div class="match-location" id="location3">June 17 - 19:00 - Fortaleza</div>
					<div class="team-name name1">Brazil</div>
					<div class="match-flag br-big team1"></div>
					<input id="score31" type="text" pattern="\d+" class="team-result res1" maxlength="1" tabindex="5" />
					<div class="dash hidden">-</div>
					<input id="score32" type="text" pattern="\d+" class="team-result res2" maxlength="1" tabindex="6" />
					<div class="match-flag mx-big team2"></div>
					<div class="team-name name2">Mexico</div>
				</div>

				<div class="match-box" id="match-4">
					<div class="match-location" id="location4">June 18 - 22:00 - Manaus</div>
					<div class="team-name name1">Cameroon</div>
					<div class="match-flag cm-big team1"></div>
					<input id="score41" type="text" pattern="\d+" class="team-result res1" maxlength="1" tabindex="7" />
					<div class="dash hidden">-</div>
					<input id="score42" type="text" pattern="\d+" class="team-result res2" maxlength="1" tabindex="8" />
					<div class="match-flag hr-big team2"></div>
					<div class="team-name name2">Croatia</div>
				</div>

				<div class="match-box" id="match-5">
					<div class="match-location" id="location5">June 23 - 20:00 - Brasilia</div>
					<div class="team-name name1">Cameroon</div>
					<div class="match-flag cm-big team1"></div>
					<input id="score51" type="text" pattern="\d+" class="team-result res1" maxlength="1" tabindex="9" />
					<div class="dash hidden">-</div>
					<input id="score52" type="text" pattern="\d+" class="team-result res2" maxlength="1" tabindex="10" />
					<div class="match-flag br-big team2"></div>
					<div class="team-name name2">Brasil</div>
				</div>

				<div class="match-box" id="match-6">
					<div class="match-location" id="location6">June 23 - 20:00 - Recife</div>
					<div class="team-name name1">Croatia</div>
					<div class="match-flag hr-big team1"></div>
					<input id="score61" type="text" pattern="\d+" class="team-result res1" maxlength="1" tabindex="11" />
					<div class="dash hidden">-</div>
					<input id="score62" type="text" pattern="\d+" class="team-result res2" maxlength="1" tabindex="12"/>
					<div class="match-flag mx-big team2"></div>
					<div class="team-name name2">Mexico</div>
				</div>

				<!--	value syntax:
						"0-1,2-1,3-2,0-0,1-1,3-2|br,hr"
						"|----------------------|----|"
						Scores of the 6 matches | 1st,2nd
				-->
				<form id="resultsForm" action="knockout-stage-norecord.php" method="post" accept-charset="utf-8">
					<input type="hidden" id="group-info" name="group_info" value="" />
				</form>

				<input type="button" id="random-scores" value="Fill with random scores" />
				<input type="button" id="clear" value="Clear"/>
				<input type="button" id="next-group" value="Done! Next group!"/>
				<input type="button" id="knockout-btn-big" value="Simulate knockout stage"/>
				
			</div>
			<!-- div#content -->
			
			
			<!-- right column -->
			<div id="right">
				<div id="predicted-matches">
					<h3>Predicted matches</h3>
					<hr />
					<div class="col1 left-bracket">
						<div class="flag-small" id="1A"></div>
						<div class="flag-small" id="2B"></div>
						<div class="flag-small" id="1C"></div>
						<div class="flag-small" id="2D"></div>
						<div class="flag-small" id="1E"></div>
						<div class="flag-small" id="2F"></div>
						<div class="flag-small" id="1G"></div>
						<div class="flag-small" id="2H"></div>
					</div>
					<div class="col2 left-bracket">
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
					</div>
					<div class="col3 left-bracket">
						<div></div>
						<div></div>
						<div></div>
						<div></div>
					</div>
					<div class="col4 left-bracket">
						<div></div>
						<div></div>
					</div>
					<div class="col5">
						<div></div>
					</div>
					<div class="col4 right-bracket">
						<div></div>
						<div></div>
					</div>
					<div class="col3 right-bracket">
						<div></div>
						<div></div>
						<div></div>
						<div></div>
					</div>
					<div class="col2 right-bracket">
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
					</div>
					<div class="col1 right-bracket last-col">
						<div class="flag-small" id="1B"></div>
						<div class="flag-small" id="2A"></div>
						<div class="flag-small" id="1D"></div>
						<div class="flag-small" id="2C"></div>
						<div class="flag-small" id="1F"></div>
						<div class="flag-small" id="2E"></div>
						<div class="flag-small" id="1H"></div>
						<div class="flag-small" id="2G"></div>
					</div>
					<input type="button" id="go-knockout" value="Simulate knockout stage"/>
				</div>
				<div id="predicted-points">
					<h3>Predicted points</h3>
					<hr />
					<table>
						<thead>
							<tr>
								<th></th>
								<th>MP</th>
								<th>W</th>
								<th>D</th>
								<th>L</th>
								<th>GF</th>
								<th>GA</th>
								<th>GD</th>
								<th>Pts</th>
							</tr>
						</thead>
						<tbody>
							<tr id="team1">
								<td class="pts-flag">
									<img src="" alt=""/>
								</td>
								<td class="mp"></td>
								<td class="won"></td>
								<td class="draw"></td>
								<td class="lost"></td>
								<td class="gf"></td>
								<td class="ga"></td>
								<td class="gd"></td>
								<td class="pts"></td>
							</tr>
							<tr id="team2">
								<td class="pts-flag">
									<img src="" alt="" />
								</td>
								<td class="mp"></td>
								<td class="won"></td>
								<td class="draw"></td>
								<td class="lost"></td>
								<td class="gf"></td>
								<td class="ga"></td>
								<td class="gd"></td>
								<td class="pts"></td>
							</tr>
							<tr id="team3">
								<td class="pts-flag">
									<img src="" alt="" />
								</td>
								<td class="mp"></td>
								<td class="won"></td>
								<td class="draw"></td>
								<td class="lost"></td>
								<td class="gf"></td>
								<td class="ga"></td>
								<td class="gd"></td>
								<td class="pts"></td>
							</tr>
							<tr id="team4">
								<td class="pts-flag">
									<img src="" alt=""/>
								</td>
								<td class="mp"></td>
								<td class="won"></td>
								<td class="draw"></td>
								<td class="lost"></td>
								<td class="gf"></td>
								<td class="ga"></td>
								<td class="gd"></td>
								<td class="pts"></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<!-- div#right -->
		
			<div id="help-dialog">
				<div id="help-header"></div>
				<div id="help-text"></div>
				<input id="help-prev" type="button" value="< Prev" />
				<input id="help-next" type="button" value="Next >" />
			</div>		
		
		</div>
		<!-- div#container -->

		<div id="overlay"></div>
		
		<script type="text/javascript">

			var _mfq = _mfq || [];
			
			(function () {
				var mf = document.createElement("script");
				mf.type = "text/javascript";
				mf.async = true;
				mf.src = "//cdn.mouseflow.com/projects/547aa86d-175a-4140-bc9c-0d14c471913d.js";
				document.getElementsByTagName("head")[0].appendChild(mf);
			})();

			_mfq.push(["setVariable", "HOST_IP", "<?php echo HOST_IP; ?>"]);
			_mfq.push(["setVariable", "DATE_TIME", "<?php echo $date; ?>"]);

		</script>
		
<?php	} else {	?>

	</head>
	
	<body>
	
		<div id="container">
			<p style="text-align: center; font-size: 11pt">You have already taken this test.<br/>Thank you for your time.</p>
		</div>
		
<?php	}	?>
		
	</body>
	
</html>