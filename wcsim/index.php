<?php
require_once 'php/db/DBHandler.php';
require_once 'php/constant.php';
require_once 'php/lib/Request.php';
Session::start();
$user = null;
if (Session::isUserLogined()) {
	$user = Session::getLoginedUser();
}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>LiveDuel</title>
		
		<script type="text/javascript">
	
		var matches = <?php echo json_encode(DBHandler::getGroupStageResult(true), true) ?>;
	
		<?php
		// BECAUSE THE REAL GROUP STAGE DONE (FASTEST SOLUTION :v)
// 		if (Session::isUserLogined() && !Session::isAdminLogined() && $user[User::F_IS_GROUP_STAGE_DONE]) {
	
// 			// Query row of 16
// 			$sql = 'SELECT
// 					mb.country1_code,
// 					mb.country2_code,
// 					mb.country1_name,
// 					mb.country2_name
// 				FROM
// 					matches_bets mb
// 				INNER JOIN matches m ON mb.match_id = m.id
// 				WHERE
// 					m.round_of_16 = 1
// 				AND mb.user_id = ' . $user[User::F_ID] . ';';
// 		} else {

			// THE REAL ROUND OF 16
			$sql = 'SELECT
					m.country1_code,
					m.country2_code,
					m.country1_name,
					m.country2_name
				FROM
					matches m
				WHERE
					m.round_of_16 = 1';
// 		}
		
		if ($sql) {
			$result = DBHandler::connect()->query($sql);
			$roundOf16 = array();
			foreach ($result->fetchAll(PDO::FETCH_ASSOC) as $match) {
				$roundOf16[] = array($match['country1_code'], $match['country2_code']);
			}
		
		?>
		
			var isUserDoneGroupStage = true;
			var roundOf16 = <?php echo json_encode($roundOf16, true)?>;
	<?php }?>
		</script>
		
		
		<link rel="stylesheet" type="text/css" href="style/core.css" />
		<link rel="stylesheet" type="text/css" href="style/common.css" />
		<!-- css files -->
		<link rel="stylesheet" type="text/css" href="style/index.css" />
		<link rel="stylesheet" type="text/css" href="style/group-stage-widget-view.css" />
		<link rel="stylesheet" type="text/css" href="style/group-stage-help.css" />
		<link rel="stylesheet" type="text/css" href="style/fonts.css" />
		<link rel="stylesheet" type="text/css" href="style/flags.css" />
		<link rel="stylesheet" type="text/css" href="style/jquery.fancybox.css" />
		<link rel="stylesheet" type="text/css" href="style/tooltipster.css" />
		
		<!-- js files -->
		
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script src="script/jquery.autotab.min.js"></script>
		<script src="script/core.js"></script>
		<script src="script/jquery.cookie.js"></script>
		<script src="script/jquery.tooltipster.min.js"></script>
		<script src="script/jquery.fancybox.pack.js"></script>
		<script src="script/group-stage-help.js"></script>
		<script src="script/index.js" charset="utf-8"></script>

	</head>
	<body>
	
		<?php require 'user-bar.php'; ?>
	
		<div id="container" class="container">
			
			<!-- Header -->
			<div id="header" class="header">
				<div id="group_title">
					&nbsp;
				</div>
			<?php if (Session::isUserLogined()) : ?>
				<div id="local_time_box" class="liveduel-radio-box timezone-radio-box">
					<input id="local_time_radio" type="radio" class="liveduel-radio" checked="checked" name="timezone_check">
					<label class="liveduel-radio-label" for="local_time_radio">Local time</label>
				</div>
				<div id="your_time_box" class="liveduel-radio-box timezone-radio-box">
					<input id="your_time_radio" type="radio" class="liveduel-radio" name="timezone_check">
					<label class="liveduel-radio-label" for="your_time_radio">Your timezone</label>
				</div>
			<?php endif;?>
			</div>
			
			<!-- Group Tabs -->
			<div id="group-buttons">
			
				<div class="group-btn bg-opacity hover first" id="group-1">
					<div class="group-text">GROUP A</div>
					<div class="flag br"></div>
					<div class="flag right-f hr"></div>
					<div class="flag mx"></div>
					<div class="flag right-f cm"></div>
				</div>
				<div class="group-connector first"></div>
				
				
				<div class="group-btn bg-opacity hover" id="group-2">
					<div class="group-text">GROUP B</div>
					<div class="flag es"></div>
					<div class="flag right-f nl"></div>
					<div class="flag cl"></div>
					<div class="flag right-f au"></div>
				</div>
				<div class="group-connector"></div>
				
				
				<div class="group-btn bg-opacity hover" id="group-3">
					<div class="group-text">GROUP C</div>
					<div class="flag co"></div>
					<div class="flag right-f gr"></div>
					<div class="flag ci"></div>
					<div class="flag right-f jp"></div>
				</div>
				<div class="group-connector"></div>
				
				
				<div class="group-btn bg-opacity hover" id="group-4">
					<div class="group-text">GROUP D</div>
					<div class="flag uy"></div>
					<div class="flag right-f cr"></div>
					<div class="flag en"></div>
					<div class="flag right-f it"></div>
				</div>
				<div class="group-connector"></div>
				
				
				<div class="group-btn bg-opacity hover" id="group-5">
					<div class="group-text">GROUP E</div>
					<div class="flag ch"></div>
					<div class="flag right-f ec"></div>
					<div class="flag fr"></div>
					<div class="flag right-f hn"></div>
				</div>
				<div class="group-connector"></div>
				
				
				<div class="group-btn bg-opacity hover" id="group-6">
					<div class="group-text">GROUP F</div>
					<div class="flag ar"></div>
					<div class="flag right-f ba"></div>
					<div class="flag ir"></div>
					<div class="flag right-f ng"></div>
				</div>
				<div class="group-connector"></div>
				
				
				<div class="group-btn bg-opacity hover" id="group-7">
					<div class="group-text">GROUP G</div>
					<div class="flag de"></div>
					<div class="flag right-f pt"></div>
					<div class="flag gh"></div>
					<div class="flag right-f us"></div>
				</div>
				<div class="group-connector"></div>
				
				
				<div class="group-btn bg-opacity hover" id="group-8">
					<div class="group-text">GROUP H</div>
					<div class="flag be"></div>
					<div class="flag right-f dz"></div>
					<div class="flag ru"></div>
					<div class="flag right-f kr"></div>
				</div>
				<div class="group-connector"></div>

			</div>
			<!-- div#group-buttons -->
			
			<!-- Content -->
			<div id="content" class="bg-opacity">

				<!-- progress bar, display selection, help button -->
				<div id="content-top">
					
					<div id="progress-bar">
						<div id="prog-perc">0%</div>
						<div id="prog-boxes">
							<span id="prog1"></span>
							<span id="prog2"></span>
							<span id="prog3"></span>
							<span id="prog4"></span>
							<span id="prog5"></span>
							<span id="prog6"></span>
							<span id="prog7"></span>
							<span id="prog8"></span>
							<span id="prog9"></span>
							<span id="prog10"></span>
							<span id="prog11"></span>
							<span id="prog12"></span>
						</div>
					</div>
					<div id="settings">
						<span id="widget-view-option" class="button active">&nbsp;</span>
						<span id="list-view-option" class="button">&nbsp;</span>
						<span id="help-btn" class="button">&nbsp;</span>
					</div>
				</div>
				<!-- div#content-top -->
			
				<div class="match-box bg-opacity1" id="match-1">
					<div class="sep">&nbsp;</div>
					<div class="match-location" id="location1">June 12 - 20:00 - Sao Paulo</div>
					<div class="team-name name1">Brazil</div>
					<div class="match-flag br-big team1"></div>
					<input id="score11" type="text" pattern="\d+" class="team-result res1" maxlength="1" tabindex="1" />
					<div class="dash hidden">-</div>
					<input id="score12" type="text" pattern="\d+" class="team-result res2" maxlength="1" tabindex="2" />
					<div class="match-flag hr-big team2"></div>
					<div class="team-name name2">Croatia</div>
				</div>

				<div class="match-box bg-opacity1" id="match-2">
					<div class="sep">&nbsp;</div>
					<div class="match-location" id="location2">June 16 - 16:00 - Natal</div>
					<div class="team-name name1">Mexico</div>
					<div class="match-flag mx-big team1"></div>
					<input id="score21" type="text" pattern="\d+" class="team-result res1" maxlength="1" tabindex="3" />
					<div class="dash hidden">-</div>
					<input id="score22" type="text" pattern="\d+" class="team-result res2" maxlength="1" tabindex="4" />
					<div class="match-flag cr-big team2"></div>
					<div class="team-name name2">Cameroon</div>
				</div>
				
				<div class="match-box bg-opacity1" id="match-3">
					<div class="sep">&nbsp;</div>
					<div class="match-location" id="location3">June 17 - 19:00 - Fortaleza</div>
					<div class="team-name name1">Brazil</div>
					<div class="match-flag br-big team1"></div>
					<input id="score31" type="text" pattern="\d+" class="team-result res1" maxlength="1" tabindex="5" />
					<div class="dash hidden">-</div>
					<input id="score32" type="text" pattern="\d+" class="team-result res2" maxlength="1" tabindex="6" />
					<div class="match-flag mx-big team2"></div>
					<div class="team-name name2">Mexico</div>
				</div>

				<div class="match-box bg-opacity1" id="match-4">
					<div class="sep">&nbsp;</div>
					<div class="match-location" id="location4">June 18 - 22:00 - Manaus</div>
					<div class="team-name name1">Cameroon</div>
					<div class="match-flag cm-big team1"></div>
					<input id="score41" type="text" pattern="\d+" class="team-result res1" maxlength="1" tabindex="7" />
					<div class="dash hidden">-</div>
					<input id="score42" type="text" pattern="\d+" class="team-result res2" maxlength="1" tabindex="8" />
					<div class="match-flag hr-big team2"></div>
					<div class="team-name name2">Croatia</div>
				</div>

				<div class="match-box bg-opacity1" id="match-5">
					<div class="sep">&nbsp;</div>
					<div class="match-location" id="location5">June 23 - 20:00 - Brasilia</div>
					<div class="team-name name1">Cameroon</div>
					<div class="match-flag cm-big team1"></div>
					<input id="score51" type="text" pattern="\d+" class="team-result res1" maxlength="1" tabindex="9" />
					<div class="dash hidden">-</div>
					<input id="score52" type="text" pattern="\d+" class="team-result res2" maxlength="1" tabindex="10" />
					<div class="match-flag br-big team2"></div>
					<div class="team-name name2">Brasil</div>
				</div>

				<div class="match-box bg-opacity1" id="match-6">
					<div class="sep">&nbsp;</div>
					<div class="match-location" id="location6">June 23 - 20:00 - Recife</div>
					<div class="team-name name1">Croatia</div>
					<div class="match-flag hr-big team1"></div>
					<input id="score61" type="text" pattern="\d+" class="team-result res1" maxlength="1" tabindex="11" />
					<div class="dash hidden">-</div>
					<input id="score62" type="text" pattern="\d+" class="team-result res2" maxlength="1" tabindex="12"/>
					<div class="match-flag mx-big team2"></div>
					<div class="team-name name2">Mexico</div>
				</div>

				<!--	value syntax:
						"0-1,2-1,3-2,0-0,1-1,3-2|br,hr"
						"|----------------------|----|"
						Scores of the 6 matches | 1st,2nd
				-->
				<form id="resultsForm" action="knockout-stage.php" method="post" accept-charset="utf-8">
					<input type="hidden" id="group_result" name="groupResult" value="" />
					<input type="hidden" id="round_of_16" name="roundOf16" value="" />
				</form>
			<div class="hidden-sep">&nbsp;</div>
			<div class="button-bar group-button-bar">
				<?php if (Session::isAdminLogined()) :?>
				
					<button id="save_real_group_stage" type="button" class="liveduel-button">Save the real</button>
				
				<?php else :?>
<!-- 					<button id="random-scores" type="button" class="liveduel-button">Fill random scores</button> -->
<!-- 					<button id="clear" type="button" class="liveduel-button">Clear</button> -->
					<button id="next-group" type="button" class="liveduel-button">Next group</button>
					<button id="knockout-btn-big" type="button" class="liveduel-button">Go knockout stage</button>
					
					<?php if (Session::isUserLogined()) :?>
						<br>
<!-- 						<button id="save_your_group_stage" type="button" class="liveduel-button septop15">Save results</button> -->
					<?php endif;?>
				<?php endif;?>
				
			</div>
			</div>
			<!-- div#content -->
			
			
			<!-- right column -->
			<div id="right">
				<div id="predicted-matches" class="bg-opacity">
					<h3>Predicted matches</h3>
					<hr />
					<div class="col1 left-bracket">
						<div round-of-sixteen="match01" class=" flag-small" id="1A"></div>
						<div round-of-sixteen="match02" class=" flag-small" id="2B"></div>
						<div round-of-sixteen="match11" class=" flag-small" id="1C"></div>
						<div round-of-sixteen="match12" class=" flag-small" id="2D"></div>
						<div round-of-sixteen="match21" class=" flag-small" id="1E"></div>
						<div round-of-sixteen="match22" class=" flag-small" id="2F"></div>
						<div round-of-sixteen="match31" class=" flag-small" id="1G"></div>
						<div round-of-sixteen="match32" class=" flag-small" id="2H"></div>
					</div>
					<div class="col2 left-bracket">
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
					</div>
					<div class="col3 left-bracket">
						<div></div>
						<div></div>
						<div></div>
						<div></div>
					</div>
					<div class="col4 left-bracket">
						<div></div>
						<div></div>
					</div>
					<div class="col5">
						<div></div>
					</div>
					<div class="col4 right-bracket">
						<div></div>
						<div></div>
					</div>
					<div class="col3 right-bracket">
						<div></div>
						<div></div>
						<div></div>
						<div></div>
					</div>
					<div class="col2 right-bracket">
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
					</div>
					<div class="col1 right-bracket last-col">
						<div round-of-sixteen="match41" class=" flag-small" id="1B"></div>
						<div round-of-sixteen="match42" class=" flag-small" id="2A"></div>
						<div round-of-sixteen="match51" class=" flag-small" id="1D"></div>
						<div round-of-sixteen="match52" class=" flag-small" id="2C"></div>
						<div round-of-sixteen="match61" class=" flag-small" id="1F"></div>
						<div round-of-sixteen="match62" class=" flag-small" id="2E"></div>
						<div round-of-sixteen="match71" class=" flag-small" id="1H"></div>
						<div round-of-sixteen="match72" class=" flag-small" id="2G"></div>
					</div>
					<button id="go-knockout" type="button" class="liveduel-button">Go knockout stage</button>
				</div>
				<div id="predicted-points" class="bg-opacity">
					<h3>Predicted points</h3>
					<hr />
					<table>
						<thead>
							<tr>
								<th></th>
								<th>MP</th>
								<th>W</th>
								<th>D</th>
								<th>L</th>
								<th>GF</th>
								<th>GA</th>
								<th>GD</th>
								<th>Pts</th>
							</tr>
						</thead>
						<tbody>
							<tr id="team1">
								<td class="pts-flag">
									<img src="" alt=""/>
								</td>
								<td class="mp"></td>
								<td class="won"></td>
								<td class="draw"></td>
								<td class="lost"></td>
								<td class="gf"></td>
								<td class="ga"></td>
								<td class="gd"></td>
								<td class="pts"></td>
							</tr>
							<tr id="team2">
								<td class="pts-flag">
									<img src="" alt="" />
								</td>
								<td class="mp"></td>
								<td class="won"></td>
								<td class="draw"></td>
								<td class="lost"></td>
								<td class="gf"></td>
								<td class="ga"></td>
								<td class="gd"></td>
								<td class="pts"></td>
							</tr>
							<tr id="team3">
								<td class="pts-flag">
									<img src="" alt="" />
								</td>
								<td class="mp"></td>
								<td class="won"></td>
								<td class="draw"></td>
								<td class="lost"></td>
								<td class="gf"></td>
								<td class="ga"></td>
								<td class="gd"></td>
								<td class="pts"></td>
							</tr>
							<tr id="team4">
								<td class="pts-flag">
									<img src="" alt=""/>
								</td>
								<td class="mp"></td>
								<td class="won"></td>
								<td class="draw"></td>
								<td class="lost"></td>
								<td class="gf"></td>
								<td class="ga"></td>
								<td class="gd"></td>
								<td class="pts"></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<!-- div#right -->
		
			<div id="help-dialog">
				<div id="help-header"></div>
				<div id="help-text"></div>
				<input id="help-prev" type="button" value="< Prev" />
				<input id="help-next" type="button" value="Next >" />
			</div>
		
		</div>
		<!-- div#container -->

		<div id="overlay"></div>
		
	</body>
</html>