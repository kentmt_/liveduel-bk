<?php
require_once 'lib/Request.php';
require_once 'lib/AjaxResponse.php';
require_once 'lib/Session.php';
require_once 'db/DBHandler.php';

if (Session::isAdminLogined()) {
	
	$knockoutResult = json_decode(Request::getString('knockoutResult'), true);
	
	$db = DBHandler::connect();
	$sql = ' INSERT INTO
		matches (
			id,
			country1_code,
			country2_code,
			country1_name,
			country2_name,
			fulltime_result1,
			fulltime_result2,
			extratime_result1,
			extratime_result2,
			penalty_result1,
			penalty_result2,
			is_played,
			winner_country_code
		) VALUES ';
	
	$i = 0;
	foreach ($knockoutResult as $match) {
			
		if (DBHandler::isKnockoutMatchAvailable($match)) {
			$sql .= ' (
				:id' . $i . ',
				:country1_code' . $i . ',
				:country2_code' . $i . ',
				:country1_name' . $i . ',
				:country2_name' . $i . ',
				:fulltime_result1' . $i . ',
				:fulltime_result2' . $i . ',
				:extratime_result1' . $i . ',
				:extratime_result2' . $i . ',
				:penalty_result1' . $i . ',
				:penalty_result2' . $i . ',
				:is_played' . $i . ',
				:winner_country_code' . $i . '
			), '
			;
									
			++ $i;
		}
	}
	
	$sql = trim($sql);
	$sql = substr($sql, 0, strlen($sql) - 1);
	$sql .= ' ON DUPLICATE KEY UPDATE
		country1_code = VALUES(country1_code)
	,	country2_code = VALUES(country2_code)
	,	country1_name = VALUES(country1_name)
	,	country2_name = VALUES(country2_name)
	,	fulltime_result1 = VALUES(fulltime_result1)
	,	fulltime_result2 = VALUES(fulltime_result2)
	,	extratime_result1 = VALUES(extratime_result1)
	,	extratime_result2 = VALUES(extratime_result2)
	,	penalty_result1 = VALUES(penalty_result1)
	,	penalty_result2 = VALUES(penalty_result2)
	,	is_played = VALUES(is_played)
	,	winner_country_code = VALUES(winner_country_code);';
	
	$query = $db->prepare($sql);
	
	$i = 0;
	foreach ($knockoutResult as $match) {
		
		if (DBHandler::isKnockoutMatchAvailable($match)) {
			$isPlayed = !empty($match['winner_country_code']);
			$query->bindValue(':id' . $i, $match['match_id'], PDO::PARAM_INT);
			$query->bindValue(':country1_code' . $i, $match['country1_code'], PDO::PARAM_STR);
			$query->bindValue(':country2_code' . $i, $match['country2_code'], PDO::PARAM_STR);
			$query->bindValue(':country1_name' . $i, $match['country1_name'], PDO::PARAM_STR);
			$query->bindValue(':country2_name' . $i, $match['country2_name'], PDO::PARAM_STR);
			$query->bindValue(':fulltime_result1' . $i, $match['fulltime_result1'], PDO::PARAM_INT);
			$query->bindValue(':fulltime_result2' . $i, $match['fulltime_result2'], PDO::PARAM_INT);
			$query->bindValue(':extratime_result1' . $i, $match['extratime_result1'], PDO::PARAM_INT);
			$query->bindValue(':extratime_result2' . $i, $match['extratime_result2'], PDO::PARAM_INT);
			$query->bindValue(':penalty_result1' . $i, $match['penalty_result1'], PDO::PARAM_INT);
			$query->bindValue(':penalty_result2' . $i, $match['penalty_result2'], PDO::PARAM_INT);
			$query->bindValue(':is_played' . $i, $isPlayed, PDO::PARAM_INT);
			$query->bindValue(':winner_country_code' . $i, $match['winner_country_code'], PDO::PARAM_STR);
			
			++ $i;
		}
		
	}
	
	// Update real knockout stage
	$query->execute();
	
	AjaxResponse::getInstance()->sendSuccessMessage("The real is updated");
}