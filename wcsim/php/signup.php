<?php

require_once 'constant.php';
require_once 'lib/Request.php';
require_once 'lib/Session.php';
require_once 'lib/AjaxResponse.php';
require_once 'db/DBHandler.php';

$response = AjaxResponse::getInstance();

// Validate
$username = Request::getString(User::F_USERNAME);
if (!preg_match('/^[A-Za-z0-9_.]+$/', $username)) {
	$response->sendValidateMessage(User::F_USERNAME, 'Only alphanumerics, underscores and dots are allowed');
}

$email = Request::getString(User::F_EMAIL);
if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	$response->sendValidateMessage(User::F_EMAIL, 'Please enter a real email');
}

$password = Request::getString(User::F_PASSWORD);
if (empty($password)) {
	$response->sendValidateMessage(User::F_PASSWORD, 'Please type a password');
}

$cpassword = Request::getString(User::F_CONFIRM_PASSWORD);
if (empty($cpassword) || $password !== $cpassword) {
	$response->sendValidateMessage(User::F_CONFIRM_PASSWORD, 'Please confirm exactly your password');
}

$countryCode = Request::getString(Country::F_CODE);
if (empty($countryCode) || strlen($countryCode) !== 2) {
	$response->sendValidateMessage(Country::F_CODE, 'Please choose your country');
}

$timezoneId = Request::getInteger(Timezone::F_ID);
if ($timezoneId === 0) {
	$response->sendValidateMessage(Timezone::F_ID, 'Please choose your timezone');
}

$db = DBHandler::connect();
$sql = 'SELECT COUNT(id) count_ FROM user WHERE username = :username';
$query = $db->prepare($sql);
$query->bindValue(':username', $username, PDO::PARAM_STR);
$query->execute();
if (((int) $query->fetchColumn()) > 0) {
	$response->sendValidateMessage(User::F_USERNAME, 'This username has been registered, please enter another one');
}

$sql = 'SELECT COUNT(id) count_ FROM user WHERE email = :email';
$query = $db->prepare($sql);
$query->bindValue(':email', $email, PDO::PARAM_STR);
$query->execute();
if (((int) $query->fetchColumn()) > 0) {
	$response->sendValidateMessage(User::F_EMAIL, 'This email has been registered, please enter another one');
}

// Get utc offset
$sql = 'SELECT floor(tz.gmt_offset/3600) utc_offset
	FROM `timezone` tz JOIN `zone` z
	ON tz.zone_id=z.zone_id
	WHERE tz.time_start < UNIX_TIMESTAMP(UTC_TIMESTAMP()) AND z.zone_id=:zone_id
	ORDER BY tz.time_start DESC LIMIT 1;';
$query = $db->prepare($sql);
$query->bindValue(':zone_id', $timezoneId, PDO::PARAM_INT);
$query->execute();
$utcOffset = (int) $query->fetchColumn();

// INSERT TO DB
$sql = 'INSERT INTO user (`username`, `email`, `password`, `country_code`, `utc_offset`)
		 values (:username, :email, :password, :country_code, :utc_offset)';

$query = $db->prepare($sql);
$query->bindValue(':username', $username, PDO::PARAM_STR);
$query->bindValue(':email', $email, PDO::PARAM_STR);
$query->bindValue(':password', md5($password), PDO::PARAM_STR);
$query->bindValue(':country_code', $countryCode, PDO::PARAM_STR);
$query->bindValue(':utc_offset', $utcOffset, PDO::PARAM_STR);
$query->execute();

Session::updateUserData($db->lastInsertId());

$knockoutResult = Request::getString('knockoutResult');
if (!empty($knockoutResult)) {
	$knockoutResult = json_decode($knockoutResult, true);
	DBHandler::updateKnockout($knockoutResult);
}

$response->redirect(ROOT_URL);

