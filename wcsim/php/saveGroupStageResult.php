<?php
require_once 'lib/Request.php';
require_once 'lib/AjaxResponse.php';
require_once 'lib/Session.php';
require_once 'db/DBHandler.php';

if (Session::isUserLogined()) {
	$groupResult = Request::getString('groupResult');
	if (!empty($groupResult)) {
		$groupResult = json_decode($groupResult, true);
		DBHandler::updateGroupStage($groupResult);
	}

	AjaxResponse::getInstance()->sendSuccessMessage('Save successful!');
}