<?php
class AjaxResponse {
	
	const STATUS_OK = 1;
	const STATUS_EXCEPTION = 2;
	const MESSAGE_TYPE_MESSAGE = 1;
	const MESSAGE_TYPE_SUCCESS = 2;
	const MESSAGE_TYPE_WARNING = 3;
	const MESSAGE_TYPE_ERROR = 4;
	
	public static function getInstance() {
		return new self();
	}
	
	public function debug($message) {
		ob_start();
		var_dump($message);
		$this->_data[JSONKey::DEBUG_MESSAGE] = ob_get_contents();
		ob_end_clean();
		$this->send();
	}
	
	public function setStatus($status) {
		$this->_status = $status;
		return $this;
	}
	
	public function put($key, $value) {
		$this->_data[$key] = $value;
		return $this;
	}
	
	public function putValidateMessage($key, $value) {
		$this->_validateMessage[$key] = $value;
		return $this;
	}
	
	public function redirect($url, $message = '') {
		$this->_data[JSONKey::REDIRECT_URL] = $url;
		$this->_data[JSONKey::MESSAGE] = $message;
		$this->_data[JSONKey::MESSAGE_TYPE] = self::MESSAGE_TYPE_MESSAGE;
		$this->send();
	}
	
	public function sendSuccessMessage($message) {
		$this->_data[JSONKey::MESSAGE] = $message;
		$this->_data[JSONKey::MESSAGE_TYPE] = self::MESSAGE_TYPE_SUCCESS;
		$this->send();
	}
	
	public function sendErrorMessage($message) {
		$this->_data[JSONKey::MESSAGE] = $message;
		$this->_data[JSONKey::MESSAGE_TYPE] = self::MESSAGE_TYPE_ERROR;
		$this->send();
	}
	
	public function sendWarningMessage($message) {
		$this->_data[JSONKey::MESSAGE] = $message;
		$this->_data[JSONKey::MESSAGE_TYPE] = self::MESSAGE_TYPE_WARNING;
		$this->send();
	}

	public function sendValidateMessage($key, $message) {
		$this->_validateMessage[$key] = $message;
		$this->_data[JSONKey::VALIDATE_MESSAGES] = $this->_validateMessage;
		$this->_data[JSONKey::VALIDATE_FLAG] = false;
		$this->send();
	}
	
	public function send() {
		$this->_data[JSONKey::STATUS] = $this->_status;
		header('Content-type: application/json');
		echo json_encode($this->_data);
		exit;
	}
	
	private function __construct() {}
	
	private $_status = self::STATUS_OK;
	private $_data = array();
	private $_validateMessage = array();
}
class JSONKey {
	const VALIDATE_FLAG		= 'validateFlag';
	const VALIDATE_MESSAGES = 'validateMessages';
	const MESSAGE 			= 'message';
	const MESSAGE_TYPE 		= 'messageType';
	const STATUS			= 'status';
	const REDIRECT_URL		= 'redirectURL';
	const GOTO_URL			= 'goToURL';
	const SEARCH_RESULT 	= 'searchResult';
	const FETCHED_COUNT		= 'fetchedCount';
	const RESULT_COUNT 		= 'resultCount';
	const DEBUG_MESSAGE 	= 'debugMessage';
}
