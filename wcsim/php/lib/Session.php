<?php
$phpFolder = dirname(dirname(__FILE__));
require_once $phpFolder . '/constant.php';
require_once $phpFolder . '/db/DBHandler.php';
require_once 'CmonUtil.php';

class Session {
	
	public static function start() {
		if(session_id() === '') {
			ini_set('session.bug_compat_warn', 0);
			ini_set('session.bug_compat_42', 0);
			session_start();
		}
	}
	
	public static function isUserLogined() {
		self::start();
		return (boolean) @$_SESSION['__isLogined'] === true;
	}
	
	public static function isAdminLogined() {
		self::start();
		$user = self::getLoginedUser();
		return isset($user) && ($user[User::F_IS_ADMIN] == 1);
	}
	
	public static function getLoginedUser() {
		self::start();
		return (array) @$_SESSION['__loginedUser'];
	}
	
	public static function storeLoginedUser($user) {
		self::start();
		$_SESSION['__loginedUser'] = $user;
		$_SESSION['__isLogined'] = true;
	}
	
	public static function updateUserData($userId = null) {
		
		if (!isset($userId)) {
			$user = self::getLoginedUser();
			$userId = $user['id'];
		}
		
		$sql = 'SELECT * FROM user WHERE id =' . $userId;
		$result = DBHandler::connect()->query($sql);
		$user = $result->fetch(PDO::FETCH_ASSOC);
		self::storeLoginedUser($user);
	}
}
