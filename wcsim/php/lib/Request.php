<?php
class Request {
	
	public static function getString($key) {
		$out = @trim((string) $_GET[$key]);
		if ($out === '') {
			$out = @trim((string) $_POST[$key]);
		}
		return $out;
	}
	
	public static function getInteger($key) {
		$out = @(int) $_GET[$key];
		if ($out === 0) {
			$out = @(int) $_POST[$key];
		}
		return $out;
	}
	
	public static function setVar($key, $val) {
		self::$_vars[$key] = $val;
	}
	
	public static function getVar($key, $defaultVal) {
		if (isset(self::$_vars[$key])) {
			return self::$_vars[$key];
		} else {
			$defaultVal;
		}
	}
	
	private static $_vars = array();
}