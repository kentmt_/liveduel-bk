<?php
require_once 'constant.php';
session_start();
session_destroy();
header('Location: ' . ROOT_URL);