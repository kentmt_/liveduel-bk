<?php
require_once 'lib/Request.php';
require_once 'lib/Session.php';
require_once 'lib/AjaxResponse.php';
require_once 'db/DBHandler.php';

if (Session::isAdminLogined()) {
	
	$groupResult = json_decode(Request::getString('groupResult'), true);

	$db = DBHandler::connect();
	$sql = ' INSERT INTO
			matches (
				id,
				fulltime_result1,
				fulltime_result2,
				is_played,
				winner_country_code
			) VALUES ';
	$i = 0;
	$codes = array();
	foreach ($groupResult as $group) {
		foreach ($group as $match) {
			
			$score1 = (string)@$match['score1'];
			$score2 = (string)@$match['score2'];
			if ($score1 !== '' && $score2 !== '') {
				if ($score1 > $score2) {
					$code = $match['flag1'];
				} else if ($score2 > $score1) {
					$code = $match['flag2'];
				} else {
					$code = '_draw';
				}
				$codes[] = $code;
				$isPlayed = 1;
				$sql .=
					' ('
					. (int) $match[Match::F_MATCH_ID] . ','
					. $score1 . ','
					. $score2 . ','
					. $isPlayed . ','
					. ' :winner_country_code' . $i
					. '), '
					;
				++ $i;
			}
		}
	}
	
	$sql = trim($sql);
	$sql = substr($sql, 0, strlen($sql) - 1);
	$sql .= ' ON DUPLICATE KEY UPDATE
		fulltime_result1 = VALUES(fulltime_result1)
	,	fulltime_result2 = VALUES(fulltime_result2)
	,	is_played = ' . $isPlayed .'
	,	winner_country_code = VALUES(winner_country_code);';
	
	$query = $db->prepare($sql);
	
	for ($j = 0; $j < $i; ++ $j) {
		$query->bindValue(':winner_country_code' . $j, $codes[$j], PDO::PARAM_STR);
	}
	
	// Update real group stage
	$query->execute();
	
	AjaxResponse::getInstance()->sendSuccessMessage("The real is updated");
}
