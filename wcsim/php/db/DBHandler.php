<?php
error_reporting(E_ERROR | E_PARSE);
$phpFolder = dirname(dirname(__FILE__));
require_once $phpFolder . '/lib/Request.php';
require_once $phpFolder . '/lib/Session.php';
require_once $phpFolder	 . '/constant.php';

class DBHandler {
	
	private static $_handler = null;
	
	public static function connect() {
		if (self::$_handler === null) {
			self::$_handler = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME, DB_USERNAME, DB_PASSWORD);
		}
		self::$_handler->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return self::$_handler;
	}
	
	public static function getKnockoutResult() {
		
		$db = self::connect();
		$isLogined = Session::isUserLogined();
		$user = Session::getLoginedUser();
		$sql = '';
	
		if ($isLogined && !Session::isAdminLogined()) {
			$sql = "SELECT
				m.id match_id
			,	mb.id match_bet_id
			,	m.start_date
			,	m.start_time
			,	m.location
			,	IF(mb.country1_code IS NULL, '', mb.country1_code) country1_code
			,	IF(mb.country2_code IS NULL, '', mb.country2_code) country2_code
			,	IF(mb.country1_name IS NULL, '', mb.country1_name) country1_name
			,	IF(mb.country2_name IS NULL, '', mb.country2_name) country2_name
			,	IF(mb.winner_country_code IS NULL, '', mb.winner_country_code) winner_country_code
					
			,	IF(mb.fulltime_result1 IS NULL,'', mb.fulltime_result1) fulltime_result1
			,	IF(mb.fulltime_result2 IS NULL, '', mb.fulltime_result2) fulltime_result2
			,	IF(m.fulltime_result1 IS NULL, '', m.fulltime_result1) real_fulltime_result1
			,	IF(m.fulltime_result2 IS NULL, '', m.fulltime_result2) real_fulltime_result2
					
			,	IF(mb.extratime_result1 IS NULL, '', mb.extratime_result1) extratime_result1
			,	IF(mb.extratime_result2 IS NULL, '', mb.extratime_result2) extratime_result2
			,	IF(m.extratime_result1 IS NULL, '', m.extratime_result1) real_extratime_result1
			,	IF(m.extratime_result2 IS NULL, '', m.extratime_result2) real_extratime_result2
					
			,	IF(mb.penalty_result1 IS NULL, '', mb.penalty_result1) penalty_result1
			,	IF(mb.penalty_result2 IS NULL, '', mb.penalty_result2) penalty_result2
			,	IF(m.penalty_result1 IS NULL, '', m.penalty_result1) real_penalty_result1
			,	IF(m.penalty_result2 IS NULL, '', m.penalty_result2) real_penalty_result2
			,	m.round_of_16
			,	m.quarter_final
			,	m.semi_final
			,	m.third_place_match
			,	m.final_match
			,	DATE_FORMAT(STR_TO_DATE(CONCAT('2014 ', m.start_date, m.start_time), '%Y %M %d %H:%i'), '%Y-%m-%d %H:%i') local_datetime
			,	m.utc_offset
			,	m.is_played
			,	IF(mb.save_time IS NULL, '', mb.save_time) save_time
			FROM
				`matches` m
			LEFT JOIN
				matches_bets mb
				ON m.id = mb.match_id
				AND mb.user_id = " . $user['id'] . "
			WHERE
				m.knockout_stage = 1
			;";
	
		} else {
			$sql = "SELECT
				m.id match_id
			,	0 match_bet_id
			,	m.start_date
			,	m.start_time
			,	m.location
			,	IF(m.country1_code IS NULL, '', m.country1_code) country1_code
			,	IF(m.country2_code IS NULL, '', m.country2_code) country2_code
			,	IF(m.country1_name IS NULL, '', m.country1_name) country1_name
			,	IF(m.country2_name IS NULL, '', m.country2_name) country2_name
			,	IF(m.winner_country_code IS NULL, '', m.winner_country_code) winner_country_code
			,	IF(m.fulltime_result1 IS NULL, '', m.fulltime_result1) fulltime_result1
			,	IF(m.fulltime_result2 IS NULL, '', m.fulltime_result2) fulltime_result2
			,	IF(m.extratime_result1 IS NULL, '', m.extratime_result1) extratime_result1
			,	IF(m.extratime_result2 IS NULL, '', m.extratime_result2) extratime_result2
			,	IF(m.penalty_result1 IS NULL, '', m.penalty_result1) penalty_result1
			,	IF(m.penalty_result2 IS NULL, '', m.penalty_result2) penalty_result2
			,	m.round_of_16
			,	m.quarter_final
			,	m.semi_final
			,	m.third_place_match
			,	m.final_match
			,	DATE_FORMAT(STR_TO_DATE(CONCAT('2014 ', m.start_date, m.start_time), '%Y %M %d %H:%i'), '%Y-%m-%d %H:%i') local_datetime
			,	m.utc_offset
			,	m.is_played
			FROM
				`matches` m
			WHERE
				m.knockout_stage = 1
			;";
		}
		
		$query = $db->query($sql);
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		$out = array();
		foreach ($result as $key => $match) {
			
			$match = self::_calResultOfMatch($match);
			
			$match['is_predicted'] = $match['is_predicted'] && !empty($match['winner_country_code']);
			
			if ($isLogined) {
				$match = self::_checkResultForReward($match);
				
				if ($match['correctResult']) {
						
					Request::setVar('totalPointsWon', Request::getVar('totalPointsWon', 0) + 50);
						
					if ($match['correctScore']) {
						Request::setVar('totalPointsWon', Request::getVar('totalPointsWon', 0) + 100);
					}
				}
					
				if ($match['final_match'] == 1
					&& $match['is_played']
					&& $match['is_valid_result']
					&& (string) $user['winner_country_code'] === (string) $user['winner_country_code']
				) {
					Request::setVar('totalPointsWon', Request::getVar('totalPointsWon', 0) + (int) $user['winner_country_point']);
				}
			}
			
			$out[$key] = $match;
		}
		
		return $out;
	}
	
	private static function _checkResultForReward($match) {

		$match['correctResult'] = false;
		$match['correctScore'] = false;
		
		if ($match['is_valid_result']) {
			
			$isFTInputed =
				$match['fulltime_result1'] !== ''
				&& $match['fulltime_result2'] !== ''
				&& $match['real_fulltime_result1'] !== ''
				&& $match['real_fulltime_result2'] !== ''
			;
			
			$isEXInputed =
				$match['extratime_result1'] !== ''
				&& $match['extratime_result2'] !== ''
				&& $match['real_extratime_result1'] !== ''
				&& $match['real_extratime_result2'] !== ''
			;
			$isPEInputed =
				$match['penalty_result1'] !== ''
				&& $match['penalty_result2'] !== ''
				&& $match['real_penalty_result1'] !== ''
				&& $match['real_penalty_result2'] !== ''
			;
			
			$correctScoreFulltime =
				$isFTInputed
				&&	((int) $match['fulltime_result1'] === (int) $match['real_fulltime_result1'])
				&&	((int) $match['fulltime_result2'] === (int) $match['real_fulltime_result2']);
				
			$correctScoreExtra =
				$isEXInputed
				&& ((int) $match['extratime_result1'] === (int) $match['real_extratime_result1'])
				&& ((int) $match['extratime_result2'] === (int) $match['real_extratime_result2']);
				
			$correctScorePenalty =
				$isPEInputed
				&& ((int) $match['penalty_result1'] === (int) $match['real_penalty_result1'])
				&&	((int) $match['penalty_result2'] === (int) $match['real_penalty_result2']);
				
			$correctFulltime =
				$isFTInputed
				&&
				(
					(
						((int) $match['fulltime_result1'] > (int) $match['fulltime_result2'])
						&&	((int) $match['real_fulltime_result1'] > (int) $match['real_fulltime_result2'])
					)
					||
					(
						((int) $match['fulltime_result1'] < (int) $match['fulltime_result2'])
						&&	((int) $match['real_fulltime_result1'] < (int) $match['real_fulltime_result2'])
					)
					||
					(
						((int) $match['fulltime_result1'] === (int) $match['fulltime_result2'])
						&&	((int) $match['real_fulltime_result1'] === (int) $match['real_fulltime_result2'])
					)
				)
				;
				
			$correctExtra =
				$isEXInputed
				&&
				(
					(
						((int) $match['extra_result1'] > (int) $match['extra_result2'])
						&&	((int) $match['real_extra_result1'] > (int) $match['real_extra_result2'])
					)
					||
					(
						((int) $match['extra_result1'] < (int) $match['extra_result2'])
						&&	((int) $match['real_extra_result1'] < (int) $match['real_extra_result2'])
					)
					||
					(
						((int) $match['extra_result1'] === (int) $match['extra_result2'])
						&&	((int) $match['real_extra_result1'] === (int) $match['real_extra_result2'])
					)
				)
				;
				
			$correctPenalty =
				$isPEInputed
				&&
				(
					(
						((int) $match['penalty_result1'] > (int) $match['penalty_result2'])
						&&	((int) $match['real_penalty_result1'] > (int) $match['real_penalty_result2'])
					)
					||
					(
						((int) $match['penalty_result1'] < (int) $match['penalty_result2'])
						&&	((int) $match['real_penalty_result1'] < (int) $match['real_penalty_result2'])
					)
					||
					(
						((int) $match['penalty_result1'] === (int) $match['penalty_result2'])
						&&	((int) $match['real_penalty_result1'] === (int) $match['real_penalty_result2'])
					)
				)
				;
				
			if ($match['group_stage'] == 1) {
				$match['correctResult'] = $correctFulltime;
			} else {
				$match['correctResult'] = ($correctFulltime && $correctExtra && $correctPenalty);
			}
				
			$match['correctScore'] = ($correctScoreFulltime && $correctScoreExtra && $correctScorePenalty);
			
		}
		
		return $match;
	}
	
	private static function _calResultOfMatch($match) {
		$user = Session::getLoginedUser();
		$match['is_predicted'] = $match['match_bet_id'] != 0;
		$match['is_played'] = $match['is_played'] == 1;
		$match['disabled'] = false;
		$match['status'] = '';
		$matchUTCOffset = (int) $match['utc_offset'];
		$brazilTime = DateTime::createFromFormat('Y-m-d G:i', $match['local_datetime']);
		$orgBrazilTime = clone $brazilTime;
		$match['local_datetime'] = $brazilTime->format('F d - H:i');
			
		$diffUTC = - ((int) $matchUTCOffset);
		$utcTimeOfMatch = $brazilTime->add(new DateInterval("PT{$diffUTC}H"));
		$orgUTCTimeOfMatch = clone $utcTimeOfMatch;
			
		date_default_timezone_set('UTC');
		$utcTimeCurrent = new DateTime();
		date_default_timezone_set(ini_get('date.timezone'));
			
		if ($match['is_played']) {
			$match['status'] = 'Done';
		} else {
			$match['status'] = $utcTimeCurrent >= $utcTimeOfMatch ? 'In progress' : 'Not yet';
		}
			
		if (!Session::isAdminLogined()) {
			$match['disabled'] = ($utcTimeCurrent >= $utcTimeOfMatch->modify('+1800 seconds'));
		}
			
		if (Session::isUserLogined()) {
			$match['is_valid_result'] = true;
			if (!empty($match['save_time'])) {
				date_default_timezone_set('UTC');
				$utcTimeSaved = DateTime::createFromFormat('Y-m-d G:i', $match['save_time']);
				$match['is_valid_result'] = ($utcTimeSaved < $orgUTCTimeOfMatch->modify('+1800 seconds'));
				date_default_timezone_set(ini_get('date.timezone'));
			}
		
			$userUCTOffset = (int) $user['utc_offset'];
			if ($userUCTOffset > $matchUTCOffset) {
				$diff = ($userUCTOffset - $matchUTCOffset);
				$orgBrazilTime->add(new DateInterval("PT{$diff}H"));
				$match['user_datetime'] = $orgBrazilTime->format('F d - H:i');
			} else {
				$diff = ($matchUTCOffset - $userUCTOffset);
				$orgBrazilTime->sub(new DateInterval("PT{$diff}H"));
				$match['user_datetime'] = $orgBrazilTime->format('F d - H:i');
			}
		}
		
		return $match;
	}
	
	public static function getGroupStageResult($realGroupStageIsDone = false) {
		$db = self::connect();
		$user = Session::getLoginedUser();
		$sql = '';
	
		if (!$realGroupStageIsDone && Session::isUserLogined() && !Session::isAdminLogined()) {
			
			$sql = "SELECT
				m.id match_id
			,	IF(mb.id IS NULL, 0, mb.id) match_bet_id
			,	m.country1_code flag1
			,	m.country2_code flag2
			,	m.country1_name team1
			,	m.country2_name team2
			,	m.start_date `date`
			,	m.start_time `time`
			,	m.location location
			,	m.`group` `group`
			,	m.utc_offset
			,	m.is_played
			,	DATE_FORMAT(STR_TO_DATE(CONCAT('2014 ', start_date, start_time), '%Y %M %d %H:%i'), '%Y-%m-%d %H:%i') local_datetime
			,	IF(mb.fulltime_result1 IS NULL, m.fulltime_result1, mb.fulltime_result1) score1
			,	IF(mb.fulltime_result2 IS NULL, m.fulltime_result2, mb.fulltime_result2) score2
			,	IF(m.fulltime_result1 IS NULL, '', m.fulltime_result1) real_score1
			,	IF(m.fulltime_result2 IS NULL, '', m.fulltime_result2) real_score2
			,	IF(mb.fulltime_result1 IS NULL, '', mb.fulltime_result1) your_score1
			,	IF(mb.fulltime_result2 IS NULL, '', mb.fulltime_result2) your_score2
			,	IF(m.winner_country_code IS NULL, '', m.winner_country_code) real_winner_code
			,	IF(mb.winner_country_code IS NULL, '', mb.winner_country_code) your_winner_code
			,	IF(mb.points_won IS NULL, 0, mb.points_won) points_won
			,	IF(mb.winner_country_code = 'draw', 1, 0) is_draw
			,	IF(mb.save_time IS NULL, '', mb.save_time) save_time
			FROM
				matches m
			LEFT JOIN matches_bets mb
				ON m.id = mb.match_id
				and mb.user_id = " . $user[User::F_ID] . "
			where m.group_stage = 1";
			
		} else {
	
			$sql = "SELECT
					m.id match_id
				,	0 match_bet_id
				,	m.country1_code flag1
				,	m.country2_code flag2
				,	m.country1_name team1
				,	m.country2_name team2
				,	m.start_date `date`
				,	m.start_time `time`
				,	m.location location
				,	m.`group` `group`
				,	m.utc_offset
				,	m.is_played
				,	DATE_FORMAT(STR_TO_DATE(CONCAT('2014 ', start_date, start_time), '%Y %M %d %H:%i'), '%Y-%m-%d %H:%i') local_datetime
				,	IF(m.fulltime_result1 IS NULL, '', m.fulltime_result1) score1
				,	IF(m.fulltime_result2 IS NULL, '', m.fulltime_result2) score2
			FROM
				matches m
			WHERE
				m.group_stage = 1
			";
		}
	
		$matchs = $db->query($sql)->fetchAll(PDO::FETCH_ASSOC);
		$out = array();
		$break = '';
		$indexByGroup = 0;
		foreach ($matchs as $match) {
			
			if ((string)$match[Match::F_GROUP] !== $break) {
				$break = $match[Match::F_GROUP];
				$out[$break] = array();
				$indexByGroup = 0;
			}
			
			$match = self::_calResultOfMatch($match);
		
			$totalPointsWon = 0;
			if ($match['is_valid_result'] && $match['real_winner_code'] === $match['your_winner_code']) {
			
				$totalPointsWon += 50;
		
				if (($match['real_score1'] === $match['your_score1'])
				&& ($match['real_score2'] === $match['your_score2'])
				) {
					$totalPointsWon += 100;
				}
			
				Request::setVar('totalPointsWon', Request::getVar('totalPointsWon', 0) + $totalPointsWon);
			}
			
			$match['indexByGroup'] = $indexByGroup;
			++ $indexByGroup;
			$out[$break][] = $match;
		}
		
		return $out;
	}
	
	public static function isKnockoutMatchAvailable($match) {
		return !empty($match['country1_code']) && !empty($match['country2_code']);
	}
	
	private static function prepareResult(&$match) {
		$match['fulltime_result1'] = empty($match['fulltime_result1']) ? null : @(int) $match['fulltime_result1'];
		$match['fulltime_result2'] = empty($match['fulltime_result2']) ? null : @(int) $match['fulltime_result2'];;
		$match['extratime_result1'] = empty($match['extratime_result1']) ? null : @(int) $match['extratime_result1'];
		$match['extratime_result2'] = empty($match['extratime_result2']) ? null : @(int) $match['extratime_result2'];
		$match['penalty_result1'] = empty($match['penalty_result1']) ? null : @(int) $match['penalty_result1'];
		$match['penalty_result2'] = empty($match['penalty_result2']) ? null : @(int) $match['penalty_result2'];
		$match['winner_country_code'] = empty($match['winner_country_code']) ? null : (string) $match['winner_country_code'];
	}
	
	public static function updateKnockout($knockoutResult) {
		
		$db = self::connect();
		$user = Session::getLoginedUser();
		$sql = ' INSERT INTO
			matches_bets (
			user_id,
			match_id,
			country1_code,
			country2_code,
			country1_name,
			country2_name,
			fulltime_result1,
			fulltime_result2,
			extratime_result1,
			extratime_result2,
			penalty_result1,
			penalty_result2,
			winner_country_code
		) VALUES ';
	
		$i = 0;
		foreach ($knockoutResult as $match) {
			if ($i < 16 && self::isKnockoutMatchAvailable($match)) {
				$sql .=	' ('
					.$user[User::F_ID] . ','
					. (int) $match['match_id'] . ','
					. ' :code1' . $i .','
					. ' :code2' . $i .','
					. ' :name1' . $i .','
					. ' :name2' . $i .','
					. ' :ftresult1' . $i .','
					. ' :ftresult2' . $i .','
					. ' :etresult1' . $i .','
					. ' :etresult2' . $i .','
					. ' :presult1' . $i .','
					. ' :presult2' . $i .','
					. ' :winner' . $i
					. '), '
					;
					++ $i;
			}
		}
	
		$sql = trim($sql);
		$sql = substr($sql, 0, strlen($sql) - 1);
		$sql .= ' ON DUPLICATE KEY UPDATE
		country1_code = VALUES(country1_code),
		country2_code = VALUES(country2_code),
		country1_name = VALUES(country1_name),
		country2_name = VALUES(country2_name),
		fulltime_result1 = VALUES(fulltime_result1),
		fulltime_result2 = VALUES(fulltime_result2),
		extratime_result1 = VALUES(extratime_result1),
		extratime_result2 = VALUES(extratime_result2),
		penalty_result1 = VALUES(penalty_result1),
		penalty_result2 = VALUES(penalty_result2),
		winner_country_code = VALUES(winner_country_code)';

		$query = $db->prepare($sql);
		$i = 0;
		foreach ($knockoutResult as $match) {
			if ($i < 16 && self::isKnockoutMatchAvailable($match)) {
				self::prepareResult($match);
				$query->bindValue(':code1' . $i, @(string) $match['country1_code'], PDO::PARAM_STR);
				$query->bindValue(':code2' . $i, @(string )$match['country2_code'], PDO::PARAM_STR);
				$query->bindValue(':name1' . $i, @(string) $match['country1_name'], PDO::PARAM_STR);
				$query->bindValue(':name2' . $i, @(string) $match['country2_name'], PDO::PARAM_STR);
				$query->bindValue(':ftresult1' . $i, $match['fulltime_result1'], PDO::PARAM_INT);
				$query->bindValue(':ftresult2' . $i, $match['fulltime_result2'], PDO::PARAM_INT);
				$query->bindValue(':etresult1' . $i, $match['extratime_result1'], PDO::PARAM_INT);
				$query->bindValue(':etresult2' . $i, $match['extratime_result2'], PDO::PARAM_INT);
				$query->bindValue(':presult1' . $i, $match['penalty_result1'], PDO::PARAM_INT);
				$query->bindValue(':presult2' . $i, $match['penalty_result2'], PDO::PARAM_INT);
				$query->bindValue(':winner' . $i, $match['winner_country_code'], PDO::PARAM_STR);
				++ $i;
			}
		}
	
		// Update group stage
		$query->execute();
	
		$sql = ' INSERT INTO
			user (
			id,
			winner_country_name,
			winner_country_code,
			winner_country_point
		) VALUES ';
	
		$sql .=	' ('
			. $user[User::F_ID] . ','
			. ' :winner_country_name,'
			. ' :winner_country_code,'
			. ' :winner_country_point'
			. '), '
			;
	
		$sql = trim($sql);
		$sql = substr($sql, 0, strlen($sql) - 1);
		$sql .= ' ON DUPLICATE KEY UPDATE
		winner_country_name = VALUES(winner_country_name),
		winner_country_code = VALUES(winner_country_code),
		winner_country_point = VALUES(winner_country_point)';
	
		$query = $db->prepare($sql);
		$query->bindValue(':winner_country_name', @(string) $knockoutResult['winner_country_name'], PDO::PARAM_STR);
		$query->bindValue(':winner_country_code', @(string) $knockoutResult['winner_country_code'], PDO::PARAM_STR);
		$query->bindValue(':winner_country_point', @(string) $knockoutResult['winner_country_point'], PDO::PARAM_INT);
		$query->execute();
		
		// Update final winner
		Session::updateUserData();
	
		if (!empty($_SESSION['groupResult'])) {
			self::updateGroupStage($_SESSION['groupResult']);
		}
	
		if (!empty($_SESSION['roundOf16'])) {
			self::updateRoundOf16($_SESSION['roundOf16']);
		}
	}
	
	public static function updateGroupStage($groupResult) {
		$db = self::connect();
		$user = Session::getLoginedUser();
		$sql = ' INSERT INTO
				matches_bets (
				user_id,
				match_id,
				fulltime_result1,
				fulltime_result2,
				winner_country_code,
				save_time
			) VALUES ';
		$i = 0;
		$winnerCodes = array();
		$saveTimes = array();
		foreach ($groupResult as $group) {
			foreach ($group as $match) {
				
				$score1 = trim((string) $match['score1']);
				$score2 = trim((string) $match['score2']);
				if ($score1 !== '' && $score2 !== '') {
					
					$score1 = (int) $score1;
					$score2 = (int) $score2;
					
					if ($score1 > $score2) {
						$winnerCodes[] = $match['flag1'];
					} else if ($score1 < $score2) {
						$winnerCodes[] = $match['flag2'];
					} else {
						$winnerCodes[] = '_draw';
					}
					$saveTimes[] = date('Y-m-d G:i', time());
					$sql .=
					' ('
					. $user[User::F_ID] . ','
					. (int) $match[Match::F_MATCH_ID] . ','
					. $score1 .','
					. $score2 . ','
					. ':winnerCode' . $i . ','
					. ':saveTime' . $i
					. '), '
					;
					
					++ $i;
				}
			}
		}
	
		$sql = trim($sql);
		$sql = substr($sql, 0, strlen($sql) - 1);
		$sql .= ' ON DUPLICATE KEY UPDATE
			 fulltime_result1 = VALUES(fulltime_result1),
			 fulltime_result2 = VALUES(fulltime_result2),
			 winner_country_code = VALUES(winner_country_code),
			 save_time = VALUES(save_time)
		';
	
		$query = $db->prepare($sql);
	
		for ($j = 0; $j < $i; ++ $j) {
			$query->bindValue(':winnerCode' . $j, $winnerCodes[$j], PDO::PARAM_STR);
			$query->bindValue(':saveTime' . $j, $saveTimes[$j], PDO::PARAM_STR);
		}
	
		// Update group stage
		$query->execute();
	}
	
	public static function saveTheRealRoundOf16($roundOf16) {
		
		$db = self::connect();
		$user = Session::getLoginedUser();
		$sql = 'SELECT ID FROM matches WHERE round_of_16 = 1';
		$result = $db->query($sql);
		$sql = ' INSERT INTO matches (
					id,
					country1_code,
					country2_code,
					country1_name,
					country2_name
				) VALUES ';
		$i = 0;
		foreach ($result->fetchAll(PDO::FETCH_COLUMN) as $matchId) {
			$sql .=
			' ('
			. 	$matchId . ','
			. ' :code1' . $i . ','
			. ' :code2' . $i . ','
			.  ':name1' . $i . ','
			.  ':name2' . $i
			. '), '
			;
			++ $i;
		}
		
		$sql = trim($sql);
		$sql = substr($sql, 0, strlen($sql) - 1);
		$sql .= ' ON DUPLICATE KEY UPDATE
			country1_code = VALUES(country1_code),
			country2_code = VALUES(country2_code),
			country1_name = VALUES(country1_name),
			country2_name = VALUES(country2_name)
		';
		
		$query = $db->prepare($sql);
		
		for ($j = 0; $j < $i; ++ $j) {
			$query->bindValue(':code1' . $j, @(string) $roundOf16[$j][0], PDO::PARAM_STR);
			$query->bindValue(':code2' . $j, @(string) $roundOf16[$j][1], PDO::PARAM_STR);
			$query->bindValue(':name1' . $j, @(string) self::$teams[$roundOf16[$j][0]], PDO::PARAM_STR);
			$query->bindValue(':name2' . $j, @(string) self::$teams[$roundOf16[$j][1]], PDO::PARAM_STR);
		}
		
		// Update round of 16
		$query->execute();
	}
	
	public static function updateRoundOf16($roundOf16) {
		
		if (Session::isAdminLogined()) {
			self::saveTheRealRoundOf16($roundOf16);
			return;
		}
		
		$db = self::connect();
		$user = Session::getLoginedUser();
		$sql = 'SELECT * FROM matches WHERE round_of_16 = 1';
		$result = $db->query($sql);
		$sql = ' INSERT INTO
					matches_bets (
					user_id,
					match_id,
					country1_code,
					country2_code,
					country1_name,
					country2_name
				) VALUES ';
		$i = 0;
		foreach ($result->fetchAll(PDO::FETCH_ASSOC) as $match) {
			$sql .=
				' ('
				. $user[User::F_ID] . ','
				. $match['id'] . ','
				. ' :code1' . $i . ','
				. ' :code2' . $i . ','
				.  ':name1' . $i . ','
				.  ':name2' . $i
				. '), '
				;
				++ $i;
		}
	
		$sql = trim($sql);
		$sql = substr($sql, 0, strlen($sql) - 1);
		$sql .= ' ON DUPLICATE KEY UPDATE
			country1_code = VALUES(country1_code),
			country2_code = VALUES(country2_code),
			country1_name = VALUES(country1_name),
			country2_name = VALUES(country2_name)
		';
	
		$query = $db->prepare($sql);
	
		for ($j = 0; $j < $i; ++ $j) {
			$query->bindValue(':code1' . $j, @(string) $roundOf16[$j][0], PDO::PARAM_STR);
			$query->bindValue(':code2' . $j, @(string) $roundOf16[$j][1], PDO::PARAM_STR);
			$query->bindValue(':name1' . $j, @(string) self::$teams[$roundOf16[$j][0]], PDO::PARAM_STR);
			$query->bindValue(':name2' . $j, @(string) self::$teams[$roundOf16[$j][1]], PDO::PARAM_STR);
		}
	
		// Update round of 16
		$query->execute();
	
		self::markAsGroupStagePassed();
	}
	
	private static $teams = array(
		'br' => 'Brazil',
		'hr' => 'Croatia',
		'mx' => 'Mexico',
		'cm' => 'Cameroon',
		'es' => 'Spain',
		'nl' => 'Netherlands',
		'cl' => 'Chile',
		'au' => 'Australia',
		'co' => 'Colombia',
		'gr' => 'Greece',
		'ci' => 'Ivory Coast',
		'jp' => 'Japan',
		'uy' => 'Uruguay',
		'cr' => 'Costa Rica',
		'en' => 'England',
		'it' => 'Italy',
		'ch' => 'Switzerland',
		'ec' => 'Ecuador',
		'fr' => 'France',
		'hn' => 'Honduras',
		'ar' => 'Argentina',
		'ba' => 'Bosnia-Herz.',
		'ir' => 'Iran',
		'ng' => 'Nigeria',
		'de' => 'Germany',
		'pt' => 'Portugal',
		'gh' => 'Ghana',
		'us' => 'USA',
		'be' => 'Belgium',
		'dz' => 'Algeria',
		'ru' => 'Russia',
		'kr' => 'Korea Rep.'
	);
	
	public static function markAsGroupStagePassed() {
		$db = self::connect();
		$user = Session::getLoginedUser();
		$db->exec('UPDATE `user` SET is_group_stage_done = 1 WHERE id = ' .  $user[User::F_ID]);
		Session::updateUserData();
	}
	
	public static function getMatchResults($matchId, $winOrLose) {
		
		if ($matchId === 0) {
			return array();
		}
		
		$sql = "SELECT
			m.id match_id
		,	IF(mb.id IS NULL, 0, mb.id) match_bet_id
		,	u.id user_id
		,	u.username
		,	m.country1_code
		,	m.country2_code
		,	m.country1_name
		,	m.country2_name
		,	m.start_date
		,	m.start_time
		,	m.location location
		,	m.`group` `group`
				
		,	m.group_stage
		,	IF(mb.winner_country_code IS NULL,'', mb.winner_country_code) winner_country_code
				
		,	IF(mb.fulltime_result1 IS NULL,'-', mb.fulltime_result1) fulltime_result1
		,	IF(mb.fulltime_result2 IS NULL, '-', mb.fulltime_result2) fulltime_result2
		,	IF(m.fulltime_result1 IS NULL, '-', m.fulltime_result1) real_fulltime_result1
		,	IF(m.fulltime_result2 IS NULL, '-', m.fulltime_result2) real_fulltime_result2
				
		,	IF(mb.extratime_result1 IS NULL, '-', mb.extratime_result1) extratime_result1
		,	IF(mb.extratime_result2 IS NULL, '-', mb.extratime_result2) extratime_result2
		,	IF(m.extratime_result1 IS NULL, '-', m.extratime_result1) real_extratime_result1
		,	IF(m.extratime_result2 IS NULL, '-', m.extratime_result2) real_extratime_result2
				
		,	IF(mb.penalty_result1 IS NULL, '-', mb.penalty_result1) penalty_result1
		,	IF(mb.penalty_result2 IS NULL, '-', mb.penalty_result2) penalty_result2
		,	IF(m.penalty_result1 IS NULL, '-', m.penalty_result1) real_penalty_result1
		,	IF(m.penalty_result2 IS NULL, '-', m.penalty_result2) real_penalty_result2
				
		,	m.utc_offset
		,	DATE_FORMAT(STR_TO_DATE(CONCAT('2014 ', start_date, start_time), '%Y %M %d %H:%i'), '%Y-%m-%d %H:%i') local_datetime
		,	IF(mb.save_time IS NULL, '', mb.save_time) save_time
		FROM
			matches m
		LEFT JOIN matches_bets mb
			ON m.id = mb.match_id
		INNER JOIN user u
			ON mb.user_id = u.id
		WHERE m.id = " . $matchId . "
			AND u.is_admin = 0
		";
		
		$result = self::connect()->query($sql)->fetchAll(PDO::FETCH_ASSOC);
		$out = array();
		
		foreach ($result as $match) {
			$match['is_predicted'] = $match['match_bet_id'] != 0 && !empty($match['winner_country_code']);
			$matchUTCOffset = (int) $match['utc_offset'];
			$brazilTime = DateTime::createFromFormat('Y-m-d G:i', $match['local_datetime']);
			$diffUTC = - ((int) $matchUTCOffset);
			$utcTimeOfMatch = $brazilTime->add(new DateInterval("PT{$diffUTC}H"));
			$match['is_valid_result'] = true;
			if (!empty($match['save_time'])) {
				date_default_timezone_set('UTC');
				$utcTimeSaved = DateTime::createFromFormat('Y-m-d G:i', $match['save_time']);
				$match['is_valid_result'] = ($utcTimeSaved < $utcTimeOfMatch->modify('+1800 seconds'));
				date_default_timezone_set(ini_get('date.timezone'));
			}
			
			$match = self::_checkResultForReward($match);
				
			if ($match['is_predicted']) {
				$match['fulltime_result1'] = '-';
				$match['fulltime_result2'] = '-';
				$match['extratime_result1'] = '-';
				$match['extratime_result2'] = '-';
				$match['penalty_result1'] = '-';
				$match['penalty_result2'] = '-';
			}
			
			if (
				$winOrLose === 'all'
				|| ($winOrLose === 'win' && $match['correctResult'])
				|| ($winOrLose === 'lose' && !$match['correctResult'])
			) {
				$out[] = $match;
			}
		}
		
		return $out;
	}
	
	public static function getPlayedMatches() {
		$sql = ' SELECT * FROM matches where is_played = 1; ';
		return self::connect()->query($sql)->fetchAll(PDO::FETCH_ASSOC);
	}
}
