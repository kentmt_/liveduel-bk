<?php

require_once 'constant.php';
require_once 'db/DBHandler.php';
require_once 'lib/Request.php';
require_once 'lib/AjaxResponse.php';

$countryCode = Request::getString(Country::F_CODE);

$sql = "
	SELECT
		zone.zone_id " . Timezone::F_ID . ",
		zone.zone_name " . Timezone::F_NAME . "
	FROM zone WHERE zone.country_code = :country_code;
";
$query = DBHandler::connect()->prepare($sql);
$query->bindParam(':country_code', strtoupper($countryCode), PDO::PARAM_STR);
$query->execute();
AjaxResponse::getInstance()
	->put('data', $query->fetchAll(PDO::FETCH_ASSOC))
	->send();
