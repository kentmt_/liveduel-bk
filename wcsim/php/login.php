<?php

require_once 'constant.php';
require_once 'lib/Request.php';
require_once 'lib/Session.php';
require_once 'lib/AjaxResponse.php';

$response = AjaxResponse::getInstance();

$username = Request::getString(User::F_USERNAME);
if (empty($username)) {
	$response->sendValidateMessage(User::F_EMAIL, 'Please type your username');
}

$password = Request::getString(User::F_PASSWORD);
if (empty($password)) {
	$response->sendValidateMessage(User::F_PASSWORD, 'Please type your password');
}

require_once 'db/DBHandler.php';
$sql = 'SELECT * FROM user where username = :username';
$query = DBHandler::connect()->prepare($sql);
$query->bindValue(':username', $username, PDO::PARAM_STR);
$query->execute();
$user = $query->fetch(PDO::FETCH_ASSOC);
if ($user === false || $user[User::F_PASSWORD] !== md5($password)) {
	$response->sendValidateMessage(User::F_EMAIL, 'Username or password is invalid');
}
Session::storeLoginedUser($user);
$response->redirect(Request::getString('backURL'));
