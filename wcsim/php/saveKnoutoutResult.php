<?php
require_once 'lib/Request.php';
require_once 'lib/AjaxResponse.php';
require_once 'lib/Session.php';
require_once 'db/DBHandler.php';

if (Session::isUserLogined()) {
	$knockoutResult = Request::getString('knockoutResult');
	if (!empty($knockoutResult)) {
		$knockoutResult = json_decode($knockoutResult, true);
		DBHandler::updateKnockout($knockoutResult);
	}
	
	AjaxResponse::getInstance()->sendSuccessMessage('Save successful!');
}