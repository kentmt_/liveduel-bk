<?php

require_once 'constant.php';
require_once 'db/DBHandler.php';
require_once 'lib/AjaxResponse.php';

$sql = 'SELECT * FROM ' . Country::TABLE_NAME . ' ORDER BY ' . Country::F_NAME;
$result = DBHandler::connect()->query($sql);
AjaxResponse::getInstance()
	->put('data', $result->fetchAll(PDO::FETCH_ASSOC))
	->send();
