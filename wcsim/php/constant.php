<?php

require_once 'independentConstants.php';

class User {
	const F_EMAIL = 'email';
	const F_ID = 'id';
	const F_PASSWORD = 'password';
	const F_CONFIRM_PASSWORD = 'confirm_password';
	const F_USERNAME = 'username';
	const F_IS_GROUP_STAGE_DONE = 'is_group_stage_done';
	const F_IS_ADMIN = 'is_admin';
}
class Country {
	const TABLE_NAME = 'country';
	const F_CODE = 'country_code';
	const F_NAME = 'country_name';
}
class Timezone {
	const F_ID = 'zone_id';
	const F_NAME = 'zone_name';
}
class Match {
	const F_MATCH_ID = 'match_id';
	const F_MATCH_BET_ID = 'match_bet_id';
	const F_START_DATE = 'start_date';
	const F_START_TIME = 'start_time';
	const F_LOCATION = 'location';
	const F_COUNTRY1_CODE = 'country1_code';
	const F_COUNTRY2_CODE = 'country2_code';
	const F_COUNTRY1_NAME = 'country1_name';
	const F_COUNTRY2_NAME = 'country2_name';
	const F_GROUP = 'group';
	const F_FULLTIME_RESULT1 = 'fulltime_result1';
	const F_FULLTIME_RESULT2 = 'fulltime_result2';
	const F_GROUP_STAGE = 'group_stage';
}