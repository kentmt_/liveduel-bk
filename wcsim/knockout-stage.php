<?php
require_once 'php/db/DBHandler.php';
require_once 'php/lib/Request.php';
require_once 'php/constant.php';
Session::start();

if (get_magic_quotes_gpc()) {
	function stripslashes_gpc(&$value)
	{
		$value = stripslashes($value);
	}
	array_walk_recursive($_GET, 'stripslashes_gpc');
	array_walk_recursive($_POST, 'stripslashes_gpc');
	array_walk_recursive($_COOKIE, 'stripslashes_gpc');
	array_walk_recursive($_REQUEST, 'stripslashes_gpc');
}


	$user = null;
	if (Session::isUserLogined()) {
		$user = Session::getLoginedUser();
	}
	
	$result = DBHandler::getKnockoutResult();
	$knockoutResult = array();
	// Map to ui
	$htmlIdRo16 = 'match-1';
	$htmlIdQuarter = 'match-2';
	$htmlIdSemi = 'match-3';
	$ro16Index = 0;
	$quarterIndex = 0;
	$semiIndex = 0;
	$htmlId3rd = 'match-41';
	$htmlIdFinal = 'match-42';
	foreach ($result as $key => $match) {
		if ($match['round_of_16'] == 1) {
			$match['htmlId'] = $htmlIdRo16 . (string)(++ $ro16Index);
			$match['round'] = 'Round of 16';
		} else if ($match['quarter_final'] == 1) {
			$match['htmlId'] = $htmlIdQuarter . (string)(++ $quarterIndex);
			$match['round'] = 'Quarter final';
		} else if ($match['semi_final'] == 1) {
			$match['htmlId'] = $htmlIdSemi . (string)(++ $semiIndex);
			$match['round'] = 'Semi final';
		} else if ($match['third_place_match'] == 1) {
			$match['htmlId'] = $htmlId3rd;
			$match['round'] = 'Playoff for 3rd place';
		} else if ($match['final_match'] == 1) {
			$match['htmlId'] = $htmlIdFinal;
			$match['round'] = 'The final match';
		}
		$knockoutResult[$match['htmlId']] = $match;
	}
	
	$empty = false;
	
	// BECAUSE THE REAL GROUP STAGE DONE (FASTEST SOLUTION :v)
// 	$roundOf16 = @$_POST['roundOf16'];
// 	$roundOf16 = array();
// 	$groupResult =  @$_POST['groupResult'];
	
// 	if (empty($roundOf16) || empty($groupResult)) {
	
// 		if ($user !== null && ((boolean) $user[User::F_IS_GROUP_STAGE_DONE]) ){
			
			// BECAUSE THE REAL GROUP STAGE DONE (FASTEST SOLUTION :v)
			// Query round of 16
// 			$sql = 'SELECT
// 					mb.country1_code,
// 					mb.country2_code,
// 					mb.country1_name,
// 					mb.country2_name
// 				FROM
// 					matches_bets mb
// 				INNER JOIN matches m ON mb.match_id = m.id
// 				WHERE
// 					m.round_of_16 = 1
// 				AND mb.user_id = ' . $user[User::F_ID] . ';';
			$sql = 'SELECT
						country1_code,
						country2_code,
						country1_name,
						country2_name
				FROM
					matches
				WHERE
					round_of_16 = 1';
			
			$result = DBHandler::connect()->query($sql);
			$roundOf16 = array();
			foreach ($result->fetchAll(PDO::FETCH_ASSOC) as $match) {
				$roundOf16[] = array($match['country1_code'], $match['country2_code']);
			}
// 			$empty = false;
			
// 		} else {
// 			$roundOf16 = @$_SESSION['roundOf16'];
// 			$groupResult =  @$_SESSION['groupResult'];
// 			$empty = (empty($roundOf16) || empty($groupResult));
// 		}
// 	} else {
		
// 		$roundOf16 = json_decode($roundOf16, true);
// 		$groupResult = json_decode($groupResult, true);
// 		$empty = (!is_array($roundOf16) || !is_array($groupResult['A'][0]));
		
// 	}
	
// 	if ($empty) {
// 		echo '<html>
// 				<head>
// 				<script type="text/javascript">
// 					alert("You must done the Group stage first");
// 					location.replace("' . ROOT_URL . '");
// 				</script>
// 				</head>
// 			</html>';
// 			exit;
// 	} else {
		
// 		if ($user !== null) {
			
			// BECAUSE THE REAL GROUP STAGE DONE (FASTEST SOLUTION :v)
// 			if (is_array($groupResult)) {
// 				DBHandler::updateGroupStage($groupResult);
// 			}
			
// 			if (is_array($roundOf16)) {
// 				DBHandler::updateRoundOf16($roundOf16);
// 			}
		
// 		}
		
		//@$_SESSION['groupResult'] = $groupResult;
		//@$_SESSION['roundOf16'] = $roundOf16;
// 	}
	
	
	$odds = array(
		'br' => 400,
		'ar' => 500,
		'de' => 700,
		'es' => 750,
		'be' => 2100,
		'fr' => 2600,
		'it' => 2900,
		'pt' => 2900,
		'en' => 2900,
		'co' => 3400,
		'nl' => 3400,
		'uy' => 3400,
		'cl' => 5100,
		'ru' => 8100,
		'ch' => 12600,
		'ci' => 12600,
		'ec' => 15100,
		'jp' => 15100,
		'hr' => 15100,
		'mx' => 15100,
		'ba' => 17600,
		'us' => 20100,
		'gh' => 20100,
		'ng' => 25100,
		'gr' => 25100,
		'kr' => 40100,
		'cm' => 50100,
		'au' => 50100,
		'ir' => 150100,
		'dz' => 200100,
		'cr' => 200100,
		'hn' => 300100
	);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>LiveDuel</title>
		<link rel="stylesheet" type="text/css" href="style/core.css" />
		<link rel="stylesheet" type="text/css" href="style/common.css" />
		<link rel="stylesheet" type="text/css" href="style/knockout-stage.css" />
		<link rel="stylesheet" type="text/css" href="style/fonts.css" />
		<link rel="stylesheet" type="text/css" href="style/flags.css" />
		<link rel="stylesheet" type="text/css" href="style/tooltipster.css" />
		<link rel="stylesheet" type="text/css" href="style/jquery.fancybox.css" />

		<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" />
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>

		<script src="script/jquery.tooltipster.min.js"></script>
		<script src="script/jquery.fancybox.pack.js"></script>
		<!-- <script src="script/jquery.autotab.min.js"></script> -->
		<script src="script/core.js"></script>
		<script src="script/knockout-stage.js"></script>
		
		<script type="text/javascript">
			var roundOf16 = <?php echo json_encode($roundOf16) ?> ;
			var knockoutResult = <?php echo json_encode($knockoutResult) ?>;
		</script>

	</head>
	<body>
		
		<?php require 'user-bar.php'; ?>
	
		<div id="container" class="container">
			<form autocomplete="off">
			<!-- Header -->
			<div id="header-container">
				<div id="header" class="header">
					
					<a id="logo_link" href="index.php">&nbsp;</a>
				
					<div id="knockout_title">
						&nbsp;
					</div>
					
				<?php if (Session::isUserLogined()) : ?>
					<div id="local_time_box" class="liveduel-radio-box timezone-radio-box">
						<input id="local_time_radio" type="radio" class="liveduel-radio" checked="checked" name="timezone_check">
						<label class="liveduel-radio-label" for="local_time_radio">Local time</label>
					</div>
					
					<div id="your_time_box" class="liveduel-radio-box timezone-radio-box">
						<input id="your_time_radio" type="radio" class="liveduel-radio" name="timezone_check">
						<label class="liveduel-radio-label" for="your_time_radio">Your timezone</label>
					</div>
				<?php endif;?>
				</div>
			</div>
			<!-- Content -->
			<div id="content">
			
				<div class="knockout-area">
			
					<!-- knockout - stage1 - left area -->
					<div class="match-box col1 left-box row1 enabled" id="match-11" order="1">
						<div class="team-and-round">
							<div class="round">Round of 16</div>
							<div class="team team1">
								<div id="flag11-1" class="match-flag no-flag"></div>
								<div class="team-name" id="name1"></div>
							</div>
							<div class="team team2">
								<div id="flag11-2" class="match-flag no-flag"></div>
								<div class="team-name" id="name2"></div>
							</div>
						</div>
						<div class="result-ctn final">
							FN
							<input type="text" value="?" class="match-result final-result final-result1 _top" maxlength="2" readonly="readonly" />
							<input type="text" value="?" class="match-result final-result final-result2 _bot" maxlength="2" readonly="readonly" />
						</div>
						<div class="result-ctn fulltime">
							FT
							<input id="score11-1" type="text" class="match-result fulltime-result fulltime-result1 _top" maxlength="1" />
							<input id="score11-2" type="text" class="match-result fulltime-result fulltime-result2 _bot" maxlength="1" />
						</div>
						<div class="result-ctn extra">
							E
							<input type="text" maxlength="1" class="match-result extra-result extra-result1 _top" />
							<input type="text" maxlength="1" class="match-result extra-result extra-result2 _bot" />
						</div>
						<div class="result-ctn penalty">
							P
							<input type="text" maxlength="1" class="match-result penalty-result penalty-result1 _top" />
							<input type="text" maxlength="1" class="match-result penalty-result penalty-result2 _bot" />
						</div>
						<div class="time-and-place">
							place and time
						</div>
						<input type="hidden" class="timezone" />
					</div>

					<div class="match-box col1 left-box row2 enabled" id="match-12" order="2">
						<div class="team-and-round">
							<div class="round">Round of 16</div>
							<div class="team team1">
								<div id="flag12-1" class="match-flag no-flag"></div>
								<div class="team-name" id="name1"></div>
							</div>
							<div class="team team2">
								<div id="flag12-2" class="match-flag no-flag"></div>
								<div class="team-name" id="name2"></div>
							</div>
						</div>
						<div class="result-ctn final">
							FN
							<input type="text" value="?" class="match-result final-result final-result1 _top" maxlength="2" readonly="readonly" />
							<input type="text" value="?" class="match-result final-result final-result2 _bot" maxlength="2" readonly="readonly" />
						</div>
						<div class="result-ctn fulltime">
							FT
							<input id="score12-1" type="text" class="match-result fulltime-result fulltime-result1 _top" maxlength="1" />
							<input id="score12-2" type="text" class="match-result fulltime-result fulltime-result2 _bot" maxlength="1" />
						</div>
						<div class="result-ctn extra">
							E
							<input type="text" maxlength="1" class="match-result extra-result extra-result1 _top" />
							<input type="text" maxlength="1" class="match-result extra-result extra-result2 _bot" />
						</div>
						<div class="result-ctn penalty">
							P
							<input type="text" maxlength="1" class="match-result penalty-result penalty-result1 _top" />
							<input type="text" maxlength="1" class="match-result penalty-result penalty-result2 _bot" />
						</div>
						<div class="time-and-place">
							place and time
						</div>
						<input type="hidden" class="timezone" />
					</div>

					<div class="match-box col1 left-box row3 enabled" id="match-13" order="3">
						<div class="team-and-round">
							<div class="round">Round of 16</div>
							<div class="team team1">
								<div id="flag13-1" class="match-flag no-flag"></div>
								<div class="team-name" id="name1"></div>
							</div>
							<div class="team team2">
								<div id="flag13-2" class="match-flag no-flag"></div>
								<div class="team-name" id="name2"></div>
							</div>
						</div>
						<div class="result-ctn final">
							FN
							<input type="text" value="?" class="match-result final-result final-result1 _top" maxlength="2" readonly="readonly" />
							<input type="text" value="?" class="match-result final-result final-result2 _bot" maxlength="2" readonly="readonly" />
						</div>
						<div class="result-ctn fulltime">
							FT
							<input id="score13-1" type="text" class="match-result fulltime-result fulltime-result1 _top" maxlength="1" />
							<input id="score13-2" type="text" class="match-result fulltime-result fulltime-result2 _bot" maxlength="1" />
						</div>
						<div class="result-ctn extra">
							E
							<input type="text" maxlength="1" class="match-result extra-result extra-result1 _top" />
							<input type="text" maxlength="1" class="match-result extra-result extra-result2 _bot" />
						</div>
						<div class="result-ctn penalty">
							P
							<input type="text" maxlength="1" class="match-result penalty-result penalty-result1 _top" />
							<input type="text" maxlength="1" class="match-result penalty-result penalty-result2 _bot" />
						</div>
						<div class="time-and-place">
							place and time
						</div>
						<input type="hidden" class="timezone" />
					</div>

					<div class="match-box col1 left-box row4 enabled" id="match-14" order="4">
						<div class="team-and-round">
							<div class="round">Round of 16</div>
							<div class="team team1">
								<div id="flag14-1" class="match-flag no-flag"></div>
								<div class="team-name" id="name1"></div>
							</div>
							<div class="team team2">
								<div id="flag14-2" class="match-flag no-flag"></div>
								<div class="team-name" id="name2"></div>
							</div>
						</div>
						<div class="result-ctn final">
							FN
							<input type="text" value="?" class="match-result final-result final-result1 _top" maxlength="2" readonly="readonly" />
							<input type="text" value="?" class="match-result final-result final-result2 _bot" maxlength="2" readonly="readonly" />
						</div>
						<div class="result-ctn fulltime">
							FT
							<input id="score14-1" type="text" class="match-result fulltime-result fulltime-result1 _top" maxlength="1" />
							<input id="score14-2" type="text" class="match-result fulltime-result fulltime-result2 _bot" maxlength="1" />
						</div>
						<div class="result-ctn extra">
							E
							<input type="text" maxlength="1" class="match-result extra-result extra-result1 _top" />
							<input type="text" maxlength="1" class="match-result extra-result extra-result2 _bot" />
						</div>
						<div class="result-ctn penalty">
							P
							<input type="text" maxlength="1" class="match-result penalty-result penalty-result1 _top" />
							<input type="text" maxlength="1" class="match-result penalty-result penalty-result2 _bot" />
						</div>
						<div class="time-and-place">
							place and time
						</div>
						<input type="hidden" class="timezone" />
					</div>


					<!-- knockout - stage 1 - right area -->
					<div class="match-box col7 right-box row1 enabled" id="match-15" order="8">
						<div class="team-and-round">
							<div class="round">Round of 16</div>
							<div class="team team1">
								<div id="flag15-1" class="match-flag no-flag"></div>
								<div class="team-name" id="name1"></div>
							</div>
							<div class="team team2">
								<div id="flag15-2" class="match-flag no-flag"></div>
								<div class="team-name" id="name2"></div>
							</div>
						</div>
						<div class="result-ctn final">
							FN
							<input type="text" value="?" class="match-result final-result final-result1 _top" maxlength="2" readonly="readonly" />
							<input type="text" value="?" class="match-result final-result final-result2 _bot" maxlength="2" readonly="readonly" />
						</div>
						<div class="result-ctn fulltime">
							FT
							<input id="score15-1" type="text" class="match-result fulltime-result fulltime-result1 _top" maxlength="1" />
							<input id="score15-2" type="text" class="match-result fulltime-result fulltime-result2 _bot" maxlength="1" />
						</div>
						<div class="result-ctn extra">
							E
							<input type="text" maxlength="1" class="match-result extra-result extra-result1 _top" />
							<input type="text" maxlength="1" class="match-result extra-result extra-result2 _bot" />
						</div>
						<div class="result-ctn penalty">
							P
							<input type="text" maxlength="1" class="match-result penalty-result penalty-result1 _top" />
							<input type="text" maxlength="1" class="match-result penalty-result penalty-result2 _bot" />
						</div>
						<div class="time-and-place">
							place and time
						</div>
						<input type="hidden" class="timezone" />
					</div>

					<div class="match-box col7 right-box row2 enabled" id="match-16" order="9">
						<div class="team-and-round">
							<div class="round">Round of 16</div>
							<div class="team team1">
								<div id="flag16-1" class="match-flag no-flag"></div>
								<div class="team-name" id="name1"></div>
							</div>
							<div class="team team2">
								<div id="flag16-2" class="match-flag no-flag"></div>
								<div class="team-name" id="name2"></div>
							</div>
						</div>
						<div class="result-ctn final">
							FN
							<input type="text" value="?" class="match-result final-result final-result1 _top" maxlength="2" readonly="readonly" />
							<input type="text" value="?" class="match-result final-result final-result2 _bot" maxlength="2" readonly="readonly" />
						</div>
						<div class="result-ctn fulltime">
							FT
							<input id="score16-1" type="text" class="match-result fulltime-result fulltime-result1 _top" maxlength="1" />
							<input id="score16-2" type="text" class="match-result fulltime-result fulltime-result2 _bot" maxlength="1" />
						</div>
						<div class="result-ctn extra">
							E
							<input type="text" maxlength="1" class="match-result extra-result extra-result1 _top" />
							<input type="text" maxlength="1" class="match-result extra-result extra-result2 _bot" />
						</div>
						<div class="result-ctn penalty">
							P
							<input type="text" maxlength="1" class="match-result penalty-result penalty-result1 _top" />
							<input type="text" maxlength="1" class="match-result penalty-result penalty-result2 _bot" />
						</div>
						<div class="time-and-place">
							place and time
						</div>
						<input type="hidden" class="timezone" />
					</div>

					<div class="match-box col7 right-box row3 enabled" id="match-17" order="10">
						<div class="team-and-round">
							<div class="round">Round of 16</div>
							<div class="team team1">
								<div id="flag17-1" class="match-flag no-flag"></div>
								<div class="team-name" id="name1"></div>
							</div>
							<div class="team team2">
								<div id="flag17-2" class="match-flag no-flag"></div>
								<div class="team-name" id="name2"></div>
							</div>
						</div>
						<div class="result-ctn final">
							FN
							<input type="text" value="?" class="match-result final-result final-result1 _top" maxlength="2" readonly="readonly" />
							<input type="text" value="?" class="match-result final-result final-result2 _bot" maxlength="2" readonly="readonly" />
						</div>
						<div class="result-ctn fulltime">
							FT
							<input id="score17-1" type="text" class="match-result fulltime-result fulltime-result1 _top" maxlength="1" />
							<input id="score17-2" type="text" class="match-result fulltime-result fulltime-result2 _bot" maxlength="1" />
						</div>
						<div class="result-ctn extra">
							E
							<input type="text" maxlength="1" class="match-result extra-result extra-result1 _top" />
							<input type="text" maxlength="1" class="match-result extra-result extra-result2 _bot" />
						</div>
						<div class="result-ctn penalty">
							P
							<input type="text" maxlength="1" class="match-result penalty-result penalty-result1 _top" />
							<input type="text" maxlength="1" class="match-result penalty-result penalty-result2 _bot" />
						</div>
						<div class="time-and-place">
							place and time
						</div>
						<input type="hidden" class="timezone" />
					</div>

					<div class="match-box col7 right-box row4 enabled" id="match-18" order="11">
						<div class="team-and-round">
							<div class="round">Round of 16</div>
							<div class="team team1">
								<div id="flag18-1" class="match-flag no-flag"></div>
								<div class="team-name" id="name1"></div>
							</div>
							<div class="team team2">
								<div id="flag18-2" class="match-flag no-flag"></div>
								<div class="team-name" id="name2"></div>
							</div>
						</div>
						<div class="result-ctn final">
							FN
							<input type="text" value="?" class="match-result final-result final-result1 _top" maxlength="2" readonly="readonly" />
							<input type="text" value="?" class="match-result final-result final-result2 _bot" maxlength="2" readonly="readonly" />
						</div>
						<div class="result-ctn fulltime">
							FT
							<input id="score18-1" type="text" class="match-result fulltime-result fulltime-result1 _top" maxlength="1" />
							<input id="score18-2" type="text" class="match-result fulltime-result fulltime-result2 _bot" maxlength="1" />
						</div>
						<div class="result-ctn extra">
							E
							<input type="text" maxlength="1" class="match-result extra-result extra-result1 _top" />
							<input type="text" maxlength="1" class="match-result extra-result extra-result2 _bot" />
						</div>
						<div class="result-ctn penalty">
							P
							<input type="text" maxlength="1" class="match-result penalty-result penalty-result1 _top" />
							<input type="text" maxlength="1" class="match-result penalty-result penalty-result2 _bot" />
						</div>
						<div class="time-and-place">
							place and time
						</div>
						<input type="hidden" class="timezone" />
					</div>


					<!-- knockout - stage 2 - left area -->
					<div class="match-box col2 left-box row21" id="match-21" order="5">
						<div class="team-and-round">
							<div class="round">Round of 16</div>
							<div class="team team1">
								<div id="flag21" class="match-flag no-flag"></div>
								<div class="team-name" id="name21"></div>
							</div>
							<div class="team team2">
								<div id="flag22" class="match-flag no-flag"></div>
								<div class="team-name" id="name22"></div>
							</div>
						</div>
						<div class="result-ctn final">
							FN
							<input type="text" value="?" class="match-result final-result final-result1 _top" maxlength="2" readonly="readonly" />
							<input type="text" value="?" class="match-result final-result final-result2 _bot" maxlength="2" readonly="readonly" />
						</div>
						<div class="result-ctn fulltime">
							FT
							<input id="score21-1" type="text" class="match-result fulltime-result fulltime-result1 _top" maxlength="1" />
							<input id="score21-2" type="text" class="match-result fulltime-result fulltime-result2 _bot" maxlength="1" />
						</div>
						<div class="result-ctn extra">
							E
							<input type="text" maxlength="1" class="match-result extra-result extra-result1 _top" />
							<input type="text" maxlength="1" class="match-result extra-result extra-result2 _bot" />
						</div>
						<div class="result-ctn penalty">
							P
							<input type="text" maxlength="1" class="match-result penalty-result penalty-result1 _top" />
							<input type="text" maxlength="1" class="match-result penalty-result penalty-result2 _bot" />
						</div>
						<div class="time-and-place">
							place and time
						</div>
						<input type="hidden" class="timezone" />
					</div>

					<div class="match-box col2 left-box row22" id="match-22" order="6">
						<div class="team-and-round">
							<div class="round">Round of 16</div>
							<div class="team team1">
								<div id="flag23" class="match-flag no-flag"></div>
								<div id="name23" class="team-name"></div>
							</div>
							<div class="team team2">
								<div id="flag24" class="match-flag no-flag"></div>
								<div id="name24" class="team-name"></div>
							</div>
						</div>
						<div class="result-ctn final">
							FN
							<input type="text" value="?" class="match-result final-result final-result1 _top" maxlength="2" readonly="readonly" />
							<input type="text" value="?" class="match-result final-result final-result2 _bot" maxlength="2" readonly="readonly" />
						</div>
						<div class="result-ctn fulltime">
							FT
							<input id="score22-1" type="text" class="match-result fulltime-result fulltime-result1 _top" maxlength="1" />
							<input id="score22-2" type="text" class="match-result fulltime-result fulltime-result2 _bot" maxlength="1" />
						</div>
						<div class="result-ctn extra">
							E
							<input type="text" maxlength="1" class="match-result extra-result extra-result1 _top" />
							<input type="text" maxlength="1" class="match-result extra-result extra-result2 _bot" />
						</div>
						<div class="result-ctn penalty">
							P
							<input type="text" maxlength="1" class="match-result penalty-result penalty-result1 _top" />
							<input type="text" maxlength="1" class="match-result penalty-result penalty-result2 _bot" />
						</div>
						<div class="time-and-place">
							place and time
						</div>
						<input type="hidden" class="timezone" />
					</div>

					<!-- knockout - stage 2 - right area -->
					<div class="match-box col6 right-box row21" id="match-23" order="12">
						<div class="team-and-round">
							<div class="round">Round of 16</div>
							<div class="team team1">
								<div id="flag25" class="match-flag no-flag"></div>
								<div class="team-name" id="name25"></div>
							</div>
							<div class="team team2">
								<div id="flag26" class="match-flag no-flag"></div>
								<div class="team-name" id="name26"></div>
							</div>
						</div>
						<div class="result-ctn final">
							FN
							<input type="text" value="?" class="match-result final-result final-result1 _top" maxlength="2" readonly="readonly" />
							<input type="text" value="?" class="match-result final-result final-result2 _bot" maxlength="2" readonly="readonly" />
						</div>
						<div class="result-ctn fulltime">
							FT
							<input id="score23-1" type="text" class="match-result fulltime-result fulltime-result1 _top" maxlength="1" />
							<input id="score23-2" type="text" class="match-result fulltime-result fulltime-result2 _bot" maxlength="1" />
						</div>
						<div class="result-ctn extra">
							E
							<input type="text" maxlength="1" class="match-result extra-result extra-result1 _top" />
							<input type="text" maxlength="1" class="match-result extra-result extra-result2 _bot" />
						</div>
						<div class="result-ctn penalty">
							P
							<input type="text" maxlength="1" class="match-result penalty-result penalty-result1 _top" />
							<input type="text" maxlength="1" class="match-result penalty-result penalty-result2 _bot" />
						</div>
						<div class="time-and-place">
							place and time
						</div>
						<input type="hidden" class="timezone" />
					</div>

					<div class="match-box col6 right-box row22" id="match-24" order="13">
						<div class="team-and-round">
							<div class="round">Round of 16</div>
							<div class="team team1">
								<div id="flag27" class="match-flag no-flag"></div>
								<div class="team-name" id="name27"></div>
							</div>
							<div class="team team2">
								<div id="flag28" class="match-flag no-flag"></div>
								<div class="team-name" id="name28"></div>
							</div>
						</div>
						<div class="result-ctn final">
							FN
							<input type="text" value="?" class="match-result final-result final-result1 _top" maxlength="2" readonly="readonly" />
							<input type="text" value="?" class="match-result final-result final-result2 _bot" maxlength="2" readonly="readonly" />
						</div>
						<div class="result-ctn fulltime">
							FT
							<input id="score24-1" type="text" class="match-result fulltime-result fulltime-result1 _top" maxlength="1" />
							<input id="score24-2" type="text" class="match-result fulltime-result fulltime-result2 _bot" maxlength="1" />
						</div>
						<div class="result-ctn extra">
							E
							<input type="text" maxlength="1" class="match-result extra-result extra-result1 _top" />
							<input type="text" maxlength="1" class="match-result extra-result extra-result2 _bot" />
						</div>
						<div class="result-ctn penalty">
							P
							<input type="text" maxlength="1" class="match-result penalty-result penalty-result1 _top" />
							<input type="text" maxlength="1" class="match-result penalty-result penalty-result2 _bot" />
						</div>
						<div class="time-and-place">
							place and time
						</div>
						<input type="hidden" class="timezone" />
					</div>


					<!-- knockout - stage 3 - left area -->
					<div class="match-box col3 left-box" id="match-31" order="7">
						<div class="team-and-round">
							<div class="round">Round of 16</div>
							<div class="team team1">
								<div id="flag31" class="match-flag no-flag"></div>
								<div class="team-name" id="name31"></div>
							</div>
							<div class="team team2">
								<div id="flag32" class="match-flag no-flag"></div>
								<div class="team-name" id="name32"></div>
							</div>
						</div>
						<div class="result-ctn final">
							FN
							<input type="text" value="?" class="match-result final-result final-result1 _top" maxlength="2" readonly="readonly" />
							<input type="text" value="?" class="match-result final-result final-result2 _bot" maxlength="2" readonly="readonly" />
						</div>
						<div class="result-ctn fulltime">
							FT
							<input id="score31-1" type="text" class="match-result fulltime-result fulltime-result1 _top" maxlength="1" />
							<input id="score31-2" type="text" class="match-result fulltime-result fulltime-result2 _bot" maxlength="1" />
						</div>
						<div class="result-ctn extra">
							E
							<input type="text" maxlength="1" class="match-result extra-result extra-result1 _top" />
							<input type="text" maxlength="1" class="match-result extra-result extra-result2 _bot" />
						</div>
						<div class="result-ctn penalty">
							P
							<input type="text" maxlength="1" class="match-result penalty-result penalty-result1 _top" />
							<input type="text" maxlength="1" class="match-result penalty-result penalty-result2 _bot" />
						</div>
						<div class="time-and-place">
							place and time
						</div>
						<input type="hidden" class="timezone" />
					</div>

					<!-- knockout - stage 3 - right area -->
					<div class="match-box col5 right-box" id="match-32" order="14">
						<div class="team-and-round">
							<div class="round">Round of 16</div>
							<div class="team team1">
								<div id="flag33" class="match-flag no-flag"></div>
								<div class="team-name" id="name33"></div>
							</div>
							<div class="team team2">
								<div id="flag34" class="match-flag no-flag"></div>
								<div class="team-name" id="name34"></div>
							</div>
						</div>
						<div class="result-ctn final">
							FN
							<input type="text" value="?" class="match-result final-result final-result1 _top" maxlength="2" readonly="readonly" />
							<input type="text" value="?" class="match-result final-result final-result2 _bot" maxlength="2" readonly="readonly" />
						</div>
						<div class="result-ctn fulltime">
							FT
							<input id="score32-1" type="text" class="match-result fulltime-result fulltime-result1 _top" maxlength="1" />
							<input id="score32-2" type="text" class="match-result fulltime-result fulltime-result2 _bot" maxlength="1" />
						</div>
						<div class="result-ctn extra">
							E
							<input type="text" maxlength="1" class="match-result extra-result extra-result1 _top" />
							<input type="text" maxlength="1" class="match-result extra-result extra-result2 _bot" />
						</div>
						<div class="result-ctn penalty">
							P
							<input type="text" maxlength="1" class="match-result penalty-result penalty-result1 _top" />
							<input type="text" maxlength="1" class="match-result penalty-result penalty-result2 _bot" />
						</div>
						<div class="time-and-place">
							place and time
						</div>
						<input type="hidden" class="timezone" />
					</div>

					<!-- knockout - finals -->
					<div class="match-box final1 left-box" id="match-41" order="15">
						<div class="team-and-round">
							<div class="round">Round of 16</div>
							<div class="team team1">
								<div id="flag-final1-1" class="match-flag no-flag"></div>
								<div id="name-final1-1" class="team-name"></div>
							</div>
							<div class="team team2">
								<div id="flag-final1-2" class="match-flag no-flag"></div>
								<div id="name-final1-2" class="team-name"></div>
							</div>
						</div>
						<div class="result-ctn final">
							FN
							<input type="text" value="?" class="match-result final-result final-result1 _top" maxlength="2" readonly="readonly" />
							<input type="text" value="?" class="match-result final-result final-result2 _bot" maxlength="2" readonly="readonly" />
						</div>
						<div class="result-ctn fulltime">
							FT
							<input id="score-final1-1" type="text" class="match-result fulltime-result fulltime-result1 _top" maxlength="1" />
							<input id="score-final1-2" type="text" class="match-result fulltime-result fulltime-result2 _bot" maxlength="1" />
						</div>
						<div class="result-ctn extra">
							E
							<input type="text" maxlength="1" class="match-result extra-result extra-result1 _top" />
							<input type="text" maxlength="1" class="match-result extra-result extra-result2 _bot" />
						</div>
						<div class="result-ctn penalty">
							P
							<input type="text" maxlength="1" class="match-result penalty-result penalty-result1 _top" />
							<input type="text" maxlength="1" class="match-result penalty-result penalty-result2 _bot" />
						</div>
						<div class="time-and-place">
							place and time
						</div>
						<input type="hidden" class="timezone" />
					</div>

					<div class="match-box final2 right-box" id="match-42" order="16">
						<div class="team-and-round">
							<div class="round">Round of 16</div>
							<div class="team team1">
								<div id="flag-final2-1" class="match-flag no-flag"></div>
								<div id="name-final2-1" class="team-name"></div>
							</div>
							<div class="team team2">
								<div id="flag-final2-2" class="match-flag no-flag"></div>
								<div id="name-final2-2" class="team-name"></div>
							</div>
						</div>
						<div class="result-ctn final">
							FN
							<input type="text" value="?" class="match-result final-result final-result1 _top" maxlength="2" readonly="readonly" />
							<input type="text" value="?" class="match-result final-result final-result2 _bot" maxlength="2" readonly="readonly" />
						</div>
						<div class="result-ctn fulltime">
							FT
							<input id="score-final2-1" type="text" class="match-result fulltime-result fulltime-result1 _top" maxlength="1" />
							<input id="score-final2-2" type="text" class="match-result fulltime-result fulltime-result2 _bot" maxlength="1" />
						</div>
						<div class="result-ctn extra">
							E
							<input type="text" maxlength="1" class="match-result extra-result extra-result1 _top" />
							<input type="text" maxlength="1" class="match-result extra-result extra-result2 _bot" />
						</div>
						<div class="result-ctn penalty">
							P
							<input type="text" maxlength="1" class="match-result penalty-result penalty-result1 _top" />
							<input type="text" maxlength="1" class="match-result penalty-result penalty-result2 _bot" />
						</div>
						<div class="time-and-place">
							place and time
						</div>
						<input type="hidden" class="timezone" />
					</div>

					<!-- brackets (only for graphic purposes) -->
					<div id="bracket11" class="bracket row1 top right"></div>
					<div id="bracket12" class="bracket row2 bottom right"></div>
					<div id="bracket13" class="bracket row3 top right"></div>
					<div id="bracket14" class="bracket row4 bottom right"></div>
					<div id="bracket15" class="bracket row1 top left"></div>
					<div id="bracket16" class="bracket row2 bottom left"></div>
					<div id="bracket17" class="bracket row3 top left"></div>
					<div id="bracket18" class="bracket row4 bottom left"></div>

					<div id="bracket21" class="bracket top right"></div>
					<div id="bracket22" class="bracket bottom right"></div>
					<div id="bracket23" class="bracket top left"></div>
					<div id="bracket24" class="bracket bottom left"></div>

					<div id="bracket3-hor-left" class="bracket horiz top"></div>
					<div id="bracket3-hor-right" class="bracket horiz top"></div>
					<div id="bracket3-vertical" class="bracket left"></div>

				</div>
				<!-- div.knockout-area -->
				
				<div class="knockout-area-foot">
					<div style="border-top: 1px solid #30DE74; height: 0; margin: 10px auto 0; padding: 0; width: 96%;">&nbsp;</div>
					
					<button id="go_group_stage" type="button" class="liveduel-button"
						onclick="location.href='index.php'">Go Group Stage</button>
						
					<?php if (Session::isAdminLogined()) { ?>
						<button type="button" id="save_the_real" class="liveduel-button save-all-button">Save the real</button>
					<?php } else { ?>
						
					<button id="fill_random_btn" type="button" class="liveduel-button random-score-button">Fill random scores</button>
<!-- 					<button id="clear_btn" type="button" class="liveduel-button clear-score-button">Clear scores</button> -->
					
					<?php } ?>
					
					<?php if (Session::isUserLogined() && !Session::isAdminLogined()) : ?>
						<button type="button" id="save_all_popup_opener" class="liveduel-button save-all-button">Save result</button>
					<?php endif; ?>
					
				</div>
				
			</div>
			<!-- div#content -->
		</form>
		</div>
		<!-- div#container -->
		
		<div id="done_popup" style="display: none; width: 350px">
			<div class=" winner-name center" style="padding: 5px 0; margin: 0; font-size: 18px;">
			
			</div>
			<div class=" winner-jersey" style="margin: 0 auto; width: 100px; height: 109px;">
			</div>
			<div class="odds" style="margin: 7px auto; font-size: 18px; text-align: center;">
			</div>
			<div class="button-bar">
				<button type="button" id="save_knockout" class="button popup-button">Save your choosen winner</button>
			</div>
		</div>
		
		<div id="overlay"></div>
		
	</body>
</html>