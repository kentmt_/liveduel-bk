<!DOCTYPE>
<html>
<head>
	<title>2014 FIFA World Cup Knockout Stage Simulator</title>
	<link href="css/knockout_styles.css" rel="stylesheet">
	<link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.3.custom.css">
	<script src="javascript/knockout_script.js"></script>
	<!-- JQuery JS -->
		<script src="bootstrap/jquery-1.js"></script>
		<!-- Bootstrap -->
		<link href="bootstrap/bootstrap.css" rel="stylesheet">
		<script src="bootstrap/bootstrap.js"></script>
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
</head>
<body class="col2d soccer worldcup">
<center>
	<div id="container">
		<div id="main">
			<h2>2014 FIFA World Cup Knockout Stage Simulator</h2>
			<div id="t4">
				<div class="ks ks-r16 ks-1A2B rtxt" title="Round of 16">
					<div class="dt">06/28<br>18:00</div>
					<div class="team ks-1A"><input data-tc="HR" data-rank="1A" pattern="[0-9]*" maxlength="1" type="text">
						<span class="f"><div class="flag-24 flag-24-HR"></div></span>
						<span class="n tc-HR">Croatia</span>
					</div>
					<div class="team ks-2B"><input data-tc="CL" data-rank="2B" pattern="[0-9]*" maxlength="1" type="text">
						<span class="f"><div class="flag-24 flag-24-CL"></div></span>
						<span class="n tc-CL">Chile</span>
					</div>
				</div>
				<div class="ks ks-r16 ks-1C2D rtxt" title="Round of 16">
					<div class="dt">06/28<br>22:00</div>
					<div class="team ks-1C">
						<input data-tc="CO" data-rank="1C" pattern="[0-9]*" maxlength="1" type="text">
						<span class="f"><div class="flag-24 flag-24-CO"></div></span>
						<span class="n tc-CO">Colombia</span>
					</div>
					<div class="team ks-2D">
						<input data-tc="IT" data-rank="2D" pattern="[0-9]*" maxlength="1" type="text">
						<span class="f"><div class="flag-24 flag-24-IT"></div></span>
						<span class="n tc-IT">Italy</span>
					</div>
				</div>
				<div class="ks ks-r16 ks-1E2F rtxt" title="Round of 16">
					<div class="dt">06/30<br>18:00</div>
					<div class="team ks-1E">
						<input data-tc="FR" data-rank="1E" pattern="[0-9]*" maxlength="1" type="text">
						<span class="f"><div class="flag-24 flag-24-FR"></div></span>
						<span class="n tc-FR">France</span>
					</div>
					<div class="team ks-2F">
						<input data-tc="NG" data-rank="2F" pattern="[0-9]*" maxlength="1" type="text">
						<span class="f"><div class="flag-24 flag-24-NG"></div></span>
						<span class="n tc-NG">Nigeria</span>
					</div>
				</div>
				<div class="ks ks-r16 ks-1G2H rtxt" title="Round of 16">
				<div class="dt">06/30<br>22:00</div>
					<div class="team ks-1G">
						<input data-tc="PT" data-rank="1G" pattern="[0-9]*" maxlength="1" type="text">
						<span class="f"><div class="flag-24 flag-24-PT"></div></span>
						<span class="n tc-PT">Portugal</span>
					</div>
					<div class="team ks-2H">
						<input data-tc="KR" data-rank="2H" pattern="[0-9]*" maxlength="1" type="text">
						<span class="f"><div class="flag-24 flag-24-KR"></div></span>
						<span class="n tc-KR">Korea Republic</span>
					</div>
				</div>
				<div class="ks ks-r16 ks-1B2A ltxt" title="Round of 16">
					<div class="dt">06/29<br>18:00</div>
					<div class="team ks-1B">
						<input data-tc="NL" data-rank="1B" pattern="[0-9]*" maxlength="1" type="text">
						<span class="f"><div class="flag-24 flag-24-NL"></div></span>
						<span class="n tc-NL">Netherlands</span>
					</div>
					<div class="team ks-2A">
						<input data-tc="BR" data-rank="2A" pattern="[0-9]*" maxlength="1" type="text">
						<span class="f"><div class="flag-24 flag-24-BR"></div></span>
						<span class="n tc-BR">Brazil</span>
					</div>
				</div>
				<div class="ks ks-r16 ks-1D2C ltxt" title="Round of 16">
					<div class="dt">06/29<br>22:00</div>
					<div class="team ks-1D">
						<input data-tc="UY" data-rank="1D" pattern="[0-9]*" maxlength="1" type="text">
						<span class="f"><div class="flag-24 flag-24-UY"></div></span>
						<span class="n tc-UY">Uruguay</span>
					</div>
					<div class="team ks-2C">
						<input data-tc="CI" data-rank="2C" pattern="[0-9]*" maxlength="1" type="text">
						<span class="f"><div class="flag-24 flag-24-CI"></div></span>
						<span class="n tc-CI">Côte d'Ivoire</span>
					</div>
				</div>
				<div class="ks ks-r16 ks-1F2E ltxt" title="Round of 16">
					<div class="dt">07/01<br>18:00</div>
					<div class="team ks-1F">
						<input data-tc="BA" data-rank="1F" pattern="[0-9]*" maxlength="1" type="text">
						<span class="f"><div class="flag-24 flag-24-BA"></div></span>
						<span class="n tc-BA">Bosnia-Herzeg.</span>
					</div>
					<div class="team ks-2E">
						<input data-tc="EC" data-rank="2E" pattern="[0-9]*" maxlength="1" type="text">
						<span class="f"><div class="flag-24 flag-24-EC"></div></span>
						<span class="n tc-EC">Ecuador</span>
					</div>
				</div>
				<div class="ks ks-r16 ks-1H2G ltxt" title="Round of 16">
					<div class="dt">07/01<br>22:00</div>
					<div class="team ks-1H">
						<input data-tc="RU" data-rank="1H" pattern="[0-9]*" maxlength="1" type="text">
						<span class="f"><div class="flag-24 flag-24-RU"></div></span>
						<span class="n tc-RU">Russia</span>
					</div>
					<div class="team ks-2G">
						<input data-tc="DE" data-rank="2G" pattern="[0-9]*" maxlength="1" type="text">
						<span class="f"><div class="flag-24 flag-24-DE"></div></span>
						<span class="n tc-DE">Germany</span>
					</div>
				</div>
				<div class="ks ks-qf ks-1A2B1C2D rtxt" title="Quarter-finals">
					<div class="dt">07/04<br>22:00</div>
					<div class="team ks-1A2B-w">
						<input data-rank="1A2B" pattern="[0-9]*" maxlength="1" type="text">
						<span class="f"></span>
					</div>
					<div class="team ks-C ks-D ks-1C2D-w">
						<input data-rank="1C2D" pattern="[0-9]*" maxlength="1" type="text">
						<span class="f"></span>
					</div>
				</div>
				<div class="ks ks-qf ks-1E2F1G2H rtxt" title="Quarter-finals">
					<div class="dt">07/04<br>18:00</div>
					<div class="team ks-1E2F-w">
						<input data-rank="1E2F" pattern="[0-9]*" maxlength="1" type="text">
						<span class="f"></span>
					</div>
					<div class="team ks-G ks-H ks-1G2H-w">
						<input data-rank="1G2H" pattern="[0-9]*" maxlength="1" type="text">
						<span class="f"></span>
					</div>
				</div>
				<div class="ks ks-qf ks-1B2A1D2C ltxt" title="Quarter-finals">
					<div class="dt">07/05<br>22:00</div>
					<div class="team ks-1B2A-w">
						<input data-rank="1B2A" pattern="[0-9]*" maxlength="1" type="text">
						<span class="f"></span>
					</div>
					<div class="team ks-D ks-C ks-1D2C-w">
						<input data-rank="1D2C" pattern="[0-9]*" maxlength="1" type="text">
						<span class="f"></span>
					</div>
				</div>
				<div class="ks ks-qf ks-1F2E1H2G ltxt" title="Quarter-finals">
					<div class="dt">07/05<br>18:00</div>
					<div class="team ks-1F2E-w">
						<input data-rank="1F2E" pattern="[0-9]*" maxlength="1" type="text">
						<span class="f"></span>
					</div>
					<div class="team ks-H ks-G ks-1H2G-w">
						<input data-rank="1H2G" pattern="[0-9]*" maxlength="1" type="text">
						<span class="f"></span>
					</div>
				</div>
				<div class="ks ks-sf ks-1A2B1C2D1E2F1G2H rtxt" title="Semi-finals">
					<div class="dt">07/08<br>22:00</div>
					<div class="rtxt team ks-1A2B1C2D-w">
						<input data-rank="1A2B1C2D" pattern="[0-9]*" maxlength="1" type="text">
						<span class="f"></span>
					</div>
					<div class="rtxt team ks-1E2F1G2H-w">
						<input data-rank="1E2F1G2H" pattern="[0-9]*" maxlength="1" type="text">
						<span class="f"></span>
					</div>
				</div>
				<div class="ks ks-sf ks-1B2A1D2C1F2E1H2G ltxt" title="Semi-finals">
					<div class="dt">07/09<br>22:00</div>
					<div class="ltxt team ks-1B2A1D2C-w">
						<input data-rank="1B2A1D2C" pattern="[0-9]*" maxlength="1" type="text">
						<span class="f"></span>
					</div>
					<div class="ltxt team ks-1F2E1H2G-w">
						<input data-rank="1F2E1H2G" pattern="[0-9]*" maxlength="1" type="text">
						<span class="f"></span>
					</div>
				</div>
				<div class="ks ks-3RD" title="Match for third place">
					<h2>Match for third place</h2>
					<div class="rtxt team ks-1A2B1C2D1E2F1G2H-l">
						<input data-rank="1A2B1C2D1E2F1G2H" pattern="[0-9]*" maxlength="1" type="text">
						<span class="f"></span>
						<span class="n"></span>
					</div>
					<div class="ltxt team ks-1B2A1D2C1F2E1H2G-l">
						<input data-rank="1B2A1D2C1F2E1H2G" pattern="[0-9]*" maxlength="1" type="text">
						<span class="f"></span>
						<span class="n"></span>
					</div>
					<div class="dt">07/12 22:00</div>
				</div>
				<div class="ks ks-FINAL ks-1A2B1C2D1E2F1G2H1B2A1D2C1F2E1H2G" title="Final">
					<h2>Final</h2>
					<div class="rtxt team ks-1A2B1C2D1E2F1G2H-w">
						<input data-rank="1A2B1C2D1E2F1G2H" pattern="[0-9]*" maxlength="1" type="text" id="final1">
						<span class="f"></span>
						<span class="n" id="finalName1"></span>
					</div>
					<div class="ltxt team ks-1B2A1D2C1F2E1H2G-w">
						<input data-rank="1B2A1D2C1F2E1H2G" pattern="[0-9]*" maxlength="1" type="text" id="final2">
						<span class="f"></span>
						<span class="n" id="finalName2"></span>
					</div>
					<div class="dt">07/13 21:00</div>
				</div>
				<div class="ks-l ks-l-1A2B1C2D"></div>
				<div class="ks-l ks-l-1E2F1G2H"></div>
				<div class="ks-l ks-l-1B2A1D2C"></div>
				<div class="ks-l ks-l-1F2E1H2G"></div>
				<div class="ks-l ks-l-1A2B1C2D1E2F1G2H"></div>
				<div class="ks-l ks-l-1B2A1D2C1F2E1H2G"></div>
				<div class="ks-l ks-l-3RDFINAL"></div>
				<div class="ks-l ks-l-3RD"></div>
				<div class="ks-l ks-l-FINAL"></div>
			</div>
			<div style="width:100%">
				<div style="float:right">
					<a class="btn" style="width:100px" href="javascript:clear()" title="Clear all fields">Clear</a>
					<a class="btn" style="width:130px" href="javascript:randomScores()" title="Clear all fields">Random Score</a>
				</div>
				<label><input class="timezone" name="timezone" value="l" type="radio">local time</label>
				<label><input class="timezone" name="timezone" value="y" checked="checked" type="radio">your time zone</label>
			</div>
		</div>
	</div>
</center>
<div class="modal fade" id="popup1" tabindex="-1" role="dialog" aria-labelledby="popup1Label" aria-hidden="true">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title" id="popup1Label">You may be a winner!</h4>
	  </div>
	  <div class="modal-body">
		The winner is <label id="winner"></label>!
		<div class="jerseypic"></div>
		<div>
			<form role="form" action="insertUser.php" method="post">
				<div class="control-group">
					<label class='control-label'>Sign up</label>
					<div class="controls">
						<input type="text" name="userName" id="userName" class="form-control" placeholder="User Name" required autofocus>
						<input type="email" name="email" id="email" class="form-control" placeholder="Email address" required autofocus>
						<input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
						<input type="password" name="confirmPassword" id="confirmPassword" class="form-control" placeholder="Confirm Password" required>
						<button class="btn btn-primary" type="submit" onclick="return validateRegister();">Sign up</button>
					</div>
				</div>
			</form>
		</div>
	  </div>
	</div>
  </div>
</div>
						
</body>
</html>