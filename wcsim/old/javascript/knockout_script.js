var RANKINGS={BR:"10",AR:"3",CO:"4",UY:"6",ES:"1",DE:"2",BE:"11",CH:"8",JP:"48",AU:"59",IR:"45",KR:"54",US:"14",CR:"31",HN:"41",MX:"20",CL:"15",EC:"23",GH:"24",CM:"51",CI:"17",NG:"36",DZ:"26",BF:"58",RU:"22",NL:"9",IT:"7",BA:"21",_england:"13",HR:"16",FR:"19",UA:"18*",GR:"12",PT:"5"};
var C = {
	"BR": "Brazil",
	"AR": "Argentina",
	"CO": "Colombia",
	"UY": "Uruguay",
	"ES": "Spain",
	"DE": "Germany",
	"BE": "Belgium",
	"CH": "Switzerland",

	"JP": "Japan",
	"AU": "Australia",
	"IR": "Iran",
	"KR": "Korea Republic",

	"US": "USA",
	"CR": "Costa Rica",
	"HN": "Honduras",
	"MX": "Mexico",

	"CL": "Chile",
	"EC": "Ecuador",

	"GH": "Ghana",
	"CM": "Cameroon",
	"CI": "C�te d'Ivoire",
	"NG": "Nigeria",
	"DZ": "Algeria",
	"BF": "Burkina Faso*",

	"FR": "France",

	"NL": "Netherlands",
	"IT": "Italy",
	"BA": "Bosnia-Herzeg.",
	"_england": "England",
	"HR": "Croatia",
	"RU": "Russia",
	"UA": "Ukraine*",
	"GR": "Greece",
	"PT": "Portugal"
};
var ks = {
	"m49":["1A2B",            "2013-06-28","13:00:+3","",""],
	"m50":["1C2D",            "2013-06-28","17:00:+3","",""],
	"m53":["1B2A",            "2013-06-29","13:00:+3","",""],
	"m54":["1D2C",            "2013-06-29","17:00:+3","",""],
	"m51":["1E2F",            "2013-06-30","13:00:+3","",""],
	"m52":["1G2H",            "2013-06-30","17:00:+3","",""],
	"m55":["1F2E",            "2013-07-01","13:00:+3","",""],
	"m56":["1H2G",            "2013-07-01","17:00:+3","",""],
	"m57":["1A2B1C2D",        "2013-07-04","17:00:+3","",""],
	"m58":["1E2F1G2H",        "2013-07-04","13:00:+3","",""],
	"m59":["1B2A1D2C",        "2013-07-05","17:00:+3","",""],
	"m60":["1F2E1H2G",        "2013-07-05","13:00:+3","",""],
	"m61":["1A2B1C2D1E2F1G2H","2013-07-08","17:00:+3","",""],
	"m62":["1B2A1D2C1F2E1H2G","2013-07-09","17:00:+3","",""],
	"m63":["3RD",             "2013-07-12","17:00:+3","",""],
	"m64":["FINAL",           "2013-07-13","16:00:+3","",""]
},
timezoned = new Date(),
timezone = timezoned.getTimezoneOffset()/60,
urlp = {};
$(function(){
	if(location.hash.match(/\#!(.+)/)){
		var kv = RegExp.$1.split("&");
		for(i=0,iLen=kv.length;i<iLen;i++){
			var k = kv[i].split("=")[0];
			var v = kv[i].split("=")[1];
			urlp[k] = decodeURIComponent(v);
		}
	}
	if(urlp["m49"])$("#warning").show();
	for(i in urlp){
		if(ks[i]){
			ks[i][3] = urlp[i].split("-")[0];
			ks[i][4] = urlp[i].split("-")[1];
		}
	}
	
	for(i in ks){
		if(ks[i][3]!=""){
			$("div.ks-"+ks[i][0]+" div.team:eq(0) span.f").html("<div class='flag-24 flag-24-"+ks[i][3]+"'></div>");
			$("div.ks-"+ks[i][0]+" div.team:eq(0) span.n").html(C[ks[i][3]]).addClass("tc-"+ks[i][3]);
			$("div.ks-"+ks[i][0]+" div.team:eq(0) input").attr("data-tc",ks[i][3]);
		}
		if(ks[i][4]!=""){
			$("div.ks-"+ks[i][0]+" div.team:eq(1) span.f").html("<div class='flag-24 flag-24-"+ks[i][4]+"'></div>");
			$("div.ks-"+ks[i][0]+" div.team:eq(1) span.n").html(C[ks[i][4]]).addClass("tc-"+ks[i][4]);
			$("div.ks-"+ks[i][0]+" div.team:eq(1) input").attr("data-tc",ks[i][4]);
		}
	}

	loadFixture();

	$(document)
	.on("change","input.timezone",function(){
		loadFixture();
	})
	.on("mouseup","#t4 input",function(){
		$(this).select();
	})
	.on("change","#t4 input",function(){
		calc();
	})
	.on("keyup","#t4 input",function(){
		calc();
	});
});

function loadFixture(){
	for(i in ks){
		var s = ks[i];
		if($("input[name='timezone']:checked").val()=="y" && timezone!=undefined){
			var ts = +new Date(s[1].split("-")[0],s[1].split("-")[1]-1,s[1].split("-")[2],s[2].split(":")[0],s[2].split(":")[1]);
			ts += s[2].split(":")[2]*3600000;
			ts -= timezone*3600000;
			var d = new Date(ts),
				gdate = ("0"+(d.getMonth()+1)).slice(-2)+"/"+("0"+d.getDate()).slice(-2),
				gtime = ("0"+d.getHours()).slice(-2)+":"+("0"+d.getMinutes()).slice(-2);
		}else{
			var gdate = s[1].split("-")[1]+"/"+s[1].split("-")[2],
				gtime = s[2].split(":")[0]+":"+s[2].split(":")[1];
		}
		if(s[0]=="3RD" || s[0]=="FINAL")
			$("div.ks-"+s[0]+" div.dt").html(gdate+" "+gtime);
		else
			$("div.ks-"+s[0]+" div.dt").html(gdate+"<br>"+gtime);
	}
}

function clear(){
	$("#t4 input").val("");
	calc();
}

function calc(){
	$("#t4 div.ks").each(function(){
		var a  = $(this).find("input:eq(0)");
		var b  = $(this).find("input:eq(1)");
		var av = a.val();
		var bv = b.val();
		var ac = a.attr("data-tc");
		var bc = b.attr("data-tc");
		var ar = a.attr("data-rank");
		var br = b.attr("data-rank");
		var wtc = "";
		var wfl = "";
		var wnm = "";
		var ltc = "";
		var lfl = "";
		var lnm = "";
		if(av.match(/^\d+$/) && bv.match(/^\d+$/) && ac && bc && av!=bv){
			if(av > bv){
				wtc = ac;
				ltc = bc;
			}else{
				wtc = bc;
				ltc = ac;
			}
			wfl = "<div class='flag-24 flag-24-"+wtc+"'></div>";
			lfl = "<div class='flag-24 flag-24-"+ltc+"'></div>";
			wnm = C[wtc];
			lnm = C[ltc];
		}
		$("#t4 div.ks-"+ar+br+"-w span.f").html(wfl);
		$("#t4 div.ks-"+ar+br+"-w span.n").html(wnm);
		$("#t4 div.ks-"+ar+br+"-w input").attr("data-tc",wtc);
		$("#t4 div.ks-"+ar+br+"-l span.f").html(lfl);
		$("#t4 div.ks-"+ar+br+"-l span.n").html(lnm);
		$("#t4 div.ks-"+ar+br+"-l input").attr("data-tc",ltc);
	});
	if($("#final1").val() != "" && $("#final2").val() != "" && $("#finalName1").html() != "" && $("#finalName2").html() != "" && $("#final1").val() != $("#final2").val()){
		if($("#final1").val() > $("#final2").val()){
			$("#winner").html($("#finalName1").html());
			var jerseyName = "images/jerseys/"+$("#finalName1").html().toLowerCase().split("-").join("").split(" ").join("").replace("\u00f4","o")+"1.png";
			$(".jerseypic").css("background","url('"+jerseyName+"')");
		}
		else if($("#final2").val() > $("#final1").val()){
			$("#winner").html($("#finalName2").html());
			var jerseyName = "images/jerseys/"+$("#finalName2").html().toLowerCase().split("-").join("").split(" ").join("").replace("\u00f4","o")+"1.png";
			$(".jerseypic").css("background","url('"+jerseyName+"')");
		}
		$('#popup1').modal('show');
		//$( "#popup1" ).dialog({modal: true,width:550,position: [360,130],show: 'blind', hide: 'explode'});
	}	
}

function randomScores(){
	var allInputs = $(":input");
	randomize(allInputs[0],allInputs[1]);
	randomize(allInputs[2],allInputs[3]);
	randomize(allInputs[4],allInputs[5]);
	randomize(allInputs[6],allInputs[7]);
	randomize(allInputs[8],allInputs[9]);
	randomize(allInputs[10],allInputs[11]);
	randomize(allInputs[12],allInputs[13]);
	randomize(allInputs[14],allInputs[15]);
	randomize(allInputs[16],allInputs[17]);
	randomize(allInputs[18],allInputs[19]);
	randomize(allInputs[20],allInputs[21]);
	randomize(allInputs[22],allInputs[23]);
	randomize(allInputs[24],allInputs[25]);
	randomize(allInputs[26],allInputs[27]);
	randomize(allInputs[28],allInputs[29]);
	randomize(allInputs[30],allInputs[31]);
	setTimeout('calc();',1000);
}

function randomize(el1, el2){
	var team1 = el1.getAttribute("data-tc");
	var team2 = el2.getAttribute("data-tc");
	var winner = getWinner(team1, team2);
	if(winner == team1){
		el1.value = getRandomWinnerGoals();
		if(el1.value == 3){
			el2.value = getRandomLoserGoals1_2();
		}
		else if(el1.value == 2){
			el2.value = getRandomNumber();
		}
		else if(el1.value == 1){
			el2.value = 0;
		}
	}
	else{
		el2.value = getRandomWinnerGoals();
		if(el2.value == 3){
			el1.value = getRandomLoserGoals1_2();
		}
		else if(el2.value == 2){
			el1.value = getRandomNumber();
		}
		else if(el2.value == 1){
			el1.value = 0;
		}
	}
}

function getWinner(team1, team2){
	if(Math.abs (parseInt(RANKINGS[team1]) - parseInt(RANKINGS[team2])) < 5){
		if(getRandomNumber() == 0){
			return team1;
		}
		else{
			return team2;
		}
	}
	else{
		if(parseInt(RANKINGS[team1]) < parseInt(RANKINGS[team2])){
			return team1;
		}
		else{
			return team2;
		}
	}
}

function getRandomNumber() {
    return Math.floor( Math.random() * ( 1 + 1 - 0 ) ) + 0;
}

function getRandomWinnerGoals() {
    return Math.floor( Math.random() * ( 1 +3 - 1 ) ) +1;
}

function getRandomLoserGoals1_2() {
    return Math.floor( Math.random() * ( 1 +2 - 1 ) ) +1;
}