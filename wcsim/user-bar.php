<?php
require_once 'php/lib/Request.php';
require_once 'php/constant.php';
?>
<?php if (Session::isUserLogined()) { ?>

	<?php
		$user = Session::getLoginedUser();
		$link = Session::isAdminLogined() ? 'statistics.php' : 'user-page.php';
	?>
	<div id="user_bar">
		Hi
		<a id="go_result_link" href="<?php echo $link ?>"><?php echo $user[User::F_USERNAME] ?></a> !
		-
		<a href="php/signout.php">Signout</a>
	</div>
	
	<?php if (!isset($isUserPage) && !Session::isAdminLogined()) { ?>
	
		<script type="text/javascript">
			$(function() {
				
				var $goResultLink = $('#go_result_link');
				
				$goResultLink.tooltipster({
					 position: 'top'
					, theme: 'tooltipster-noir'
					, delay: 50
					, speed: 100
					, content: 'Click to see your results'
				});
	
				$goResultLink.tooltipster('show');
				setTimeout(function() { $goResultLink.tooltipster('hide'); }, 5000);
				
			});
			
		</script>
	
	<?php } ?>

<?php } else { ?>
	<div id="user_bar">
		<a id="signup_popup_opener" href="#">Signup</a>
		or
		<a id="login_popup_opener" href="#">Login</a>
	</div>
	
	<div id="login_popup" style="display: none; width: 300px">
		<div style="padding: 0 5px; margin: 0; font-size: 18px;text-align: center;">
			Login to your account
		</div>
		<form id="login_form" method="post" action="php/login.php">
			<input type="hidden" value="" name="backURL" id="backURL" />
			<div class="form-item-wrap">
				<div class="form-item">
					<div class="form-label-wrap">
						<label class="form-label">Username:</label>
					</div>
					<div class="form-input-wrap">
						<input type="text" class="form-input" name="<?php echo User::F_USERNAME; ?>" maxlength="100" />
					</div>
				</div>
			</div>
			<div class="form-item-wrap clear">
				<div class="form-item">
					<div class="form-label-wrap">
						<label class="form-label">Password:</label>
					</div>
					<div class="form-input-wrap">
						<input type="password" class="form-input" name="<?php echo User::F_PASSWORD; ?>" maxlength="30" />
					</div>
				</div>
			</div>
			<div class="hidden-sep">&nbsp;</div>
			<div class="button-bar">
				<button type="submit" class="button popup-button">
				Login
				</button>
			</div>
		</form>
	</div>
	
	<div id="sign_up_popup" style="display: none; width: 450px">
		<div class="has-final winner-name center" style="display:none; padding: 5px 0; margin: 0; font-size: 18px;">
		
		</div>
		<div class="has-final winner-jersey" style="display:none; margin: 0 auto; width: 100px; height: 109px;">
		</div>
		<div class="has-final odds" style="display:none; margin: 7px auto; font-size: 18px; text-align: center;">
		</div>
		<div class="has-final" style="display:none; border-top: 1px dashed #CCCCCC;height: 7px;">&nbsp;</div>
		<div style="padding: 0 5px; margin: 0; font-size: 18px;text-align: center;">
			Join LIVEDUEL today
		</div>
		<form id="signup_form" method="post" action="php/signup.php" autocomplete="off">
			<div class="form-item-wrap">
				<div class="form-item">
					<div class="form-label-wrap">
						<label class="form-label">Username:</label>
					</div>
					<div class="form-input-wrap">
						<input type="text" class="form-input" name="<?php echo User::F_USERNAME; ?>" maxlength="30" />
					</div>
				</div>
			</div>
			<div class="form-item-wrap clear">
				<div class="form-item">
					<div class="form-label-wrap">
						<label class="form-label">Password:</label>
					</div>
					<div class="form-input-wrap">
						<input type="password" class="form-input" name="<?php echo User::F_PASSWORD; ?>" maxlength="30" />
					</div>
				</div>
			</div>
			<div class="form-item-wrap clear">
				<div class="form-item">
					<div class="form-label-wrap">
						<label class="form-label">Confirm password:</label>
					</div>
					<div class="form-input-wrap">
						<input type="password" class="form-input" name="<?php echo User::F_CONFIRM_PASSWORD; ?>" maxlength="30" />
					</div>
				</div>
			</div>
			<div class="form-item-wrap">
				<div class="form-item">
					<div class="form-label-wrap">
						<label class="form-label">Email:</label>
					</div>
					<div class="form-input-wrap">
						<input type="text" class="form-input" name="<?php echo User::F_EMAIL; ?>" maxlength="100" />
					</div>
				</div>
			</div>
			<div class="form-item-wrap clear w50">
				<div class="form-item">
					<div class="form-label-wrap">
						<label class="form-label">Your country:</label>
					</div>
					<div class="form-input-wrap">
						<select class="form-input" name="<?php echo Country::F_CODE ?>">
							<option class="default">Choose your country</option>
						</select>
					</div>
				</div>
			</div>
			<div class="form-item-wrap w50">
				<div class="form-item">
					<div class="form-label-wrap">
						<label class="form-label">Your timezone:</label>
					</div>
					<div class="form-input-wrap">
						<select class="form-input" name="<?php echo Timezone::F_ID ?>">
							<option class="default">Choose your timezone</option>
							<option class="default">Please choose your country first</option>
						</select>
					</div>
				</div>
			</div>
			<div class="hidden-sep">&nbsp;</div>
			<div class="button-bar">
				<button type="submit" class="button popup-button">
				Register
				</button>
			</div>
		</form>
	</div>
	
<script type="text/javascript">
	var __notShowYet = true;
	function openSignupPopup(winnerName, countryCode) {
		var $popup= $('#sign_up_popup');
		
		winnerName = winnerName || openSignupPopup.winnerName;
		countryCode = countryCode || openSignupPopup.countryCode;

		openSignupPopup.winnerName = winnerName;
		openSignupPopup.countryCode = countryCode;
		
		function _afterShow() {
	
			$popup.find(':input:first:visible').focus();
			
			if (__notShowYet) {
				__notShowYet = false;
				var $countrySel, $timezoneSel, timezoneAjax;
				$countrySel = $popup.findInput('<?php echo Country::F_CODE ?>');
				
				Ajax.forURL('php/getListCountry.php')
					.setLoading($countrySel, true)
					.onSuccess(function(response) {
						$countrySel.fillOptionHTML(
							response.data,
							'<?php echo Country::F_CODE ?>',
							'<?php echo Country::F_NAME ?>'
						);
					})
					.run()
				;
				
				$timezoneSel = $popup.findInput('<?php echo Timezone::F_ID ?>');
				timezoneAjax = Ajax
					.forURL('php/getTimezone.php')
					.setLoading($timezoneSel, true)
					.onSuccess(function(response) {
	
						if (Object.size(response.data) === 1) {
							$timezoneSel.fillOptionHTML(
								response.data,
								'<?php echo Timezone::F_ID ?>',
								'<?php echo Timezone::F_NAME ?>',
								true
							);
						} else {
							$timezoneSel.fillOptionHTML(
								response.data,
								'<?php echo Timezone::F_ID ?>',
								'<?php echo Timezone::F_NAME ?>'
							);
						}
					})
				;
				
				$countrySel.change(function() {
					var val = VarUtil.getString(this.value);
					if (val.length === 2) {
						timezoneAjax.setRequestParam(this.name, val).run();
					}
				});
			}
		}
		
		if (winnerName !== undefined) {
			var jersey = "images/jerseys/" + countryCode + '.png';
			$.fancybox( $popup,
				{
					autoHeight:true
				,	autoWidth:true
				,	closeEffect	: 'none'
				,	beforeShow: function() {
						$popup.find('.has-final').show();
						$popup.find('.winner-name').html('<strong>' + winnerName + '</strong> is your final winner');
						$popup.find('.winner-jersey').css('background', 'url("' + jersey + '") repeat scroll 0 0 rgba(0, 0, 0, 0)');
						$popup.find('.odds').html('You may earn <strong>' + odds[countryCode] + '</strong> for this choice');
					}
				,	afterShow:_afterShow
				}
			);
		} else {
			$.fancybox( $popup,
				{
					autoHeight:true
				,	autoWidth:true
				,	closeEffect	: 'none'
				,	beforeShow: function() {
						$popup.find('.has-final').hide();
					}
				,	afterShow:_afterShow
				}
			);
		}
	}
	
	function openLoginPopup() {
		var $popup= $('#login_popup');
		$.fancybox( $popup,
			{
				autoHeight:true
			,	autoWidth:true
			,	closeEffect	: 'none'
			,	afterShow: function() {
					$popup.find('input[type="text"]').focus();
				}
			}
		);
	}
	
	$(function() {

		$('#signup_popup_opener').click(function(ev) {
			openSignupPopup();
			ev.preventDefault();
		});
		
		$('#login_popup_opener').click(function(ev) {
			openLoginPopup();
			ev.preventDefault();
		});
		
		Ajax.formInput({
			form: '#signup_form',
			loading: SCREEN_LOADING,
			onBefore: function() {
				if (openSignupPopup.winnerName) {
					this.setRequestParam('knockoutResult', JSON.stringify(knockoutResult));
				}
			}
		});
		Ajax.formInput({
			form: '#login_form',
			loading: SCREEN_LOADING
		});

		$('#backURL').val(location.href);

	});
	
	</script>
	
<?php } ?>