<?php
require_once 'php/db/DBHandler.php';
require_once 'php/lib/Request.php';
require_once 'php/constant.php';
require_once 'php/lib/CmonUtil.php';
Session::start();
if (!Session::isUserLogined()) {
	header('Location: ' . ROOT_URL);
}
$user = Session::getLoginedUser();
$groupResult = DBHandler::getGroupStageResult();
$knockoutResult = DBHandler::getKnockoutResult();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>LiveDuel</title>
		<link rel="stylesheet" type="text/css" href="style/core.css" />
		<link rel="stylesheet" type="text/css" href="style/common.css" />
		<link rel="stylesheet" type="text/css" href="style/tooltipster.css" />
		<!-- css files -->
		<link rel="stylesheet" type="text/css" href="style/flags.css" />
		<link rel="stylesheet" type="text/css" href="style/jquery.fancybox.css" />
		<link rel="stylesheet" type="text/css" href="style/user-page.css" />
		
		<script type="text/javascript">
			var groupResult = <?php echo json_encode($groupResult, true) ?>;
			var knockoutResult = <?php echo json_encode($knockoutResult, true) ?>;
			var user = <?php echo json_encode($user, true) ?>;
		</script>
		
		<!-- js files -->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script src="script/jquery.fancybox.pack.js"></script>
		<script src="script/core.js"></script>
		<script src="script/jquery.tooltipster.min.js"></script>
		<script src="script/user-page.js"></script>

	</head>
	<body>
	
		<?php $isUserPage = true; ?>
		<?php require 'user-bar.php'; ?>
	
		<div id="container" class="container">
			
			<!-- Header -->
			<div id="header" class="header">
				<div id="user_page_title">
					&nbsp;
				</div>
			<?php if (Session::isUserLogined()) : ?>
				<div id="local_time_box" class="liveduel-radio-box timezone-radio-box">
					<input id="local_time_radio" type="radio" class="liveduel-radio" checked="checked" name="timezone_check">
					<label class="liveduel-radio-label" for="local_time_radio">Local time</label>
				</div>
				<div id="your_time_box" class="liveduel-radio-box timezone-radio-box">
					<input id="your_time_radio" type="radio" class="liveduel-radio" name="timezone_check">
					<label class="liveduel-radio-label" for="your_time_radio">Your timezone</label>
				</div>
			<?php endif;?>
			</div>
			
			<div id="go_area">
				<button id="random-scores" type="button" class="liveduel-button"
					onclick="location.href='index.php'"
				>Go group stage</button>
				<button id="random-scores" type="button" class="liveduel-button"
					onclick="location.href='knockout-stage.php'"
				>Go knockout stage</button>
			</div>
						
			<div id="middle_area">
				<div class="clearfix result-box-ctn">
					<div class="left result-box" id="choosen_winner">
						<div class="result-box-head">
							Your choosen winner
						</div>
						<div class="result-box-body"
						<?php if (!empty($user['winner_country_code'])) { ?>
							style="background-image: url('images/jerseys/<?php echo $user['winner_country_code'] ?>.png')"
						<?php } ?>
						>
						
						</div>
					</div>
					<div class="left result-box">
						<div class="result-box-head">
							Real winner
						</div>
						<div class="result-box-body">
							?
						</div>
					</div>
					<div class="left result-box">
						<div class="result-box-head">
							Total point you won
						</div>
						<div class="result-box-body total-point">
							<?php echo Request::getVar('totalPointsWon', '?'); ?>
						</div>
					</div>
				</div>
			</div>
			
			<div id="grid_area">
				<div class="grid-ctn">
					<div class="grid-head">
						<div id="group_tab" class="grid-head-tab group-stage-tag selected">
							Group stage
						</div>
						<div id="knock_tab" class="grid-head-tab knock-tab">
							Knockout stage
						</div>
					</div>
					<div id="group_grid_body" class="grid-body group-grid">
						<table class="grid">
							<thead>
								<tr>
									<th class="group">Group</th>
									<th class="match">Match</th>
									<th class="real">Real</th>
									<th class="your">Your</th>
									<th class="result" style="width: 120px">Result</th>
									<th class="point" style="width: 180px">Points won</th>
									<th class="time">Time &amp; place</th>
									<th class="status">Status</th>
								</tr>
							</thead>
							<tbody>
						<?php foreach ($groupResult as $group) {
						 		foreach ($group as $match) { ?>
								<tr class="match" group="<?php echo $match['group'] ?>" index="<?php echo $match['indexByGroup'] ?>">
									<td><?php echo $match['group'] ?></td>
									<td>
										<div class="cell-split clearfix team team1">
											<div class="flag flag1 <?php echo $match['flag1'] ?>">
											
											</div>
											<div class="teamname teamname1">
												<?php echo $match['team1'] ?>
											</div>
										</div>
										<div class="cell-split clearfix team team2">
											<div class="flag flag2 <?php echo $match['flag2'] ?>">
											
											</div>
											<div class="teamname teamname2">
												<?php echo $match['team2'] ?>
											</div>
										</div>
									</td>
									<td>
										<div class="cell-split clearfix real-result real-result1">
											<?php echo $match['real_score1'] != '' ?  $match['real_score1'] : '?' ?>
										</div>
										<div class="cell-split clearfix real-result real-result2">
											<?php echo $match['real_score1'] != '' ?  $match['real_score2'] : '?' ?>
										</div>
									</td>
									<td>
										<div class="cell-split clearfix your-result your-result1">
											<?php echo $match['is_predicted'] ? $match['your_score1'] : '' ?>
										</div>
										<div class="cell-split clearfix your-result your-result2">
											<?php echo $match['is_predicted'] ? $match['your_score2'] : '' ?>
										</div>
									</td>
								<?php
									$text = '?';
									$earn50ForResult = '?';
									$earn100ForScore = '?';
									if ($match['is_played'] && $match['is_predicted']) {
									
										$correctScoreFulltime =
											((int) $match['your_score1'] === (int) $match['real_score1'])
											&&	((int) $match['your_score2'] === (int) $match['real_score2']);
										
										$correctFulltime =
											(
											((int) $match['your_score1'] > (int) $match['your_score2'])
											&&	((int) $match['real_score1'] > (int) $match['real_score2'])
											)
											||
											(
											((int) $match['your_score1'] < (int) $match['your_score2'])
											&&	((int) $match['real_score1'] < (int) $match['real_score2'])
											)
											||
											(
												((int) $match['your_score1'] === (int) $match['your_score2'])
											&&	((int) $match['real_score1'] === (int) $match['real_score2'])
											)
											;
										
										if ($correctFulltime) {

											$text = 'Congratulations';
											$earn50ForResult = '50: Exact result';
		
											if ($correctScoreFulltime) {
												$earn100ForScore = '100: Exact score';
											}
											
										} else {
											$text = 'Sorry';
										}
									}
								 ?>
									
									<td class="congra <?php echo $match['is_valid_result'] ? strtolower($text) : 'sorry' ?>">
										<?php echo ($match['is_predicted'] && $match['is_valid_result']) ? $text : "You are too late" ?>
									</td>
									<td>
										<?php echo $match['is_valid_result'] ? $earn50ForResult : '' ?>
										<br>
										<?php echo $match['is_valid_result'] ? $earn100ForScore : '' ?>
									</td>
									<td class="place-and-time"><?php echo $match['local_datetime'] . '<br>' . $match['location']?></td>
									<td>
										<?php echo $match['status']?>
									</td>
								</tr>
						<?php }
							} ?>
							</tbody>
						</table>
					</div>
					<div id="knock_grid_body" class="grid-body knock-grid" style="display: none;">
						<div class="grid-wrap">
							<table class="grid">
							<thead>
								<tr>
									<th class="group">Round</th>
									<th class="match">Match</th>
									<th class="real">Real</th>
									<th class="your">Your</th>
									<th class="result" style="width: 120px">Result</th>
									<th class="point" style="width: 180px">Points won</th>
									<th class="time">Time &amp; place</th>
									<th class="status">Status</th>
								</tr>
							</thead>
							<tbody>
							<?php foreach ($knockoutResult as $match) { ?>
								<?php $valid = $match['is_predicted'] && $match['is_valid_result']; ?>
								<tr class="match">
									<td>
										<?php if ($match['round_of_16'] == 1) : ?>
											Round of 16
										<?php elseif ($match['quarter_final'] == 1) : ?>
											Quarter final
										<?php elseif ($match['semi_final'] == 1) : ?>
											Semi final
										<?php elseif ($match['third_place_match'] == 1) : ?>
											Play-off for 3rd
										<?php elseif ($match['final_match'] == 1) : ?>
											The final match
										<?php endif; ?>
									
									</td>
									<td>
										<div class="cell-split clearfix team team1">
											<div class="flag1 <?php echo !empty($match['country1_code']) ? 'flag ' . $match['country1_code'] : '' ?>">
											
											</div>
											<div class="teamname teamname1">
												<?php echo $match['country1_name'] ?>
											</div>
										</div>
										<div class="cell-split clearfix team team2">
											<div class="flag2 <?php echo !empty($match['country2_code']) ? 'flag ' . $match['country2_code'] : '' ?>">
											
											</div>
											<div class="teamname teamname2">
												<?php echo $match['country2_name'] ?>
											</div>
										</div>
									</td>
									<td>
										<div class="cell-split clearfix scores-wrap">
											<span class="score-box">
												<?php echo $match['is_played'] ? (($match['real_fulltime_result1'] === '') ? '-' : $match['real_fulltime_result1']) : '?' ?>
											</span>
											<span class="score-box">
												<?php echo $match['is_played'] ? (($match['real_extratime_result1'] === '') ? '-' : $match['real_extratime_result1'])  : '?' ?>
											</span>
											<span class="score-box">
												<?php echo $match['is_played'] ? (($match['real_penalty_result1'] === '') ? '-' : $match['real_penalty_result1'])  : '?' ?>
											</span>
										</div>
										<div class="cell-split clearfix scores-wrap">
											<span class="score-box">
												<?php echo $match['is_played'] ?  (($match['real_fulltime_result2'] === '') ? '-' : $match['real_fulltime_result2'])  : '?' ?>
											</span>
											<span class="score-box">
												<?php echo $match['is_played'] ? (($match['real_extratime_result2'] === '') ? '-' : $match['real_extratime_result2'])  : '?' ?>
											</span>
											<span class="score-box">
												<?php echo $match['is_played'] ? (($match['real_penalty_result2'] === '') ? '-' : $match['real_penalty_result2'])  : '?' ?>
											</span>
										</div>
									</td>
									<td>
										<div class="cell-split clearfix scores-wrap">
											<span class="score-box">
												<?php echo $valid ? (($match['fulltime_result1'] === '') ? '-' : $match['fulltime_result1']) : '?' ?>
											</span>
											<span class="score-box">
												<?php echo $valid ? (($match['extratime_result1'] === '') ? '-' : $match['extratime_result1']) : '?' ?>
											</span>
											<span class="score-box">
												<?php echo $valid ? (($match['penalty_result1'] === '') ? '-' : $match['penalty_result1']) : '?' ?>
											</span>
										</div>
										<div class="cell-split clearfix scores-wrap">
											<span class="score-box">
												<?php echo $valid ? (($match['fulltime_result2'] === '') ? '-' : $match['fulltime_result2']) : '?' ?>
											</span>
											<span class="score-box">
												<?php echo $valid ? (($match['extratime_result2'] === '') ? '-' : $match['extratime_result2']) : '?' ?>
											</span>
											<span class="score-box">
												<?php echo $valid ? (($match['penalty_result2'] === '') ? '-' : $match['penalty_result2']) : '?'?>
											</span>
										</div>
									</td>
									<td class="congra <?php echo (!$match['is_valid_result'] || !$match['correctResult']) ? 'sorry' : '' ?>">
										<?php if (!$match['is_played']) : ?>
											?
										<?php elseif (!$valid) : ?>
											You are too late
										<?php elseif ($match['is_played'] && $match['correctResult']) : ?>
											Congratulations
										<?php else: ?>
											Sorry
										<?php endif; ?>
									</td>
									<td>
										<?php
											if ($valid && $match['correctResult']) {
												echo '50 for correct result';
												if ($match['correctScore']) {
													echo '<br>';
													echo '100 for correct score';
												}
											}
										?>
									</td>
									<td class="place-and-time"><?php echo $match['local_datetime'] . '<br>' . $match['location']?></td>
									<td>
										<?php echo $match['status']?>
									</td>
								</tr>
							<?php } ?>
							</tbody>
						</table>
						</div>
					</div>
				</div>
			</div>
			
		
		</div>
		<!-- div#container -->

		<div id="overlay"></div>
		
	</body>
</html>