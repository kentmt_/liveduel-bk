function setTime(isLocalTimeSet) {	
	
	$('#group_grid_body tr.match').each(function() {
		
		var $tr = $(this);
		var data = groupResult[$tr.attr('group')][$tr.attr('index')];
		
		if (isLocalTimeSet) {
			$tr.children('.place-and-time').html(data.local_datetime + '<br>' + data.location);
		} else {
			$tr.children('.place-and-time').html(data.user_datetime + '<br>' + data.location);
		}
	});

	$('#knock_grid_body tr.match').each(function(ix) {
		var $tr = $(this);
		var data = knockoutResult[ix];

		if (isLocalTimeSet) {
			$tr.children('.place-and-time').html(data.local_datetime + '<br>' + data.location);
		} else {
			$tr.children('.place-and-time').html(data.user_datetime + '<br>' + data.location);
		}
	});
}

$(function() {
	
	if (user['winner_country_code']) {
		$('#choosen_winner').tooltipster({
			 position: 'top'
			, theme: 'tooltipster-noir'
			, delay: 50
			, speed: 100
			, content:
			'You will earn ' + user['winner_country_point'] + ' points if ' + user['winner_country_name'] + ' win'
		});
	}
	
	$('#local_time_radio').bind('click', function() {
		setTime(true);
	});
	
	$('#your_time_radio').bind('click', function() {
		setTime(false);
	});

	$('.grid-head-tab').click(function() {
		$('.grid-head-tab').removeClass('selected');
		$(this).addClass('selected');
		$('.grid-body').hide();
		$('#' + this.id.split('_')[0] + '_grid_body').show();
	});
	
});