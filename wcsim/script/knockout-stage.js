(function(){
    var ua= navigator.userAgent, tem, 
    M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE '+(tem[1] || '');
    }
    var browser = M[1].toLowerCase();
    $(document.documentElement).addClass(browser);
})();

 
// VARIABLES

// used to retrieve the team name, give the short tag
var teams = {
    'br': 'Brazil',
    'hr': 'Croatia',
    'mx': 'Mexico',
    'cm': 'Cameroon',
    'es': 'Spain',
    'nl': 'Netherlands',
    'cl': 'Chile',
    'au': 'Australia',
    'co': 'Colombia',
    'gr': 'Greece',
    'ci': 'Ivory Coast',
    'jp': 'Japan',
    'uy': 'Uruguay',
    'cr': 'Costa Rica',
    'en': 'England',
    'it': 'Italy',
    'ch': 'Switzerland',
    'ec': 'Ecuador',
    'fr': 'France',
    'hn': 'Honduras',
    'ar': 'Argentina',
    'ba': 'Bosnia-Herz.',
    'ir': 'Iran',
    'ng': 'Nigeria',
    'de': 'Germany',
    'pt': 'Portugal',
    'gh': 'Ghana',
    'us': 'USA',
    'be': 'Belgium',
    'dz': 'Algeria',
    'ru': 'Russia',
    'kr': 'Korea Rep.'
};

var odds = {
	'br' : 400,
	'ar' : 500,
	'de' : 700,
	'es' : 750,
	'be' : 2100,
	'fr' : 2600,
	'it' : 2900,
	'pt' : 2900,
	'en' : 2900,
	'co' : 3400,
	'nl' : 3400,
	'uy' : 3400,
	'cl' : 5100,
	'ru' : 8100,
	'ch' : 12600,
	'ci' : 12600,
	'ec' : 15100,
	'jp' : 15100,
	'hr' : 15100,
	'mx' : 15100,
	'ba' : 17600,
	'us' : 20100,
	'gh' : 20100,
	'ng' : 25100,
	'gr' : 25100,
	'kr' : 40100,
	'cm' : 50100,
	'au' : 50100,
	'ir' : 150100,
	'dz' : 200100,
	'cr' : 200100,
	'hn' : 300100
};


// FUNCTIONS

/* Given a time string it adds the integer value hourNum
 * and returns the result string.
 * @input	dateString	the date string (e.g. "12:00")
 * @input	hourNum		integer value to add to the time
 * @return	the result string
 */
function sumHours(dateString, hourNum) {

	var dateTimeArr = dateString.split(":");

	var newTime = (parseInt(dateTimeArr[0], 10) + parseInt(hourNum, 10))%24;

	dateTimeArr[0] = newTime.toString();

	dateString = dateTimeArr.join(":");
	
	return dateString;

}

function setTime(isLocal) {	

	$('.match-box').each(function() {
		var $matchBox = $(this);
		var $timeHolder = $matchBox.find('.time-and-place');
		var data = getDataByHTMLID($matchBox.prop('id'));
		if (isLocal) {
			$timeHolder.text(data.local_datetime + ' - ' + data.location);
		} else {
			$timeHolder.text(data.user_datetime + ' - ' + data.location);
		}
	});
}

function getDataByHTMLID(htmlID) {
	return knockoutResult[htmlID];
}

function calMatchResult($matchBox) {
	 var score1 = $matchBox.find('.final-result1').val().trim(),
     	score2 = $matchBox.find('.final-result2').val().trim();
	 
	 if (score1.indexOf('*') !== -1) {
		 return [1,2];
	 } else if (score2.indexOf('*') !== -1) {
		 return [2,1];
	 } else if (score1 !== '?' && score2 !== '?') {
		 score1 = toNumber(score1);
		 score2 = toNumber(score2);
		 if (score1 > score2) {
			 return [1,2];
	     } else {
	    	 return [2,1];
	     }
	 } else {
		 return [0,0];
	 }
}

function updateData() {
	$('.match-box').each(function() {
		var $matchBox = $(this);
		if ($matchBox.hasClass('enabled')) {
			updateMatchData($matchBox, calMatchResult($matchBox)[0]);
		}
	});
}

function updateMatchData($matchBox, winner) {
	var data = getDataByHTMLID($matchBox.prop('id'));
	data.fulltime_result1 = $matchBox.find('.fulltime-result1').val();
	data.fulltime_result2 = $matchBox.find('.fulltime-result2').val();
	data.extratime_result1 = $matchBox.find('.extra-result1').val();
	data.extratime_result2 = $matchBox.find('.extra-result2').val();
	data.penalty_result1 = $matchBox.find('.penalty-result1').val();
	data.penalty_result2 = $matchBox.find('.penalty-result2').val();
	
	var $flag1, $flag2, code1, code2;
	$flag1 = $matchBox.find('.match-flag:first');
	$flag2 = $matchBox.find('.match-flag:last');
	code1 = $flag1.removeClass('match-flag').attr('class').trim();
	code2 = $flag2.removeClass('match-flag').attr('class').trim();
	$flag1.addClass('match-flag');
	$flag2.addClass('match-flag');
	
	data.country1_code = code1;
	data.country1_name = $matchBox.find('.team-name:first').text();
	data.country2_code = code2;
	data.country2_name = $matchBox.find('.team-name:last').text();
	
	if (winner === 1) {
		data.winner_country_code = code1;
	} else if (winner === 2) {
		data.winner_country_code = code2;
	}
}

function updateFinalWinnerData(finalWinnerCode, finalWinnerPoint, finalWinnerName) {
	knockoutResult.winner_country_code = finalWinnerCode;
	knockoutResult.winner_country_point = finalWinnerPoint;
	knockoutResult.winner_country_name = finalWinnerName;
}

function goToNextBox($matchBox) {
	if (!window.autoFill) {
		var nextOrder = toNumber($matchBox.attr('order')) + 1;
		var $nextBox = $('.match-box[order="' + nextOrder + '"]');
		
		if ($nextBox.length && $nextBox.hasClass('enabled')) {
			displayBigger($nextBox);
		} else if ((nextOrder === 7 || nextOrder === 14 || nextOrder === 15 || nextOrder === 16)) {
			var itv = -1;
			itv = setInterval(
				function() {
					if ($nextBox.hasClass('enabled')) {
						displayBigger($nextBox);
						clearInterval(itv);
					}
				}, 10);
		} 
	}
}

function updateKnockout() {
	
	 // first knockout level
    for (var i=1; i<=8; ++i) {
        
    	var $matchBox = $("#score1"+i+"-1").closest('.match-box');
     
     	// retrieve the id of the related field of the 2nd knockout level
        var scoreString = "#score2"+(Math.floor((i-1)/2)+1)+"-"+((i-1)%2+1)
        ,	$nextBox = $(scoreString).closest('.match-box')
        ,	winnerTeam = calMatchResult($matchBox)[0];
        
        if (winnerTeam !== 0) {
        	
	        // retrieve and set the team name
	        var winnerName = $("#match-1"+i+" #name" + winnerTeam).html();
	        $("#name2"+i).html(winnerName);
	
	        // retrieve the winner flag tag
	        var winnerFlag = $("#match-1"+i+" .team" + winnerTeam + " .match-flag").removeClass("match-flag").attr("class");
	        $("."+winnerFlag).addClass("match-flag");
	
	        // set the winner flag tag
	        $("#flag2"+i).removeClass().addClass("match-flag "+winnerFlag);
	    	$("#bracket1"+i).addClass("selected");
	    	
			activeTheBox($nextBox);
        }
    }

    // Second knockout level
    for (var i=1; i<=4; ++i) {
        
    	var $matchBox = $("#score2"+i+"-1").closest('.match-box');
     
        // retrieve the id of the related field of the 3rd knockout level
     	var scoreString = "#score3"+(Math.floor((i-1)/2)+1)+"-"+((i-1)%2+1)
     	,	$nextBox = $(scoreString).closest('.match-box')
        ,	winnerTeam = calMatchResult($matchBox)[0];
        
        	
   	 // retrieve and set the team name
     	if (winnerTeam !== 0) {
     		
	        var winnerName = $("#match-2"+i+" #name2" + ((i-1)*2+winnerTeam) ).html();
	        $("#name3"+i).html(winnerName);
	
	        // retrieve the winner flag tag
	        var winnerFlag = $("#match-2"+i+" .team" + winnerTeam + " .match-flag").removeClass("match-flag").attr("class");
	        $("."+winnerFlag).addClass("match-flag");
	
	        // set the winner flag tag
	        $("#flag3"+i).removeClass().addClass("match-flag "+winnerFlag);
	        $(scoreString).removeAttr("disabled");
	    	$("#bracket2"+i).addClass("selected");
	    	
			activeTheBox($nextBox);
     	}
    }
   
    // third knockout level
    for (var i=1; i<=2; ++i) {
        
    	var $matchBox = $("#score3"+i+"-1").closest('.match-box');

        // retrieve the id of the related field of the 3rd knockout level
     	var final1String = "#score-final1-"+i
		,	final2String = "#score-final2-"+i
		,	$finaBox1 = $(final1String).closest('.match-box')
		,	$finaBox2 = $(final2String).closest('.match-box')
		,	tmp = calMatchResult($matchBox)
        ,	winnerTeam, loserTeam;
     	
     	winnerTeam = tmp[0];
     	loserTeam = tmp[1];
        
     	// retrieve and set the team name
     	if (winnerTeam !== 0) {
     		
	        var winnerName = $("#match-3"+i+" #name3" + ((i-1)*2+winnerTeam) ).html();
	        var loserName  =  $("#match-3"+i+" #name3" + ((i-1)*2+loserTeam) ).html();
	        
	        $("#name-final2-"+i).html(winnerName);
	        $("#name-final1-"+i).html(loserName);
	
	        // retrieve the winner and loser flag tag
	        var winnerFlag = $("#match-3"+i+" .team" + winnerTeam + " .match-flag").removeClass("match-flag").attr("class");
	        $("."+winnerFlag).addClass("match-flag");
	        
	        var loserFlag = $("#match-3"+i+" .team" + loserTeam + " .match-flag").removeClass("match-flag").attr("class");
	        $("."+loserFlag).addClass("match-flag");
	
	        // set the winner and loser flag tag
	        $("#flag-final2-"+i).removeClass().addClass("match-flag "+winnerFlag);
	        $("#flag-final1-"+i).removeClass().addClass("match-flag "+loserFlag);
	    	
	    	$(final1String).removeAttr("disabled");
	    	$(final2String).removeAttr("disabled");
	    	
	    	if (i == 1) {
	    		$("#bracket3-hor-left").addClass("selected");
	    	} else {
	    		$("#bracket3-hor-right").addClass("selected");
	    	}
	    		
	    	if ($("#bracket3-hor-left").hasClass("selected") && $("#bracket3-hor-right").hasClass("selected")) {
	    		$("#bracket3-vertical").addClass("selected");
	    	}
	    	
			activeTheBox($finaBox1);
			activeTheBox($finaBox2);
     	}
    }
  
    
    var $3rdMatchBox = $('#match-41');
    var winner = calMatchResult($3rdMatchBox)[0];
    var $finalMatchBox;
    $finalMatchBox = $('#match-42');
    winner = calMatchResult($finalMatchBox)[0];
    if (winner !== 0) {
    	
    	winner = getTheFinalWinnerInfo();
    	
    	// Update data for final winner
    	updateFinalWinnerData(winner.countryCode, odds[winner.countryCode], winner.name);
    	
    	if (window.openSignupPopup) {
    		openSignupPopup.winnerName = winner.name;
        	openSignupPopup.countryCode = winner.countryCode;
    		if (!openSignupPopup.isLaunchedWithFinalWinner) {
    			openSignupPopup.isLaunchedWithFinalWinner = true;
        		openSignupPopup(winner.name, winner.countryCode);
    		}
    	}
    }
}

function activeTheBox($matchBox) {
	
	if (!$matchBox.find('.no-flag').length && !$matchBox.hasClass('enabled')) {
		$matchBox.addClass('enabled');
		registTooltip($matchBox, tooltipSetting);
	}
}

function deactiveTheBox($matchBox) {
	$matchBox.each(function() {
		var $matchBox = $(this);
		$matchBox.removeClass('enabled');
		$matchBox.find('.match-flag').addClass('no-flag');
		$matchBox.find('.team-name').text('');
		destroyTooltip($matchBox);
	});
}

function destroyTooltip($matchBox) {
	try {
		$matchBox.find('.final').tooltipster('destroy');
		$matchBox.find('.fulltime').tooltipster('destroy');
		$matchBox.find('.extra').tooltipster('destroy');
		$matchBox.find('.penalty').tooltipster('destroy');
	} catch (e) {
		console.log(e);
	}
	
	$matchBox.find('.match-result').unbind('focusin').unbind('focusout');
}

function doWhenPenaltiScoreChange($target) {
	var score1, score2;
	score1 = $target.val();
	score2 = $target.siblings('.penalty-result').val();
	
	if (score1 !== '' && score2 !== '') {
		$matchBox = $target.closest('.match-box');
		countTotal($matchBox);
		if (score1 === score2) {
			//showPenalti($target.closest('.match-box'));
		} else {
			if ($matchBox.hasClass('bigger')) {
				displayNormal($matchBox);
				goToNextBox($matchBox);
			}
			updateKnockout();
		}
	}
}

function doWhenExtraScoreChange($target) {
	var score1, score2;
	score1 = $target.val();
	score2 = $target.siblings('.extra-result').val();
	if (score1 !== '' && score2 !== '') {
		$matchBox = $target.closest('.match-box');
		countTotal($matchBox);
		if (score1 === score2) {
			showPenalti($matchBox);
		} else {
			if ($matchBox.hasClass('bigger')) {
				hidePenalti($matchBox);
				displayNormal($matchBox);
				goToNextBox($matchBox);
			}
			updateKnockout();
		}
	}
}

function doWhenMainScoreChange($target) {
	
	var $matchBox, score1, score2;
	score1 = $target.val();
	score2 = $target.siblings('.fulltime-result').val();
	
	if (score1 !== '' && score2 !== '') {
		$matchBox = $target.closest('.match-box');
		countTotal($matchBox);
		if (score1 === score2) {
			showExtra($matchBox);
		} else {
			if ($matchBox.hasClass('bigger')) {
				hideExtra($matchBox);
				hidePenalti($matchBox);
				displayNormal($matchBox);
				goToNextBox($matchBox);
			}
			updateKnockout();
		}
	}
}

function countTotal($matchBox) {
	var $final1 = $matchBox.find('.final-result1'),
		$final2 = $matchBox.find('.final-result2');
	
	$final1.val(
		toNumber($matchBox.find('.fulltime-result1').val())
	+	toNumber($matchBox.find('.extra-result1').val())
	);
	$final2.val(
		toNumber($matchBox.find('.fulltime-result2').val())
	+	toNumber($matchBox.find('.extra-result2').val())
	);
	
	if ($final1.val() === $final2.val()) {

		var p1 = $matchBox.find('.penalty-result1').val().trim(),
			p2 = $matchBox.find('.penalty-result2').val().trim();
		
		if (p1 !== '' && p2 !== '') {
			p1 = toNumber(p1);
			p2 = toNumber(p2);
			if (p1 > p2) {
				$final1.val($final1.val() + '*');
			} else if (p2 > p1) {
				$final2.val($final2.val() + '*');
			}
		}
	}
}

function toNumber(val) {
	val = parseInt(val, 10);
	return !isFinite(val) ? 0 : val;
}

function showExtra($matchBox) {
	$matchBox.addClass('has-extra');
	if ($matchBox.hasClass('bigger')) {
		locateTheBigBox($matchBox, true);
	}
}

function hideExtra($matchBox) {
	$matchBox.removeClass('has-extra');
	$matchBox.find('.extra-result').val('');
	if ($matchBox.hasClass('bigger')) {
		locateTheBigBox($matchBox, false);
	}
}

function showPenalti($matchBox) {
	$matchBox.addClass('has-penalty');
	if ($matchBox.hasClass('bigger')) {
		locateTheBigBox($matchBox, true);
	}
}

function hidePenalti($matchBox) {
	$matchBox.removeClass('has-penalty');
	$matchBox.find('.penalty-result').val('');
	if ($matchBox.hasClass('bigger')) {
		locateTheBigBox($matchBox, false);
	}
}

function showTooltip($target) {
	$target.each(function() {
		try {
			$(this).tooltipster('show');
		} catch (e) {
			console.log(e);
		}
	});
}

function hideTooltip($target) {
	$target.each(function() {
		try {
			$(this).tooltipster('hide');
		} catch (e) {
			console.log(e);
		}
	});
	
	$('.tooltipster-base').remove();
}

function displayNormal($matchBox) {
	
	$matchBox.each(function() {
		var $matchBox = $(this);
		if ($matchBox.hasClass('bigger')) {
			storeLatestBigBoxPosition($matchBox);
			hideTooltip($matchBox.find('.result-ctn'));
			$matchBox.removeClass('bigger');
			restoreBoxInitPosition($matchBox);
			hideTooltip($matchBox);
		}
	});
}

function doTheFirstBiggerThings($matchBox) {
	
	// Reposition these box more top/bottom
	if ($matchBox.hasClass('row1') || $matchBox.hasClass('row3')) {
		$matchBox.css('top', (parseInt($matchBox.css('top'), 10) - 70) + 'px');
	} else if ($matchBox.hasClass('row21') 
		|| $matchBox.hasClass('row22') 
		|| $matchBox.hasClass('col3') 
		|| $matchBox.hasClass('col5')
	) {
		
		$matchBox.css('top', (parseInt($matchBox.css('top'), 10) - 30) + 'px');
		
	} else if ($matchBox.hasClass('final1')) {
		$matchBox.css('top', '435px');
	}
	
	// The fulltime result has displayed, so we reposition the box more left/right
	locateTheBigBox($matchBox, true);
	
	// Turn the flag to on
	$.data($matchBox.get(0), 'hasBigger', true);
}

function displayBigger($matchBox) {
	if (!$matchBox.hasClass('bigger')) {
		
		hideTooltip($matchBox);
		
		displayNormal($('.match-box').not($matchBox));
		
		$matchBox.addClass('bigger');
		
		var data = $.data($matchBox.get(0));
//		For the first time display bigger, this flag = undefined
		if (!data.hasBigger) {
			doTheFirstBiggerThings($matchBox);
		}
		
		restoreLatestBigBoxPosition($matchBox);
		
		// Wait for transition effect complete
		setTimeout(function() {
			
			// Scroll to bottom
			if ($matchBox.hasClass('row4')) {
				$('html,body').animate(
					{scrollTop: 1000},
			    	'fast'
				);
				
			// Scroll to top
			} else if ($matchBox.hasClass('row1')) {
				$('html,body').animate(
					{scrollTop: 0},
			    	'fast'
				);
			}
			
			// Set focus to first input
			$matchBox.find('.fulltime-result:first').focus();
			
		}, 250);
	}
};

function storeLatestBigBoxPosition($matchBox) {
	
	// The left boxes
	if ($matchBox.hasClass('col1') || $matchBox.hasClass('col2') || $matchBox.hasClass('col3')) {
		
		$.data($matchBox.get(0), 'latestLeft', $matchBox.css('left'));
		
	// The right boxes
	} else if ($matchBox.hasClass('col5') || $matchBox.hasClass('col6') || $matchBox.hasClass('col7')) {
		
		$.data($matchBox.get(0), 'latestRight', $matchBox.css('right'));
	}
	
	$.data($matchBox.get(0), 'latestTop', $matchBox.css('top'));
}

function restoreLatestBigBoxPosition($matchBox) {
	
	var data = $.data($matchBox.get(0));
	
	if (data.latestLeft) {
		$matchBox.css('left', data.latestLeft);
	} else if (data.latestRight) {
		$matchBox.css('right', data.latestRight);
	}
	
	if (data.latestTop) {
		$matchBox.css('top', data.latestTop);
	}
}

function restoreBoxInitPosition($matchBox) {
	
	var data = $.data($matchBox.get(0));
	
	if (data.initLeft) {
		$matchBox.css('left', data.initLeft);
	} else if (data.initRight) {
		$matchBox.css('right', data.initRight);
	}
	
	$matchBox.css('top', data.initTop);
}

// Store first position of boxes for later usage
function storeBoxInitPosition() {
	$('.match-box').each(function() {
		var $matchBox = $(this);
		
		// The left boxes
		if ($matchBox.hasClass('col1') 
			|| $matchBox.hasClass('col2') 
			|| $matchBox.hasClass('col3')
			|| $matchBox.hasClass('final1')
			|| $matchBox.hasClass('final2')
		) {
			
			$.data($matchBox.get(0), 'initLeft', $matchBox.css('left'));
			
		// The right boxes
		} else if ($matchBox.hasClass('col5') || $matchBox.hasClass('col6') || $matchBox.hasClass('col7')) {
			
			$.data($matchBox.get(0), 'initRight', $matchBox.css('right'));
		}
		
		$.data($matchBox.get(0), 'initTop', $matchBox.css('top'));

	});
}

function locateTheBigBox($matchBox, isDilation) {
	
	var data = $.data($matchBox.get(0));
	
	if (isDilation) {
		
		if ($matchBox.hasClass('col1') 
			|| $matchBox.hasClass('col2') 
			|| $matchBox.hasClass('col3')
		) {
			
			$matchBox.css('left', (parseInt($matchBox.css('left'), 10) - 40) + 'px');
			
		} else if ($matchBox.hasClass('col7') || $matchBox.hasClass('col6') || $matchBox.hasClass('col5')) {
			
			$matchBox.css('right', (parseInt($matchBox.css('right'), 10) - 40) + 'px');
			
		} else if ($matchBox.hasClass('final1') || $matchBox.hasClass('final2')) {
			
			$matchBox.css('left', (parseInt($matchBox.css('left'), 10) - 20) + 'px');
		}
		
	} else {
		
		if ($matchBox.hasClass('col1') 
			|| $matchBox.hasClass('col2') 
			|| $matchBox.hasClass('col3')
			|| $matchBox.hasClass('final1')
			|| $matchBox.hasClass('final2')
		) {
			
			$matchBox.css('left', (parseInt($matchBox.css('left'), 10) + 40) + 'px');
			
		} else if ($matchBox.hasClass('col7') || $matchBox.hasClass('col6') || $matchBox.hasClass('col5')) {
			
			$matchBox.css('right', (parseInt($matchBox.css('right'), 10) + 40) + 'px');
			
		} else if ($matchBox.hasClass('final1') || $matchBox.hasClass('final2')) {
			
			$matchBox.css('left', (parseInt($matchBox.css('left'), 10) + 20) + 'px');
		}
	}
}

function doWhenNumberKeyPress(target) {
	
	var currentValue = $.data(target, 'currentValue');
	if (target.value !== currentValue && target.value !== '') {
		$.data(target, 'currentValue', target.value);
		var $target, $sibling, func;
		$target = $(target);
		$sibling = $();
		
		if ($target.hasClass('fulltime-result')) {
			$sibling = $target.siblings('.fulltime-result');
			func = doWhenMainScoreChange;
		} else if ($target.hasClass('extra-result')) {
			$sibling = $target.siblings('.extra-result');
			func = doWhenExtraScoreChange;
		} else if ($target.hasClass('penalty-result')) {
			$sibling = $target.siblings('.penalty-result');
			func = doWhenPenaltiScoreChange;
		}
		
		func($target);
		
		// Auto tab
		if ($target.closest('.match-box').hasClass('bigger')) {
			if ($sibling.val() === '') {
				$sibling.focus();
			} else {
				var $ctn, $nextCtn;
				$ctn = $target.closest('.result-ctn');
				$nextCtn = $ctn.next('.result-ctn');
				if (!$nextCtn.length || !$nextCtn.is(':visible')) {
					$nextCtn = $ctn.prev('.result-ctn');
				}
				if (!$nextCtn.hasClass('final-result')) {
					$nextCtn.find('.match-result:first').focus();
				}
			}
		}
	}
}
var TAB_KEY = 9;
var ENTER_KEY = 13;
function isControlKey(key) {
    return key === 8 || key === 9 || key === 13 || key === 16 || (key >= 16 && key <= 20) || key === 27 || (key >= 33 && key <= 40) || key === 45 || key === 46;
}

function isNumberKey(key) {
	return (key >= 48 && key <= 57) || (key >= 96 && key <= 105);
}

function fillRandomScoreForBox($matchBox) {
	
	if (!$matchBox.hasClass('is-played')) {
		var score1, score2, $result1;
		score1 = Math.floor(Math.random()*6);
		score2 = Math.floor(Math.random()*6);
		
		if (score1 === score2) {
			-- score1;
		}
		
		if (score1 === -1) {
			score1 = 1;
		}
		
		$result1 = $matchBox.find('.fulltime-result1');
		$result1.val(score1);
		$matchBox.find('.fulltime-result2').val(score2);
		
		$matchBox.find('.extra-result').val('');
		$matchBox.find('.penalty-result').val('');
		
		doWhenMainScoreChange($result1);
	}
	
}

function fillBoxInfo($matchBox) {
	var data = getDataByHTMLID($matchBox.prop('id'));
	var $input;
	
	$matchBox.find('.time-and-place').text(data.local_datetime + " - " + data.location);
	$matchBox.find('.round').text(data.round);
	
	$input = $matchBox.find('.fulltime-result1');
	$input.val(data['fulltime_result1']);
	$matchBox.find('.fulltime-result2').val(data['fulltime_result2']);
	doWhenMainScoreChange($input);
	
	$input = $matchBox.find('.extra-result1');
	$input.val(data['extratime_result1']);
	$matchBox.find('.extra-result2').val(data['extratime_result2']);
	doWhenExtraScoreChange($input);
	
	$input = $matchBox.find('.penalty-result1');
	$input.val(data['penalty_result1']);
	$matchBox.find('.penalty-result2').val(data['penalty_result2']);
	doWhenPenaltiScoreChange($input);
	
    if (data.disabled) {
		$matchBox.find('.match-result').prop('disabled', true);
		$matchBox.addClass('is-played');
	} 
}

function fillRandomScore() {
	window.autoFill = true;
	
	// Round of 16
	$('.match-box.col1, .match-box.col7').each(function() {
		fillRandomScoreForBox($(this).removeClass('has-extra').removeClass('has-penalty'));
	});
	
	// Quarter-final
	$('.match-box.col2, .match-box.col6').each(function() {
		fillRandomScoreForBox($(this).removeClass('has-extra').removeClass('has-penalty'));
	});
	
	// Semi-final
	$('.match-box.col3, .match-box.col5').each(function() {
		fillRandomScoreForBox($(this).removeClass('has-extra').removeClass('has-penalty'));
	});
	
	// Final and Play-off for 3rd place
	$('.match-box.final1, .match-box.final2').each(function() {
		fillRandomScoreForBox($(this).removeClass('has-extra').removeClass('has-penalty'));
	});
	
	window.autoFill = false;
}

function getTheFinalWinnerInfo() {
	var $finalMatchBox, result, winner;
	$finalMatchBox = $('.match-box.final2');
	result = calMatchResult($finalMatchBox);
	winner = result[0];
	if (winner !== 0) {
		var $winerFlag, winnerCode, winnerName;
		$winerFlag = $('#flag-final2-' + winner);
		winnerCode = $winerFlag.removeClass('match-flag').attr('class').trim();
		$winerFlag.addClass('match-flag');
		winnerName = $('#name-final2-' + winner).text();
		return {
			name: winnerName
		,	countryCode: winnerCode
		};
	} else {
		return undefined;
	}
}

function clearScores() {
	$('.match-box').each(function() {
		var $matchBox = $(this);
		var index = $matchBox.prop('id').right(2);
		
		if (!$matchBox.hasClass('is-played')) {
			$matchBox.find('.match-result').val('');
			$matchBox.find('.final-result').val('?');
			$matchBox.removeClass('has-extra');
			$matchBox.removeClass('has-penalty');
			$('#bracket' + index).removeClass('selected');
		} else {
			
		}

	});
	
	
	
	$('.bracket').removeClass('selected');
	
	deactiveTheBox($(
		'.match-box.col2, .match-box.col6, .match-box.col3, .match-box.col5, .match-box.final1, .match-box.final2'
	));
}

//Default tooltip setting
var tooltipSetting = {
	 position: 'top'
	, theme: 'tooltipster-noir'
	, delay: 200
	, speed: 100
	, offsetY: -5
	, functionBefore: function($target, continueTooltip) {
		
		if ($target.hasClass('result-ctn')) {
			
			hideTooltip($target.closest('.match-box').find('.result-ctn'));
			
			if ($target.hasClass('final') && $target.closest('.match-box').hasClass('bigger')) {
				continueTooltip();
			} else if (!$target.hasClass('final')) {
				continueTooltip();
			}
			
		} else if ($target.hasClass('match-box')) {
			var $matchBox, content = '';
			$matchBox = $target;
			var data = getDataByHTMLID($matchBox.prop('id'));
			hideTooltip($('.match-box').not($matchBox));
			
			content = data.round
				+ '<br>' + $matchBox.find('.time-and-place').text();
			
			if ($matchBox.hasClass('enabled')) {
				content += '<br>' +  'The blue box show the final result';
			}
			
			if ($matchBox.hasClass('is-played')) {
				content += '<br>' +  'Time is over, you can not edit anymore';
			}
			
			$matchBox.tooltipster('content', content);
			
			if (!$matchBox.hasClass('bigger')) {
				continueTooltip();
			}
			
		} else {
			continueTooltip();
		}
	}
};

function registTooltip($matchBox, defaultSetting) {
	
	$matchBox.each(function() {
		var $matchBox = $(this);
		$matchBox.find('.final')
			.tooltipster($.extend(defaultSetting, 
				{
					content: 'Final result',
					contentAsHTML: true,
				}
			));
	
		$matchBox.find('.fulltime').tooltipster($.extend(defaultSetting, {content: 'Full time result'}));
		$matchBox.find('.extra').tooltipster($.extend(defaultSetting, {content: 'Extra time result'}));
		$matchBox.find('.penalty').tooltipster($.extend(defaultSetting, {content: 'Penalty result'}));
		
		$matchBox.find('.match-result')
			.focusin(function() {
				var $this = $(this);
				setTimeout(
					function() {
						showTooltip($this.closest('.result-ctn'));
					},
					250
				);
			})
			.focusout(function() {
				hideTooltip($(this).closest('.result-ctn'));
			});
	});
}

$(document).ready(function() {
    
    // 8 = number of matches
    for (var i=0; i<8; ++i) {
        
        $("#match-1" + (i+1) + " #name1").html(teams[roundOf16[i][0]]);
        $("#match-1" + (i+1) + " #name2").html(teams[roundOf16[i][1]]);
    
        $("#match-1" + (i+1) + " .team1 > .match-flag").removeClass("no-flag").addClass(roundOf16[i][0]);
        $("#match-1" + (i+1) + " .team2 > .match-flag").removeClass("no-flag").addClass(roundOf16[i][1]);
        
    }
    
    for (var key in knockoutResult) {
    	window.autoFill = true;
    	fillBoxInfo($('#' + key));
    	window.autoFill = false;
    }
    
    // setting button, visible only in small browser windows
    $("#settings-btn").click(function() {
        $("#settings-box").toggle();
    });
    
	$(".match-result").not('.final-result')
		.keyup(function(ev){
			if (isNumberKey(ev.which)) {
				doWhenNumberKeyPress(this);
			}
		})
		.keydown(function(ev){
			return isControlKey(ev.which) || isNumberKey(ev.which);
		});
	
	// display normally when click outside the box, 
	//	except the bigger box it self and the timezone radioes
	$(document.body).bind('click', function(e) {
		var $target = $(e.target);
		if (!$target.closest('.match-box.bigger').length
			&& !$target.closest('.timezone-radio-box').length
		) {
			
			displayNormal($('.match-box.bigger'));
		}
	});
	
	// Only show tooltip for enableddbox in round of 16
	registTooltip($('.match-box.enabled'), tooltipSetting);
	
	$('.match-box').tooltipster($.extend(tooltipSetting, { contentAsHTML: true } ));
	
	$(document.body).delegate('.match-box', 'click', function() {
		var $matchBox = $(this);
		if ($matchBox.hasClass('enabled')) {
			displayBigger($matchBox);
		}
	});
	
	storeBoxInitPosition();
	
	// Buttons
	$('#fill_random_btn').click(function() {
		fillRandomScore();
	});
	
	$('#clear_btn').click(function() {
		$confirm('You really want to clear all the scrores?', clearScores);
	});
	
	// Timezone radioes
	$('#local_time_radio').bind('click', function() {
		setTime(true);
	});
	
	$('#your_time_radio').bind('click', function() {
		setTime(false);
	});
	
	
	var $saveAllBtn = $('#save_knockout');
	var saveAjax = Ajax.forURL('php/saveKnockoutResult.php')
		.setLoading(SCREEN_LOADING)
		.onBefore(function() {
			$.fancybox.close();
		})
		.onSuccess(function(response) {
			this.showResponseMessage(response);
		});
	
	function saveKnockoutResult() {
		updateData();
		saveAjax.setRequestParam('knockoutResult', JSON.stringify(knockoutResult));
		saveAjax.run();
	}
	
	$('#save_all_popup_opener').click(function() {
		winner = getTheFinalWinnerInfo();
		if (winner !== undefined) {
			var winnerName = winner.name;
			var countryCode = winner.countryCode;
			var jersey = "images/jerseys/" + winner.countryCode + '.png';
			var $popup = $('#done_popup');
			$.fancybox( $popup,
				{
					autoHeight:true
				,	autoWidth:true
				,	closeEffect	: 'none'
				,	beforeShow: function() {
						$popup.find('.winner-name').show().html('<strong>' + winnerName + '</strong> is your final winner');
						$popup.find('.winner-jersey').show().css('background', 'url("' + jersey + '") repeat scroll 0 0 rgba(0, 0, 0, 0)');
						$popup.find('.odds').show().html('You may earn <strong>' + odds[countryCode] + '</strong> for this choice');
					}
				}
			);
		} else {
			saveKnockoutResult();
		}
		
	});
	
	$saveAllBtn.click(saveKnockoutResult);
	
	var $saveTheReal = $('#save_the_real');
	if ($saveTheReal.length) {
		var saveTheRealAjax = Ajax.forURL('php/saveRealKnockoutResult.php')
			.setLoading(SCREEN_LOADING)
			.onSuccess(function(response) {
				this.showResponseMessage(response);
			});
		
		$saveTheReal.click(function() {
			updateData();
			saveTheRealAjax.setRequestParam('knockoutResult', JSON.stringify(knockoutResult));
			saveTheRealAjax.run();
		});
	}
	
});