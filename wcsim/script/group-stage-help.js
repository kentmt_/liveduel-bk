/* parameters
 * ----------
 * position 	String	[bottom|right|top|left] relative to the element, 'center' (in the page). Default: center
 * text			String	the content text of the box
 * prevLabel	String	label of the left button (deafult "< Prev")
 * nextLabel	String	label of the right button (default "Next >")
 * width		String	width of the box, in css format (i.e. "300px". Default: "auto")
 * height		String	height of the box, in css format (i.e. "200px". Default: auto)
 * showPrev		Boolean	[true|false] show/hide the left button
 * showNext		Boolean	[true|false] show/hide the right button
 * focus		String	[prev|next], button to focus once showed
 */
var tutorial = [
	
	{
		position: 'center',
		text: 'Welcome to LiveDuel!<\/br>\
			This is a short overview of the element of this app.<\/br>\
			To start, click \'next\', or close the window and have fun!',
		prevLabel: 'No, thanks',
		nextLabel: 'Start',
		width: '500px'
	},
	{
		elementId: 'list-view-option',
		position: 'bottom',
		text: 'You can choose to display every match in Widget mode or List mode.',
		showPrev: false
	},
	{
		elementId: 'score12',
		position: 'right',
		text: 'Type in your score predictions.<br>You will win 50 points for correct result and 100 points for correct score'
	},
	{
		elementId: 'random-scores',
		position: 'top',
		text: 'If you don\'t know what score to put, you can fill the remaining ones with random scores.'
	},
	{
		elementId: 'progress-bar',
		position: 'left',
		text: 'This progress bar will show the completion level of this group.'
	},
	{
		elementId: 'group-2',
		position: 'bottom',
		text: 'You can switch from one group to another by clicking on these tabs.'
	},
	{
		elementId: 'predicted-points',
		position: 'left',
		text: 'This table will be filled while you fill in the scores.'
	},
	{
		elementId: 'go-knockout',
		position: 'bottom',
		text: 'Once you type in all scores, you will be able to predict the Knockout Stage by clicking on this button.',
		nextLabel: 'Close'
	}
	
	
];

var knockoutBtnShown = false; // true, if the button #go-knockout is shown, false otherwise

// this variable will track the current turorial object
var i = 0;

function initTutorial() {
	
	// click on #help-prev button
	$("#help-prev").click(function() {

		$("#help-dialog").fadeOut({

			// once the animation completes...
			done: function() {


				if (i == 6 && !knockoutBtnShown) {
					$("#go-knockout").hide();
				}

				if (i == 0) {
					
					var functionAfter = function() {};
					
					if (!$.cookie('isNoThanksClicked')) {
						
						$.cookie('isNoThanksClicked', true, { expires: 7 });
						
						var $hellButton = $('#help-btn');
						functionAfter = function() {
							$hellButton.tooltipster({
								 position: 'top'
								, theme: 'tooltipster-noir'
								, delay: 50
								, speed: 100
								, content: 'You can see the tutorial by click here'
							});
							$hellButton.tooltipster('show');
							setTimeout(function() { $hellButton.tooltipster('destroy'); }, 2000);
						};
						
					}
					
					$("#overlay").fadeOut('fast', functionAfter);
					
					// focus on the first score empty field
					focusOnFirstEmptyField();

				} else {

					i--;

					// show the previous tip
					showHelpBaloon(tutorial[i], i, tutorial.length-1);

				}

			}

		});

	});


	// click on #help-next button
	$("#help-next").click(function() {

		$("#help-dialog").fadeOut({

			// once the animation completes...
			done: function() {

				if (i < 7) {

					// element hidden
					if (i == 6 && !knockoutBtnShown) {
						$("#go-knockout").fadeIn();
					}

					i++;

					// show the next tip
					showHelpBaloon(tutorial[i], i, tutorial.length-1);


				} else { // i == 7

					if (!knockoutBtnShown) {

						// hide the knockout button
						$("#go-knockout").fadeOut({

							// once the animation completes, hide the tutorial overlay
							done: function() {

								$("#overlay").fadeOut();
								
								// focus on the first score empty field
								focusOnFirstEmptyField();

							}

						});
						
					} else {
						
						$("#overlay").fadeOut();
						
						// focus on the first score empty field
						focusOnFirstEmptyField();
						
					}

				}

			}

		});

	});
	
	if (!$.cookie('isTutorialSaw')) {
		$.cookie('isTutorialSaw', true, { expires: 7 });
		showTutorial();
	}
	
}

function showTutorial() {

	knockoutBtnShown = ( $("#go-knockout").css("display") == "none") ? false : true;

	$("#overlay").fadeIn();

	i = 0;

	showHelpBaloon(tutorial[i]);

}



/* Shows the help baloon
 * @input	object
 * @return  void
 */
function showHelpBaloon(obj, currentNumber, totalNumber) {
	
	var element = $("#"+obj.elementId);
	var position = obj.position;
	var text = obj.text;
	
	// default offset: 5px
	var offset = (obj.offset == undefined) ? 5 : obj.offset;
	
	// default showPrev: true
	var showPrev = (obj.showPrev == undefined) ? true : obj.showPrev;
	
	// default showNext: true
	var showNext = (obj.showNext == undefined) ? true : obj.showNext;
	
	// default prevLabel: '< Prev'
	var prevLabel = (obj.prevLabel == undefined) ? "< Prev" : obj.prevLabel;
	
	// default nextLabel: 'Next >'
	var nextLabel = (obj.nextLabel == undefined) ? "Next >" : obj.nextLabel;
	
	// default currentNumber
	var currentNumber = (currentNumber == undefined) ? "" : currentNumber;
	
	// default totalNumber
	var totalNumber = (totalNumber == undefined) ? "" : totalNumber;
	
	// element of reference: div#container
	var containerTopPos = $("#container").offset().top;
	var containerLeftPos = $("#container").offset().left;
	
	var elementTopPos = 0;
	var elementLeftPos = 0;
	var elementWidth = 0;
	var elementHeight = 0;
	
	if (obj.elementId != undefined) {
	
		// get the absolute position of the element, with respect to the content div
		elementTopPos = element.offset().top - containerTopPos;
		elementLeftPos = element.offset().left - containerLeftPos;

		// get width and height of the element
		elementWidth = parseInt(element.css("width").slice(0,-2));
		elementHeight = parseInt(element.css("height").slice(0,-2));
		
	}
	
	
	// set width and height of the baloon, if defined
	
	if (obj.width != undefined)
		$("#help-dialog").css("width", obj.width);
	else
		$("#help-dialog").css("width","");
		
	if (obj.height != undefined)
		$("#help-dialog").css("height", obj.height);
	else
		$("#help-dialog").css("height","");
	
	
	var topPos;
	var leftPos;
	
	switch(position) {

		case "top":
			topPos = elementTopPos - offset;
			leftPos = elementLeftPos + Math.floor(elementWidth/2);
			break;

		case "bottom":
			topPos = elementTopPos + elementHeight + offset;
			leftPos = elementLeftPos + Math.floor(elementWidth/2);
			break;

		case "left":
			topPos = elementTopPos + Math.floor(elementHeight/2);
			leftPos = elementLeftPos - offset;
			break;

		case "right":
			topPos = elementTopPos + Math.floor(elementHeight/2);
			leftPos = elementLeftPos + elementWidth + offset;
			break;
			
		case "center":
			topPos = 150;
			leftPos = $("#group-buttons").width() + ($("#content").width()+5)/2 - $("#help-dialog").width()/2;
			break;
		
		default: // center
			topPos = 150;
			leftPos = $("#group-buttons").width() + ($("#content").width()+5)/2 - $("#help-dialog").width()/2;

	}



	$("#help-dialog").fadeIn();
	
	// show/hide help-prev button
	if (showPrev) {

		$("#help-prev").show();
		$("#help-prev").val(prevLabel);

	} else {

		$("#help-prev").hide();

	}
		
	// show/hide help-next button
	if (showNext) {

		$("#help-next").show();
		$("#help-next").val(nextLabel);

	} else {

		$("#help-next").hide();

	}
	
	// set the position of the baloon with respect to the element
	$("#help-dialog").removeClass();
	
	// do not add any arrow class if position == 'center'
	if (position != "center")
		$("#help-dialog").addClass(position + "-baloon");
	
	// set the position of the dialog (top/left)
	
	if (obj.elementId === 'score12') {
		topPos = topPos - 30;
		leftPos = leftPos - 5;
	}
	
	$("#help-dialog").css("top", topPos + "px");
	$("#help-dialog").css("left", leftPos + "px");
	
	// set the content
	$("#help-text").html(text);
	
	// set focus
	if (obj.focus == "prev")
		$("#help-prev").focus();
	else
		$("#help-next").focus();

	
}