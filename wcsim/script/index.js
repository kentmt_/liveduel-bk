// VARIABLES

var isLocalTimeSet = true;								// true = local time is selected, false = host time zone is selected
var currentGroup = 1; 									// Group A selected by default
var letterArray = ["A","B","C","D","E","F","G","H"];	// used to find correspondance between group number and group letter
var isWidgetView = true;								// true = widget view selected, false = list view selected
var firstTwoTeams = new Array(2);						// array containing the first two teams of the current group
var completed = [0,0,0,0,0,0,0,0];						// 0-12, the number of filled scores per group (completed[0] = group A)

// short tag array per group
var group1 = ['br','hr','mx','cm'];
var group2 = ['es','nl','cl','au'];
var group3 = ['co','gr','ci','jp'];
var group4 = ['uy','cr','en','it'];
var group5 = ['ch','ec','fr','hn'];
var group6 = ['ar','ba','ir','ng'];
var group7 = ['de','pt','gh','us'];
var group8 = ['be','dz','ru','kr'];

/**
 *  JSON - Match information.
 *  Structure:
 *  matches["GROUP_LETTER"][MATCH_INDEX]["parameter"], where
 *  GROUP_LETTER = A...H;
 *  MATCH_INDEX = 0...5;
 *  parameter = date|time|location|timezone|team1|flag1|score1|team2|flag2|score2;
 */


/**
 *  JSON containing the points information for each group.
 *  Structure:
 *		* scores.xx		point object of the team xx
 *		* scores.xx.pts	array containing the points against each team of the group (the order is based on the group variables above)
 * 						e.g. scores.br.pts = [0,1,3,3] ==> Brazil drew against Croatia and won against Mexico and Cameroon (same-country
 *						points is always 0, like scores.hr.pts[1] == 0).
 *		* scores.xx.mp	matches played by the team xx
 *		* scores.xx.w	matches won by the team xx
 *		* scores.xx.d	matches drew by the team xx
 *		* scores.xx.l	matches lost by the team xx
 *		* scores.xx.gf	goals for
 *		* scores.xx.ga	goals against
 *
 *	Total points and goals difference is calculated real-time
 */
var scores = {

    'br': {	pts: [0,0,0,0], mp: 0, w: 0, d: 0, l: 0, gf: 0, ga: 0 },
    'hr': { pts: [0,0,0,0], mp: 0, w: 0, d: 0, l: 0, gf: 0, ga: 0 },
    'mx': { pts: [0,0,0,0], mp: 0, w: 0, d: 0, l: 0, gf: 0, ga: 0 },
	'cm': { pts: [0,0,0,0], mp: 0, w: 0, d: 0, l: 0, gf: 0, ga: 0 },
	'es': { pts: [0,0,0,0], mp: 0, w: 0, d: 0, l: 0, gf: 0, ga: 0 },
	'nl': { pts: [0,0,0,0], mp: 0, w: 0, d: 0, l: 0, gf: 0, ga: 0 },
	'cl': { pts: [0,0,0,0], mp: 0, w: 0, d: 0, l: 0, gf: 0, ga: 0 },
	'au': { pts: [0,0,0,0], mp: 0, w: 0, d: 0, l: 0, gf: 0, ga: 0 },
	'co': { pts: [0,0,0,0], mp: 0, w: 0, d: 0, l: 0, gf: 0, ga: 0 },
	'gr': { pts: [0,0,0,0], mp: 0, w: 0, d: 0, l: 0, gf: 0, ga: 0 },
	'ci': { pts: [0,0,0,0], mp: 0, w: 0, d: 0, l: 0, gf: 0, ga: 0 },
	'jp': { pts: [0,0,0,0], mp: 0, w: 0, d: 0, l: 0, gf: 0, ga: 0 },
	'uy': { pts: [0,0,0,0], mp: 0, w: 0, d: 0, l: 0, gf: 0, ga: 0 },
	'cr': { pts: [0,0,0,0], mp: 0, w: 0, d: 0, l: 0, gf: 0, ga: 0 },
	'en': { pts: [0,0,0,0], mp: 0, w: 0, d: 0, l: 0, gf: 0, ga: 0 },
	'it': { pts: [0,0,0,0], mp: 0, w: 0, d: 0, l: 0, gf: 0, ga: 0 },
    'ch': { pts: [0,0,0,0], mp: 0, w: 0, d: 0, l: 0, gf: 0, ga: 0 },
	'ec': { pts: [0,0,0,0], mp: 0, w: 0, d: 0, l: 0, gf: 0, ga: 0 },
	'fr': { pts: [0,0,0,0], mp: 0, w: 0, d: 0, l: 0, gf: 0, ga: 0 },
	'hn': { pts: [0,0,0,0], mp: 0, w: 0, d: 0, l: 0, gf: 0, ga: 0 },
	'ar': { pts: [0,0,0,0], mp: 0, w: 0, d: 0, l: 0, gf: 0, ga: 0 },
	'ba': { pts: [0,0,0,0], mp: 0, w: 0, d: 0, l: 0, gf: 0, ga: 0 },
	'ir': { pts: [0,0,0,0], mp: 0, w: 0, d: 0, l: 0, gf: 0, ga: 0 },
	'ng': { pts: [0,0,0,0], mp: 0, w: 0, d: 0, l: 0, gf: 0, ga: 0 },
	'de': { pts: [0,0,0,0], mp: 0, w: 0, d: 0, l: 0, gf: 0, ga: 0 },
	'pt': { pts: [0,0,0,0], mp: 0, w: 0, d: 0, l: 0, gf: 0, ga: 0 },
	'gh': { pts: [0,0,0,0], mp: 0, w: 0, d: 0, l: 0, gf: 0, ga: 0 },
	'us': { pts: [0,0,0,0], mp: 0, w: 0, d: 0, l: 0, gf: 0, ga: 0 },
	'be': { pts: [0,0,0,0], mp: 0, w: 0, d: 0, l: 0, gf: 0, ga: 0 },
	'dz': { pts: [0,0,0,0], mp: 0, w: 0, d: 0, l: 0, gf: 0, ga: 0 },
	'ru': { pts: [0,0,0,0], mp: 0, w: 0, d: 0, l: 0, gf: 0, ga: 0 },
    'kr': { pts: [0,0,0,0], mp: 0, w: 0, d: 0, l: 0, gf: 0, ga: 0 }
   
};
// scores

var emptyRoundOf16 = [
    ["",""],
    ["",""],
	["",""],
	["",""],
	["",""],
	["",""],
	["",""],
    ["",""] 
];


// FUNCTIONS

/**
 *  Checks if all match scores of the group stage are filled
 *  @input       void
 *  @return      true, if all scores are filled, false otherwise
 */
function areAllFilled() {
    
    var allFilled = true;
    
    for (var i=0; i<completed.length; ++i) {
        
        if (completed[i] != 12) {
            allFilled = false;
            break;
        }
        
    }
    
    return allFilled;
    
}
// areAllFilled()



/**
 *  Given a group number, returns the two teams
 *  with the highest score.
 *  @input  groupNumber 1=A, 8=H
 *  @return placeIndexes
 */
function getFirstByGroup(groupNumber) {
    
	return getSortedTeamArrayOfGroup(groupNumber).slice(0,2);
	
}


function groupTabClicked(groupNum) {
	
	if (groupNum == 9) {
		groupNum = 1;
	}
	
	// save current Group status
	saveGroupStatus();

	// 'disconnect' all tabs
	for (var i=1 ; i<=8; i++) {

		$("#group-"+i).addClass('hover');
		$("#group-"+i).next().removeClass('connected');

	}

	$("#group-"+groupNum).removeClass('hover');
	$("#group-"+groupNum).next().addClass('connected');

	setMatchInformation(groupNum);

}

function showListView() {
	
	$("#view-select").removeClass("widgetSelect").addClass("listSelect");
	
	$('link[href="style/group-stage-widget-view.css"]').attr('href','style/group-stage-list-view.css');
	
}

function showWidgetView() {

	$("#view-select").removeClass("listSelect").addClass("widgetSelect");
	
	$('link[href="style/group-stage-list-view.css"]').attr('href','style/group-stage-widget-view.css');
	
}

function setTime(isLocalTimeSet) {	

	var letterGroup = letterArray[currentGroup-1];

	for (var i=1; i<=6; ++i) {

		// currentMatch = eval("match"+letterGroup+i);
		currentMatch = matches[letterGroup][i-1];

		if (isLocalTimeSet) {
			$("#location"+i).html(currentMatch.local_datetime + ' - ' + currentMatch.location);
		} else {
			$("#location"+i).html(currentMatch.user_datetime + ' - ' + currentMatch.location);
		}
	}
}

/* input: number of fields filled (1-12) */
function updateProgressBar(filledFieldsNum) {

	
	for (var i=1; i<=12; i++) {
		
		if (i < filledFieldsNum) {
			
			$("#prog"+i).addClass("filled");
			$("#prog"+i).removeClass("last-filled");
		
		} else if (i == filledFieldsNum) {
			
			$("#prog"+i).addClass("filled");
			$("#prog"+i).addClass("last-filled");
				
		} else {
			
			$("#prog"+i).removeClass();
			
		}
		
	}
	
	$("#prog-perc").html(Math.floor(filledFieldsNum*100/12)+"%");

}

/* updates the score, renders them, updates the progress bar
 * and, if all matches have been played, shows the selected
 * teams in the knockout bracket on the right of the page
 */
function checkScores() {
	
	var emptyFieldsNum = 0;
	
	// count the number of empty fields for this group
	for (var i=1; i<=6; ++i) {
		
		if ($("#score"+i+"1").val() == "")
			emptyFieldsNum++;
		
		if ($("#score"+i+"2").val() == "")
			emptyFieldsNum++;
			
	}
	
	// update score objects
	updateScores();
	
	// render the scores in the table
	renderScores();
	
	// completed is defined in team-scores.js
	completed[currentGroup-1] = 12-emptyFieldsNum;

	var letter = letterArray[currentGroup-1];

	// remove the team flag background
	$("#1"+letter).removeClass().addClass("flag-small");
	$("#2"+letter).removeClass().addClass("flag-small");
	
	// add team flag background only if all scores are filled, leave empty otherwise
	if (emptyFieldsNum == 0) {		

		// get the first two teams of this group
		var firstTwoTeams = getFirstByGroup(currentGroup);

		// all scores of this group filled in - we already know 1st and 2nd places
		$("#1"+letter).addClass("bg-img " + firstTwoTeams[0] + "-s");
		$("#2"+letter).addClass("bg-img " + firstTwoTeams[1] + "-s");

	}
	
	// check if all match scores of the group stage are filled
	if (areAllFilled()) {
		
		// all scores filled in, show the 'knockout' button
		$("#go-knockout").show();
		$("#knockout-btn-big").show();
		$("#knockout-btn-big").focus();
		
	} else if (!isUserLogined()) {
	
		// hide the knockout button, if visible
		$("#go-knockout").hide();
		$("#knockout-btn-big").hide();
	
		if (emptyFieldsNum == 0 && currentGroup < 8) {

			// don't show 'next group' if group H is the current group

			$("#next-group").show();	// show the 'next group' button
			$("#next-group").focus();	// focus the shown button

		} else {

			$("#next-group").hide();

		}
		
	}

	updateProgressBar(12-emptyFieldsNum);
	
}

/* Random score range: [0,4], based on
http://en.wikipedia.org/wiki/FIFA_World_Cup_records#Team
*/
function fillEmptyScores() {
	
	for (var i=1; i<=6; ++i) {
		
		var $input = $("#score"+i+"1");
		if (!$input.prop('disabled') && $input.val() === "") {
			$input.val(Math.floor(Math.random()*5));
		}
		
		$input = $("#score"+i+"2");
		if (!$input.prop('disabled') && $input.val() === "") {
			$input.val(Math.floor(Math.random()*5));
		}
		
	}
	
	checkScores();
}


function clearAllFields() {

	for (var i=1; i<=6; ++i) {
		
		var $input = $("#score"+i+"1");
		if (!$input.prop('disabled')) {
			$input.val("");
		}
		
		$input = $("#score"+i+"2");
		if (!$input.prop('disabled')) {
			$input.val("");
		}
	}
	
	checkScores();

}

/*	This function saves the status of the visible groupLetter
 *	in the relative json object
 */
function saveGroupStatus(callback) {
	
	var group = letterArray[currentGroup-1]; 
	
	for (var i=0; i<6; ++i) {
		
		var score1val = $("#score"+(i+1)+"1").val();
		var score2val = $("#score"+(i+1)+"2").val();
		
		var score1;
		var score2;
		
		score1 = score1val != "" ? parseInt(score1val) : undefined;
		score2 = score2val != "" ? parseInt(score2val) : undefined;
		
		matches[group][i].score1 = score1;
		matches[group][i].score2 = score2;
		
	}
	
	roundOf16[currentGroup-1] = getFirstByGroup(currentGroup);
	
	if (callback && typeof(callback) === "function") {
		callback();
	}
	
}

/*	This function updates all the match-related fields.
 *	@input	group (int)
 *			integer value that identifies the group to be showed (1=A, 8=H)
 */
function setMatchInformation(group) {
	
	currentGroup = group;

	// reset the first-two-teams array
	firstTwoTeams = ["",""];

	var groupLetter = letterArray[group-1];	

	// Remove all xx-big flags css from .team1 and .team2
	$(".team1").removeClass().addClass("match-flag").addClass("team1");
	$(".team2").removeClass().addClass("match-flag").addClass("team2");

	// render flags in the score table
	renderFlags();

	// Update match information for every match
	for (var i=1; i<=6; ++i) {
		
		// var currentMatch = eval("match"+groupLetter+i);
		var currentMatch = matches[groupLetter][i-1];
		
		var dateTimeLocation = currentMatch.date + " - ";
		
		dateTimeLocation += currentMatch.time;
			
		dateTimeLocation += " - " + currentMatch.location;
		
		// Set date, time and location to the related div
		$("#match-" + i + " > .match-location").html(dateTimeLocation);
		
		// set team names
		$("#match-" + i + " > .name1").html(currentMatch.team1);
		$("#match-" + i + " > .name2").html(currentMatch.team2);
		
		// set team flags
		$("#match-" + i + " > .team1").addClass(currentMatch.flag1+"-big");
		$("#match-" + i + " > .team2").addClass(currentMatch.flag2+"-big");

		// set team score (if already set in this session)
		
		var $input1, $input2, $box;
		$input1 = $("#score"+i+"1");
		$input2 = $("#score"+i+"2");
		
		$input1.val(currentMatch.score1 != undefined ? currentMatch.score1 : "");
		$input2.val(currentMatch.score2 != undefined ? currentMatch.score2 : "");
		$box = $input1.closest('.match-box');
		
		if (currentMatch.disabled) {
			$input1.prop('disabled', true);
			$input2.prop('disabled', true);
			showDisabledToolTip($box);
		} else {
			$input1.prop('disabled', false);
			$input2.prop('disabled', false);
			hideDisabledTooltip($box);
		}
	}
	
	focusOnFirstEmptyField();
	
	// set the time format, according to the variable isLocalTimeSet
	if (!isLocalTimeSet)
		setTime(false);
		
	checkScores();

}

function updateScores() {
	
	var group = eval("group"+currentGroup);
		
	// reset the score of all team, as the are going to be re-calculated
	for (var i=0; i< group.length; ++i) {
				
		var teamScore = scores[group[i]];
		
		// reset all values in the object
		scores[group[i]].pts = [0,0,0,0];
		scores[group[i]].mp = 0;
    	scores[group[i]].w  = 0;
		scores[group[i]].d  = 0;
		scores[group[i]].l  = 0;
		scores[group[i]].gf = 0;
		scores[group[i]].ga = 0;
		
	}
	
	// scan every match score
	for (var i=1; i<=6; i++) {
		
		// retrieve the first team short tag
		var team1 = $("#match-"+i+" > div.team1").removeClass("match-flag").removeClass("team1").attr("class").substr(0,2);
		$("#match-"+i+" > div."+team1+"-big").addClass("match-flag").addClass("team1");
				
		// retrieve the second team short tag
		var team2 = $("#match-"+i+" > div.team2").removeClass("match-flag").removeClass("team2").attr("class").substr(0,2);
		$("#match-"+i+" > div."+team2+"-big").addClass("match-flag").addClass("team2");

		// retrieve the score object of both teams of the match
		var scoreObj1 = scores[team1];
		var scoreObj2 = scores[team2];

		// get the value of the score fields		
		var score1 = parseInt($("#score"+i+"1").val());
		var score2 = parseInt($("#score"+i+"2").val());

		// update the information only if the scores are not empty		
		if ( !isNaN(score1) && !isNaN(score2) ) {
			
			// update the number of matches played
			scoreObj1.mp++;			
			scoreObj2.mp++;
		
			if (score1 > score2) {	// team1 won
			
				scoreObj1.w++;	// increase won matches for team 1
				scoreObj2.l++; // increase lost matches for team 2

				// update direct confrontation info
				scoreObj1.pts[group.indexOf(team2)] = 3;
				scoreObj2.pts[group.indexOf(team1)] = 0;
	
			} else if (score1 < score2) { // team2 won
	
				scoreObj1.l++; // increase lost matches for team 1
				scoreObj2.w++; // increase won matches for team 2			
				
				// update direct confrontation info
				scoreObj1.pts[group.indexOf(team2.substr(0,2))] = 0;
				scoreObj2.pts[group.indexOf(team1)] = 3;

			} else { 		// draw result
			
				// increase draw matches for both team
				scoreObj1.d++;
				scoreObj2.d++;

				// update direct confrontation info
				scoreObj1.pts[group.indexOf(team2)] = 1;
				scoreObj2.pts[group.indexOf(team1)] = 1;
			
			}
			
			// update goals for and goals against number for team 1
			scoreObj1.gf += score1;
			scoreObj1.ga += score2;
			
			// update goals for and goals against number for team 2
			scoreObj2.gf += score2;
			scoreObj2.ga += score1;
			
			// update the score object of both teams of the match
			scores[team1] = scoreObj1;
			scores[team2] = scoreObj2;
			
		}
		
	}
}

function getSortedTeamArrayOfGroup(groupNumber) {
	var teamArray, teamCount, sortedTeamArray = [], index = 0, tmp;
	teamArray= eval("group"+groupNumber);
	teamCount = teamArray.length;
	sortedTeamArray = teamArray;
	
	// Sorting
	for (var i = 0; i < teamCount; ++ i) {
		for (var j = index; j < teamCount; ++ j) {
			
			if (j > index) {
				var teamObj, pts, gd, gf;
				teamObj = scores[sortedTeamArray[j]],
				pts = teamObj.w*3+teamObj.d,
				gd = teamObj.gf-teamObj.ga,
				gf = teamObj.gf;
				
				var cur, curPTS, curGD, curGF;
				cur = scores[sortedTeamArray[index]];
				curPTS = cur.w*3+cur.d;
				
				if (pts == curPTS) {
					curGD = cur.gf - cur.ga;
					
					if (gd == curGD) {
						curGF = cur.gf;
						if (gf >= curGF) {
							tmp = sortedTeamArray[j];
							sortedTeamArray[j] = sortedTeamArray[index];
							sortedTeamArray[index] = tmp;
						}
					} else if (gd > curGD) {
						tmp = sortedTeamArray[j];
						sortedTeamArray[j] = sortedTeamArray[index];
						sortedTeamArray[index] = tmp;
					}
				} else if (pts > curPTS) {
					tmp = sortedTeamArray[j];
					sortedTeamArray[j] = sortedTeamArray[index];
					sortedTeamArray[index] = tmp;
				}
			}
		}
		++ index;
	}
	
	return sortedTeamArray;
}

/* Renders the scores of the object scores (team-scores.js)
 * in the score table.
 */
function renderScores() {
	
	var sortedTeamArray = getSortedTeamArrayOfGroup(currentGroup);
	
	for (var i = 0; i < sortedTeamArray.length; ++ i) {
		var teamObj = scores[sortedTeamArray[i]];
		$("#team"+(i+1)+" > td.pts-flag > img").attr("src","images/flags-50x30/"+sortedTeamArray[i]+".png");
		$("#team"+(i+1)+" > td.pts").html(teamObj.w*3+teamObj.d);
		$("#team"+(i+1)+" > td.mp").html(teamObj.mp);
		$("#team"+(i+1)+" > td.won").html(teamObj.w);
		$("#team"+(i+1)+" > td.draw").html(teamObj.d);
		$("#team"+(i+1)+" > td.lost").html(teamObj.l);
		$("#team"+(i+1)+" > td.gf").html(teamObj.gf);
		$("#team"+(i+1)+" > td.ga").html(teamObj.ga);
		$("#team"+(i+1)+" > td.gd").html(teamObj.gf-teamObj.ga);
	}
	
}

// renders the flags of the current group
function renderFlags() {
	
	var groupArray = eval("group"+currentGroup);
	
	for (var i=0; i<groupArray.length; ++i) {
	
		$("#team"+(i+1)+" > td.pts-flag > img").attr("src","images/flags-50x30/"+groupArray[i]+".png");
		
	}
	
}

function isUserLogined() {
	return window.isUserDoneGroupStage === true;
}

// shows the Knockout page
function goKnockout() {
	
	// save current Group status
	
	saveGroupStatus(function() {
		
		var roundOf16 = [],
			match = [],
			roundOf16JSON;
		
		$('[round-of-sixteen]').each(function(index) {
			
			var $flag = $(this) ;
			
		    if (index%2 === 0) {
		        match = [];
		    }
		    
		    match.push(
    			$flag
		            .removeClass('flag-small bg-img')
		            .prop('className').left(2)
		    );
		    
		    $flag.addClass('flag-small bg-img');
		    
		    if (index%2 === 1) {
		    	roundOf16.push(match);
		    }
		});
		
		roundOf16JSON = JSON.stringify(roundOf16);
		$("#round_of_16").val(roundOf16JSON);
		$('#group_result').val(JSON.stringify(matches));
		$("#resultsForm").submit();
	});
}


function focusOnFirstEmptyField() {
	
	
	/*
	 * This script is wrapped in an anonymous function
	 * the loop interrupts when the first empty fields
	 * is found.
	 */
	(function() {

		for (var i=1; i<=8; ++i) {

			for (var j=1; j<=2; ++j) {

				if ($("#score"+i+j).val() == "") {
					$("#score"+i+j).focus();
					return;

				}
			}
		}

	})();
	
}

function hideDisabledTooltip($matchBox) {
	try {
		$matchBox.tooltipster('destroy');
		$matchBox.prop('title', '');
	} catch (e) {
		
	}
}

function showDisabledToolTip($matchBox) {
	$matchBox.tooltipster({
		 position: 'top'
		, theme: 'tooltipster-noir'
		, delay: 50
		, speed: 100
		, content: 'Time is over, you can not edit anymore'
	});
}

// DOCUMENT READY BODY
$(document).ready(function() {

	// Initialization settings
	$("#group-1").removeClass('hover');
	$("#group-1").next().addClass('connected');

	// Timezone radioes
	$('#local_time_radio').bind('click', function() {
		setTime(true);
	});
	
	$('#your_time_radio').bind('click', function() {
		setTime(false);
	});

	// click on group tab
	$(".group-btn").click(function(event) {
	
		var groupNum;
	
		// if the clicked event has no id, the current div is a flag
		// this first braked is a workaround to the issue that occurs
		// if the user clicks the small flag (no reaction)
		if (event.target.id == "") {

			$(event.target).parent().removeClass('hover');
			$(event.target).parent().next().addClass('connected');

			groupNum = parseInt($(event.target).parent().attr('id').slice(-1));

		} else {

			$(event.target).removeClass('hover');
			$(event.target).next().addClass('connected');

			groupNum = parseInt(event.target.id.slice(-1));

		}

		groupTabClicked(groupNum);
	
	});
	
	
	// click on view selection menu
	$("#view-select").click(function(event) {
		
		$("#view-options").toggle();
		
	});
	

	// click on widget view option
	$("#widget-view-option").click(function() {
						
		if (!isWidgetView)
			showWidgetView();
		
		isWidgetView = true;
		$(this).addClass('active');	
		$("#list-view-option").removeClass('active');	
		
	});
	
	// click on list view select option
	$("#list-view-option").click(function() {
		
		if (isWidgetView)
			showListView();
		
		isWidgetView = false;
		$(this).addClass('active');	
		$("#widget-view-option").removeClass('active');	
		
	});

	// click on '?' button
	$("#help-btn").click(function() {
	
		// show the tutorial
		showTutorial();
		
	});
	
	
	// click on random scores button
	$("#random-scores").click(function() {
		fillEmptyScores();
	});
	
	// click on clear button
	$("#clear").click(function() {
		clearAllFields();
	});

				
	// shows the next group
	$("#next-group").click(function() {
		groupTabClicked(currentGroup+1);
	});

	// set the autotab for the score fields
	$(".team-result").autotab({ maxlength: 1, format: 'number' });

	// update the score information displayed at every key press
	$(".team-result").keyup(function(){
		checkScores();
	});

	// opens the knockout page if clicked
	$("#knockout-btn-big").click(function() {
		
		goKnockout();
		
	});
	
	// opens the knockout page if clicked
	$("#go-knockout").click(function() {
		
		goKnockout();
		
	});
	
	
	// show the group A as default
	setMatchInformation(1);
	
	if(window.roundOf16) {
		
		for (var i = 0; i < window.roundOf16.length; ++ i) {
			
			$('[round-of-sixteen="match' + i + '1"]').removeClass().addClass("flag-small").addClass("bg-img " + roundOf16[i][0] + "-s");
			$('[round-of-sixteen="match' + i + '2"]').removeClass().addClass("flag-small").addClass("bg-img " + roundOf16[i][1] + "-s");
			
		}
		
		$("#go-knockout").show();
		$("#knockout-btn-big").show();
	}
	
	// show the tutorial
	initTutorial();
	
	// Defaud, load from database if passed group-stage or just an empty array
	window.roundOf16 = window.roundOf16 || emptyRoundOf16;
	
	var adminAjax = Ajax
		.forURL('php/saveRealGroupStageResult.php')
		.setLoading(SCREEN_LOADING)
		.onSuccess(function(response) {
			this.showResponseMessage(response);
		});
	
	$('#save_real_group_stage').click(function() {
		
		// save current Group status
		saveGroupStatus(function() {
			adminAjax.setRequestParam('groupResult', JSON.stringify(matches)).run();
		});
	});
	
	var groupStageAjax = Ajax
		.forURL('php/saveGroupStageResult.php')
		.setLoading(SCREEN_LOADING)
		.onSuccess(function(response) {
			this.showResponseMessage(response);
		});
	
	$('#save_your_group_stage').click(function() {
		
		// save current Group status
		saveGroupStatus(function() {
			groupStageAjax.setRequestParam('groupResult', JSON.stringify(matches)).run();
		});
	});
	
});