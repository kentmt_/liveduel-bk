<?php
echo 'LOCAL@ ' . date('Y-m-d H:i:s') . '<br>';
$ld = new DateTime();
echo 'LOCAL TIMEZONE@ ' . ini_get('date.timezone') . '<br>';

date_default_timezone_set('America/Sao_Paulo') . '<br>';
echo 'BRAZIL@ ' . date('Y-m-d H:i:s') . '<br>';

date_default_timezone_set("UTC") . '<br>';
$ud = new DateTime();
echo 'UTC@ ' . $ud->format('Y-m-d H:i:s') . '<br>';

var_dump($ld > $ud);
var_dump($ld < $ud);
var_dump($ld == $ud);
