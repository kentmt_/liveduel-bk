<?php
require_once 'php/db/DBHandler.php';
require_once 'php/lib/Request.php';
require_once 'php/constant.php';
require_once 'php/lib/CmonUtil.php';
Session::start();
if (!Session::isAdminLogined()) {
	header('Location: ' . ROOT_URL);
}

$matchId = Request::getInteger('matchId');
$winOrLose = Request::getString('winOrLose');
$matches = DBHandler::getPlayedMatches();
$result = DBHandler::getMatchResults($matchId, $winOrLose);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>LiveDuel</title>
		<link rel="stylesheet" type="text/css" href="style/core.css" />
		<link rel="stylesheet" type="text/css" href="style/common.css" />
		<link rel="stylesheet" type="text/css" href="style/tooltipster.css" />
		<!-- css files -->
		<link rel="stylesheet" type="text/css" href="style/flags.css" />
		<link rel="stylesheet" type="text/css" href="style/jquery.fancybox.css" />
		<link rel="stylesheet" type="text/css" href="style/user-page.css" />
		
		<!-- js files -->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script src="script/jquery.fancybox.pack.js"></script>
		<script src="script/core.js"></script>
		<script src="script/jquery.tooltipster.min.js"></script>
		
		<style type="text/css">
			#filter {
				width: 95%;
				background-color: #fff;
				margin: 10px auto 0;
			}
			
			.grid-body {
				top: 0;
			}
			.grid-ctn {
				margin-top: 10px;
			}
		</style>
		
	</head>
	<body>
	
		<?php require 'user-bar.php'; ?>
	
		<div id="container" class="container">
			
			<!-- Header -->
			<div id="header" class="header">
				
				<a id="logo_link" href="index.php">&nbsp;</a>
			
				<div id="user_page_title">
					&nbsp;
				</div>
			</div>
			
			<div id="filter" class="clearfix">
				<form id="filter_form" type="get">
					<div class="form-item-wrap w80">
						<div class="form-item">
							<div class="form-label-wrap">
								<label class="form-label">Matches:</label>
							</div>
							<div class="form-input-wrap">
								<select class="form-input" name="matchId" onchange="document.getElementById('filter_form').submit()">
									<option class="default" value="0">Choose a match</option>
									<?php foreach ($matches as $match) : ?>
									
									<option value="<?php echo $match['id'] ?>" <?php echo $matchId == $match['id'] ? 'selected' : ''?> >
										
										<?php if ($match['group_stage'] == 1) :  ?>
											Group <?php echo $match['group'] ?>
										<?php elseif ($match['round_of_16'] == 1) : ?>
											Round of 16
										<?php elseif ($match['quarter_final'] == 1) : ?>
											Quarter final
										<?php elseif ($match['semi_final'] == 1) : ?>
											Semi final
										<?php elseif ($match['third_place_match'] == 1) : ?>
											Play-off for 3rd
										<?php elseif ($match['final_match'] == 1) : ?>
											The final match
										<?php endif; ?>
										
										-
										<?php echo $match['start_date'] ?>
										<?php echo $match['start_time'] ?>
										
										-
										<?php echo $match['location'] ?>
										
										-
										<?php echo $match['country1_name'] ?>
										vs
										<?php echo $match['country2_name'] ?>
									</option>
									
									<?php endforeach; ?>
								</select>
							</div>
						</div>
					</div>
					<div class="form-item-wrap w20">
						<div class="form-item">
							<div class="form-label-wrap">
								<label class="form-label">Win or lose:</label>
							</div>
							<div class="form-input-wrap">
								<select class="form-input" name="winOrLose" onchange="document.getElementById('filter_form').submit()">
									<option value="all" <?php echo $winOrLose === 'all' ? 'selected' : ''?> >
										All
									</option>
									<option value="win" <?php echo $winOrLose === 'win' ? 'selected' : ''?> >
										Win
									</option>
									<option value="lose" <?php echo $winOrLose === 'lose' ? 'selected' : ''?> >
										Lose
									</option>
								</select>
							</div>
						</div>
					</div>
				</form>
			</div>
			
			<div id="grid_area">
				<div class="grid-ctn">
					<div class="grid-body group-grid">
						<table class="grid">
							<thead>
								<tr>
									<th>User</th>
									<th>Team</th>
									<th>Real</th>
									<th>Predicted</th>
									<th>Result</th>
									<th>Points won</th>
								</tr>
							</thead>
							<tbody>
							<?php foreach ($result as $match) : ?>
							<?php $valid = $match['is_predicted'] && $match['is_valid_result']; ?>
								<tr class="match">
									<td style="text-align: left; padding: 5px">
										<?php echo $match['username'] ?>
										<input name="userId" type="hidden" value="<?php echo $match['user_id'] ?>" />
										<input name="matchBetId" type="hidden" value="<?php echo $match['match_bet_id'] ?>" />
									</td>
									<td>
										<div class="cell-split clearfix team team1">
											<div class="flag flag1 <?php echo $match['country1_code'] ?>">
											
											</div>
											<div class="teamname teamname1">
												<?php echo $match['country1_name'] ?>
											</div>
										</div>
										<div class="cell-split clearfix team team2">
											<div class="flag flag2 <?php echo $match['country2_code'] ?>">
											
											</div>
											<div class="teamname teamname2">
												<?php echo $match['country2_name'] ?>
											</div>
										</div>
									</td>
									<td>
										<div class="cell-split clearfix scores-wrap">
											<span class="score-box">
												<?php echo $match['real_fulltime_result1'] ?>
											</span>
											<span class="score-box">
												<?php echo $match['real_extratime_result1'] ?>
											</span>
											<span class="score-box">
												<?php echo $match['real_penalty_result1'] ?>
											</span>
										</div>
										<div class="cell-split clearfix scores-wrap">
											<span class="score-box">
												<?php echo $match['real_fulltime_result2'] ?>
											</span>
											<span class="score-box">
												<?php echo $match['real_extratime_result2'] ?>
											</span>
											<span class="score-box">
												<?php echo $match['real_penalty_result2'] ?>
											</span>
										</div>
									</td>
									<td>
										<div class="cell-split clearfix scores-wrap">
											<span class="score-box">
												<?php echo $valid ? $match['fulltime_result1'] : '-' ?>
											</span>
											<span class="score-box">
												<?php echo $valid ? $match['extratime_result1'] : '-' ?>
											</span>
											<span class="score-box">
												<?php echo $valid ? $match['penalty_result1'] : '-' ?>
											</span>
										</div>
										<div class="cell-split clearfix scores-wrap">
											<span class="score-box">
												<?php echo $valid ? $match['fulltime_result2'] : '-' ?>
											</span>
											<span class="score-box">
												<?php echo $valid ? $match['extratime_result2'] : '-' ?>
											</span>
											<span class="score-box">
												<?php echo $valid ? $match['penalty_result2'] : '-'?>
											</span>
										</div>
									</td>
									<td>
										<?php if (!$valid) : ?>
											Miss
										<?php elseif ($match['correctResult']) : ?>
											Win
										<?php else : ?>
											Lose
										<?php endif; ?>
									</td>
									<td>
									<?php
										if ($valid && $match['correctResult']) {
											echo '50 for correct result';
											if ($match['correctScore']) {
												echo '<br>';
												echo '100 for correct score';
											}
										}
									?>
									</td>
								</tr>
							<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		
		</div>
		<!-- div#container -->

		<div id="overlay"></div>
		
	</body>
</html>