<?php 
if(!isset($_COOKIE['user'])) {
	header("Location: index.php");
}

$user = $_COOKIE['user'];
include("connect.php");

$query = "SELECT * FROM top3 WHERE user = '" . $user . "'";
$result = mysql_query($query);
$num_rows = mysql_num_rows($result);

if($num_rows > 0) {
	header("Location: home.php");		
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Pitch Contest - Overall Leaderboard</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
        rel="stylesheet">
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/pages/dashboard.css" rel="stylesheet">
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
	
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.3.2/jquery.mobile-1.3.2.min.css">
<script src="js/jquery-1.9.1.min.js"></script>
<script src="js/jquery.mobile-1.3.2.min.js"></script>

<script>
$(document).ready(function() {
	var status = 0;
	
	$('input[type="checkbox"]').bind( "change", function() {
   if($(this).is(':checked')){
     // perform operation for checked
	 status = status + 1;
   }
   else{
     // perform operation for unchecked
	 status = status - 1;
   }
   
   $("#status").val(status);

});


// Code for submit
$("#submit").on("click", function() {
	var status = 0;
	$("input[type='checkbox']:checked").each(
	    function() {
	       status = status + 1;
	    }
	);
	$("#status").val(status);
	if(status < 3 || status > 3) {
		return false;
	}
});



});
</script>
	
</head>
<body>

<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="index.php" style = "color: #fff;"><img src = "img/logo.png" width = "150" height = "30" /></a>
      <div class="nav-collapse">
        <ul class="nav pull-right">
          <li>
		  <a href="javascript:location.replace('./home.php');"  ><i class="icon-home"></i> Go to Home</a>
          </li>
          <li>
		  <a href="javascript:location.replace('./logout.php');"  ><i class="icon-signout"></i> Logout</a>
          </li>
          
        </ul>
       
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
<!-- /navbar -->

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="span6">
		  <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-trophy"></i>
              <h3>Guess who will win ?</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			
			<form action = "gamesuccess.php" method = "POST" data-ajax="false" id = "formpost">
				<input type = "hidden" name = "status" id = "status">
				
              <table class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>Companies</th>
                    <th>Coins</th>
                    <th class="td-actions">Select Top 3</th>
                  </tr>
                </thead>
                <tbody>
				<?php 
					include("connect.php");
					$query = "SELECT SUM(points) AS total FROM pitch";
					$result = mysql_query($query);
					while($row = mysql_fetch_array($result)) {
						$total = $row['total'];
					}
					
					$query = "SELECT * FROM pitch WHERE points > 0 ORDER BY id";
					$result = mysql_query($query);
					
					$count = 1;
					while($row = mysql_fetch_array($result)) {
						$pitchname = $row['pitchname'];
						$points = $row['points'];
						$divide = $points / $total;
						$minus = 1 - $divide;
						$minus = round($minus, 2);
						$final = $minus * 100;
				?>
                  <tr>
                    <td> <?php echo $pitchname; ?> </td>
                    <td style = "background: #333; color: #fff; font-size: 2.0em; text-align: center;"> <?php echo $final; ?> <font size = "2px">coins</font></td>
                    <td class="td-actions">
						<div data-role="fieldcontain">
							<fieldset data-role="controlgroup" data-type="horizontal" data-role="fieldcontain">
							<input type="checkbox" name="top3[]" id="checkbox-1" class="custom" value = "<?php echo $pitchname; ?>"/>
							<label for="checkbox-1"><i class="icon-trophy"></i></label>
							</fieldset>
						</div>
					</td>
                  </tr>
                </tbody>
				<?php 
				}
				?>
              </table>
			  <button id = "submit" name = "submit" data-theme="b">Submit Top 3</button>
			  </form>
			  
			  
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget -->
          
		  <a href="javascript:location.replace('./home.php');" class="button btn btn-large"  >
				<i class="icon-home"></i> Back to Home</a>
        </div>
        <!-- /span6 -->
        
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<div class="footer">
  <div class="footer-inner">
    <div class="container">
      <div class="row">
        <div class="span12"> Copyright &copy; 2013 - LiveDuel.com </div>
        <!-- /span12 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /footer-inner --> 
</div>
<!-- /footer --> 
<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="js/jquery-1.7.2.min.js"></script> 
<script src="js/bootstrap.js"></script>
 
<script src="js/base.js"></script> 
<script>     

</script>
</body>
</html>
