<?php

if(isset($_COOKIE['user'])) {
	header("Location: home.php");
}

if(isset($_POST['submit'])) {
	$emailid = $_POST['emailid'];
	$password = $_POST['password'];
	$password = md5($password);
	
	include("connect.php");
	$query = "SELECT * FROM user WHERE email = '" . $emailid . "' AND password = '" . $password . "'";
	$result = mysql_query($query);
	$num_rows = mysql_num_rows($result);
	
	if($num_rows <= 0) {
		$login = 0;
	}
	else {
		$login = 1;
		setcookie("user", $emailid, time()+365*24*60*60);
		header("Location: home.php");
	}
}

?>
<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Pitch Contest Website</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes"> 
    
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />

<link href="css/font-awesome.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/pages/signin.css" rel="stylesheet" type="text/css">

</head>

<body>
	
	<div class="navbar navbar-fixed-top">
	
	<div class="navbar-inner">
		
		<div class="container">
			
			<a class="brand" href="index.html">
				<img src = "img/logo.png" width = "150" height = "30" />				
			</a>		
				
		</div> <!-- /container -->
		
	</div> <!-- /navbar-inner -->
	
</div> <!-- /navbar -->



<div class="account-container">
	
	<div class="content clearfix">
		
		<form action="" method="post">
		
			<h1>Member Login</h1>		
			
			<div class="login-fields">
				
				
				<div class="field">
					<label for="emailid">Email: </label>
					<input type="text" id="emailid" name="emailid" value="" placeholder="Email Address" class="login username-field" />
				</div> <!-- /field -->
				
				<div class="field">
					<label for="password">Password:</label>
					<input type="password" id="password" name="password" value="" placeholder="Password" class="login password-field"/>
				</div> <!-- /password -->
				
				<?php 
				if(isset($login) AND $login = 1) {
				?>
				<div class="field">
					<h4>Incorrect Login Details.</h4>
				</div> <!-- /password -->
				<?php 
				}
				?>
				
			</div> <!-- /login-fields -->
			
			<div class="login-actions">
						
				<a href="index.php" data-theme="b" class="button btn btn-large">Register</a> 
				<input type="submit" data-theme="b" class="button btn btn-success btn-large" name="submit" value = "Login">
				
				
			</div> <!-- .actions -->
			
		</form>
		
	</div> <!-- /content -->
	
</div> <!-- /account-container -->

<script src="js/jquery-1.7.2.min.js"></script>
<script src="js/bootstrap.js"></script>

<script src="js/signin.js"></script>

</body>

</html>
