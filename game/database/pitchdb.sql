-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 29, 2013 at 08:55 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pitchdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `pitch`
--

CREATE TABLE IF NOT EXISTS `pitch` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `pitchname` varchar(100) NOT NULL,
  `points` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `pitch`
--

INSERT INTO `pitch` (`id`, `pitchname`, `points`) VALUES
(1, 'LiveDuel', 0),
(2, 'FoodCloud', 0),
(3, 'Senddr', 0),
(4, 'Allogen Biotech', 0),
(5, 'Game Spark Technologies Limited', 0),
(6, 'SparkPage', 0),
(7, 'InverCloud', 0),
(8, 'LearnUpon', 0),
(9, 'Temptster', 0),
(10, 'MMSOFT Design', 0),
(11, 'Pharmapod Ltd', 0),
(12, 'Bizimply', 0),
(13, 'FabAllThings', 0),
(14, 'NVMdurance Limited', 0),
(15, 'AdMe Apps Ltd', 0),
(16, 'Fonsense', 0),
(17, 'Popdeem', 0),
(18, 'Medxnote', 0),
(19, 'FanFootage', 0),
(20, 'Truepivot', 0),
(21, 'Zinc Software Limited', 0),
(22, 'Hiri', 0),
(23, 'GetHealth', 0),
(24, 'Bragbet', 0),
(25, 'Tempity', 0),
(26, 'Pulsate', 0),
(27, 'Viddyad', 0),
(28, 'Scurri', 0),
(29, 'Sedicii', 0),
(30, 'Sians Plan', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pitch_top3`
--

CREATE TABLE IF NOT EXISTS `pitch_top3` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `pitchname` varchar(100) NOT NULL,
  `points` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

CREATE TABLE IF NOT EXISTS `rating` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `pitchname` varchar(100) NOT NULL,
  `user` varchar(100) NOT NULL,
  `points` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Table structure for table `rating_top3`
--

CREATE TABLE IF NOT EXISTS `rating_top3` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `pitchname` varchar(100) NOT NULL,
  `user` varchar(100) NOT NULL,
  `points` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `top3`
--

CREATE TABLE IF NOT EXISTS `top3` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `user` varchar(100) NOT NULL,
  `company1` varchar(100) NOT NULL,
  `company2` varchar(100) NOT NULL,
  `company3` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Table structure for table `top_final`
--

CREATE TABLE IF NOT EXISTS `top_final` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `user` varchar(100) NOT NULL,
  `company1` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `first_status` int(10) NOT NULL,
  `final_status` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
