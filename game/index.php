<?php
if(isset($_COOKIE['user'])) {
	header("Location: home.php");
}
?>
<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Pitch Contest Website - Sign up here</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes"> 
    
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />

<link href="css/font-awesome.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/pages/signin.css" rel="stylesheet" type="text/css">

<script src="js/jquery-1.9.1.min.js"></script>


</head>

<body>
	
	<div class="navbar navbar-fixed-top">
	
	<div class="navbar-inner">
		
		<div class="container">
			
			<a class="brand" href="index.php">
				<img src = "img/logo.png" width = "150" height = "30" />				
			</a>		
				
		</div> <!-- /container -->
		
	</div> <!-- /navbar-inner -->
	
</div> <!-- /navbar -->


<div class="account-container">
	
	<div class="content clearfix">
		
		<form action="signupdone.php" method="post">
		
			<h1>Signup</h1>		
			
			<div class="login-fields">
				
				<p>Please provide your details</p>
				
				<div class="field">
					<label for="fullname">Name: </label>
					<input type="text" id="fullname" name="fullname" value="" placeholder="Full Name" class="login username-field" required />
					<div id = "fullnameerror"></div>
				</div> <!-- /field -->
				
				<div class="field">
					<label for="username">Email: </label>
					<input type="text" id="emailid" name="emailid" value="" placeholder="Email Address" class="login email-field" required />
					<div id = "emailerror"></div>
				</div> <!-- /field -->
				
				<div class="field">
					<label for="password">Password:</label>
					<input type="password" id="password" name="password" value="" placeholder="Password" class="login password-field" required />
					<div id = "passworderror"></div>
				</div> <!-- /password -->
				
				<div class="field">
					<label for="password">Confirm Password:</label>
					<input type="password" id="confirmpassword" name="confirmpassword" value="" placeholder="Confirm Password" class="login password-field" required />
					<div id = "confirmerror"></div>
				</div> <!-- /password -->
				
			</div> <!-- /login-fields -->
			
			<div class="login-actions">
					
				<a href="login.php" data-theme="b" class="button btn btn-large">Login</a> 
				<input type="submit" data-theme="b" class="button btn btn-success btn-large" name="submit" id = "submit" value = "Register">
				
			</div> <!-- .actions -->
			
		</form>
		
	</div> <!-- /content -->
	
</div> <!-- /account-container -->



<script>

function validateEmail(email) {
	    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	    if (filter.test(email)) {
	        return true;
	    }
	    else {
	        return false;
	    }
}


$(document).ready(function() {
	$('input[type="submit"]').attr('disabled','disabled');
	
	$("#emailerror").hide();
	$("#emailid").keyup(function() {
		var email = $('#emailid').val();
	        if ($.trim(email).length == 0) {
				$("#emailerror").show();
	            $("#emailerror").html('<img src = "img/warning.png" /> Please email address');
				$('input[type="submit"]').attr('disabled','disabled');
	            e.preventDefault();
	        }
		    if (validateEmail(email)) {
			
			
				$.ajax
				({
					type: "GET",
					url: "check_available.php",
					data: "email=" + email,
					cache: false,
					success: function(msg) {
						if (msg == 1) {
							$("#emailerror").show();
							$("#emailerror").html('<img src = "img/warning.png" /> This email already have account');
							$('input[type="submit"]').attr('disabled','disabled');
						}
						else {
							$("#emailerror").hide();
						}
					}
				});
		
		
					$('input[type="submit"]').removeAttr('disabled','disabled');
				}
			else {
				$("#emailerror").show();
	            $("#emailerror").html('<img src = "img/warning.png" /> Enter valid email address');
				$('input[type="submit"]').attr('disabled','disabled');
	            e.preventDefault();
			}
	});
	
	
	$("#fullnameerror").hide();
	$("#fullname").keyup(function() {
		var fullname = $('#fullname').val();
	        if ($.trim(fullname).length <= 4) {
				$("#fullnameerror").show();
	            $("#fullnameerror").html('<img src = "img/warning.png" /> Enter Full Name');
				$('input[type="submit"]').attr('disabled','disabled');
	            e.preventDefault();
	        }
		    else {
				$("#fullnameerror").hide();
				$('input[type="submit"]').removeAttr('disabled','disabled');
			}
			
	});
	
	
	$("#passworderror").hide();
	$("#password").keyup(function() {
		var password = $('#password').val();
	        if ($.trim(password).length < 6) {
				$("#passworderror").show();
	            $("#passworderror").html('<img src = "img/warning.png" /> Should be 6 or more characters');
				$('input[type="submit"]').attr('disabled','disabled');
	            e.preventDefault();
	        }
		    else {
				$("#passworderror").hide();
				$('input[type="submit"]').removeAttr('disabled','disabled');
			}
			
	});
	
	
	$("#confirmerror").hide();
	$("#confirmpassword").keyup(function() {
		var confirmpassword = $('#confirmpassword').val();
		var password = $('#password').val();
	        if (confirmpassword != password) {
				$("#confirmerror").show();
				$("#confirmerror").html('<img src = "img/warning.png" /> Passwords do not match');
				$('input[type="submit"]').attr('disabled','disabled');
	            e.preventDefault();
	        }
		    else {
				$("#confirmerror").hide();
				$('input[type="submit"]').removeAttr('disabled','disabled');
			}
			
	});
	
});

</script>
<script src="js/bootstrap.js"></script>

<script src="js/signin.js"></script>

</body>

</html>
