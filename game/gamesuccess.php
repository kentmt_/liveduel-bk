<?php 
if(!isset($_COOKIE['user'])) {
	header("Location: index.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Pitch Contest - Overall Leaderboard</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
        rel="stylesheet">
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/pages/dashboard.css" rel="stylesheet">
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
	
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.3.2/jquery.mobile-1.3.2.min.css">
<script src="js/jquery-1.9.1.min.js"></script>
<script src="js/jquery.mobile-1.3.2.min.js"></script>

<script>
$(document).ready(function() {
	var status = 0;
	
	$('input[type="checkbox"]').bind( "change", function() {
   if($(this).is(':checked')){
     // perform operation for checked
	 status = status + 1;
   }
   else{
     // perform operation for unchecked
	 status = status - 1;
   }
   
   $("#status").val(status);

});


// Code for submit
$("#submit").on("click", function() {
	var status = 0;
	$("input[type='checkbox']:checked").each(
	    function() {
	       status = status + 1;
	    }
	);
	$("#status").val(status);
	if(status < 3 || status > 3) {
		return false;
	}
});



});
</script>
	
</head>
<body>

<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="index.php" style = "color: #fff;"><img src = "img/logo.png" width = "150" height = "30" /></a>
      <div class="nav-collapse">
        <ul class="nav pull-right">
          <li>
		  <a href="javascript:location.replace('./home.php');"  ><i class="icon-home"></i> Go to Home</a>
          </li>
          <li>
		  <a href="javascript:location.replace('./logout.php');"  ><i class="icon-signout"></i> Logout</a>
          </li>
          
        </ul>
       
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
<!-- /navbar -->

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="span6">
		  <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-trophy"></i>
              <h3>Game Status</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content" style = "padding: 20px;">
			<?php
				if(isset($_POST['submit'])) {
					$selected = $_POST['status'];
					$top3 = $_POST['top3'];
					if($selected > 3 OR $selected < 3) {
						echo "Please select 3 winners";
					}
					else {
					$user = $_COOKIE['user'];
					include("connect.php");
					
					$query = "SELECT * FROM top3 WHERE user = '" . $user . "'";
					$result = mysql_query($query);
					$num_rows = mysql_num_rows($result);
					if($num_rows > 0) {
						echo "<h3>You have already selected your top 3 winners in the game</h3><br>";	
						
						while($row = mysql_fetch_array($result)) {
							echo "<i class=icon-trophy></i> " . $row['company1'] . "<br>";
							echo "<i class=icon-trophy></i> " . $row['company2'] . "<br>";
							echo "<i class=icon-trophy></i> " . $row['company3'] . "<br>";
						}
					}
					
					else {
						$company1 = $top3[0];
						$company2 = $top3[1];
						$company3 = $top3[2];
						
						$query = "INSERT INTO top3(user, company1, company2, company3) VALUES('" . $user . "', '" . $company1 .
						        "', '" . $company2 . "', '" . $company3 . "')";
						mysql_query($query);

						echo "<h3>Thank you for participating in the game.</h3><br>";	
						}
						
					}
				}
			?>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget -->
          
		  <a href="javascript:location.replace('./home.php');" class="button btn btn-large"  >
				<i class="icon-home"></i> Back to Home</a>
        </div>
        <!-- /span6 -->
        
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<div class="footer">
  <div class="footer-inner">
    <div class="container">
      <div class="row">
        <div class="span12"> Copyright &copy; 2013 - LiveDuel.com </div>
        <!-- /span12 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /footer-inner --> 
</div>
<!-- /footer --> 
<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="js/jquery-1.7.2.min.js"></script> 
<script src="js/bootstrap.js"></script>
 
<script src="js/base.js"></script> 
<script>     

</script>
</body>
</html>
