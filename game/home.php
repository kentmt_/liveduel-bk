<?php 
if(!isset($_COOKIE['user'])) {
	header("Location: index.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Pitch Contest Website</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
        rel="stylesheet">
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/pages/dashboard.css" rel="stylesheet">
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="index.php"><img src = "img/logo.png" width = "150" height = "30" /></a>
      <div class="nav-collapse">
        <ul class="nav pull-right">
          <li>
		  <a href="javascript:location.replace('./logout.php');"  ><i class="icon-signout"></i> Logout</a>
          </li>
          
        </ul>
       
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
<!-- /navbar -->
<div class="subnavbar">
  
  <!-- /subnavbar-inner --> 
</div>
<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="span6">
          
		  <div class="widget">
            <div class="widget-header"> <i class="icon-dashboard"></i>
              <h3>Dashboard</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			
			<?php
			  include("connect.php");
			  $user = $_COOKIE['user'];
			  $query = "SELECT * FROM top3 WHERE user = '" . $user . "'";
					$result = mysql_query($query);
					$num_rows = mysql_num_rows($result);
					if($num_rows > 0) {
						echo "<h3>You have already selected your top 3 winners in the game</h3><br>";	
						
						while($row = mysql_fetch_array($result)) {
							echo "<i class=icon-trophy></i> " . $row['company1'] . "<br>";
							echo "<i class=icon-trophy></i> " . $row['company2'] . "<br>";
							echo "<i class=icon-trophy></i> " . $row['company3'] . "<br>";
						}
						echo "<br><br>";
					}
			  ?>
			
				
			
              <div class="shortcuts"><a href="scorecard.php" class="shortcut"><i class="shortcut-icon icon-list-alt"></i><span
                                        class="shortcut-label">Score Card</span> </a><a href="leaderboard.php" class="shortcut"><i
                                            class="shortcut-icon icon-bookmark"></i><span class="shortcut-label">My Leaderboard</span> </a><a href="overall.php" class="shortcut"><i class="shortcut-icon icon-trophy"></i> <span class="shortcut-label">Overall Leaderboard</span> </a><?php if($num_rows <= 0) {?><a href="game.php" class="shortcut"> <i class="shortcut-icon icon-heart"></i><span class="shortcut-label">Game</span> </a>
<?php } ?>											</div>
              <!-- /shortcuts --> 
            
			
			
			</div>
            <!-- /widget-content -->

			<br>
			  
          </div>
          <!-- /widget -->
          
        </div>
        <!-- /span6 -->
        
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<div class="footer">
  <div class="footer-inner">
    <div class="container">
      <div class="row">
        <div class="span12"> Copyright &copy; 2013 - LiveDuel.com </div>
        <!-- /span12 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /footer-inner --> 
</div>
<!-- /footer --> 
<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="js/jquery-1.7.2.min.js"></script>  
<script src="js/bootstrap.js"></script>
 
<script src="js/base.js"></script> 
<script>     

    </script><!-- /Calendar -->
</body>
</html>
