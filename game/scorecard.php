<?php 
if(!isset($_COOKIE['user'])) {
	header("Location: index.php");
}

if(isset($_COOKIE['user'])) {
	$user = $_COOKIE['user'];
}

if(isset($_POST['submit'])) {
	$pitchname = $_POST['pitchname'];
	$para1 = $_POST['para1'];
	$para2 = $_POST['para2'];
	$para3 = $_POST['para3'];
	$para4 = $_POST['para4'];
	$para5 = $_POST['para5'];
	
	$total = $para1 + $para2 + $para3 + $para4 + $para5;
	
	include("connect.php");
	$query = "INSERT INTO rating(pitchname, user, points) VALUES('" . $pitchname . "', '" . $user . "', '" . $total . "')";
	mysql_query($query);
	
	$query = "UPDATE pitch SET points = points + '" . $total . "' WHERE pitchname = '" . $pitchname . "'";
	mysql_query($query);
	
	$feedback_done = 1;
}
else {
	$feedback_done = 0;
}
?>
<!DOCTYPE html>
<html lang="en">
  
 <head>
    <meta charset="utf-8">
    <title>Mobile Website - Scorecard</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes"> 
    
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />

<link href="css/font-awesome.css" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/pages/signin.css" rel="stylesheet" type="text/css">


<link rel="stylesheet" href="http://code.jquery.com/mobile/1.3.2/jquery.mobile-1.3.2.min.css">
<script src="js/jquery-1.9.1.min.js"></script>
<script src="js/jquery.mobile-1.3.2.min.js"></script>

<script>
$(document).ready(function() {

	$("#pitchname").change(function() {
		var pitchname = $("#pitchname").val();
		
		$.ajax
		({
			type: "GET",
			url: "check_already.php",
			data: "pitchname=" + pitchname,
			cache: false,
			success: function(msg) {
				if (msg == 1) {
					$("#feedback").html("You have already rated this pitch !!");
					$("#pitch_error").html("");
				}
				else {
					$("#pitch_error").html("");
					$("#feedback").html("");
				}
			}
		});
	});
	
	
});

function validate() {
	
	if(document.feedback_form.pitchname.value == "Choose Pitch Name") {
	document.getElementById("pitch_error").innerHTML="Select the pitch";
	return false;
	}
	
	if(document.getElementById("feedback").innerHTML == "You have already rated this pitch !!") {
	document.getElementById("feedback").innerHTML="You have already rated this pitch !!";
	return false;
	}
	
}


</script>

</head>

<body>
	
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="index.php" style = "color: #fff;"><img src = "img/logo.png" width = "150" height = "30" /></a>
      <div class="nav-collapse">
        <ul class="nav pull-right">
			
		  <li>
		  <a href="javascript:location.replace('./home.php');"  ><i class="icon-home"></i> Go to Home</a>
          </li>
          <li>
		  <a href="javascript:location.replace('./logout.php');"  ><i class="icon-signout"></i> Logout</a>
          </li>
		  
        </ul>
       
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
<!-- /navbar -->

<div class="account-container register">
	
	<div class="content clearfix">
		
		<form action="" method="post" data-ajax="false" name = "feedback_form" onsubmit = "return validate();">
		
			<h1 style = "font-size: 1.5em;"><img src = "img/feedback.png" /> Give Feedback</h1>			
			<div id = "feedback" name = "feedback"></div>
			<div class="login-fields">
				<?php 
				if(isset($feedback_done) AND $feedback_done == 1) {
				?>
				<div class="field">
					<h4>Feedback posted. Now choose other company.</h4>
				</div>
				<?php 
				}
				?>
				
				<div class="field">	
					<select id = "pitchname" name = "pitchname" class="login" />
						<option>Choose Pitch Name</option>
						<?php
							include("connect.php");
							$query = "SELECT pitchname FROM pitch";
							$result = mysql_query($query);
							
							while($row = mysql_fetch_array($result)) {
						?>
						<option value = "<?php echo $row['pitchname']; ?>"><?php echo $row['pitchname']; ?></option>
						<?php 
						}
						?>
						
					</select>
					
				</div> <!-- /field -->
				
				<div id = "pitch_error"></div>
				<hr>
				
				<p><img src = "img/preso.png" /> Overall Presentation</p>
				<div class="field">
						
						<div data-role="fieldcontain">
							<input type="range" name="para1" id="slider" value="0" min="0" max="5"  />
						</div>
						
				</div> <!-- /field -->
				
				<p><img src = "img/idea.png" width = "16" height = "16" /> Quality of the Idea</p>
				<div class="field">
						<div data-role="fieldcontain" >
							<input type="range" name="para2" id="slider" value="0" min="0" max="5"  />
						</div>
				</div> <!-- /field -->
				
				<p><img src = "img/industry.png" width = "16" height = "16" /> Size of the Industry</p>
				<div class="field">
						<div data-role="fieldcontain">
							<input type="range" name="para3" id="slider" value="0" min="0" max="5"  />
						</div>
				</div> <!-- /field -->
				
				<p><img src = "img/team.png" width = "16" height = "16" /> Quality of the Team</p>
				<div class="field">
						<div data-role="fieldcontain">
							<input type="range" name="para4" id="slider" value="0" min="0" max="5"  />
						</div>
				</div> <!-- /field -->
				
				<p><img src = "img/funds.png" width = "16" height = "16" /> Likelihood of Raising Funds</p>
				<div class="field">
						<div data-role="fieldcontain">
							<input type="range" name="para5" id="slider" value="0" min="0" max="5"  />
						</div>
				</div> <!-- /field -->
				
			</div> <!-- /login-fields -->
			
				<button type="submit" data-theme="b" name="submit" value="submit-value" >Submit</button>
				<a href="javascript:location.replace('./home.php');" class="button btn btn-large"  >
				<i class="icon-home"></i> Back to Home</a>
			
		</form>
		
	</div> <!-- /content -->
	
</div> <!-- /account-container -->

<br><br>


<script src="js/jquery-1.7.2.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/signin.js"></script>

	<script type="text/javascript" src="jquery/jquery.mobile.js"></script>
	<script type="text/javascript" src="jquery/jRating.jquery.js"></script>
	
	
</body>

 </html>
