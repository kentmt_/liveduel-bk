<?php session_start(); 
	$to = $_REQUEST["email"];
	$subject = 'Invitation to LiveDuel.com';
	$message = 'Hello, this is an invitation from '.$_SESSION['USER_NAME'].' to register to www.liveduel.com';
	$headers = 'From: support@liveduel.com' . "\r\n" .
		'Reply-To: support@liveduel.com' . "\r\n" .
		'X-Mailer: PHP/' . phpversion();

	mail($to, $subject, $message, $headers);
	header("Location: invite.php?action=success");
?>