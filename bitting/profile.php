<?php session_start(); 
	if(!isset($_SESSION['USER_ID'])){ 
		header("Location: index.html");
	}
	include('db_config.php');
	$userId = $_SESSION['USER_ID'];

	$userTotalPoints = 0;
	$sql = "SELECT 	( SELECT COALESCE(SUM(bets.points),0) FROM bets WHERE bets.user_id = l.id AND bets.is_correct = 'Y')+
					( SELECT COALESCE(SUM(matches_bets.points),0) FROM matches_bets WHERE matches_bets.user_id = l.id AND matches_bets.is_correct = 'Y')+
					( SELECT COALESCE(SUM(competition_bets.points),0) FROM competition_bets WHERE competition_bets.user_id = l.id AND competition_bets.is_correct = 'Y') as 'points'
							FROM logins l
							WHERE l.id = $userId ";
	foreach ($db_handler->query($sql) as $row) {
		$userTotalPoints += $row[0];
		break;
	}
	
	$userTotalPoints += getUserChallengesPoints($userId);

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>LiveDuel</title>
		<!-- JQuery JS -->
		<script src="bootstrap/jquery-1.js"></script>
		<!-- Bootstrap -->
		<link href="bootstrap/bootstrap.css" rel="stylesheet">
		<script src="bootstrap/bootstrap.js"></script>
		<!-- Liveduel custom styles -->
		<link href="styles/liveduel.css" rel="stylesheet">
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div id="wrap">
			<div class="navbar navbar-default navbar-fixed-top" role="navigation">
			  <div class="container">
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				  <a class="navbar-brand" href="home.php">LiveDuel</a>
				</div>
				<div style="height: 1px;" class="navbar-collapse collapse">
				  <ul class="nav navbar-nav">
					<li><a href="bet.php">Make a Bet!</a></li>
					<?php
					$sql = "SELECT id, name FROM sports";
					foreach ($db_handler->query($sql) as $row) {
					?>
					<li><a href="bet.php?spotId=<?php echo $row["id"];?>"><?php echo $row["name"]; ?></a></li>
					<?php } ?>
					<li><a href="invite.php">Invite Friends</a></li>
					<li class="active"><a href="profile.php">Profile</a></li>
				  </ul>
				  
				</div><!--/.nav-collapse -->
			  </div>
			</div>
			<div class="container">
				<p><span style="float:right;">Welcome, <?php echo $_SESSION['USER_NAME']; ?>&nbsp;&nbsp;&nbsp;&nbsp;<a href="logout.php">Log Out</a></span></p>
				<h3><?php echo $_SESSION['USER_NAME']; ?>'s Profile</h3>
				<div style="float:right;margin-top:-40px;"><h4><?php echo $userTotalPoints; ?> points</h4></div>
				<hr/>
				<!-- Past Bets -->
				<h4>Past Bets</h4>
				<table class="table table-striped">
				  <thead>
					<tr>
					  <th>Match</th>
					  <th>Competition</th>
					  <th>My Answer</th>
					  <th>Correct</th>
					  <th>Points</th>
					</tr>
				  </thead>
				  <tbody>
					<!-- All pending matches-->
					<?php
						$sql = "SELECT  (SELECT name from teams where id = m.team1) as team1 ,
										   (SELECT name from teams where id = m.team2) as team2 ,
										   c.name,
										(SELECT name from teams where id = mb.user_selection) as myAnswer,
										( SELECT CASE
												WHEN winner = team1 THEN team1_points
												WHEN winner = team2 THEN team2_points
												ELSE draw_points
												END AS 'points'  FROM matches WHERE id = m.id ) as points,
												mb.is_correct
								FROM matches m, competitions c, matches_bets mb  WHERE c.id = m.competition_id AND mb.match_id = m.id AND m.is_played = 'Y' AND mb.user_id = $userId ORDER BY m.id ";
						$counter=0;
						foreach ($db_handler->query($sql) as $row) {
							if( ($counter % 2) == 0 ){
								echo "<tr class='light'>";
							}
							else{
								echo "<tr class='dark'>";
							}
							
							echo "<td>".$row["team1"]." VS ".$row["team2"]."</td>";
							echo "<td>".$row["name"]."</td>";
							if($row["myAnswer"] != ""){
								echo "<td>".$row["myAnswer"]."</td>";
							}
							else{
								echo "<td>Match is draw</td>";
							}
							if( $row["is_correct"] == "Y" ){
								echo "<td>Yes</td>";
								echo "<td>".$row["points"]."</td>";
							}
							else{
								echo "<td>No</td>";
								echo "<td>0</td>";
							}
							
							echo "</tr>";
							$counter = $counter + 1;
						}
						if($counter==0){
							echo "<tr class='light'><td colspan='5' align='center'>No data found</td></tr>";
						}
					?>
				  </tbody>
				</table>
				
				<table class="table table-striped">
				  <thead>
					<tr>
					  <th>Question</th>
					  <th>Match</th>
					  <th>Competition</th>
					  <th>My Answer</th>
					  <th>Correct</th>
					  <th>Points</th>
					</tr>
				  </thead>
				  <tbody>
					<?php
						$sql = "SELECT q.name as 'Question', 
								(SELECT name from teams where id = m.team1) as team1 ,
								(SELECT name from teams where id = m.team2) as team2 ,
								a.answer as 'My Answer', b.points as 'Points' , c.name as 'Competition', b.is_correct as 'is_correct'
								FROM match_questions q, matches m, match_answers a, bets b, competitions c
								WHERE b.user_id = $userId AND q.is_answered = 'Y'
									AND m.id = q.match_id AND a.question_id = q.id AND b.question_id = q.id AND b.user_answer_id = a.id AND c.id= m.competition_id
								ORDER BY q.id";
						$counter=0;
						foreach ($db_handler->query($sql) as $row) {
							if( ($counter % 2) == 0 ){
								echo "<tr class='light'>";
							}
							else{
								echo "<tr class='dark'>";
							}
							
							echo "<td>".$row["Question"]."</td>";
							echo "<td>".$row["team1"]." VS ".$row["team2"]."</td>";
							echo "<td>".$row["Competition"]."</td>";
							echo "<td>".$row["My Answer"]."</td>";
							if(isset($row["is_correct"]) && $row["is_correct"]!="N"){
								echo "<td>Yes</td>";
								echo "<td>".$row["Points"]."</td>";
							}
							else{
								echo "<td>No</td>";
								echo "<td>0</td>";
							}
							echo "</tr>";
							$counter = $counter + 1;
						}
						if($counter==0){
							echo "<tr class='light'><td colspan='6' align='center'>No data found</td></tr>";
						}
						
					?>
				  </tbody>
				</table>
				
				<table class="table table-striped">
				  <thead>
					<tr>
					  <th>Question</th>
					  <th>Competition</th>
					  <th>My Answer</th>
					  <th>Correct</th>
					  <th>Points</th>
					</tr>
				  </thead>
				  <tbody>
					<?php
						$sql = "SELECT q.name as 'Question', 
								a.answer as 'My Answer', b.points as 'Points' , c.name as 'Competition', b.is_correct as 'is_correct'
								FROM questions q, answers a, competition_bets b, competitions c
								WHERE b.user_id = $userId AND q.is_answered = 'Y'
									AND b.question_id = q.id 
									AND b.user_answer_id = a.id 
									AND c.id= q.competition_id
								ORDER BY q.id";
						$counter=0;
						foreach ($db_handler->query($sql) as $row) {
							if( ($counter % 2) == 0 ){
								echo "<tr class='light'>";
							}
							else{
								echo "<tr class='dark'>";
							}
							
							echo "<td>".$row["Question"]."</td>";
							echo "<td>".$row["Competition"]."</td>";
							echo "<td>".$row["My Answer"]."</td>";
							if(isset($row["is_correct"]) && $row["is_correct"]!="N"){
								echo "<td>Yes</td>";
								echo "<td>".$row["Points"]."</td>";
							}
							else{
								echo "<td>No</td>";
								echo "<td>0</td>";
							}
							echo "</tr>";
							$counter = $counter + 1;
						}
						if($counter==0){
							echo "<tr class='light'><td colspan='6' align='center'>No data found</td></tr>";
						}
						
					?>
				  </tbody>
				</table>
				
				<hr/>
				<h4>Past Challenges</h4>
				<table class="table table-striped">
				  <thead>
					<tr>
					  <th>Challenger</th>
					  <th>Challenged</th>
					  <th>Match</th>
					  <th>Competition</th>
					  <th>Challenge Question(s)</th>
					  <th>Points Earned</th>
					</tr>
				  </thead>
				  <tbody>
					<!-- All past Challenges-->
					<?php
						
						$sql = "SELECT  (SELECT logins.user_name from logins where id = ch.user1_id) as user1 ,
								(SELECT logins.user_name from logins where id = ch.user2_id) as user2 ,
								(SELECT teams.name from teams where id = m.team1) as team1 ,
								(SELECT teams.name from teams where id = m.team2) as team2 ,
								c.name as competition,
								ch.points as points,
								c.id as competitionId,
								m.id as matchId,
								ch.id as challengeId
							FROM  challenges ch , matches m , competitions c
							WHERE c.id = m.competition_id AND ch.match_id = m.id
									AND (ch.user1_id = $userId OR ch.user2_id = $userId )
									AND (SELECT COUNT(*) FROM challenge_questions chq, match_questions mq, challenges
											WHERE challenges.id = chq.challenge_id AND mq.id = chq.question_id
												AND mq.is_answered = 'N'
												AND challenges.id = ch.id
										) = 0
							ORDER BY ch.id  ";
						$counter = 0;
						foreach ($db_handler->query($sql) as $row) {
							echo "<tr >";
							echo "<td>".$row["user1"]."</td>";
							echo "<td>".$row["user2"]."</td>";
							echo "<td>".$row["team1"]." VS ".$row["team2"]."</td>";
							echo "<td>".$row["competition"]."</td>";
							$sql_questions = "SELECT mq.name as question, mq.is_answered ,
												(SELECT match_answers.answer FROM match_answers, bets WHERE bets.user_answer_id=match_answers.id AND bets.user_id = $userId AND bets.question_id = mq.id ) as answer,
												(SELECT match_answers.points FROM match_answers, bets WHERE bets.user_answer_id=match_answers.id AND bets.user_id = $userId AND bets.question_id = mq.id ) as answer_points,
												(SELECT match_answers.is_correct FROM match_answers, bets WHERE bets.user_answer_id=match_answers.id AND bets.user_id = $userId AND bets.question_id = mq.id ) as is_correct
												FROM challenge_questions chq, match_questions mq, challenges ch
												WHERE ch.id = chq.challenge_id AND mq.id = chq.question_id AND ch.id = ".$row["challengeId"];
							echo "<td>";
							foreach ($db_handler->query($sql_questions) as $row_questions) {
								echo $row_questions["question"];
								if(isset($row_questions["answer"]))
									echo " ( ".$row_questions["answer"]." )";
								if(isset($row_questions["is_correct"])){
									if($row_questions["is_correct"] == "Y"){
										echo " <span style='color:green;'><b><small>Correct ".$row_questions["answer_points"]." pts</small></b></span>";
									}
									else{
										echo " <span style='color:red;'><b><small>Wrong 0 pts</small></b></span>";
									}
								}
								echo "<br/>";
							}
							echo "</td>";
							if(getChallengeWinnerId($row["challengeId"]) == $userId)
								echo "<td>".$row["points"]." pts</td>";
							else if(getChallengeWinnerId($row["challengeId"]) == 0)
								echo "<td>0 pts</td>";
							else
								echo "<td>-".$row["points"]." pts</td>";
							echo "</tr>";
							$counter++;
						}
						if($counter==0){
							echo "<tr ><td colspan='6' align='center'>No data found</td></tr>";
						}
						
					?>
				  </tbody>
				</table>
				<hr/>
				<!-- pending Bets-->
				<h4>Pending Bets</h4>
				<table class="table table-striped">
				  <thead>
					<tr>
					  <th>Match</th>
					  <th>Competition</th>
					  <th>My Answer</th>
					  <th>Points</th>
					</tr>
				  </thead>
				  <tbody>
					<!-- All pending matches-->
					<?php
						$sql = "SELECT  (SELECT name from teams where id = m.team1) as team1 ,
										   (SELECT name from teams where id = m.team2) as team2 ,
										   c.name,
										(SELECT name from teams where id = mb.user_selection) as myAnswer,
										( SELECT CASE
												WHEN mb.user_selection = mm.team1 THEN mm.team1_points
												WHEN mb.user_selection = mm.team2 THEN mm.team2_points
												ELSE mm.draw_points
												END AS 'points'  FROM matches mm WHERE id = m.id ) as points
								FROM matches m, competitions c, matches_bets mb  WHERE c.id = m.competition_id AND mb.match_id = m.id AND m.is_played = 'N' AND mb.user_id = $userId ORDER BY m.id ";
						$counter=0;
						foreach ($db_handler->query($sql) as $row) {
							if( ($counter % 2) == 0 ){
								echo "<tr class='light'>";
							}
							else{
								echo "<tr class='dark'>";
							}
							
							echo "<td>".$row["team1"]." VS ".$row["team2"]."</td>";
							echo "<td>".$row["name"]."</td>";
							if($row["myAnswer"] != ""){
								echo "<td>".$row["myAnswer"]."</td>";
							}
							else{
								echo "<td>Match is draw</td>";
							}
							echo "<td>".$row["points"]."</td>";
							echo "</tr>";
							$counter = $counter + 1;
						}
						if($counter==0){
							echo "<tr class='light'><td colspan='4' align='center'>No data found</td></tr>";
						}
					?>
				  </tbody>
				</table>
				
				<table class="table table-striped">
				  <thead>
					<tr>
					  <th>Question</th>
					  <th>Match</th>
					  <th>Competition</th>
					  <th>My Answer</th>
					  <th>Points</th>
					</tr>
				  </thead>
				  <tbody>
					<?php
						$sql = "SELECT q.name as 'Question', 
								(SELECT name from teams where id = m.team1) as team1 ,
								(SELECT name from teams where id = m.team2) as team2 ,
								a.answer as 'My Answer', b.points as 'Points' , c.name as 'Competition'
								FROM match_questions q, matches m, match_answers a, bets b, competitions c
								WHERE b.user_id = $userId AND q.is_answered <> 'Y'
									AND m.id = q.match_id AND a.question_id = q.id AND b.question_id = q.id AND b.user_answer_id = a.id AND c.id= m.competition_id
								ORDER BY q.id";
						$counter=0;
						foreach ($db_handler->query($sql) as $row) {
							if( ($counter % 2) == 0 ){
								echo "<tr class='light'>";
							}
							else{
								echo "<tr class='dark'>";
							}
							
							echo "<td>".$row["Question"]."</td>";
							echo "<td>".$row["team1"]." VS ".$row["team2"]."</td>";
							echo "<td>".$row["Competition"]."</td>";
							echo "<td>".$row["My Answer"]."</td>";
							echo "<td>".$row["Points"]."</td>";
							echo "</tr>";
							$counter = $counter + 1;
						}
						if($counter==0){
							echo "<tr class='light'><td colspan='5' align='center'>No data found</td></tr>";
						}
						
					?>
				  </tbody>
				</table>
				
				<table class="table table-striped">
				  <thead>
					<tr>
					  <th>Question</th>
					  <th>Competition</th>
					  <th>My Answer</th>
					  <th>Points</th>
					</tr>
				  </thead>
				  <tbody>
					<?php
						$sql = "SELECT q.name as 'Question', 
								a.answer as 'My Answer', b.points as 'Points' , c.name as 'Competition'
								FROM questions q, answers a, competition_bets b, competitions c
								WHERE b.user_id = $userId AND q.is_answered <> 'Y'
									AND a.question_id = q.id 
									AND b.question_id = q.id 
									AND b.user_answer_id = a.id 
									AND q.competition_id = c.id
								ORDER BY q.id";
						$counter=0;
						foreach ($db_handler->query($sql) as $row) {
							if( ($counter % 2) == 0 ){
								echo "<tr class='light'>";
							}
							else{
								echo "<tr class='dark'>";
							}
							
							echo "<td>".$row["Question"]."</td>";
							echo "<td>".$row["Competition"]."</td>";
							echo "<td>".$row["My Answer"]."</td>";
							echo "<td>".$row["Points"]."</td>";
							echo "</tr>";
							$counter = $counter + 1;
						}
						if($counter==0){
							echo "<tr class='light'><td colspan='5' align='center'>No data found</td></tr>";
						}
						
					?>
				  </tbody>
				</table>
				
				<hr/>
				<h4>Pending Challenges</h4>
				<table class="table table-striped">
				  <thead>
					<tr>
					  <th>Challenger</th>
					  <th>Challenged</th>
					  <th>Match</th>
					  <th>Competition</th>
					  <th>Challenge Question(s)</th>
					  <th>Challenge Points</th>
					</tr>
				  </thead>
				  <tbody>
					<!-- All pending Challenges-->
					<?php
						
						$sql = "SELECT  (SELECT logins.user_name from logins where id = ch.user1_id) as user1 ,
								(SELECT logins.user_name from logins where id = ch.user2_id) as user2 ,
								(SELECT teams.name from teams where id = m.team1) as team1 ,
								(SELECT teams.name from teams where id = m.team2) as team2 ,
								c.name as competition,
								ch.points as points,
								c.id as competitionId,
								m.id as matchId,
								ch.id as challengeId
							FROM  challenges ch , matches m , competitions c
							WHERE c.id = m.competition_id AND ch.match_id = m.id
									AND (ch.user1_id = $userId OR ch.user2_id = $userId )
									AND (SELECT COUNT(*) FROM challenge_questions chq, match_questions mq, challenges
											WHERE challenges.id = chq.challenge_id AND mq.id = chq.question_id
												AND mq.is_answered <> 'Y'
												AND challenges.id = ch.id
										) > 0
							ORDER BY ch.id  ";
						$counter = 0;
						foreach ($db_handler->query($sql) as $row) {
							echo "<tr >";
							echo "<td>".$row["user1"]."</td>";
							echo "<td>".$row["user2"]."</td>";
							echo "<td>".$row["team1"]." VS ".$row["team2"]."</td>";
							echo "<td>".$row["competition"]."</td>";
							$sql_questions = "SELECT mq.name as question, mq.is_answered ,
												(SELECT match_answers.answer FROM match_answers, bets WHERE bets.user_answer_id=match_answers.id AND bets.user_id = $userId AND bets.question_id = mq.id ) as answer
												FROM challenge_questions chq, match_questions mq, challenges ch
												WHERE ch.id = chq.challenge_id AND mq.id = chq.question_id AND ch.id = ".$row["challengeId"];
							echo "<td>";
							foreach ($db_handler->query($sql_questions) as $row_questions) {
								echo "<a href='bet.php?action=match&matchId=".$row["matchId"]."&competitionId=".$row["competitionId"]."'>".$row_questions["question"]."</a>";
								if(isset($row_questions["answer"]))
									echo " ( ".$row_questions["answer"]." )";
								echo "<br/>";
							}
							echo "</td>";
							echo "<td>".$row["points"]."</td>";
							echo "</tr>";
							$counter++;
						}
						if($counter==0){
							echo "<tr ><td colspan='6' align='center'>No data found</td></tr>";
						}
						
					?>
				  </tbody>
				</table>
				
				<hr/>
				<!-- Leaderboard -->
				<h4>Leaderboard</h4>
				<table class="table table-striped" style="width:300px;">
				  <thead>
					<tr>
					  <th>Rank</th>
					  <th>Name</th>
					  <th>Score</th>
					</tr>
				  </thead>
				  <tbody>
					<?php
						$sql = "SELECT DISTINCT l.user_name , l.id,  
								( SELECT COALESCE(SUM(bets.points),0) FROM bets WHERE bets.user_id = l.id AND bets.is_correct = 'Y')+
								( SELECT COALESCE(SUM(matches_bets.points),0) FROM matches_bets WHERE matches_bets.user_id = l.id AND matches_bets.is_correct = 'Y')+
								( SELECT COALESCE(SUM(competition_bets.points),0) FROM competition_bets WHERE competition_bets.user_id = l.id AND competition_bets.is_correct = 'Y') as 'points'
							FROM logins l
							ORDER BY points DESC";
						$counter=0;
						foreach ($db_handler->query($sql) as $row) {
							if( ($counter % 2) == 0 ){
								echo "<tr class='light'>";
							}
							else{
								echo "<tr class='dark'>";
							}
							
							echo "<td>".($counter+1)."</td>";
							echo "<td><a href='viewProfile.php?id=".$row["id"]."'>".$row["user_name"]."</a></td>";
							$userChallengesPoints = getUserChallengesPoints($row["id"]);
							$userChallengesPoints += $row["points"];
							echo "<td>$userChallengesPoints</td>";
							echo "</tr>";
							$counter = $counter + 1;
						}
						if($counter==0){
							echo "<tr class='light'><td colspan='3' align='center'>No data found</td></tr>";
						}
					?>
				  </tbody>
				</table>
			  <div class="alert alert-danger" id="errorDiv"></div>
			</div> <!-- /container -->
		</div>
		<div id="footer">
		  <div class="container">
			<p class="text-muted">Copyright &copy; 2014 - All Rights Reserved - <a href="home.php">livedule.com</a></p>
		  </div>
		</div>
	</body>
</html>
<?php
	function getChallengeWinnerId($challengeId){
		global $db_handler;
		$sql = "SELECT user1_id, user2_id FROM challenges WHERE challenges.id = $challengeId";
		$user1_id = 0;
		$user2_id = 0;
		$user1_points = 0;
		$user2_points = 0;
		foreach ($db_handler->query($sql) as $row) {
			$user1_id = $row["user1_id"];
			$user2_id = $row["user2_id"];
			break;
		}
		
		$sql = "SELECT SUM(ma.points) as points
				FROM challenges ch, challenge_questions chq, match_questions mq, match_answers ma, bets b
				WHERE ch.id = chq.challenge_id AND chq.question_id = mq.id AND mq.id = ma.question_id AND b.user_answer_id = ma.id
					AND b.user_id = $user1_id AND ma.is_correct = 'Y' AND ch.id = $challengeId";
		foreach ($db_handler->query($sql) as $row) {
			$user1_points = $row["points"];
			break;
		}
		
		$sql = "SELECT SUM(ma.points) as points
				FROM challenges ch, challenge_questions chq, match_questions mq, match_answers ma, bets b
				WHERE ch.id = chq.challenge_id AND chq.question_id = mq.id AND mq.id = ma.question_id AND b.user_answer_id = ma.id
					AND b.user_id = $user2_id AND ma.is_correct = 'Y' AND ch.id = $challengeId";
		foreach ($db_handler->query($sql) as $row) {
			$user2_points = $row["points"];
			break;
		}
		if($user1_points > $user2_points)
			return $user1_id;
		else if($user1_points < $user2_points)
			return $user2_id;
		else
			return 0;
	}
	
	function getUserChallengesPoints($userId){
		global $db_handler;
		$sql = "SELECT id FROM challenges ch 
				WHERE (ch.user1_id = $userId OR ch.user2_id = $userId )
					AND (SELECT COUNT(*) FROM challenge_questions chq, match_questions mq, challenges
											WHERE challenges.id = chq.challenge_id AND mq.id = chq.question_id
												AND mq.is_answered = 'N'
												AND challenges.id = ch.id
										) = 0";
		$userEarnedPoints = 0;
		foreach ($db_handler->query($sql) as $row) {
			$winnerId = getChallengeWinnerId($row["id"]);
			if($winnerId == $userId){
				$sql2 = "SELECT points
						FROM challenges ch
						WHERE ch.id = ".$row["id"];
				foreach ($db_handler->query($sql2) as $row2) {
					$userEarnedPoints += $row2["points"];
					break;
				}
			}
			else if($winnerId == 0){
				$sql2 = "SELECT points
						FROM challenges ch
						WHERE ch.id = ".$row["id"];
				foreach ($db_handler->query($sql2) as $row2) {
					$userEarnedPoints += 0;
					break;
				}
			}
			else{
				$sql2 = "SELECT points
						FROM challenges ch
						WHERE ch.id = ".$row["id"];
				foreach ($db_handler->query($sql2) as $row2) {
					$userEarnedPoints -= $row2["points"];
					break;
				}
			}
		}
		return $userEarnedPoints;
	}
?>