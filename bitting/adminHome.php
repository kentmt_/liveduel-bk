<?php session_start(); 
if(!isset($_SESSION['IS_ADMIN']) || $_SESSION['IS_ADMIN'] != "Y"){ 
	header("Location: index.html");
}
include('db_config.php');
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
<title>LiveDuel</title>
<meta charset="iso-8859-1">
<link rel="stylesheet" href="styles/layout.css" type="text/css">
<script src="scripts/jquery-1.8.2.min.js"></script>
<script src="scripts/jquery-defaultvalue.js"></script>
<script src="scripts/jquery-browsercheck.js"></script>
<!--[if lt IE 9]>
<link rel="stylesheet" href="styles/ie.css" type="text/css">
<script src="scripts/ie/html5shiv.min.js"></script>
<![endif]-->
</head>
<body>
<div class="wrapper row1">
  <header id="header" class="clear">
    <hgroup>
      <h1><a href="home.php"><span>LiveDuel</span></a></h1>
    </hgroup>
    <form action="#" method="post">
      <fieldset>
		<span style="float:right;">Welcome, <?php echo $_SESSION['USER_NAME']; ?>&nbsp;&nbsp;&nbsp;&nbsp;<a href="adminLogout.php">Log Out</a></span>
      </fieldset>
    </form>
	<nav>
      <ul class="clear">
        <li class="first active"><a href="adminHome.php">Admin Home</a></li>        
      </ul>
    </nav>
	
  </header>
  <div id="breadcrumb">
	<a href="adminHome.php">Sports</a>
  </div>
</div>
<!-- content -->
<div class="wrapper row3">
  <div id="container" class="clear">
    <div id="homepage">
		<h1>Administration Panel</h1>
		<?php if(!isset($_REQUEST['action']) || $_REQUEST['action']!="edit") {?>
		<section id="comments" style="margin:5px;">
			<h2>Manage Sports</h2>
			<form action="insertSport.php" method="post" enctype="multipart/form-data">
				<input name="MAX_FILE_SIZE" value="102400" type="hidden">
				<p>
					<input type="text" name="sportName" id="sportName" value="" size="22" />
					<label for="sportName" style="width:120px;"><small>Sport Name</small></label>
				</p>
				<p>
					<input type="file" name="sportIcon" id="sportIcon" value="" size="22" />
					<label for="sportIcon" style="width:120px;"><small>Icon</small></label>
				</p>
				<p>
					<input name="submit" type="submit" id="submit" value="Add Sport" onclick="return validateAddSport();" />
				</p>
			</form>
		</section>
		<?php }?>
		
		<?php if(isset($_REQUEST['action']) && $_REQUEST['action']=="edit" && isset($_REQUEST['id'])) {
			$sportId = $_REQUEST['id'];
			$sportName = "";
			$sql = "SELECT id, name FROM sports WHERE id= $sportId";
			foreach ($db_handler->query($sql) as $row) {
				$sportName = $row["name"];
				break;
			}
		?>
		<section id="comments" style="margin:5px;">
			<h2>Manage Sports</h2>
			<form action="editSport.php" method="post" enctype="multipart/form-data">
				<input name="MAX_FILE_SIZE" value="102400" type="hidden">
				<input name="sportId" value="<?php echo $sportId; ?>" type="hidden">
				<p>
					<input type="text" name="sportName" id="sportName" value="<?php echo $sportName; ?>" size="22" />
					<label for="sportName" style="width:120px;"><small>Sport Name</small></label>
				</p>
				<p>
					<input type="file" name="sportIcon" id="sportIcon" value="" size="22" />
					<label for="sportIcon" style="width:120px;"><small>Icon</small></label>
				</p>
				<p>
					<input name="submit" type="submit" id="submit" value="Update Sport" onclick="return validateUpdateSport();" />
				</p>
			</form>
		</section>
		<?php }?>
		</br>
		<section style="width:100%;">
			<table>
			  <thead>
				<tr>
				  <th>Sport Name</th>
				  <th>Sport Icon</th>
				  <th>Manage</th>
				  <th>Edit</th>
				  <th>Delete</th>
				</tr>
			  </thead>
			  <tbody>
				<?php
					$sql = "SELECT id, name, icon FROM sports ORDER BY name";
					$counter=0;
					foreach ($db_handler->query($sql) as $row) {
						if( ($counter % 2) == 0 ){
							echo "<tr class='light'>";
						}
						else{
							echo "<tr class='dark'>";
						}
						
						echo "<td>".$row["name"]."</td>";
						echo '<td><img src="data:image/jpeg;base64,' . base64_encode($row['icon']) . '" width="100" height="100"></td>';
						echo "<td><a href='manageSport.php?id=".$row["id"]."'>Manage</a></td>";
						echo "<td align='center'><a title='Edit' href='adminHome.php?action=edit&id=".$row["id"]."'><img src='images/edit.png' /></a></td>";
						echo '<td align="center"><a title="Delete" href="deleteSport.php?id='.$row["id"].'" onclick="return confirm(\'Are you sure you want to delete '.$row["name"].' sport?\');"><img src="images/delete.png" /></a></td>';
						echo "</tr>";
						$counter = $counter + 1;
					}
					if($counter==0){
						echo "<tr class='light'><td colspan='5' align='center'>No data found</td></tr>";
					}
				?>
			  </tbody>
			</table>
		<section>
    </div>
    <!-- / content body -->
  </div>
</div>
<!-- Copyright -->
<div class="wrapper row5">
  <footer id="copyright" class="clear">
    <p class="fl_left">Copyright &copy; 2014 - All Rights Reserved - <a href="home.php">livedule.com</a></p>
  </footer>
</div>
<script>
	function getParameterByName(name) {
		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
			results = regex.exec(location.search);
		return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
	
	if(getParameterByName("action")=="found"){
		alert("sport name already exists");
	}
	
	function validateAddSport(){
		if(document.getElementById("sportName").value==""){
			alert("please enter sport name");
			document.getElementById("sportName").focus();
		}
		else if(document.getElementById("sportIcon").value==""){
			alert("please enter sport icon");
			document.getElementById("sportIcon").focus();
		}
		else if(!endsWith(document.getElementById("sportIcon").value,".png") && !endsWith(document.getElementById("sportIcon").value,".jpg") && !endsWith(document.getElementById("sportIcon").value,".bmp") && !endsWith(document.getElementById("sportIcon").value,".gif")){
			alert("invalid sport icon format");
		}
		else{
			return true;
		}
		return false;
	}
	
	function validateUpdateSport(){
		if(document.getElementById("sportName").value==""){
			alert("please enter sport name");
			document.getElementById("sportName").focus();
			return false;
		}
		if(document.getElementById("sportIcon").value != ""){
			if(!endsWith(document.getElementById("sportIcon").value,".png") && !endsWith(document.getElementById("sportIcon").value,".jpg") && !endsWith(document.getElementById("sportIcon").value,".bmp") && !endsWith(document.getElementById("sportIcon").value,".gif")){
				alert("invalid sport icon format");
				return false;
			}
			return true;
		}
		else{
			return true;
		}
		return false;
	}
	
	function endsWith(str, suffix) {
		return str.toLowerCase().indexOf(suffix, str.length - suffix.length) !== -1;
	}
	</script>
</body>
</html>