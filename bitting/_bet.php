<?php session_start(); 
if(!isset($_SESSION['USER_ID'])){ 
	header("Location: index.html");
}
include('db_config.php');
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
<title>LiveDuel</title>
<meta charset="iso-8859-1">
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/redmond/jquery-ui.css" rel="stylesheet" type="text/css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="styles/layout.css" type="text/css">
<link rel="stylesheet" href="styles/occordion.css" type="text/css">

<script src="scripts/jquery.nestedAccordion.js"></script>
<script src="scripts/jquery.ui.message.js"></script>
<!--[if lt IE 9]>
<link rel="stylesheet" href="styles/ie.css" type="text/css">
<script src="scripts/ie/html5shiv.min.js"></script>
<![endif]-->
</head>
<body>
<div class="wrapper row1">
  <header id="header" class="clear">
    <hgroup>
      <h1><a href="home.php"><span>LiveDuel</span></a></h1>
    </hgroup>
    <form action="#" method="post">
      <fieldset>
		<span style="float:right;">Welcome, <?php echo $_SESSION['USER_NAME']; ?>&nbsp;&nbsp;&nbsp;&nbsp;<a href="logout.php">Log Out</a></span>
      </fieldset>
    </form>
	<nav>
	
      <ul class="clear">
        <!-- <li class="first"><a href="home.php">Home</a></li> -->
		<li class="first active"><a href="bet.php">Make a Bet!</a></li>
		<?php
		$sql = "SELECT id, name FROM sports";
		foreach ($db_handler->query($sql) as $row) {
		?>
		<li><a href="bet.php?spotId=<?php echo $row["id"];?>"><?php echo $row["name"]; ?></a></li>
		<?php } ?>
		<li><a href="profile.php">Profile</a></li>
      </ul>
	  
    </nav>
	
  </header>
  
</div>
<!-- content -->
<div class="wrapper row3">
  <div id="container" class="clear">
    <div id="homepage">
		<div id="message"></div>
		<h1>Make a BET!</h1>
		
		<section id="comments" style="float:left;">
		
		<div id="accordion" class="accordion">
		<?php
		$sql = "SELECT id, name FROM sports";
		foreach ($db_handler->query($sql) as $row) {
			echo "<h4>".$row['name']."</h4>";
			$class = "inner";
			if(isset($_REQUEST["spotId"]) && $_REQUEST["spotId"] == $row["id"]){
				$class= "inner shown";
			}
		?>
			<div class="<?php echo $class; ?>">
			<?php
			$sql2 = "SELECT id, name FROM competitions WHERE sport_id = ".$row['id'];
			foreach ($db_handler->query($sql2) as $row2){
				echo "<h5>".$row2['name']."</h5>";
			?>
			<?php
				$class= "inner";
				if(isset($_REQUEST["competitionId"]) && $_REQUEST["competitionId"]==$row2["id"]){
					$class= "inner shown";
				}
			?>
				<div class="<?php echo $class; ?>">
					<?php
					$sql5 = "SELECT id, (SELECT name from teams where id = m.team1) as team1 ,
								   (SELECT name from teams where id = m.team2) as team2 , team1_points , team2_points, draw_points, team1 as team1_id, team2 as team2_id, is_played, winner
						FROM matches m WHERE is_played = 'N' AND competition_id = ".$row2['id'];
					foreach ($db_handler->query($sql5) as $row5){
						echo "<h5><a href='bet.php?action=match&matchId=".$row5["id"]."&competitionId=".$row2["id"]."'>".$row5['team1']." VS ".$row5['team2']."</a></h5>";
					?>
						
					<?php } ?>
				</div>
			<?php } ?>
			</div>
		<?php } ?>
		</div>
    
	</div>
		<?php if( isset($_REQUEST["matchId"]) ) {
				$matchId = $_REQUEST["matchId"];
				$sql = "SELECT id, (SELECT name from teams where id = m.team1) as team1 ,
									   (SELECT name from teams where id = m.team2) as team2 ,
									   (SELECT icon from teams where id = m.team1) as icon1 ,
									   (SELECT icon from teams where id = m.team2) as icon2 ,
									   (SELECT home from teams where id = m.team1) as home1 ,
									   (SELECT home from teams where id = m.team2) as home2 ,
									   (SELECT display from teams where id = m.team1) as display1 ,
									   (SELECT display from teams where id = m.team2) as display2 ,
									   team1_points , team2_points, draw_points, team1 as team1_id, team2 as team2_id, is_played, winner
							FROM matches m WHERE id = $matchId ";
				
				foreach ($db_handler->query($sql) as $row) {
					$team1 = $row["team1"];
					$team2 = $row["team2"];
					$icon1 = $row["icon1"];
					$icon2 = $row["icon2"];
					$home1 = $row["home1"];
					$home2 = $row["home2"];
					$display1 = $row["display1"];
					$display2 = $row["display2"];
					$points1 = $row["team1_points"];
					$points2 = $row["team2_points"];
					$pointsDraw = $row["draw_points"];
					$team1Id = $row["team1_id"];
					$team2Id = $row["team2_id"];
					break;
				}
				
		?>
		<section id="comments" style="width:50%;">
			<form action="insertMatchBet.php" name="betForm" id="betForm">
				<input type="hidden" name="matchId" value="<?php echo $matchId; ?>" />
				<input type="hidden" name="userSelection" id="userSelection" value="" />
				<table class='tableNoBorder'>
					<tr>
						<?php if($display1 == 'Y') {?>
						<td align="center"><img title="<?php echo $team1; ?>" src="data:image/jpeg;base64,<?php echo base64_encode($icon1); ?>" width="30" height="20" /></td>
						<?php } else{ ?>
						<td align="center"><?php echo $team1; ?></td>
						<?php } ?>
						<td align="center"><img src="images/vs.PNG" title="VS"/></td>
						<?php if($display2 == 'Y') {?>
						<td align="center"><img title="<?php echo $team2; ?>" src="data:image/jpeg;base64,<?php echo base64_encode($icon2); ?>" width="30" height="20" /></td>
						<?php } else{ ?>
						<td align="center"><?php echo $team2; ?></td>
						<?php } ?>
					</tr>
					<tr>
						<td align="center"><img style="cursor:pointer;" onclick="selectTeam(<?php echo $team1Id ; ?>,'<?php echo $team1; ?>',<?php echo $points1; ?>, 1)" src="data:image/jpeg;base64,<?php echo base64_encode($home1); ?>" width="100" height="100" /></td>
						<td align="center" style="vertical-align:middle;"><img src="images/draw.png" style="cursor:pointer;" onclick="selectTeam(0,'',<?php echo $pointsDraw; ?>, 0)" width="100" height="100" /></td>
						<td align="center"><img style="cursor:pointer;" onclick="selectTeam(<?php echo $team2Id ; ?>,'<?php echo $team2; ?>',<?php echo $points2; ?>, 2)" src="data:image/jpeg;base64,<?php echo base64_encode($home2); ?>" width="100" height="100" /></td>
					</tr>
					<tr>
						<td align="center"><input style="cursor:pointer;background-color:white;" type="button" onclick="selectTeam(<?php echo $team1Id ; ?>,'<?php echo $team1; ?>',<?php echo $points1; ?>, 1)" value="<?php echo $team1; ?> wins <?php echo $points1; ?> Pts" /></td>
						<td align="center" style="vertical-align:middle;"><input style="cursor:pointer;background-color:white;" type="button" onclick="selectTeam(0,'',<?php echo $pointsDraw; ?>, 0)" value="Draw <?php echo $pointsDraw; ?> Pts" /></td>
						<td align="center"><input style="cursor:pointer;background-color:white;" type="button" onclick="selectTeam(<?php echo $team2Id ; ?>,'<?php echo $team2; ?>',<?php echo $points2; ?>, 2)" value="<?php echo $team2; ?> wins <?php echo $points2; ?> Pts" /></td>
					</tr>
				</table>
				
				<div id="popup1" title="Are you sure?" style="display:none;">
					<br/>
					The winner is <?php echo $team1; ?> (<?php echo $points1; ?> Pts)!
					<img style="float:right;" src="data:image/jpeg;base64,<?php echo base64_encode($home1); ?>" width="100" height="100" />
					<br/><br/><br/>
					<input style="cursor:pointer;background-color:white;" type="button" onclick="submitBet(<?php echo $team1Id ; ?>)" value="Submit Bet" />
				</div>
				<div id="popup2" title="Are you sure?" style="display:none;">
					<br/>
					The winner is <?php echo $team2; ?> (<?php echo $points2; ?> Pts)!
					<img style="float:right;" src="data:image/jpeg;base64,<?php echo base64_encode($home2); ?>" width="100" height="100" />
					<br/><br/><br/>
					<input style="cursor:pointer;background-color:white;" type="button" onclick="submitBet(<?php echo $team2Id ; ?>)" value="Submit Bet" />
				</div>
				<div id="popup3" title="Are you sure?" style="display:none;">
					<br/>
					The match result is draw (<?php echo $pointsDraw; ?> Pts)!
					<img style="float:right;" src="images/draw.png" width="100" height="100" />
					<br/><br/><br/>
					<input style="cursor:pointer;background-color:white;" type="button" onclick="submitBet(0)" value="Submit Bet" />
				</div>
			</form>
			
		</section>
		<section style="float:right;">
				<?php
						$sql = "SELECT id, name FROM match_questions WHERE match_id = $matchId AND is_answered = 'N' ORDER BY name";
						foreach ($db_handler->query($sql) as $row){
							echo "<h2 style='margin:0px;'>".$row["name"]."</h2>";
							echo "<table class='tableNoBorder' style='width: 100%;margin-left:20px;margin-bottom:10px;'>";
							
							echo "<tr>";
							$sql2 = "SELECT id , answer, points, icon FROM answers WHERE question_id = ".$row["id"]." ORDER BY answer";
							foreach ($db_handler->query($sql2) as $row2){
								echo '<td align="center"><a href="insertBet.php?competitionId='.$_REQUEST["competitionId"].'&questionId='.$row["id"].'&answerId='.$row2["id"].'&points='.$row2["points"].'" title="Record answer" onclick="return confirmAnswer();"><img src="data:image/jpeg;base64,' . base64_encode($row2['icon']) . '" width="50" height="50"></a></td>';
							}
							echo "</tr>";
							
							echo "<tr>";
							$sql2 = "SELECT id , answer, points, icon FROM answers WHERE question_id = ".$row["id"]." ORDER BY answer";
							foreach ($db_handler->query($sql2) as $row2){
								echo "<td align='center'><a href='insertBet.php?competitionId=".$_REQUEST["competitionId"]."&questionId=".$row["id"]."&answerId=".$row2["id"]."&points=".$row2["points"]."' title='Record answer' onclick='return confirmAnswer();' class='linkButton'><b>".$row2["answer"]."</b> <small>".$row2["points"]." Pts</small></a></td>";
							}
							echo "</tr>";
							echo "</table>";
						}
					?>
		</section>
		<?php } ?>
	<!-- / content body -->
  </div>
</div>
<!-- Copyright -->
<div class="wrapper row5">
  <footer id="copyright" class="clear">
    <p class="fl_left">Copyright &copy; 2014 - All Rights Reserved - <a href="home.php">livedule.com</a></p>
  </footer>
</div>

<script>
	
	function getParameterByName(name) {
		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
			results = regex.exec(location.search);
		return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
	
	<!--//--><![CDATA[//><!--
	$("html").addClass("js");
	$.fn.accordion.defaults.container = false; 
	$(function() {

	  $("#accordion").accordion({
		  obj: "div", 
		  wrapper: "div", 
		  el: ".h", 
		  head: "h4, h5", 
		  next: "div", 
		  showMethod: "slideFadeDown",
		  hideMethod: "slideFadeUp",
		  initShow : "div.shown",
		  elToWrap: "sup, img"
		});
	});
	//--><!]]>
	
	function selectTeam(id, team, points, winner){
		/*
		var answer;
		if(id != 0)
			answer = confirm("Are you sure you want to make this bet ("+team+" wins "+points+" Pts)");
		else
			answer = confirm("Are you sure you want to make this bet (match is draw "+points+" Pts)");
		if(answer== true){
			$("#userSelection").val(id);	
			$("#betForm").submit();
		}
		*/
		if(winner == 1 ){
			$( "#popup1" ).dialog({modal: true,width:400,position: [360,130],show: 'blind', hide: 'explode'});
		}
		else if(winner == 2 ){
			$( "#popup2" ).dialog({modal: true,width:400,position: [360,130],show: 'blind', hide: 'explode'});
		}
		else{
			$( "#popup3" ).dialog({modal: true,width:400,position: [360,130],show: 'blind', hide: 'explode'});
		}
	}
	
	function submitBet(id){
		$("#userSelection").val(id);	
		$("#betForm").submit();
		$('#popup1').dialog('close');
		$('#popup2').dialog('close');
		$('#popup3').dialog('close');
	}
	
	function confirmAnswer(){
		return confirm("Are you sure you want to record this answer?");
	}
	
	if(getParameterByName("action")=="successBet"){
		$("#message").message({ dismiss: false, type: "info", message: "Bet is placed successfully" });
		window.setTimeout("$('#message').hide('slow');",2000);
	}
	else if(getParameterByName("action")=="successAnswer"){
		$("#message").message({ dismiss: false, type: "info", message: "Answer is recorded successfully" });
		window.setTimeout("$('#message').hide('slow');",2000);
	}
</script>
</body>
</html>