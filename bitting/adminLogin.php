<?php
	session_start();
	
	$userName=$_REQUEST["userName"];
	$password=$_REQUEST["password"];
	$flag=false;
	
	// see if this is admin
	$doc = new DOMDocument();
	$doc->load( 'admin.xml' );

	$admins = $doc->getElementsByTagName( "admin" );
	foreach( $admins as $admin )
	{
		$userNames = $admin->getElementsByTagName( "userName" );
		$uname = $userNames->item(0)->nodeValue;

		$passwords = $admin->getElementsByTagName( "password" );
		$pwd = $passwords->item(0)->nodeValue;
		if($uname==$userName && $pwd==$password){
			$flag=true;
			break;
		}
	}
	if($flag){
		$_SESSION['IS_ADMIN']="Y";
		$_SESSION['USER_NAME']=$userName;
		header( 'Location: adminHome.php' ) ;
	}
	else{
		$_SESSION['isAdmin']=null;
		header( 'Location: admin.html?action=failed' ) ;
	}
?>