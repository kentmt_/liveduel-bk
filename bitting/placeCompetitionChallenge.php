<?php session_start();
	include('db_config.php');

	if( isset($_REQUEST["questions"]) && isset($_REQUEST["competitionId"]) && isset($_REQUEST["friend"]) ){
		// check if friend name exists
		$exists = false;
		
		$sql = "SELECT id FROM logins WHERE user_name = '".$_REQUEST["friend"]."' ";
		foreach ($db_handler->query($sql) as $row) {
			$friendId=$row['id'];
			$exists = true;
			break;
		}
		if($exists == true){
			// insert challenge here
			$user1Id = $_SESSION['USER_ID'];
			$matchId = $_REQUEST["matchId"];
			$points = $_REQUEST["points"];
			$competitionId= $_REQUEST["competitionId"];
			$questions = $_REQUEST["questions"];
			
			// delete any challenge questions for user with same match
			//$st = $db_handler->prepare("DELETE FROM challenges WHERE user1_id = $user1Id AND user2_id = $friendId AND match_id = $matchId AND question_id = $questions ");
			//$st->execute();
			// delete any challenge for user with same match
			//$st = $db_handler->prepare("DELETE FROM challenges WHERE user1_id = $user1Id AND user2_id = $friendId AND match_id = $matchId ");
			//$st->execute();
			
			
			// insert challenge 
			$st = $db_handler->prepare("INSERT INTO competition_challenges( user1_id, user2_id, match_id ) VALUES( $user1Id, $friendId, $matchId )");
			$st->execute();	
			$challengeId = $db_handler->lastInsertId(); 
			
			// insert challenge question
			foreach ($questions as $question) {
				$points = $_REQUEST["coins".$question];
				$st = $db_handler->prepare("INSERT INTO challenge_questions( challenge_id, question_id, points ) VALUES( $challengeId, $question, $points )");
				$st->execute();	
			}
		}
		else{
			header("Location: bet.php?competitionId=".$_REQUEST["competitionId"]."&matchId=".$_REQUEST["matchId"]."&action=friendNotFound");
			exit;
		}
		
	}
	// redirect to betting page
	header("Location: bet.php?competitionId=".$_REQUEST["competitionId"]."&matchId=".$_REQUEST["matchId"]."&action=successChallenge");
?>