<?php include('db_config.php');
	$id=$_REQUEST['id'];
	$winner=$_REQUEST['winner'];
	$competitionId=$_REQUEST['competitionId'];

	$st = $db_handler->prepare("UPDATE matches SET is_played = 'Y' , winner = $winner WHERE id = $id ");
	$st->execute();
	
	// add points to correct answers
	$st = $db_handler->prepare("UPDATE matches_bets SET is_correct = 'Y' , points = ( SELECT CASE
																							WHEN winner = team1 THEN team1_points
																							WHEN winner = team2 THEN team2_points
																							ELSE draw_points
																							END AS 'points'  FROM matches WHERE id = $id ) 
								WHERE match_id = $id AND user_selection = $winner ");
	$st->execute();
	
	// add 0 points to wrong answers
	$st = $db_handler->prepare("UPDATE matches_bets SET is_correct = 'N' , points = 0 WHERE match_id = $id AND user_selection <> $winner AND id <> 0 ");
	$st->execute();
	
	header("Location: manageCompetitionMatches.php?id=$competitionId");
?>