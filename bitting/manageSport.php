<?php session_start(); 
if(!isset($_SESSION['IS_ADMIN'])){ 
	header("Location: index.html");
}
$sportId = $_REQUEST["id"];
$sportName = "";
include('db_config.php');
$sql = "SELECT id, name FROM sports WHERE id= $sportId";
foreach ($db_handler->query($sql) as $row) {
	$sportName = $row["name"];
	break;
}
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
<title>LiveDuel</title>
<meta charset="iso-8859-1">
<link rel="stylesheet" href="styles/layout.css" type="text/css">
<script src="scripts/jquery-1.8.2.min.js"></script>
<script src="scripts/jquery-defaultvalue.js"></script>
<script src="scripts/jquery-browsercheck.js"></script>
<!--[if lt IE 9]>
<link rel="stylesheet" href="styles/ie.css" type="text/css">
<script src="scripts/ie/html5shiv.min.js"></script>
<![endif]-->

</head>
<body>
<div class="wrapper row1">
  <header id="header" class="clear">
    <hgroup>
      <h1><a href="home.php"><span>LiveDuel</span></a></h1>
    </hgroup>
    <form action="#" method="post">
      <fieldset>
		<span style="float:right;">Welcome, <?php echo $_SESSION['USER_NAME']; ?>&nbsp;&nbsp;&nbsp;&nbsp;<a href="adminLogout.php">Log Out</a></span>
      </fieldset>
    </form>
	<nav>
      <ul class="clear">
        <li class="first active"><a href="adminHome.php">Admin Home</a></li>        
      </ul>
    </nav>
	
  </header>
  <div id="breadcrumb">
	<a href="adminHome.php">Sports</a> > <a href="manageSport.php?id=<?php echo $sportId ;?>"><?php echo $sportName ;?></a>
  </div>
</div>
<!-- content -->
<div class="wrapper row3">
  <div id="container" class="clear">
    <div id="homepage">
		<h1>Administration Panel</h1>
		<?php if(!isset($_REQUEST['action']) || $_REQUEST['action']!="edit") {?>
		<section id="comments" style="margin:5px;">
			<h2>Manage <?php echo $sportName; ?> Competitions</h2>
			<form action="insertCompetition.php" method="post" enctype="multipart/form-data">
				<input name="MAX_FILE_SIZE" value="102400" type="hidden">
				<input id="sportId" name="sportId" value="<?php echo $sportId ; ?>" type="hidden">
				<p>
					<input type="text" name="competitionName" id="competitionName" value="" size="22" />
					<label for="competitionName" style="width:120px;"><small>Competition Name</small></label>
				</p>
				<p>
					<input type="file" name="competitionIcon" id="competitionIcon" value="" size="22" />
					<label for="competitionIcon" style="width:120px;"><small>Icon</small></label>
				</p>
				<p>
					<input name="submit" type="submit" id="submit" value="Add Competition" onclick="return validateAddCompetition();" />
				</p>
			</form>
		</section>
		<?php }?>
		<?php if(isset($_REQUEST['action']) && $_REQUEST['action']=="edit" && isset($_REQUEST['competitionId'])) {
			$competitionId = $_REQUEST['competitionId'];
			$competitionName = "";
			$sql = "SELECT id, name FROM competitions WHERE id= $competitionId";
			foreach ($db_handler->query($sql) as $row) {
				$competitionName = $row["name"];
				break;
			}
		?>
		<section id="comments" style="margin:5px;">
			<h2>Manage <?php echo $sportName; ?> Competitions</h2>
			<form action="editCompetition.php" method="post" enctype="multipart/form-data">
				<input name="MAX_FILE_SIZE" value="102400" type="hidden">
				<input id="sportId" name="sportId" value="<?php echo $sportId ; ?>" type="hidden">
				<input id="competitionId" name="competitionId" value="<?php echo $competitionId ; ?>" type="hidden">
				<p>
					<input type="text" name="competitionName" id="competitionName" value="<?php echo $competitionName ; ?>" size="22" />
					<label for="competitionName" style="width:120px;"><small>Competition Name</small></label>
				</p>
				<p>
					<input type="file" name="competitionIcon" id="competitionIcon" value="" size="22" />
					<label for="competitionIcon" style="width:120px;"><small>Icon</small></label>
				</p>
				<p>
					<input name="submit" type="submit" id="submit" value="Update Competition" onclick="return validateUpdateCompetition();" />
				</p>
			</form>
		</section>
		<?php }?>
		<br/>
		<section style="width:100%;">
			<table>
			  <thead>
				<tr>
				  <th>Competition Name</th>
				  <th>Competition Icon</th>
				  <th>Manage Teams</th>
				  <th>Manage Questions</th> 
				  <th>Manage Matches</th>
				  <th>Edit</th>
				  <th>Delete</th>
				</tr>
			  </thead>
			  <tbody>
				<?php
					$sql = "SELECT id, name, icon FROM competitions WHERE sport_id = $sportId ORDER BY name";
					$counter=0;
					foreach ($db_handler->query($sql) as $row) {
						if( ($counter % 2) == 0 ){
							echo "<tr class='light'>";
						}
						else{
							echo "<tr class='dark'>";
						}
						
						echo "<td>".$row["name"]."</td>";
						echo '<td><img src="data:image/jpeg;base64,' . base64_encode($row['icon']) . '" width="100" height="100"></td>';
						echo "<td><a href='manageCompetitionTeams.php?id=".$row["id"]."'>Manage Teams</a></td>";
						echo "<td><a href='manageCompetitionQuestions.php?id=".$row["id"]."'>Manage Questions</a></td>";
						echo "<td><a href='manageCompetitionMatches.php?id=".$row["id"]."'>Manage Matches</a></td>";
						echo "<td align='center'><a title='Edit' href='manageSport.php?action=edit&id=$sportId&competitionId=".$row["id"]."'><img src='images/edit.png' /></a></td>";
						echo '<td align="center"><a title="Delete" href="deleteCompetition.php?id='.$row["id"].'&sportId='.$sportId.'" onclick="return confirm(\'Are you sure you want to delete '.$row["name"].' competition?\');"><img src="images/delete.png" /></a></td>';
						echo "</tr>";
						$counter = $counter + 1;
					}
					if($counter==0){
						echo "<tr class='light'><td colspan='6' align='center'>No data found</td></tr>";
					}
				?>
			  </tbody>
			</table>
		<section>
    </div>
    <!-- / content body -->
  </div>
</div>
<!-- Copyright -->
<div class="wrapper row5">
  <footer id="copyright" class="clear">
    <p class="fl_left">Copyright &copy; 2014 - All Rights Reserved - <a href="home.php">livedule.com</a></p>
  </footer>
</div>
<script>
	function getParameterByName(name) {
		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
			results = regex.exec(location.search);
		return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
	
	if(getParameterByName("action")=="found"){
		alert("competition name already exists");
	}
	
	function validateAddCompetition(){
		if(document.getElementById("competitionName").value==""){
			alert("please enter competition name");
			document.getElementById("competitionName").focus();
		}
		else if(document.getElementById("competitionIcon").value==""){
			alert("please enter competition icon");
			document.getElementById("competitionIcon").focus();
		}
		else if(!endsWith(document.getElementById("competitionIcon").value,".png") && !endsWith(document.getElementById("competitionIcon").value,".jpg") && !endsWith(document.getElementById("competitionIcon").value,".bmp") && !endsWith(document.getElementById("competitionIcon").value,".gif")){
			alert("invalid competition icon format");
		}
		else{
			return true;
		}
		return false;
	}
	
	function validateUpdateCompetition(){
		if(document.getElementById("competitionName").value==""){
			alert("please enter competition name");
			document.getElementById("competitionName").focus();
			return false;
		}
		if(document.getElementById("competitionIcon").value != ""){
			if(!endsWith(document.getElementById("competitionIcon").value,".png") && !endsWith(document.getElementById("competitionIcon").value,".jpg") && !endsWith(document.getElementById("competitionIcon").value,".bmp") && !endsWith(document.getElementById("competitionIcon").value,".gif")){
				alert("invalid competition icon format");
				return false;
			}
		}
		return true;
	}
	
	function endsWith(str, suffix) {
		return str.toLowerCase().indexOf(suffix, str.length - suffix.length) !== -1;
	}
	</script>
</body>
</html>