<?php session_start(); 
if(!isset($_SESSION['USER_ID'])){ 
	header("Location: index.html");
}
include('db_config.php');

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>LiveDuel</title>
		<!-- JQuery JS -->
		<script src="bootstrap/jquery-1.js"></script>
		<!-- Bootstrap -->
		<link href="bootstrap/bootstrap.css" rel="stylesheet">
		<script src="bootstrap/bootstrap.js"></script>
		<!-- Liveduel custom styles -->
		<link href="styles/liveduel.css" rel="stylesheet">
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div id="wrap">
			<div class="navbar navbar-default navbar-fixed-top" role="navigation">
			  <div class="container">
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				  <a class="navbar-brand" href="home.php">LiveDuel</a>
				</div>
				<div style="height: 1px;" class="navbar-collapse collapse">
				  <ul class="nav navbar-nav">
					<li><a href="bet.php">Make a Bet!</a></li>
					<?php
					$sql = "SELECT id, name FROM sports";
					foreach ($db_handler->query($sql) as $row) {
					?>
					<li><a href="bet.php?spotId=<?php echo $row["id"];?>"><?php echo $row["name"]; ?></a></li>
					<?php } ?>
					<li class="active"><a href="invite.php">Invite Friends</a></li>
					<li ><a href="profile.php">Profile</a></li>
				  </ul>
				  
				</div><!--/.nav-collapse -->
			  </div>
			</div>
			<div class="container">
				<p><span style="float:right;">Welcome, <?php echo $_SESSION['USER_NAME']; ?>&nbsp;&nbsp;&nbsp;&nbsp;<a href="logout.php">Log Out</a></span></p>
				<!-- Past Bets -->
				<h3>Invite Friends</h3>
				<br/>
				<div class="alert alert-info" id="messageDiv"></div>
				<form class="form-inline" role="form" style="width:70%;" action="sendEmail.php" method="post">
				  <div class="form-group">
					<label class="control-label">Email Address</label>
					<input type="email" class="form-control" style="width:300px;" id="email" name="email" placeholder="email@example.com" required>
					<button type="submit" class="btn btn-primary" >Send Invitation</button>
				  </div>
				</form>
				
			</div> <!-- /container -->
		</div>
		<div id="footer">
		  <div class="container">
			<p class="text-muted">Copyright &copy; 2014 - All Rights Reserved - <a href="home.php">livedule.com</a></p>
		  </div>
		</div>
		<script>
			if(getParameterByName("action")=="success"){
				$("#messageDiv").html("Invitation has been sent successfully");
				$('#messageDiv').show();
				window.setTimeout("$('#messageDiv').hide('slow');",2000);
			}
			function getParameterByName(name) {
				name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
				var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
					results = regex.exec(location.search);
				return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
			}
		</script>
	</body>
</html>