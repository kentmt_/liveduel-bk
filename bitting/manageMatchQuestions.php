<?php session_start(); 
if(!isset($_SESSION['IS_ADMIN'])){
	header("Location: index.html");
}
$matchId = $_REQUEST["id"];
$competitionId = "";
$matchName = "";
$competitionName = "";
$sportId = "";
include('db_config.php');
$sql = "SELECT competition_id , (SELECT name from teams where id = m.team1) as team1 ,
				(SELECT name from teams where id = m.team2) as team2 
		FROM matches m WHERE id = $matchId ";
foreach ($db_handler->query($sql) as $row) {
	$matchName = $row["team1"]." VS ".$row["team2"];
	$competitionId = $row["competition_id"];
	break;
}
$sql = "SELECT id, name, sport_id FROM competitions WHERE id= $competitionId";
foreach ($db_handler->query($sql) as $row) {
	$competitionName = $row["name"];
	$sportId = $row["sport_id"];
	break;
}
$sportName = "";
$sql = "SELECT name FROM sports WHERE id= $sportId";
foreach ($db_handler->query($sql) as $row) {
	$sportName = $row["name"];
	break;
}
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
<title>LiveDuel</title>
<meta charset="iso-8859-1">
<link rel="stylesheet" href="styles/layout.css" type="text/css">
<script src="scripts/jquery-1.8.2.min.js"></script>
<script src="scripts/jquery-defaultvalue.js"></script>
<script src="scripts/jquery-browsercheck.js"></script>
<!--[if lt IE 9]>
<link rel="stylesheet" href="styles/ie.css" type="text/css">
<script src="scripts/ie/html5shiv.min.js"></script>
<![endif]-->

</head>
<body>
<div class="wrapper row1">
  <header id="header" class="clear">
    <hgroup>
      <h1><a href="home.php"><span>LiveDuel</span></a></h1>
    </hgroup>
    <form action="#" method="post">
      <fieldset>
		<span style="float:right;">Welcome, <?php echo $_SESSION['USER_NAME']; ?>&nbsp;&nbsp;&nbsp;&nbsp;<a href="adminLogout.php">Log Out</a></span>
      </fieldset>
    </form>
	<nav>
      <ul class="clear">
        <li class="first active"><a href="adminHome.php">Admin Home</a></li>        
      </ul>
    </nav>
	
  </header>
  <div id="breadcrumb">
	<a href="adminHome.php">Sports</a> > <a href="manageSport.php?id=<?php echo $sportId ;?>"><?php echo $sportName ;?></a> > <a href="manageCompetitionMatches.php?id=<?php echo $competitionId ;?>"><?php echo $competitionName ;?></a> > <a href="manageMatchQuestions.php?id=<?php echo $matchId ;?>"><?php echo $matchName ;?> Questions</a>
  </div>
</div>
<!-- content -->
<div class="wrapper row3">
  <div id="container" class="clear">
    <div id="homepage">
		<h1>Administration Panel</h1>
		<?php if(!isset($_REQUEST['action']) || $_REQUEST['action']!="edit") {?>
		<section id="comments" style="margin:5px;width:100%;">
			<h2>Manage <?php echo $matchName; ?> Questions</h2>
			<form action="insertMatchQuestion.php" method="post" enctype="multipart/form-data" >
				<input name="MAX_FILE_SIZE" value="102400" type="hidden">
				<input id="matchId" name="matchId" value="<?php echo $matchId ; ?>" type="hidden">
				<p>
					<input type="text" style="width:375px;" name="question" id="question" value="" size="22" />
					<label for="question" ><small>Question</small></label>
				</p>
				<p style="height:300px; overflow:scroll">
					<label style="width:120px;"><small>Answers</small></label><br/>
					<small>Answer 1 </small><input type="text" name="answer1" id="answer1" value="" /><small>Points 1 </small><input type="text" name="points1" id="points1" value="" style="width:45px;" /><small>Icon 1 </small><input type="file" name="icon1" id="icon1" value="" size="22" /><br/>
					<small>Answer 2 </small><input type="text" name="answer2" id="answer2" value="" /><small>Points 2 </small><input type="text" name="points2" id="points2" value="" style="width:45px;" /><small>Icon 2 </small><input type="file" name="icon2" id="icon2" value="" size="22" /><br/>
					<small>Answer 3 </small><input type="text" name="answer3" id="answer3" value="" /><small>Points 3 </small><input type="text" name="points3" id="points3" value="" style="width:45px;" /><small>Icon 3 </small><input type="file" name="icon3" id="icon3" value="" size="22" /><br/>
					<small>Answer 4 </small><input type="text" name="answer4" id="answer4" value="" /><small>Points 4 </small><input type="text" name="points4" id="points4" value="" style="width:45px;" /><small>Icon 4 </small><input type="file" name="icon4" id="icon4" value="" size="22" /><br/>
					<small>Answer 5 </small><input type="text" name="answer5" id="answer5" value="" /><small>Points 5 </small><input type="text" name="points5" id="points5" value="" style="width:45px;" /><small>Icon 5 </small><input type="file" name="icon5" id="icon5" value="" size="22" /><br/>
					<small>Answer 6 </small><input type="text" name="answer6" id="answer6" value="" /><small>Points 6 </small><input type="text" name="points6" id="points6" value="" style="width:45px;" /><small>Icon 6 </small><input type="file" name="icon6" id="icon6" value="" size="22" /><br/>
					<small>Answer 7 </small><input type="text" name="answer7" id="answer7" value="" /><small>Points 7 </small><input type="text" name="points7" id="points7" value="" style="width:45px;" /><small>Icon 7 </small><input type="file" name="icon7" id="icon7" value="" size="22" /><br/>
					<small>Answer 8 </small><input type="text" name="answer8" id="answer8" value="" /><small>Points 8 </small><input type="text" name="points8" id="points8" value="" style="width:45px;" /><small>Icon 8 </small><input type="file" name="icon8" id="icon8" value="" size="22" /><br/>
					<small>Answer 9 </small><input type="text" name="answer9" id="answer9" value="" /><small>Points 9 </small><input type="text" name="points9" id="points9" value="" style="width:45px;" /><small>Icon 9 </small><input type="file" name="icon9" id="icon9" value="" size="22" /><br/>
					<small>Answer 10 </small><input type="text" name="answer10" id="answer10" value="" /><small>Points 10 </small><input type="text" name="points10" id="points10" value="" style="width:45px;" /><small>Icon 10 </small><input type="file" name="icon10" id="icon10" value="" size="22" /><br/>
					<small>Answer 11 </small><input type="text" name="answer11" id="answer11" value="" /><small>Points 11 </small><input type="text" name="points11" id="points11" value="" style="width:45px;" /><small>Icon 11 </small><input type="file" name="icon11" id="icon11" value="" size="22" /><br/>
					<small>Answer 12 </small><input type="text" name="answer12" id="answer12" value="" /><small>Points 12 </small><input type="text" name="points12" id="points12" value="" style="width:45px;" /><small>Icon 12 </small><input type="file" name="icon12" id="icon12" value="" size="22" /><br/>
					<small>Answer 13 </small><input type="text" name="answer13" id="answer13" value="" /><small>Points 13 </small><input type="text" name="points13" id="points13" value="" style="width:45px;" /><small>Icon 13 </small><input type="file" name="icon13" id="icon13" value="" size="22" /><br/>
					<small>Answer 14 </small><input type="text" name="answer14" id="answer14" value="" /><small>Points 14 </small><input type="text" name="points14" id="points14" value="" style="width:45px;" /><small>Icon 14 </small><input type="file" name="icon14" id="icon14" value="" size="22" /><br/>
					<small>Answer 15 </small><input type="text" name="answer15" id="answer15" value="" /><small>Points 15 </small><input type="text" name="points15" id="points15" value="" style="width:45px;" /><small>Icon 15 </small><input type="file" name="icon15" id="icon15" value="" size="22" /><br/>
					<small>Answer 16 </small><input type="text" name="answer16" id="answer16" value="" /><small>Points 16 </small><input type="text" name="points16" id="points16" value="" style="width:45px;" /><small>Icon 16 </small><input type="file" name="icon16" id="icon16" value="" size="22" /><br/>
					<small>Answer 17 </small><input type="text" name="answer17" id="answer17" value="" /><small>Points 17 </small><input type="text" name="points17" id="points17" value="" style="width:45px;" /><small>Icon 17 </small><input type="file" name="icon17" id="icon17" value="" size="22" /><br/>
					<small>Answer 18 </small><input type="text" name="answer18" id="answer18" value="" /><small>Points 18 </small><input type="text" name="points18" id="points18" value="" style="width:45px;" /><small>Icon 18 </small><input type="file" name="icon18" id="icon18" value="" size="22" /><br/>
					<small>Answer 19 </small><input type="text" name="answer19" id="answer19" value="" /><small>Points 19 </small><input type="text" name="points19" id="points19" value="" style="width:45px;" /><small>Icon 19 </small><input type="file" name="icon19" id="icon19" value="" size="22" /><br/>
					<small>Answer 20 </small><input type="text" name="answer20" id="answer20" value="" /><small>Points 20 </small><input type="text" name="points20" id="points20" value="" style="width:45px;" /><small>Icon 20 </small><input type="file" name="icon20" id="icon20" value="" size="22" /><br/>	
					<small>Answer 21 </small><input type="text" name="answer21" id="answer21" value="" /><small>Points 21 </small><input type="text" name="points21" id="points21" value="" style="width:45px;" /><small>Icon 21 </small><input type="file" name="icon21" id="icon21" value="" size="22" /><br/>	
					<small>Answer 22 </small><input type="text" name="answer22" id="answer22" value="" /><small>Points 22 </small><input type="text" name="points22" id="points22" value="" style="width:45px;" /><small>Icon 22 </small><input type="file" name="icon22" id="icon22" value="" size="22" /><br/>	
					<small>Answer 23 </small><input type="text" name="answer23" id="answer23" value="" /><small>Points 23 </small><input type="text" name="points23" id="points23" value="" style="width:45px;" /><small>Icon 23 </small><input type="file" name="icon23" id="icon23" value="" size="22" /><br/>	
					<small>Answer 24 </small><input type="text" name="answer24" id="answer24" value="" /><small>Points 24 </small><input type="text" name="points24" id="points24" value="" style="width:45px;" /><small>Icon 24 </small><input type="file" name="icon24" id="icon24" value="" size="22" /><br/>	
					<small>Answer 25 </small><input type="text" name="answer25" id="answer25" value="" /><small>Points 25 </small><input type="text" name="points25" id="points25" value="" style="width:45px;" /><small>Icon 25 </small><input type="file" name="icon25" id="icon25" value="" size="22" /><br/>	
					<small>Answer 26 </small><input type="text" name="answer26" id="answer26" value="" /><small>Points 26 </small><input type="text" name="points26" id="points26" value="" style="width:45px;" /><small>Icon 26 </small><input type="file" name="icon26" id="icon26" value="" size="22" /><br/>	
					<small>Answer 27 </small><input type="text" name="answer27" id="answer27" value="" /><small>Points 27 </small><input type="text" name="points27" id="points27" value="" style="width:45px;" /><small>Icon 27 </small><input type="file" name="icon27" id="icon27" value="" size="22" /><br/>	
					<small>Answer 28 </small><input type="text" name="answer28" id="answer28" value="" /><small>Points 28 </small><input type="text" name="points28" id="points28" value="" style="width:45px;" /><small>Icon 28 </small><input type="file" name="icon28" id="icon28" value="" size="22" /><br/>	
					<small>Answer 29 </small><input type="text" name="answer29" id="answer29" value="" /><small>Points 29 </small><input type="text" name="points29" id="points29" value="" style="width:45px;" /><small>Icon 29 </small><input type="file" name="icon29" id="icon29" value="" size="22" /><br/>	
					<small>Answer 30 </small><input type="text" name="answer30" id="answer30" value="" /><small>Points 30 </small><input type="text" name="points30" id="points30" value="" style="width:45px;" /><small>Icon 30 </small><input type="file" name="icon30" id="icon30" value="" size="22" /><br/>	
					<small>Answer 31 </small><input type="text" name="answer31" id="answer31" value="" /><small>Points 31 </small><input type="text" name="points31" id="points31" value="" style="width:45px;" /><small>Icon 31 </small><input type="file" name="icon31" id="icon31" value="" size="22" /><br/>	
					<small>Answer 32 </small><input type="text" name="answer32" id="answer32" value="" /><small>Points 32 </small><input type="text" name="points32" id="points32" value="" style="width:45px;" /><small>Icon 32 </small><input type="file" name="icon32" id="icon32" value="" size="22" /><br/>	
				</p>
				<p>
					<input name="submit" type="submit" id="submit" value="Add Question" onclick="return validateAddQuestion();" />
				</p>
			</form>
		</section>
		<?php }?>
		<?php if(isset($_REQUEST['action']) && $_REQUEST['action']=="edit" && isset($_REQUEST['questionId'])) {
			$questionId = $_REQUEST['questionId'];
			$question = "";
			$answers = "";
			$sql = "SELECT id, name FROM match_questions WHERE id= $questionId";
			foreach ($db_handler->query($sql) as $row) {
				$question = $row["name"];
				break;
			}
			$sql2 = "SELECT answer, points FROM match_answers WHERE question_id = $questionId";
			$answers = array();
			$points = array();
			foreach ($db_handler->query($sql2) as $row2) {
				array_push($answers, $row2["answer"]);
				array_push($points, $row2["points"]);
			}
		?>
		<section id="comments" style="margin:5px;width:100%;">
			<h2>Manage <?php echo $matchName; ?> Questions</h2>
			<form action="editMatchQuestion.php" method="post" enctype="multipart/form-data" >
				<input name="MAX_FILE_SIZE" value="102400" type="hidden">
				<input id="matchId" name="matchId" value="<?php echo $matchId ; ?>" type="hidden">
				<input id="questionId" name="questionId" value="<?php echo $questionId ; ?>" type="hidden">
				<p>
					<input type="text" name="question" style="width:375px;" id="question" value="<?php echo $question; ?>" size="22" />
					<label for="question" style="width:120px;"><small>Question</small></label>
				</p>
				<p style="height:300px; overflow:scroll">
					<label style="width:120px;"><small>Answers</small></label><br/>
					<small>Answer 1 </small><input type="text" name="answer1" id="answer1" value="<?php if(count($answers)>=1) {echo $answers[0];} ?>" /><small>Points 1 </small><input type="text" name="points1" id="points1" value="<?php if(count($points)>=1) {echo $points[0];} ?>" style="width:45px;" /><small>Icon 1 </small><input type="file" name="icon1" id="icon1" value="" size="22" /><br/>
					<small>Answer 2 </small><input type="text" name="answer2" id="answer2" value="<?php if(count($answers)>=2) {echo $answers[1];} ?>" /><small>Points 2 </small><input type="text" name="points2" id="points2" value="<?php if(count($points)>=2) {echo $points[1];} ?>" style="width:45px;" /><small>Icon 2 </small><input type="file" name="icon2" id="icon2" value="" size="22" /><br/>
					<small>Answer 3 </small><input type="text" name="answer3" id="answer3" value="<?php if(count($answers)>=3) {echo $answers[2];} ?>" /><small>Points 3 </small><input type="text" name="points3" id="points3" value="<?php if(count($points)>=3) {echo $points[2];} ?>" style="width:45px;" /><small>Icon 3 </small><input type="file" name="icon3" id="icon3" value="" size="22" /><br/>
					<small>Answer 4 </small><input type="text" name="answer4" id="answer4" value="<?php if(count($answers)>=4) {echo $answers[3];} ?>" /><small>Points 4 </small><input type="text" name="points4" id="points4" value="<?php if(count($points)>=4) {echo $points[3];} ?>" style="width:45px;" /><small>Icon 4 </small><input type="file" name="icon4" id="icon4" value="" size="22" /><br/>
					<small>Answer 5 </small><input type="text" name="answer5" id="answer5" value="<?php if(count($answers)>=5) {echo $answers[4];} ?>" /><small>Points 5 </small><input type="text" name="points5" id="points5" value="<?php if(count($points)>=5) {echo $points[4];} ?>" style="width:45px;" /><small>Icon 5 </small><input type="file" name="icon5" id="icon5" value="" size="22" /><br/>
					<small>Answer 6 </small><input type="text" name="answer6" id="answer6" value="<?php if(count($answers)>=6) {echo $answers[4];} ?>" /><small>Points 6 </small><input type="text" name="points6" id="points6" value="<?php if(count($points)>=6) {echo $points[4];} ?>" style="width:46px;" /><small>Icon 6 </small><input type="file" name="icon6" id="icon6" value="" size="22" /><br/>
					<small>Answer 7 </small><input type="text" name="answer7" id="answer7" value="<?php if(count($answers)>=7) {echo $answers[4];} ?>" /><small>Points 7 </small><input type="text" name="points7" id="points7" value="<?php if(count($points)>=7) {echo $points[4];} ?>" style="width:47px;" /><small>Icon 7 </small><input type="file" name="icon7" id="icon7" value="" size="22" /><br/>
					<small>Answer 8 </small><input type="text" name="answer8" id="answer8" value="<?php if(count($answers)>=8) {echo $answers[4];} ?>" /><small>Points 8 </small><input type="text" name="points8" id="points8" value="<?php if(count($points)>=8) {echo $points[4];} ?>" style="width:48px;" /><small>Icon 8 </small><input type="file" name="icon8" id="icon8" value="" size="22" /><br/>
					<small>Answer 9 </small><input type="text" name="answer9" id="answer9" value="<?php if(count($answers)>=9) {echo $answers[4];} ?>" /><small>Points 9 </small><input type="text" name="points9" id="points9" value="<?php if(count($points)>=9) {echo $points[4];} ?>" style="width:49px;" /><small>Icon 9 </small><input type="file" name="icon9" id="icon9" value="" size="22" /><br/>
					<small>Answer 10 </small><input type="text" name="answer10" id="answer10" value="<?php if(count($answers)>=10) {echo $answers[4];} ?>" /><small>Points 10 </small><input type="text" name="points10" id="points10" value="<?php if(count($points)>=10) {echo $points[4];} ?>" style="width:410px;" /><small>Icon 10 </small><input type="file" name="icon10" id="icon10" value="" size="22" /><br/>
					<small>Answer 11 </small><input type="text" name="answer11" id="answer11" value="<?php if(count($answers)>=11) {echo $answers[4];} ?>" /><small>Points 11 </small><input type="text" name="points11" id="points11" value="<?php if(count($points)>=11) {echo $points[4];} ?>" style="width:411px;" /><small>Icon 11 </small><input type="file" name="icon11" id="icon11" value="" size="22" /><br/>
					<small>Answer 12 </small><input type="text" name="answer12" id="answer12" value="<?php if(count($answers)>=12) {echo $answers[4];} ?>" /><small>Points 12 </small><input type="text" name="points12" id="points12" value="<?php if(count($points)>=12) {echo $points[4];} ?>" style="width:412px;" /><small>Icon 12 </small><input type="file" name="icon12" id="icon12" value="" size="22" /><br/>
					<small>Answer 13 </small><input type="text" name="answer13" id="answer13" value="<?php if(count($answers)>=13) {echo $answers[4];} ?>" /><small>Points 13 </small><input type="text" name="points13" id="points13" value="<?php if(count($points)>=13) {echo $points[4];} ?>" style="width:413px;" /><small>Icon 13 </small><input type="file" name="icon13" id="icon13" value="" size="22" /><br/>
					<small>Answer 14 </small><input type="text" name="answer14" id="answer14" value="<?php if(count($answers)>=14) {echo $answers[4];} ?>" /><small>Points 14 </small><input type="text" name="points14" id="points14" value="<?php if(count($points)>=14) {echo $points[4];} ?>" style="width:414px;" /><small>Icon 14 </small><input type="file" name="icon14" id="icon14" value="" size="22" /><br/>
					<small>Answer 15 </small><input type="text" name="answer15" id="answer15" value="<?php if(count($answers)>=15) {echo $answers[4];} ?>" /><small>Points 15 </small><input type="text" name="points15" id="points15" value="<?php if(count($points)>=15) {echo $points[4];} ?>" style="width:415px;" /><small>Icon 15 </small><input type="file" name="icon15" id="icon15" value="" size="22" /><br/>
					<small>Answer 16 </small><input type="text" name="answer16" id="answer16" value="<?php if(count($answers)>=16) {echo $answers[4];} ?>" /><small>Points 16 </small><input type="text" name="points16" id="points16" value="<?php if(count($points)>=16) {echo $points[4];} ?>" style="width:416px;" /><small>Icon 16 </small><input type="file" name="icon16" id="icon16" value="" size="22" /><br/>
					<small>Answer 17 </small><input type="text" name="answer17" id="answer17" value="<?php if(count($answers)>=17) {echo $answers[4];} ?>" /><small>Points 17 </small><input type="text" name="points17" id="points17" value="<?php if(count($points)>=17) {echo $points[4];} ?>" style="width:417px;" /><small>Icon 17 </small><input type="file" name="icon17" id="icon17" value="" size="22" /><br/>
					<small>Answer 18 </small><input type="text" name="answer18" id="answer18" value="<?php if(count($answers)>=18) {echo $answers[4];} ?>" /><small>Points 18 </small><input type="text" name="points18" id="points18" value="<?php if(count($points)>=18) {echo $points[4];} ?>" style="width:418px;" /><small>Icon 18 </small><input type="file" name="icon18" id="icon18" value="" size="22" /><br/>
					<small>Answer 19 </small><input type="text" name="answer19" id="answer19" value="<?php if(count($answers)>=19) {echo $answers[4];} ?>" /><small>Points 19 </small><input type="text" name="points19" id="points19" value="<?php if(count($points)>=19) {echo $points[4];} ?>" style="width:419px;" /><small>Icon 19 </small><input type="file" name="icon19" id="icon19" value="" size="22" /><br/>
					<small>Answer 20 </small><input type="text" name="answer20" id="answer20" value="<?php if(count($answers)>=20) {echo $answers[4];} ?>" /><small>Points 20 </small><input type="text" name="points20" id="points20" value="<?php if(count($points)>=20) {echo $points[4];} ?>" style="width:420px;" /><small>Icon 20 </small><input type="file" name="icon20" id="icon20" value="" size="22" /><br/>
					<small>Answer 21 </small><input type="text" name="answer21" id="answer21" value="<?php if(count($answers)>=21) {echo $answers[4];} ?>" /><small>Points 21 </small><input type="text" name="points21" id="points21" value="<?php if(count($points)>=21) {echo $points[4];} ?>" style="width:421px;" /><small>Icon 21 </small><input type="file" name="icon21" id="icon21" value="" size="22" /><br/>
					<small>Answer 22 </small><input type="text" name="answer22" id="answer22" value="<?php if(count($answers)>=22) {echo $answers[4];} ?>" /><small>Points 22 </small><input type="text" name="points22" id="points22" value="<?php if(count($points)>=22) {echo $points[4];} ?>" style="width:422px;" /><small>Icon 22 </small><input type="file" name="icon22" id="icon22" value="" size="22" /><br/>
					<small>Answer 23 </small><input type="text" name="answer23" id="answer23" value="<?php if(count($answers)>=23) {echo $answers[4];} ?>" /><small>Points 23 </small><input type="text" name="points23" id="points23" value="<?php if(count($points)>=23) {echo $points[4];} ?>" style="width:423px;" /><small>Icon 23 </small><input type="file" name="icon23" id="icon23" value="" size="22" /><br/>
					<small>Answer 24 </small><input type="text" name="answer24" id="answer24" value="<?php if(count($answers)>=24) {echo $answers[4];} ?>" /><small>Points 24 </small><input type="text" name="points24" id="points24" value="<?php if(count($points)>=24) {echo $points[4];} ?>" style="width:424px;" /><small>Icon 24 </small><input type="file" name="icon24" id="icon24" value="" size="22" /><br/>
					<small>Answer 25 </small><input type="text" name="answer25" id="answer25" value="<?php if(count($answers)>=25) {echo $answers[4];} ?>" /><small>Points 25 </small><input type="text" name="points25" id="points25" value="<?php if(count($points)>=25) {echo $points[4];} ?>" style="width:425px;" /><small>Icon 25 </small><input type="file" name="icon25" id="icon25" value="" size="22" /><br/>
					<small>Answer 26 </small><input type="text" name="answer26" id="answer26" value="<?php if(count($answers)>=26) {echo $answers[4];} ?>" /><small>Points 26 </small><input type="text" name="points26" id="points26" value="<?php if(count($points)>=26) {echo $points[4];} ?>" style="width:426px;" /><small>Icon 26 </small><input type="file" name="icon26" id="icon26" value="" size="22" /><br/>
					<small>Answer 27 </small><input type="text" name="answer27" id="answer27" value="<?php if(count($answers)>=27) {echo $answers[4];} ?>" /><small>Points 27 </small><input type="text" name="points27" id="points27" value="<?php if(count($points)>=27) {echo $points[4];} ?>" style="width:427px;" /><small>Icon 27 </small><input type="file" name="icon27" id="icon27" value="" size="22" /><br/>
					<small>Answer 28 </small><input type="text" name="answer28" id="answer28" value="<?php if(count($answers)>=28) {echo $answers[4];} ?>" /><small>Points 28 </small><input type="text" name="points28" id="points28" value="<?php if(count($points)>=28) {echo $points[4];} ?>" style="width:428px;" /><small>Icon 28 </small><input type="file" name="icon28" id="icon28" value="" size="22" /><br/>
					<small>Answer 29 </small><input type="text" name="answer29" id="answer29" value="<?php if(count($answers)>=29) {echo $answers[4];} ?>" /><small>Points 29 </small><input type="text" name="points29" id="points29" value="<?php if(count($points)>=29) {echo $points[4];} ?>" style="width:429px;" /><small>Icon 29 </small><input type="file" name="icon29" id="icon29" value="" size="22" /><br/>
					<small>Answer 30 </small><input type="text" name="answer30" id="answer30" value="<?php if(count($answers)>=30) {echo $answers[4];} ?>" /><small>Points 30 </small><input type="text" name="points30" id="points30" value="<?php if(count($points)>=30) {echo $points[4];} ?>" style="width:430px;" /><small>Icon 30 </small><input type="file" name="icon30" id="icon30" value="" size="22" /><br/>
					<small>Answer 31 </small><input type="text" name="answer31" id="answer31" value="<?php if(count($answers)>=31) {echo $answers[4];} ?>" /><small>Points 31 </small><input type="text" name="points31" id="points31" value="<?php if(count($points)>=31) {echo $points[4];} ?>" style="width:431px;" /><small>Icon 31 </small><input type="file" name="icon31" id="icon31" value="" size="22" /><br/>
					<small>Answer 32 </small><input type="text" name="answer32" id="answer32" value="<?php if(count($answers)>=32) {echo $answers[4];} ?>" /><small>Points 32 </small><input type="text" name="points32" id="points32" value="<?php if(count($points)>=32) {echo $points[4];} ?>" style="width:432px;" /><small>Icon 32 </small><input type="file" name="icon32" id="icon32" value="" size="22" /><br/>
				</p>
				<p>
					<input name="submit" type="submit" id="submit" value="Update Question" onclick="return validateAddQuestion();" />
				</p>
			</form>
		</section>
		<?php }?>
		<br/>
		<section style="width:100%;">
			<table>
			  <thead>
				<tr>
				  <th>Question</th>
				  <th>List of Answers</th>
				  <th>Correct Answer</th>
				  <th>Edit</th>
				  <th>Delete</th>
				</tr>
			  </thead>
			  <tbody>
				<?php
					$sql = "SELECT id, name, is_answered FROM match_questions WHERE match_id = $matchId ORDER BY id desc";
					$counter=0;
					foreach ($db_handler->query($sql) as $row) {
						if( ($counter % 2) == 0 ){
							echo "<tr class='light'>";
						}
						else{
							echo "<tr class='dark'>";
						}
						
						echo "<td>".$row["name"]."</td>";
						echo '<td>';
						$sql2 = "SELECT id , answer, points , is_correct FROM match_answers WHERE question_id = ".$row["id"]." ORDER BY answer";
						foreach ($db_handler->query($sql2) as $row2) {
							$color = "red";
							$weight = "normal";
							$title = "set as correct answer";
							if($row2["is_correct"] == "Y"){
								$color = "green";
								$weight = "bold";
								$title = "";
							}
							//if($row["is_answered"] == "Y"){
							//	echo "<a title='$title' style='color:$color; font-weight:$weight;' >".$row2['answer']." ".$row2['points']."</a><br/>";
							//}
							//else{
								echo "<a title='$title' style='color:$color; font-weight:$weight;' href='setAsCorrectMatchAnswer.php?id=".$row2['id']."&questionId=".$row["id"]."&matchId=$matchId'>".$row2['answer']." ".$row2['points']."</a><br/>";
							//}
						}
						echo '</td>';
						echo '<td>';
						$sql3 = "SELECT answer FROM match_answers WHERE question_id = ".$row["id"]." and is_correct = 'Y' ";
						foreach ($db_handler->query($sql3) as $row3) {
							echo $row3['answer'];
							break;
						}
						echo '</td>';
						//if($row["is_answered"] != "Y"){
							echo "<td align='center'><a title='Edit' href='manageMatchQuestions.php?action=edit&id=$matchId&questionId=".$row["id"]."'><img src='images/edit.png' /></a></td>";
							echo '<td align="center"><a title="Delete" href="deleteMatchQuestion.php?id='.$row["id"].'&matchId='.$matchId.'" onclick="return confirm(\'Are you sure you want to delete selected question?\');"><img src="images/delete.png" /></a></td>';
						//}
						//else{
						//	echo "<td></td>";
						//	echo "<td></td>";
						//}
						
						echo "</tr>";
						$counter = $counter + 1;
					}
					if($counter==0){
						echo "<tr class='light'><td colspan='5' align='center'>No data found</td></tr>";
					}
				?>
			  </tbody>
			</table>
		<section>
    </div>
    <!-- / content body -->
  </div>
</div>
<!-- Copyright -->
<div class="wrapper row5">
  <footer id="copyright" class="clear">
    <p class="fl_left">Copyright &copy; 2014 - All Rights Reserved - <a href="home.php">livedule.com</a></p>
  </footer>
</div>
<script>
	function getParameterByName(name) {
		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
			results = regex.exec(location.search);
		return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
	
	if(getParameterByName("action")=="found"){
		alert("question already exists for this match");
	}
	
	function validateAddQuestion(){
		if(document.getElementById("question").value==""){
			alert("please enter question");
			document.getElementById("question").focus();
		}
		else if(document.getElementById("answer1").value==""){
			alert("please enter at least 2 answers");
			document.getElementById("answer1").focus();
		}
		else if(document.getElementById("answer2").value==""){
			alert("please enter at least 2 answers");
			document.getElementById("answer2").focus();
		}
		else if(document.getElementById("answer1").value != "" && document.getElementById("points1").value == ""){
			alert("please enter points for answer1");
			document.getElementById("points1").focus();
		}
		else if(!validateNumber(document.getElementById("points1").value)){
			alert("Invalid number for points 1");
			document.getElementById("points1").focus();
		}
		else if(document.getElementById("answer2").value != "" && document.getElementById("points2").value == ""){
			alert("please enter points for answer2");
			document.getElementById("points2").focus();
		}
		else if(!validateNumber(document.getElementById("points2").value)){
			alert("Invalid number for points 2");
			document.getElementById("points2").focus();
		}
		else if(document.getElementById("answer3").value != "" && document.getElementById("points3").value == ""){
			alert("please enter points for answer3");
			document.getElementById("points3").focus();
		}
		else if(document.getElementById("answer3").value != "" && !validateNumber(document.getElementById("points3").value)){
			alert("Invalid number for points 3");
			document.getElementById("points3").focus();
		}
		else if(document.getElementById("answer4").value != "" && document.getElementById("points4").value == ""){
			alert("please enter points for answer4");
			document.getElementById("points4").focus();
		}
		else if(document.getElementById("answer4").value != "" && !validateNumber(document.getElementById("points4").value)){
			alert("Invalid number for points 4");
			document.getElementById("points4").focus();
		}
		else if(document.getElementById("answer5").value != "" && document.getElementById("points5").value == ""){
			alert("please enter points for answer5");
			document.getElementById("points5").focus();
		}
		else if(document.getElementById("answer5").value != "" && !validateNumber(document.getElementById("points5").value)){
			alert("Invalid number for points 5");
			document.getElementById("points5").focus();
		}
		else if(document.getElementById("answer6").value != "" && document.getElementById("points6").value == ""){
			alert("please enter points for answer6");
			document.getElementById("points6").focus();
		}
		else if(document.getElementById("answer6").value != "" && !validateNumber(document.getElementById("points6").value)){
			alert("Invalid number for points 6");
			document.getElementById("points6").focus();
		}
		else if(document.getElementById("answer7").value != "" && document.getElementById("points7").value == ""){
			alert("please enter points for answer7");
			document.getElementById("points7").focus();
		}
		else if(document.getElementById("answer7").value != "" && !validateNumber(document.getElementById("points7").value)){
			alert("Invalid number for points 7");
			document.getElementById("points7").focus();
		}
		else if(document.getElementById("answer8").value != "" && document.getElementById("points8").value == ""){
			alert("please enter points for answer8");
			document.getElementById("points8").focus();
		}
		else if(document.getElementById("answer8").value != "" && !validateNumber(document.getElementById("points8").value)){
			alert("Invalid number for points 8");
			document.getElementById("points8").focus();
		}
		else if(document.getElementById("answer9").value != "" && document.getElementById("points9").value == ""){
			alert("please enter points for answer9");
			document.getElementById("points9").focus();
		}
		else if(document.getElementById("answer9").value != "" && !validateNumber(document.getElementById("points9").value)){
			alert("Invalid number for points 9");
			document.getElementById("points9").focus();
		}
		else if(document.getElementById("answer10").value != "" && document.getElementById("points10").value == ""){
			alert("please enter points for answer10");
			document.getElementById("points10").focus();
		}
		else if(document.getElementById("answer10").value != "" && !validateNumber(document.getElementById("points10").value)){
			alert("Invalid number for points 10");
			document.getElementById("points10").focus();
		}
		else if(document.getElementById("answer11").value != "" && document.getElementById("points11").value == ""){
			alert("please enter points for answer11");
			document.getElementById("points11").focus();
		}
		else if(document.getElementById("answer11").value != "" && !validateNumber(document.getElementById("points11").value)){
			alert("Invalid number for points 11");
			document.getElementById("points11").focus();
		}
		else if(document.getElementById("answer12").value != "" && document.getElementById("points12").value == ""){
			alert("please enter points for answer12");
			document.getElementById("points12").focus();
		}
		else if(document.getElementById("answer12").value != "" && !validateNumber(document.getElementById("points12").value)){
			alert("Invalid number for points 12");
			document.getElementById("points12").focus();
		}
		else if(document.getElementById("answer13").value != "" && document.getElementById("points13").value == ""){
			alert("please enter points for answer13");
			document.getElementById("points13").focus();
		}
		else if(document.getElementById("answer13").value != "" && !validateNumber(document.getElementById("points13").value)){
			alert("Invalid number for points 13");
			document.getElementById("points13").focus();
		}
		else if(document.getElementById("answer14").value != "" && document.getElementById("points14").value == ""){
			alert("please enter points for answer14");
			document.getElementById("points14").focus();
		}
		else if(document.getElementById("answer14").value != "" && !validateNumber(document.getElementById("points14").value)){
			alert("Invalid number for points 14");
			document.getElementById("points14").focus();
		}
		else if(document.getElementById("answer15").value != "" && document.getElementById("points15").value == ""){
			alert("please enter points for answer15");
			document.getElementById("points15").focus();
		}
		else if(document.getElementById("answer15").value != "" && !validateNumber(document.getElementById("points15").value)){
			alert("Invalid number for points 15");
			document.getElementById("points15").focus();
		}
		else if(document.getElementById("answer16").value != "" && document.getElementById("points16").value == ""){
			alert("please enter points for answer16");
			document.getElementById("points16").focus();
		}
		else if(document.getElementById("answer16").value != "" && !validateNumber(document.getElementById("points16").value)){
			alert("Invalid number for points 16");
			document.getElementById("points16").focus();
		}
		else if(document.getElementById("answer17").value != "" && document.getElementById("points17").value == ""){
			alert("please enter points for answer17");
			document.getElementById("points17").focus();
		}
		else if(document.getElementById("answer17").value != "" && !validateNumber(document.getElementById("points17").value)){
			alert("Invalid number for points 17");
			document.getElementById("points17").focus();
		}
		else if(document.getElementById("answer18").value != "" && document.getElementById("points18").value == ""){
			alert("please enter points for answer18");
			document.getElementById("points18").focus();
		}
		else if(document.getElementById("answer18").value != "" && !validateNumber(document.getElementById("points18").value)){
			alert("Invalid number for points 18");
			document.getElementById("points18").focus();
		}
		else if(document.getElementById("answer19").value != "" && document.getElementById("points19").value == ""){
			alert("please enter points for answer19");
			document.getElementById("points19").focus();
		}
		else if(document.getElementById("answer19").value != "" && !validateNumber(document.getElementById("points19").value)){
			alert("Invalid number for points 19");
			document.getElementById("points19").focus();
		}
		else if(document.getElementById("answer20").value != "" && document.getElementById("points20").value == ""){
			alert("please enter points for answer20");
			document.getElementById("points20").focus();
		}
		else if(document.getElementById("answer20").value != "" && !validateNumber(document.getElementById("points20").value)){
			alert("Invalid number for points 20");
			document.getElementById("points20").focus();
		}
		else if(document.getElementById("answer21").value != "" && document.getElementById("points21").value == ""){
			alert("please enter points for answer21");
			document.getElementById("points21").focus();
		}
		else if(document.getElementById("answer21").value != "" && !validateNumber(document.getElementById("points21").value)){
			alert("Invalid number for points 21");
			document.getElementById("points21").focus();
		}
		else if(document.getElementById("answer22").value != "" && document.getElementById("points22").value == ""){
			alert("please enter points for answer22");
			document.getElementById("points22").focus();
		}
		else if(document.getElementById("answer22").value != "" && !validateNumber(document.getElementById("points22").value)){
			alert("Invalid number for points 22");
			document.getElementById("points22").focus();
		}
		else if(document.getElementById("answer23").value != "" && document.getElementById("points23").value == ""){
			alert("please enter points for answer23");
			document.getElementById("points23").focus();
		}
		else if(document.getElementById("answer23").value != "" && !validateNumber(document.getElementById("points23").value)){
			alert("Invalid number for points 23");
			document.getElementById("points23").focus();
		}
		else if(document.getElementById("answer24").value != "" && document.getElementById("points24").value == ""){
			alert("please enter points for answer24");
			document.getElementById("points24").focus();
		}
		else if(document.getElementById("answer24").value != "" && !validateNumber(document.getElementById("points24").value)){
			alert("Invalid number for points 24");
			document.getElementById("points24").focus();
		}
		else if(document.getElementById("answer25").value != "" && document.getElementById("points25").value == ""){
			alert("please enter points for answer25");
			document.getElementById("points25").focus();
		}
		else if(document.getElementById("answer25").value != "" && !validateNumber(document.getElementById("points25").value)){
			alert("Invalid number for points 25");
			document.getElementById("points25").focus();
		}
		else if(document.getElementById("answer26").value != "" && document.getElementById("points26").value == ""){
			alert("please enter points for answer26");
			document.getElementById("points26").focus();
		}
		else if(document.getElementById("answer26").value != "" && !validateNumber(document.getElementById("points26").value)){
			alert("Invalid number for points 26");
			document.getElementById("points26").focus();
		}
		else if(document.getElementById("answer27").value != "" && document.getElementById("points27").value == ""){
			alert("please enter points for answer27");
			document.getElementById("points27").focus();
		}
		else if(document.getElementById("answer27").value != "" && !validateNumber(document.getElementById("points27").value)){
			alert("Invalid number for points 27");
			document.getElementById("points27").focus();
		}
		else if(document.getElementById("answer28").value != "" && document.getElementById("points28").value == ""){
			alert("please enter points for answer28");
			document.getElementById("points28").focus();
		}
		else if(document.getElementById("answer28").value != "" && !validateNumber(document.getElementById("points28").value)){
			alert("Invalid number for points 28");
			document.getElementById("points28").focus();
		}
		else if(document.getElementById("answer29").value != "" && document.getElementById("points29").value == ""){
			alert("please enter points for answer29");
			document.getElementById("points29").focus();
		}
		else if(document.getElementById("answer29").value != "" && !validateNumber(document.getElementById("points29").value)){
			alert("Invalid number for points 29");
			document.getElementById("points29").focus();
		}
		else if(document.getElementById("answer30").value != "" && document.getElementById("points30").value == ""){
			alert("please enter points for answer30");
			document.getElementById("points30").focus();
		}
		else if(document.getElementById("answer30").value != "" && !validateNumber(document.getElementById("points30").value)){
			alert("Invalid number for points 30");
			document.getElementById("points30").focus();
		}
		else if(document.getElementById("answer31").value != "" && document.getElementById("points31").value == ""){
			alert("please enter points for answer31");
			document.getElementById("points31").focus();
		}
		else if(document.getElementById("answer31").value != "" && !validateNumber(document.getElementById("points31").value)){
			alert("Invalid number for points 31");
			document.getElementById("points31").focus();
		}
		else if(document.getElementById("answer32").value != "" && document.getElementById("points32").value == ""){
			alert("please enter points for answer32");
			document.getElementById("points32").focus();
		}
		else if(document.getElementById("answer32").value != "" && !validateNumber(document.getElementById("points32").value)){
			alert("Invalid number for points 32");
			document.getElementById("points32").focus();
		}
		else{
			return true;
		}
		return false;
	}

	function endsWith(str, suffix) {
		return str.toLowerCase().indexOf(suffix, str.length - suffix.length) !== -1;
	}
	
	function validateNumber(numb) {  
	  var re = /^[0-9]+$/;  
	  return re.test(numb);	
	} 
	
	</script>
</body>
</html>