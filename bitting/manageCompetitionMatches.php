<?php session_start(); 
if(!isset($_SESSION['IS_ADMIN'])){ 
	header("Location: index.html");
}
$competitionId = $_REQUEST["id"];
$competitionName = "";
$sportId = "";
include('db_config.php');
$sql = "SELECT id, name, sport_id FROM competitions WHERE id= $competitionId";
foreach ($db_handler->query($sql) as $row) {
	$competitionName = $row["name"];
	$sportId = $row["sport_id"];
	break;
}
$sportName = "";
$sql = "SELECT name FROM sports WHERE id= $sportId";
foreach ($db_handler->query($sql) as $row) {
	$sportName = $row["name"];
	break;
}
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
<title>LiveDuel</title>
<meta charset="iso-8859-1">
<link rel="stylesheet" href="styles/layout.css" type="text/css">
<script src="scripts/jquery-1.8.2.min.js"></script>
<script src="scripts/jquery-defaultvalue.js"></script>
<script src="scripts/jquery-browsercheck.js"></script>
<!--[if lt IE 9]>
<link rel="stylesheet" href="styles/ie.css" type="text/css">
<script src="scripts/ie/html5shiv.min.js"></script>
<![endif]-->

</head>
<body>
<div class="wrapper row1">
  <header id="header" class="clear">
    <hgroup>
      <h1><a href="home.php"><span>LiveDuel</span></a></h1>
    </hgroup>
    <form action="#" method="post">
      <fieldset>
		<span style="float:right;">Welcome, <?php echo $_SESSION['USER_NAME']; ?>&nbsp;&nbsp;&nbsp;&nbsp;<a href="adminLogout.php">Log Out</a></span>
      </fieldset>
    </form>
	<nav>
      <ul class="clear">
        <li class="first active"><a href="adminHome.php">Admin Home</a></li>        
      </ul>
    </nav>
  </header>
  <div id="breadcrumb">
	<a href="adminHome.php">Sports</a> > <a href="manageSport.php?id=<?php echo $sportId ;?>"><?php echo $sportName ;?></a> > <a href="manageCompetitionMatches.php?id=<?php echo $competitionId ;?>"><?php echo $competitionName ;?> Matches</a>
  </div>
</div>
<!-- content -->
<div class="wrapper row3">
  <div id="container" class="clear">
    <div id="homepage">
		<h1>Administration Panel</h1>
		<?php if(!isset($_REQUEST['action']) || $_REQUEST['action']!="edit") {?>
		<section id="comments" style="float:left;">
			<h2>Manage <?php echo $competitionName; ?> Matches</h2>
			<form action="insertMatch.php" method="post" enctype="multipart/form-data">
				<input name="MAX_FILE_SIZE" value="102400" type="hidden">
				<p>
					<input type="text" name="group" id="group" value="" size="22" style="width:180px;" />
					<label for="drawPoints" ><small>Group</small></label>
				</p>
				<p>
					<input type="text" name="stadium" id="stadium" value="" size="22" style="width:180px;" />
					<label for="drawPoints" ><small>Statium</small></label>
				</p>
				<p>
					<input type="text" name="city" id="city" value="" size="22" style="width:180px;" />
					<label for="drawPoints" ><small>City</small></label>
				</p>
				<input id="competitionId" name="competitionId" value="<?php echo $competitionId ; ?>" type="hidden">
				<p>
					<select style="width:200px;" name="team1" id="team1">
						<?php
							$sql = "SELECT id, name FROM teams WHERE competition_id= $competitionId ORDER BY name";
							foreach ($db_handler->query($sql) as $row) {
								echo "<option value='".$row["id"]."' >". $row["name"]."</option>";
							}
						?>
					</select>
					<input type="text" name="team1Points" id="team1Points" value="" size="22" style="width:50px;" />
					<label for="team1Points"><small>Team 1</small></label>
				</p>
				<p>
					<select style="width:200px;" name="team2" id="team2">
						<?php
							$sql = "SELECT id, name FROM teams WHERE competition_id= $competitionId ORDER BY name";
							foreach ($db_handler->query($sql) as $row) {
								echo "<option value='".$row["id"]."' >". $row["name"]."</option>";
							}
						?>
					</select>
					<input type="text" name="team2Points" id="team2Points" value="" size="22" style="width:50px;" />
					<label for="team2Points"><small>Team 2</small></label>
				</p>
				<p>
					<input type="text" name="drawPoints" id="drawPoints" value="" size="22" style="width:180px;" />
					<label for="drawPoints" ><small>Draw Points</small></label>
				</p>
				<p>
					<input type="text" name="endDate" id="endDate" value="" size="10" style="width:180px;" />
					<label for="endDate" ><small>End Date</small></label>
				</p>
				<p>
					<input type="text" name="endTime" id="endTime" value="" size="5" style="width:180px;" />
					<label for="endTime" ><small>End Time</small></label>
				</p>
				<p>
					<input name="submit" type="submit" id="submit" value="Add Match" onclick="return validateAddMatch();" />
				</p>
			</form>
		</section>
		<?php }?>
		<?php if(isset($_REQUEST['action']) && $_REQUEST['action']=="edit" && isset($_REQUEST['matchId'])) {
			$matchId = $_REQUEST['matchId'];
			$team1Id = 0;
			$team2Id = 0;
			$team1Points =0;
			$team2Points =0;
			$drawPoints =0;
			
			$sql = "SELECT id, team1 , team2, team1_points , team2_points, draw_points, end_date, end_time FROM matches m WHERE id = $matchId ";
			foreach ($db_handler->query($sql) as $row) {
				$team1Id = $row["team1"];
				$team2Id = $row["team2"];
				$team1Points = $row["team1_points"];
				$team2Points = $row["team2_points"];
				$drawPoints = $row["draw_points"];
				$endDate = $row["end_date"];
				$endTime = $row["end_time"];
				break;
			}
		?>
		<section id="comments" style="float:left;">
			<h2>Manage <?php echo $competitionName; ?> Matches</h2>
			<form action="editMatch.php" method="post" enctype="multipart/form-data">
				<input name="MAX_FILE_SIZE" value="102400" type="hidden">
				<input id="competitionId" name="competitionId" value="<?php echo $competitionId ; ?>" type="hidden">
				<input id="matchId" name="matchId" value="<?php echo $matchId ; ?>" type="hidden">
				<p>
					<select style="width:200px;" name="team1" id="team1">
						<?php
							$sql = "SELECT id, name FROM teams WHERE competition_id= $competitionId ORDER BY name";
							foreach ($db_handler->query($sql) as $row) {
								if($row["id"] == $team1Id){
									echo "<option selected value='".$row["id"]."' >". $row["name"]."</option>";
								}
								else{
									echo "<option value='".$row["id"]."' >". $row["name"]."</option>";
								}
							}
						?>
					</select>
					<input type="text" name="team1Points" id="team1Points" value="<?php echo $team1Points; ?>" size="22" style="width:50px;" />
					<label for="team1Points"><small>Team 1</small></label>
				</p>
				<p>
					<select style="width:200px;" name="team2" id="team2">
						<?php
							$sql = "SELECT id, name FROM teams WHERE competition_id= $competitionId ORDER BY name";
							foreach ($db_handler->query($sql) as $row) {
								if($row["id"] == $team2Id){
									echo "<option selected value='".$row["id"]."' >". $row["name"]."</option>";
								}
								else{
									echo "<option value='".$row["id"]."' >". $row["name"]."</option>";
								}
							}
						?>
					</select>
					<input type="text" name="team2Points" id="team2Points" value="<?php echo $team2Points; ?>" size="22" style="width:50px;" />
					<label for="team2Points"><small>Team 2</small></label>
				</p>
				<p>
					<input type="text" name="drawPoints" id="drawPoints" value="<?php echo $drawPoints; ?>" size="22" style="width:180px;" />
					<label for="drawPoints" ><small>Draw Points</small></label>
				</p>
				<p>
					<input type="text" name="endDate" id="endDate" value="<?php echo $endDate; ?>" size="10" style="width:180px;" />
					<label for="endDate" ><small>End Date</small></label>
				</p>
				<p>
					<input type="text" name="endTime" id="endTime" value="<?php echo $endTime; ?>" size="5" style="width:180px;" />
					<label for="endTime" ><small>End Time</small></label>
				</p>
				<p>
					<input name="submit" type="submit" id="submit" value="Update Match" onclick="return validateAddMatch();" />
				</p>
			</form>
		</section>
		<?php }?>
		
		<section style="width:40%;float:right; padding-top:50px;">
			<table class="tableNoBorder" width="100%">
				<tr><td align="center"><a href="manageCompetitionQuestions.php?id=<?php echo $competitionId ; ?>" ><input type="button" style="cursor:pointer;" value="Manage Questions"  /></a></td></tr>
				<tr><td align="center"><a href="manageCompetitionTeams.php?id=<?php echo $competitionId ; ?>" ><input type="button" style="cursor:pointer;" value="Manage Teams"  /></a></td></tr>
			</table>
		</section>
		<br/>
		<section style="width:100%;float:left;">
			<table>
			  <thead>
				<tr>
				  <th>Match</th>
				  <th>Points</th>
				  <th>End Date Time</th>
				  <th>Manage Questions</th>
				  <th>Edit</th>
				  <th>Delete</th>
				</tr>
			  </thead>
			  <tbody>
				<?php
					$sql = "SELECT id, (SELECT name from teams where id = m.team1) as team1 ,
									   (SELECT name from teams where id = m.team2) as team2 ,
									   end_datetime,
									   team1_points , team2_points, draw_points, team1 as team1_id, team2 as team2_id, is_played, winner
							FROM matches m WHERE competition_id = $competitionId ORDER BY id DESC";
					$counter=0;
					foreach ($db_handler->query($sql) as $row) {
						if( ($counter % 2) == 0 ){
							echo "<tr class='light'>";
						}
						else{
							echo "<tr class='dark'>";
						}
						
						echo "<td>".$row["team1"]." VS ".$row["team2"]."</td>";
						if($row["is_played"] != "Y"){
							echo "<td><a href='endMatch.php?id=".$row["id"]."&winner=".$row["team1_id"]."&competitionId=$competitionId'>".$row["team1"]." wins</a> ".$row["team1_points"]." points<br/><a href='endMatch.php?id=".$row["id"]."&winner=".$row["team2_id"]."&competitionId=$competitionId'>".$row["team2"]." wins</a> ".$row["team2_points"]." points<br/><a href='endMatch.php?id=".$row["id"]."&winner=0&competitionId=$competitionId'>Draw</a> ".$row["draw_points"]." points<br/></td>";
						}
						else{
							if($row["winner"] == $row["team1_id"]){
								echo "<td><a span style='color:green;font-weight:bold' href='endMatch.php?id=".$row["id"]."&winner=".$row["team1_id"]."&competitionId=$competitionId'>".$row["team1"]." wins</a> ".$row["team1_points"]." points<br/><a href='endMatch.php?id=".$row["id"]."&winner=".$row["team2_id"]."&competitionId=$competitionId'>".$row["team2"]." wins</a> ".$row["team2_points"]." points<br/><a href='endMatch.php?id=".$row["id"]."&winner=0&competitionId=$competitionId'>Draw</a> ".$row["draw_points"]." points<br/></td>";
								//echo "<td><span style='color:green;font-weight:bold'>".$row["team1"]." wins</span> ".$row["team1_points"]." points<br/>".$row["team2"]." wins ".$row["team2_points"]." points<br/>Draw ".$row["draw_points"]." points<br/></td>";
							}
							else if ($row["winner"] == $row["team2_id"]){
								echo "<td><a href='endMatch.php?id=".$row["id"]."&winner=".$row["team1_id"]."&competitionId=$competitionId'>".$row["team1"]." wins</a> ".$row["team1_points"]." points<br/><a style='color:green;font-weight:bold' href='endMatch.php?id=".$row["id"]."&winner=".$row["team2_id"]."&competitionId=$competitionId'>".$row["team2"]." wins</a> ".$row["team2_points"]." points<br/><a href='endMatch.php?id=".$row["id"]."&winner=0&competitionId=$competitionId'>Draw</a> ".$row["draw_points"]." points<br/></td>";
								//echo "<td>".$row["team1"]." wins ".$row["team1_points"]." points<br/><span style='color:green;font-weight:bold'>".$row["team2"]." wins</span> ".$row["team2_points"]." points<br/>Draw ".$row["draw_points"]." points<br/></td>";
							}
							else{
								echo "<td><a href='endMatch.php?id=".$row["id"]."&winner=".$row["team1_id"]."&competitionId=$competitionId'>".$row["team1"]." wins</a> ".$row["team1_points"]." points<br/><a href='endMatch.php?id=".$row["id"]."&winner=".$row["team2_id"]."&competitionId=$competitionId'>".$row["team2"]." wins</a> ".$row["team2_points"]." points<br/><a style='color:green;font-weight:bold' href='endMatch.php?id=".$row["id"]."&winner=0&competitionId=$competitionId'>Draw</a> ".$row["draw_points"]." points<br/></td>";
								//echo "<td>".$row["team1"]." wins ".$row["team1_points"]." points<br/>".$row["team2"]." wins ".$row["team2_points"]." points<br/><span style='color:green;font-weight:bold'>Draw</span> ".$row["draw_points"]." points<br/></td>";
							}
						}
						echo "<td>".$row["end_datetime"]."</td>";
						echo "<td><a href='manageMatchQuestions.php?id=".$row["id"]."'>Manage Questions</a></td>";		
						echo "<td align='center'><a title='Edit' href='manageCompetitionMatches.php?action=edit&id=$competitionId&matchId=".$row["id"]."'><img src='images/edit.png' /></a></td>";
						echo '<td align="center"><a title="Delete" href="deleteCompetitionMatch.php?id='.$row["id"].'&competitionId='.$competitionId.'" onclick="return confirm(\'Are you sure you want to delete this match?\');"><img src="images/delete.png" /></a></td>';						
						echo "</tr>";
						$counter = $counter + 1;
					}
					if($counter==0){
						echo "<tr class='light'><td colspan='6' align='center'>No data found</td></tr>";
					}
				?>
			  </tbody>
			</table>
		<section>
    </div>
    <!-- / content body -->
  </div>
</div>
<!-- Copyright -->
<div class="wrapper row5">
  <footer id="copyright" class="clear">
    <p class="fl_left">Copyright &copy; 2014 - All Rights Reserved - <a href="home.php">livedule.com</a></p>
  </footer>
</div>
<script>
	function getParameterByName(name) {
		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
			results = regex.exec(location.search);
		return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
	
	if(getParameterByName("action")=="found"){
		alert("team name already exists for this competition");
	}
	
	function validateAddMatch(){
		if(document.getElementById("team1Points").value==""){
			alert("please enter team1 points");
			document.getElementById("team1Points").focus();
		}
		else if(document.getElementById("team2Points").value==""){
			alert("please enter team2 points");
			document.getElementById("team2Points").focus();
		}
		else if(document.getElementById("drawPoints").value==""){
			alert("please enter draw points");
			document.getElementById("drawPoints").focus();
		}
		else if(!validateNumber(document.getElementById("team1Points").value)){
			alert("invalid value for team1 points");
			document.getElementById("team1Points").focus();
		}
		else if(!validateNumber(document.getElementById("team2Points").value)){
			alert("invalid value for team2 points");
			document.getElementById("team2Points").focus();
		}
		else if(!validateNumber(document.getElementById("drawPoints").value)){
			alert("invalid value for draw points");
			document.getElementById("drawPoints").focus();
		}
		else if(document.getElementById("endDate").value == ""){
			alert("please enter end date");
			document.getElementById("endDate").focus();
		}
		else if(!validateDate(document.getElementById("endDate").value)){
			alert("please enter end date in valid format mm/dd/yyyy");
			document.getElementById("endDate").focus();
		}
		else if(document.getElementById("endTime").value == ""){
			alert("please enter end time");
			document.getElementById("endTime").focus();
		}
		else if(!validateTime(document.getElementById("endTime").value)){
			alert("please enter end time in valid format hh:mm");
			document.getElementById("endTime").focus();
		}
		else if(document.getElementById("team1").value==document.getElementById("team2").value){
			alert("team1 and team2 can't be the same team");
			document.getElementById("team1").focus();
		}
		else{
			return true;
		}
		return false;
	}
	
	function endsWith(str, suffix) {
		return str.toLowerCase().indexOf(suffix, str.length - suffix.length) !== -1;
	}
	
	function validateNumber(numb) {  
	  var re = /^[0-9]+$/;  
	  return re.test(numb);	
	} 
	
	
	function validateDate(dateStr){
		var validformat=/^\d{2}\/\d{2}\/\d{4}$/ //Basic check for format validity
		var returnval=false
		if (!validformat.test(dateStr)){
			returnval=false
		}
		else{ //Detailed check for valid date ranges
			var monthfield=dateStr.split("/")[0]
			var dayfield=dateStr.split("/")[1]
			var yearfield=dateStr.split("/")[2]
			var dayobj = new Date(yearfield, monthfield-1, dayfield)
			if ((dayobj.getMonth()+1!=monthfield)||(dayobj.getDate()!=dayfield)||(dayobj.getFullYear()!=yearfield))
				returnval=false
			else
				returnval=true
		}
		return returnval
	}
	function validateTime(timeStr) {
		// Checks if time is in HH:MM format.
		var pattern = /^\d{1,2}:\d{2}$/;
		var matchArray1 = timeStr.match(pattern);
		var matchArray = timeStr.split(":");
		if (matchArray1 == null){
			return false;
		}
		hour = matchArray[0];
		minute = matchArray[1];
		if (hour < 0 || hour > 23){
			return false;
		}
		if (minute<0 || minute > 59){
			return false;
		}
		return true; 
	}
	</script>
</body>
</html>