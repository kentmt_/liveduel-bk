<?php

require_once '../include/DbHandler.php';
require_once '../include/PassHash.php';
require '.././libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

// User id from db - Global Variable
$user_id = NULL;

/**
 * Adding Middle Layer to authenticate every request
 * Checking if the request has valid api key in the 'Authorization' header
 */
function authenticate(\Slim\Route $route) {
    // Getting request headers
    $out = '';
    if (!function_exists('apache_request_headers')) { 
            foreach($_SERVER as $key=>$value) { 
                if (substr($key,0,5)=="HTTP_") { 
                    $key=str_replace(" ","-",ucwords(strtolower(str_replace("_"," ",substr($key,5))))); 
                    $out[$key]=$value; 
                }else{ 
                    $out[$key]=$value; 
                } 
            } 
            $headers = $out;
    } 
    else{
            $headers = apache_request_headers();
    }
     
    $response = array();
    $app = \Slim\Slim::getInstance();
    $api_key = $app->request->post('api_key');
    if(!isset($api_key)){
        $api_key = $app->request->get('api_key');
    }
    // var_dump($api_key);
    // die;
    // Verifying Authorization Header
    if (isset($api_key)) {
        $db = new DbHandler();

        // get the api key
        // $api_key = $api_key;
        // validating api key
        if (!$db->isValidApiKey($api_key)) {
            // api key is not present in users table
            $response["error"] = true;
            $response["message"] = "Access Denied. Invalid Api key";
            echoRespnse(401, $response);
            $app->stop();
        } else {
            global $user_id;
            // get user primary key id
            $user_id = $db->getUserId($api_key);
        }
    } else {
        // api key is missing in header
        $response["error"] = true;
        $response["message"] = $headers;
        echoRespnse(400, $response);
        $app->stop();
    }
}
$app->get('/test', function() {
           echo 'aaa';
        });
/**
 * ----------- METHODS WITHOUT AUTHENTICATION ---------------------------------
 */
/**
 * User Registration
 * url - /register
 * method - POST
 * params - name, email, password
 */
$app->post('/register', function() use ($app) {
            // check for required params
            verifyRequiredParams(array('fullname', 'email', 'password','country','country_champion'));
            $letters = 'abcefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
            $imagename =  substr(str_shuffle($letters), 0, 20);
            $response = array();
            $testfile = '';
            $file_path = "images/";
            if(isset($_FILES['file']['name'])){
                // $testfile = basename( $_FILES['file']['name']); 
                $file_path = $file_path . $imagename . basename( $_FILES['file']['name']);
                if(move_uploaded_file($_FILES['file']['tmp_name'], $file_path)) {
                    $avatar = 'www.liveduel.com/bitting/api/v1/'.$file_path;
                } else{
                    $avatar ='www.liveduel.com/bitting/api/img/user-placeholder.jpg';
                }  
            }
            else{
                $avatar = 'www.liveduel.com/bitting/api/img/user-placeholder.jpg';
            }
            // reading post params
            $name = $app->request->post('fullname');
            $email = $app->request->post('email');
            $password = $app->request->post('password');
            $country = $app->request->post('country');
            // $avatar = $app->request->post('avatar');
            $country_champion = $app->request->post('country_champion');

            // validating email address
            validateEmail($email);

            $db = new DbHandler();
            $res = $db->createUser($name, $email, $password,$country,$avatar,$country_champion);

            if ($res == USER_CREATED_SUCCESSFULLY) {
                $response["error"] = false;
                $response["message"] = "You are successfully registered";
            } else if ($res == USER_CREATE_FAILED) {
                $response["error"] = true;
                $response["message"] = "Oops! An error occurred while registereing";
            } else if ($res == USER_ALREADY_EXISTED) {
                $response["error"] = true;
                $response["message"] = "Sorry, this email already existed";
            }
            // echo json response
            echoRespnse(201, $response);
        });

/**
 * User Login
 * url - /login
 * method - POST
 * params - email, password
 */
$app->post('/login', function() use ($app) {
            // check for required params
            verifyRequiredParams(array('email', 'password'));

            // reading post params
            $email = $app->request()->post('email');
            $password = $app->request()->post('password');
            $response = array();

            $db = new DbHandler();
            // check for correct email and password
            if ($db->checkLogin($email, $password)===TRUE) {
                // get the user by email
                $user = $db->getUserByEmail($email);

                if ($user != NULL) {
                    $response["error"] = false;
                    $response['name'] = $user['user_name'];
                    $response['email'] = $user['email'];
                    $response['apiKey'] = $user['api_key'];
                    $response['createdAt'] = $user['created_at'];
                    $response['avatar'] = $user["avatar"];
                    $response['country'] = $user["country"];
                    $response['country_champion'] = $user["country_champion"];
                    $response["quizzestaken"] = $user["quizzestaken"];
                    $response["totaltime"] = $user["totaltime"];
                    $response["totalanswers"] = $user["totalanswers"];
                    $response["correctanswers"] = $user["correctanswers"];
                    $response["incorrectanswers"] = $user["incorrectanswers"];
                    $response["totalscore"] = $user["totalscore"];
                } else {
                    // unknown error occurred
                    $response['error'] = true;
                    $response['message'] = "An error occurred. Please try again";
                }
            } else {
                // user credentials are wrong
                $response['error'] = true;
                $response['message'] = 'Login failed. Incorrect credentials';
                $response['code'] = $db->checkLogin($email, $password) ;
            }

            echoRespnse(200, $response);
        });

/**
 * User Forgotpassword
 * url - /forgotpassword
 * method - POST
 * params - email
 */
$app->post('/forgotpassword', function() use ($app) {
            // check for required params
            verifyRequiredParams(array('email'));

            $response = array();

            // reading post params
            $email = $app->request->post('email');
        
            $db = new DbHandler();

            $res = $db->checkemailexits($email);
            $updatepassword = $db->updatepassword('anckdenckske12as',$email);
            if($res && $updatepassword){
                $to = $email;
                $subject = 'ForgotPassword LiveDuel.com';
                $message = 'Hello, New your password: anckdenckske12as ';
                $headers = 'From: support@liveduel.com' . "\r\n" .
                    'Reply-To: support@liveduel.com' . "\r\n" .
                    'X-Mailer: PHP/' . phpversion();

                mail($to, $subject, $message, $headers);
                $response['error'] = false;
                $response['message'] = 'successfully'; 
            }
            else{
                $response['error'] = true;
                $response['message'] = 'Email not Exists'; 
            }
            // echo json response
            echoRespnse(201, $response);
        });

$app->get('/achievement','authenticate', function() use ($app) {
            // check for required params
            // reading post params
            global $user_id;
            $response = array();
            $db = new DbHandler();

            $result = $db->getallachievements($user_id);
            $resultachievement = $db->getachievement();
            $response["error"] = false;
            $response["achievement"] = array();
            $listid = array();
            $checkduplicate = true;
            while ($achievement = $result->fetch_assoc()) {
                $tmp = array();
                $tmp["id"] = $achievement["achievement_id"];
                $tmp["progess"] = $achievement["progess"];
                $tmp["awarded"] = $achievement["awarded"];
                $tmp["name"] = $achievement["name"];
                array_push($listid,$tmp["id"]);
                // $tmp["quizzestaken"] = $achievement["quizzestaken"];
                // $tmp["totaltime"] = $achievement["totaltime"];
                // $tmp["totalanswers"] = $achievement["totalanswers"];
                // $tmp["correctanswers"] = $achievement["correctanswers"];
                // $tmp["incorrectanswers"] = $achievement["incorrectanswers"];
                // $tmp["totalscore"] = $achievement["totalscore"];
                array_push($response["achievement"], $tmp);
            }
            while ($achievement = $resultachievement->fetch_assoc()) {
                $tmp = array();
                $checkduplicate = true;
                foreach ($listid as $value) { 
                    if($achievement["id"]==$value){
                        $checkduplicate = false;
                    }
                }
                if($checkduplicate){
                    $tmp["id"] = $achievement["id"];
                    $tmp["progess"] = $achievement["progess"];
                    $tmp["awarded"] = $achievement["awarded"];
                    $tmp["name"] = $achievement["name"];
                    // $tmp["quizzestaken"] = $achievement["quizzestaken"];
                    // $tmp["totaltime"] = $achievement["totaltime"];
                    // $tmp["totalanswers"] = $achievement["totalanswers"];
                    // $tmp["correctanswers"] = $achievement["correctanswers"];
                    // $tmp["incorrectanswers"] = $achievement["incorrectanswers"];
                    // $tmp["totalscore"] = $achievement["totalscore"];
                    array_push($response["achievement"], $tmp);
                }
            }
            echoRespnse(200, $response);
        });

$app->post('/achievement', 'authenticate', function() use ($app) {

            global $user_id;            
            $handle = fopen('php://input','r');
            $jsonInput = fgets($handle);
            // Decoding JSON into an Array
            $decoded = json_decode($jsonInput,true);
            $db = new DbHandler();
            $response = array(); 
            foreach ($decoded as $key => $value) {
                $id = $db->getidachivement($key);
                $progess = $value["progress"];
                $awarded = $value["awarded"];
                $response["progess"] = $awarded;
                if($awarded){
                    $awarded="YES";
                }
                else{
                    $awarded="NO";
                }
                if(!$db->checkifidachivementexits($id,$user_id)){
                    $name = $db->createUserAchievement($user_id, $id , $progess ,$awarded);
                }
                else{
                    $name = $db->UpdateUserAchievement($user_id, $id , $progess ,$awarded);
                }
            }
            $response["message"] = 'Save successfully';
            $response["error"] = false;
            // $response["message"] = $array;
            // if(isset($decoded))
            //     // array_push($response["message"], $decoded);
            // else{
            //     $response["message"] ='faild';
            // }
            echoRespnse(200, $response);

            // creating new task
            // $achievement_user_id = $db->createUserAchievement($user_id, $achievement);

            // if ($achievement_user_id != NULL) {
            //     $response["error"] = false;
            //     $response["message"] = "Save successfully";
            //     $response["achievement_id"] = $achievement_user_id;
            //     echoRespnse(201, $response);
            // } else {
            //     $response["error"] = true;
            //     $response["message"] = "Failed to save achievement. Please try again";
            //     echoRespnse(200, $response);
            // }            
        });
$app->post('/updateimageprofile', 'authenticate', function() use ($app) {
            // check for required params
            $letters = 'abcefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
            $imagename =  substr(str_shuffle($letters), 0, 20);
            $response = array();
            $file_path = "images/";
            $avatar='';
            if(isset($_FILES['file']['name'])){
                // $testfile = basename( $_FILES['file']['name']); 
                $file_path = $file_path . $imagename . basename( $_FILES['file']['name']);
                if(move_uploaded_file($_FILES['file']['tmp_name'], $file_path)) {
                    $avatar = 'www.liveduel.com/bitting/api/v1/'.$file_path;
                } else{
                    $avatar ='www.liveduel.com/bitting/api/img/user-placeholder.jpg';
                }  
            }
            else{
                $avatar = 'www.liveduel.com/bitting/api/img/user-placeholder.jpg';
            }
            global $user_id;
            $db = new DbHandler();
            $result = $db->updateimage($user_id, $avatar);
            if ($result) {
                $response["error"] = false;
                $response["message"] = "Image updated successfully";
            } else {
                $response["error"] = true;
                $response["message"] = "Image to update. Please try again!";
            }
            echoRespnse(200, $response);   
        });
$app->post('/updateprofile', 'authenticate', function() use ($app) {
            // check for required params
            $name = $app->request->post('name');
            $password = $app->request->post('password');
            $newpassword = $app->request->post('newpassword');
            $country = $app->request->post('country');
            $country_champion = $app->request->post('country_champion');
            $response = array();
            global $user_id;
            $db = new DbHandler();
            if(isset($password) && isset($newpassword))
                if ($db->checkpass($user_id, $password)===TRUE) {
                    $result = $db->updatepassprofile($user_id,$name, $country,$country_champion,$newpassword);
                    $response["error"] = false;
                    $response["message"] = "Profile updated successfully";
                }
                else{
                    $response["error"] = true;
                    $response["message"] = "Profile  fail to update. Please try again!";
                }
            else{
                $result = $db->updateprofile($user_id,$name, $country,$country_champion);
                if ($result) {
                    $response["error"] = false;
                    $response["message"] = "Profile updated successfully";
                } else {
                    $response["error"] = true;
                    $response["message"] = "Profile  fail to update. Please try again!";
                }
            }
            echoRespnse(200, $response);   
        });
$app->post('/updatetotal', 'authenticate', function() use ($app) {
            // check for required params
            global $user_id;            
            $quizzestaken = $app->request->post('quizzestaken');
            $totaltime = $app->request->post('totaltime');
            $totalanswers = $app->request->post('totalanswers');
            $correctanswers = $app->request->post('correctanswers');
            $incorrectanswers = $app->request->post('incorrectanswers');
            $totalscore = $app->request->post('totalscore');

            $db = new DbHandler();
            $response = array();

            // updating task
            $result = $db->updateTotal($user_id,$quizzestaken,$totaltime,$totalanswers,$correctanswers,$incorrectanswers,$totalscore );
            if ($result) {
                // task updated successfully
                $response["error"] = false;
                $response["message"] = "Total updated successfully";
            } else {
                // task failed to update
                $response["error"] = true;
                $response["message"] = "Total failed to update. Please try again!";
            }
            echoRespnse(200, $response);
 
        });

$app->get('/getallusers','authenticate', function() use ($app) {
            // check for required params
            // reading post params
            global $user_id;
            $response = array();
            $db = new DbHandler();

            $result = $db->getalluser();
            $response["error"] = false;
            $response["users"] = array();
            $listid = array();
            while ($users = $result->fetch_assoc()) {
                $tmp = array();
                $tmp['id'] = $users['id'];
                $tmp['name'] = $users['user_name'];
                $tmp['email'] = $users['email'];
                // $tmp['apiKey'] = $user['api_key'];
                $tmp['createdAt'] = $users['created_at'];
                $tmp['avatar'] = $users["avatar"];
                $tmp['country'] = $users["country"];
                $tmp['country_champion'] = $users["country_champion"];
                $tmp["quizzestaken"] = $users["quizzestaken"];
                $tmp["totaltime"] = $users["totaltime"];
                $tmp["totalanswers"] = $users["totalanswers"];
                $tmp["correctanswers"] = $users["correctanswers"];
                $tmp["incorrectanswers"] = $users["incorrectanswers"];
                $tmp["totalscore"] = $users["totalscore"];
                array_push($response["users"], $tmp);
            }
            echoRespnse(200, $response);
        });
//api quiz
$app->get('/fixtures','authenticate', function() use ($app) {
            // check for required params
            // reading post params
            global $user_id;
            $response = array();
            $datearray = array();
            include('../../db_config.php');
            $sql = "SELECT id,bang ,city,stadium, (SELECT name from teams where id = m.team1) as team1 ,
                                       (SELECT name from teams where id = m.team2) as team2 ,
                                       end_datetime,end_date,
                                       team1_points , team2_points, draw_points, team1 as team1_id, team2 as team2_id, is_played, winner
                            FROM matches m WHERE competition_id = 4 ORDER BY end_datetime ASC";
            // $response["error"] = false;
            
            foreach ($db_handler->query($sql) as $row){
                // array_push($row['stadium'], $datearray);
                $check = true;
                foreach ($datearray as $value) {
                    if($row['end_date'] == $value){
                        $check = false;
                        break;
                    }

                }
                if($check)
                    $datearray[] = $row['end_date'];
                
            }
            // $response["datearray"] = $datearray;
            foreach ($datearray as $value) {
                $response[$value] = array();
                foreach ($db_handler->query($sql) as $row) {
                    if($value ==$row['end_date'] ){
                    $tmp = array();
                    $tmp['id'] = $row["id"];
                    $tmp['group'] = $row["bang"];
                    $tmp['city'] = $row["city"];
                    $tmp['stadium'] = $row["stadium"];
                    $tmp['team1'] = strtoupper($row["team1"]);
                    $tmp['team2'] = strtoupper($row["team2"]);
                    $tmp['end_datetime'] = $row['end_datetime'];
                    $tmp['end_date'] = $row['end_date'];
                    $tmp['team1_points'] = $row['team1_points'];
                    $tmp['team2_points'] = $row['team2_points'];
                    $tmp['draw_points'] = $row['draw_points'];
                    $tmp['subquestion'] = array();
                    $id_match = (int)$row["id"];
                    $questionarray = array();
                    $questionarray[] = 0;
                    $sql0 = "SELECT question_id FROM bets WHERE user_id = '$user_id'  ORDER BY id";
                    foreach ($db_handler->query($sql0) as $row) {
                        $questionarray[] = $row['question_id'];
                    }
                    $matches = implode(',', $questionarray);
                    $question = " SELECT * FROM (SELECT id, name FROM match_questions  WHERE is_answered = 'N' and match_id = $id_match and id NOT IN ( $matches ) LIMIT 3 UNION ALL SELECT id, name FROM match_questions  WHERE is_answered = 'Y' and match_id = $id_match  and id NOT IN ( $matches ) LIMIT 7) a ORDER BY RAND() ";
                    foreach ($db_handler->query($question) as $row) {
                        $tmp1 = array();
                        $tmp1["name"] = $row["name"];
                        $tmp1["id"] = $row["id"];
                        $tmp1["answer"] = array();
                        $id_question = (int)$row["id"];
                        $answer = "SELECT id, answer ,question_id,is_correct,points FROM match_answers  WHERE question_id = $id_question ORDER BY id DESC";
                        foreach ($db_handler->query($answer) as $row) {
                            $tmp2 = array();
                            $tmp2["id"] = $row["id"];
                            $tmp2["answer"] = $row["answer"];
                            $tmp2["question_id"] = $row["question_id"];
                            $tmp2["is_correct"] = $row["is_correct"];
                            $tmp2["points"] = $row["points"];
                            array_push($tmp1["answer"], $tmp2);
                        }
                        array_push($tmp["subquestion"], $tmp1);
                    }
                    array_push($response[$value], $tmp);
                    }
                }
            }
            echoRespnse(200, $response);
        });

$app->get('/history','authenticate', function() use ($app) {
            // check for required params
            // reading post params
            global $user_id;
            $response = array();
            $datearray = array();
            $questionarray = array();
            $questionarray[] = 0;
            include('../../db_config.php');
            $sql0 = "SELECT question_id FROM competition_bets WHERE user_id = '$user_id'  ORDER BY id";
            foreach ($db_handler->query($sql0) as $row) {
                $questionarray[] = $row['question_id'];
            }
            $matches = implode(',', $questionarray);
            // $response['array'] = $matches;
            $sql = "SELECT id, name, is_answered FROM questions WHERE is_answered='Y' and competition_id = 4 and category='history' and id NOT IN ( $matches ) ORDER BY RAND() LIMIT 7";

            $response["question"] = array();
            foreach ($db_handler->query($sql) as $row) {
                $tmp = array();
                $tmp["id"] = $row["id"];
                $tmp["name"] = $row["name"];
                $id_question = (int)$row["id"];
                $tmp["answers"] = array();
                $sql1 = "SELECT id, is_correct, answer ,points FROM answers WHERE question_id = $id_question  ORDER BY id";
                foreach ($db_handler->query($sql1) as $row) {
                    $tmp1 = array();
                    $tmp1["id"] = $row["id"];
                    $tmp1["answer"] = $row["answer"];
                    $tmp1["is_correct"] = $row["is_correct"];
                    $tmp1["points"] = $row["points"];
                    array_push($tmp["answers"], $tmp1);
                }
                array_push($response["question"], $tmp);
            }
            echoRespnse(200, $response);
        });
$app->get('/predictions','authenticate' ,function() use ($app) {
            // check for required params
            // reading post params
            global $user_id;
            $response = array();
            $datearray = array();
            $questionarray = array();
            $questionarray[] = 0;
            include('../../db_config.php');
            $sql0 = "SELECT question_id FROM competition_bets WHERE user_id = '$user_id'  ORDER BY id";
            foreach ($db_handler->query($sql0) as $row) {
                $questionarray[] = $row['question_id'];
            }
            $matches = implode(',', $questionarray);
            $sql = "SELECT id, name, is_answered FROM questions WHERE is_answered='N' and competition_id = 4 and category='predictions' and id NOT IN ( $matches ) ORDER BY RAND() LIMIT 7";

            $response["question"] = array();
            foreach ($db_handler->query($sql) as $row) {
                $tmp = array();
                $tmp["id"] = $row["id"];
                $tmp["name"] = $row["name"];
                $id_question = (int)$row["id"];
                $tmp["answers"] = array();
                $sql1 = "SELECT id, is_correct, answer ,points FROM answers WHERE question_id = $id_question  ORDER BY id";
                foreach ($db_handler->query($sql1) as $row) {
                    $tmp1 = array();
                    $tmp1["id"] = $row["id"];
                    $tmp1["answer"] = $row["answer"];
                    // $tmp1["is_correct"] = $row["is_correct"];
                    $tmp1["points"] = $row["points"];
                    array_push($tmp["answers"], $tmp1);
                }
                array_push($response["question"], $tmp);
            }
            echoRespnse(200, $response);
        });
$app->get('/funfacts','authenticate', function() use ($app) {
            // check for required params
            // reading post params
            global $user_id;
            $response = array();
            $datearray = array();
            $questionarray = array();
            $questionarray[] = 0;
            include('../../db_config.php');
            $sql0 = "SELECT question_id FROM competition_bets WHERE user_id = '$user_id'  ORDER BY id";
            foreach ($db_handler->query($sql0) as $row) {
                $questionarray[] = $row['question_id'];
            }
            $matches = implode(',', $questionarray);
            $sql = "SELECT id, name, is_answered FROM questions WHERE is_answered='Y' and competition_id = 4 and category='funfacts' and id NOT IN ( $matches ) ORDER BY RAND() LIMIT 7";

            $response["question"] = array();
            foreach ($db_handler->query($sql) as $row) {
                $tmp = array();
                $tmp["id"] = $row["id"];
                $tmp["name"] = $row["name"];
                $id_question = (int)$row["id"];
                $tmp["answers"] = array();
                $sql1 = "SELECT id, is_correct, answer ,points FROM answers WHERE question_id = $id_question  ORDER BY id";
                foreach ($db_handler->query($sql1) as $row) {
                    $tmp1 = array();
                    $tmp1["id"] = $row["id"];
                    $tmp1["answer"] = $row["answer"];
                    $tmp1["is_correct"] = $row["is_correct"];
                    $tmp1["points"] = $row["points"];
                    array_push($tmp["answers"], $tmp1);
                }
                array_push($response["question"], $tmp);
            }
            echoRespnse(200, $response);
        });
$app->get('/resultpredic','authenticate', function() use ($app) {
            // check for required params
            // reading post params
            global $user_id;
            $score = 0;
            $correct = 0;
            $is_correct =0;
            $response = array();
            $datearray = array();
            include('../../db_config.php');
            $sql = "SELECT id, name, is_answered FROM questions WHERE competition_id = 4 and category='predictions' and is_answered='Y' ORDER BY id";

            // $response["question"] = array();
            foreach ($db_handler->query($sql) as $row) {
                $id_question =  $row['id'] ;
                $sql1 = "SELECT id, is_correct, points,question_id  FROM competition_bets WHERE question_id = $id_question and status = 1 and user_id = $user_id ORDER BY id";
                foreach ($db_handler->query($sql1) as $row1) {
                    if($row1['is_correct']=='Y'){
                        $correct++ ;
                        $score+=$row1['points'];
                    }
                    else
                        $is_correct++;
                }
                $st = $db_handler->prepare("UPDATE competition_bets SET status = 0 WHERE  question_id = $id_question and status = 1 and user_id = $user_id ");
                $st->execute();
            }
            $sql = "SELECT id, name, is_answered FROM match_questions WHERE  is_answered='Y' ORDER BY id";

            // $response["question"] = array();
            foreach ($db_handler->query($sql) as $row) {
                $id_question =  $row['id'] ;
                $sql1 = "SELECT id, is_correct, points,question_id  FROM bets WHERE question_id = $id_question and status = 1 and user_id = $user_id ORDER BY id";
                foreach ($db_handler->query($sql1) as $row1) {
                    if($row1['is_correct']=='Y'){
                        $correct++ ;
                        $score+=$row1['points'];
                    }
                    else
                        $is_correct++;
                }
                $st = $db_handler->prepare("UPDATE bets SET status = 0 WHERE  question_id = $id_question and status = 1 and user_id = $user_id ");
                $st->execute();
            }
            if($correct > 0 || $is_correct >0)
                $response['error'] = false;
            else
                $response['error'] = true;
            $response['score'] = $score;
            $response['correct'] = $correct;
            $response['is_correct'] = $is_correct;
            echoRespnse(200, $response);
        });
$app->get('/pictures','authenticate', function() use ($app) {
            // check for required params
            // reading post params
            global $user_id;
            $response = array();
            $datearray = array();
            $questionarray = array();
            $questionarray[] = 0;
            include('../../db_config.php');
            $sql0 = "SELECT question_id FROM competition_bets WHERE user_id = '$user_id'  ORDER BY id";
            foreach ($db_handler->query($sql0) as $row) {
                $questionarray[] = $row['question_id'];
            }
            $matches = implode(',', $questionarray);
            $sql = "SELECT id, name, is_answered,image FROM questions WHERE is_answered='Y' and competition_id = 4 and category='pictures' and id NOT IN ( $matches ) ORDER BY RAND() LIMIT 7";

            $response["question"] = array();
            foreach ($db_handler->query($sql) as $row) {
                $tmp = array();
                $tmp["id"] = $row["id"];
                $tmp["name"] = $row["name"];
                $tmp["image"] = $row["image"];
                $id_question = (int)$row["id"];
                $tmp["answers"] = array();
                $sql1 = "SELECT id, is_correct, answer ,points FROM answers WHERE question_id = $id_question  ORDER BY id";
                foreach ($db_handler->query($sql1) as $row) {
                    $tmp1 = array();
                    $tmp1["id"] = $row["id"];
                    $tmp1["answer"] = $row["answer"];
                    $tmp1["is_correct"] = $row["is_correct"];
                    $tmp1["points"] = $row["points"];
                    array_push($tmp["answers"], $tmp1);
                }
                array_push($response["question"], $tmp);
            }
            echoRespnse(200, $response);
        });
//api quiz

$app->post('/invite', 'authenticate', function() use ($app) {
            // check for required params
            global $user_id;            
            $email = $app->request->post('email');
            $db = new DbHandler();
            $response = array();
            $user = $db->getUserById($user_id);
            $to = $email;
            $subject = 'Invitation to LiveDuel.com';
            $message = 'Hello, this is an invitation from '.$user["email"].' to register to www.liveduel.com';
            $headers = 'From: support@liveduel.com' . "\r\n" .
            'Reply-To: support@liveduel.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
            mail($to, $subject, $message, $headers);
            
            $response["error"] = false;
            $response["message"] = "Invite successfully";
            echoRespnse(200, $response);
 
        });

$app->post('/sendmailtoplayer', 'authenticate', function() use ($app) {
            // check for required params
            global $user_id;            
            $email = $app->request->post('email');
            $message = $app->request->post('message');
            $db = new DbHandler();
            $response = array();
            $user = $db->getUserById($user_id);
            $to = $email;
            $subject = 'Message from'.' '.$user["email"];
            $message = $message;
            $headers = 'From: support@liveduel.com' . "\r\n" .
            'Reply-To: support@liveduel.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
            mail($to, $subject, $message, $headers);
            
            $response["error"] = false;
            $response["message"] = "Invite successfully";
            echoRespnse(200, $response);
 
        });

$app->get('/user', 'authenticate', function() use ($app) {
            // check for required params
            global $user_id;            
            $email = $app->request->get('email');
            $db = new DbHandler();
            $response = array();
                // get the user by email
            $user = $db->getUserByEmail($email);

            if ($user != NULL) {
                $response["error"] = false;
                $response['name'] = $user['user_name'];
                $response['email'] = $user['email'];
                $response['apiKey'] = $user['api_key'];
                $response['createdAt'] = $user['created_at'];
                $response['avatar'] = $user["avatar"];
                $response['country'] = $user["country"];
                $response['country_champion'] = $user["country_champion"];
                $response["quizzestaken"] = $user["quizzestaken"];
                $response["totaltime"] = $user["totaltime"];
                $response["totalanswers"] = $user["totalanswers"];
                $response["correctanswers"] = $user["correctanswers"];
                $response["incorrectanswers"] = $user["incorrectanswers"];
                $response["totalscore"] = $user["totalscore"];
            } else {
                // unknown error occurred
                $response['error'] = true;
                $response['message'] = "An error occurred. Please try again";
            }
            echoRespnse(200, $response);
 
        });
/*
 * ------------------------ METHODS WITH AUTHENTICATION ------------------------
 */

/**
 * Listing all tasks of particual user
 * method GET
 * url /tasks          
 */
$app->get('/tasks', 'authenticate', function() {
            global $user_id;
            $response = array();
            $db = new DbHandler();

            // fetching all user tasks
            $result = $db->getAllUserTasks($user_id);

            $response["error"] = false;
            $response["tasks"] = array();

            // looping through result and preparing tasks array
            while ($task = $result->fetch_assoc()) {
                $tmp = array();
                $tmp["id"] = $task["id"];
                $tmp["task"] = $task["task"];
                $tmp["status"] = $task["status"];
                $tmp["createdAt"] = $task["created_at"];
                array_push($response["tasks"], $tmp);
            }

            echoRespnse(200, $response);
        });

/**
 * Listing single task of particual user
 * method GET
 * url /tasks/:id
 * Will return 404 if the task doesn't belongs to user
 */
$app->get('/tasks/:id', 'authenticate', function($task_id) {
            global $user_id;
            $response = array();
            $db = new DbHandler();

            // fetch task
            $result = $db->getTask($task_id, $user_id);

            if ($result != NULL) {
                $response["error"] = false;
                $response["id"] = $result["id"];
                $response["task"] = $result["task"];
                $response["status"] = $result["status"];
                $response["createdAt"] = $result["created_at"];
                echoRespnse(200, $response);
            } else {
                $response["error"] = true;
                $response["message"] = "The requested resource doesn't exists";
                echoRespnse(404, $response);
            }
        });

/**
 * Creating new task in db
 * method POST
 * params - name
 * url - /matchesbets/
 */
$app->post('/update_competition', 'authenticate', function() use ($app) {
            // check for required params
            
            // verifyRequiredParams(array('matchesbets'));
            $handle = fopen('php://input','r');
            $jsonInput = fgets($handle);
            // Decoding JSON into an Array
            $decoded = json_decode($jsonInput,true);
            $matchesbets = $app->request->post('questions');
            $response = array();
            $response["error"] = false;
            // $response["message"] = $matchesbets;
            // $response["getquestion"] = $decoded;
            global $user_id;
            $db = new DbHandler();
            // $decoded = array(1,2,3,4);
            foreach ($decoded as $value) {
                // $response["testquetsion"] = $value;
                $matches_bets = $db->CreateMatchesBets($user_id, $value);
            }
            echoRespnse(200, $response);       
        });

$app->post('/update_answer_predictions', 'authenticate', function() use ($app) {

            $handle = fopen('php://input','r');
            $jsonInput = fgets($handle);
            // Decoding JSON into an Array
            $decoded = json_decode($jsonInput,true);
            // $matchesbets = $app->request->post('questions');
            $response = array();
            $response["error"] = false;
            // $response["message"] = $decoded;
            global $user_id;
            $db = new DbHandler();
            foreach ($decoded as $value) {
                $response["testquetsion"] = $value["answerID"];
                $response["getquestion"] = $value["questionID"];
                $matches_bets = $db->CreateAnswerPredictionBets($user_id, $value["questionID"],$value["answerID"]);
            }
            echoRespnse(200, $response);       
        });

$app->post('/save_answer_fixture', 'authenticate', function() use ($app) {

            $handle = fopen('php://input','r');
            $jsonInput = fgets($handle);
            // Decoding JSON into an Array
            $decoded = json_decode($jsonInput,true);
            // $matchesbets = $app->request->post('questions');
            $response = array();
            $response["error"] = false;
            // $response["message"] = $decoded;
            global $user_id;
            $db = new DbHandler();
            foreach ($decoded as $value) {
                // $response["testquetsion"] = $value["answerID"];
                // $response["getquestion"] = $value["questionID"];
                $matches_bets = $db->CreateAnswerMatchquestion($user_id, $value["questionID"],$value["answerID"]);
            }
            echoRespnse(200, $response);       
        });

$app->get('/result_fixture','authenticate', function() use ($app) {
            // check for required params
            // reading post params
            global $user_id;
            $score = 0;
            $correct = 0;
            $is_correct =0;
            $response = array();
            $datearray = array();
            include('../../db_config.php');
            $sql = "SELECT id, name, is_answered FROM match_questions WHERE  is_answered='Y' ORDER BY id";

            // $response["question"] = array();
            foreach ($db_handler->query($sql) as $row) {
                $id_question =  $row['id'] ;
                $sql1 = "SELECT id, is_correct, points,question_id  FROM bets WHERE question_id = $id_question and status = 1 and user_id = $user_id ORDER BY id";
                foreach ($db_handler->query($sql1) as $row1) {
                    if($row1['is_correct']=='Y'){
                        $correct++ ;
                        $score+=$row1['points'];
                    }
                    else
                        $is_correct++;
                }
            }
            if($correct > 0 || $is_correct >0)
                $response['error'] = false;
            else
                $response['error'] = true;
            $response['score'] = $score;
            $response['correct'] = $correct;
            $response['is_correct'] = $is_correct;
            echoRespnse(200, $response);
        });
/**
 * Creating new task in db
 * method POST
 * params - name
 * url - /tasks/
 */
$app->post('/tasks', 'authenticate', function() use ($app) {
            // check for required params
            verifyRequiredParams(array('task'));

            $response = array();
            $task = $app->request->post('task');

            global $user_id;
            $db = new DbHandler();

            // creating new task
            $task_id = $db->createTask($user_id, $task);

            if ($task_id != NULL) {
                $response["error"] = false;
                $response["message"] = "Task created successfully";
                $response["task_id"] = $task_id;
                echoRespnse(201, $response);
            } else {
                $response["error"] = true;
                $response["message"] = "Failed to create task. Please try again";
                echoRespnse(200, $response);
            }            
        });

/**
 * Updating existing task
 * method PUT
 * params task, status
 * url - /tasks/:id
 */
$app->put('/tasks/:id', 'authenticate', function($task_id) use($app) {
            // check for required params
            verifyRequiredParams(array('task', 'status'));

            global $user_id;            
            $task = $app->request->put('task');
            $status = $app->request->put('status');

            $db = new DbHandler();
            $response = array();

            // updating task
            $result = $db->updateTask($user_id, $task_id, $task, $status);
            if ($result) {
                // task updated successfully
                $response["error"] = false;
                $response["message"] = "Task updated successfully";
            } else {
                // task failed to update
                $response["error"] = true;
                $response["message"] = "Task failed to update. Please try again!";
            }
            echoRespnse(200, $response);
        });

/**
 * Deleting task. Users can delete only their tasks
 * method DELETE
 * url /tasks
 */
$app->delete('/tasks/:id', 'authenticate', function($task_id) use($app) {
            global $user_id;

            $db = new DbHandler();
            $response = array();
            $result = $db->deleteTask($user_id, $task_id);
            if ($result) {
                // task deleted successfully
                $response["error"] = false;
                $response["message"] = "Task deleted succesfully";
            } else {
                // task failed to delete
                $response["error"] = true;
                $response["message"] = "Task failed to delete. Please try again!";
            }
            echoRespnse(200, $response);
        });

/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        $response["code"]=substr($error_fields, 0, -2);
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

    echo json_encode($response);
}

$app->run();
?>