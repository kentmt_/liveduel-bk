<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Ravi Tamada
 * @link URL Tutorial link
 */
class DbHandler {

    private $conn;

    function __construct() {
        require_once dirname(__FILE__) . '/DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

    /* ------------- `users` table method ------------------ */

    /**
     * Creating new user
     * @param String $name User full name
     * @param String $email User login email id
     * @param String $password User login password
     */
    public function createUser($name, $email, $password,$country,$avatar,$country_champion) {
        require_once 'PassHash.php';
        $response = array();

        // First check if user already existed in db
        if (!$this->isUserExists($email)) {
            // Generating password hash
            $password_hash = PassHash::hash($password);

            // Generating API key
            $api_key = $this->generateApiKey();

            // insert query
            $stmt = $this->conn->prepare("INSERT INTO logins(user_name,email,password,api_key,country,avatar,country_champion,status) values(?, ?, ?, ?, ?, ?,?, 1)");
            $stmt->bind_param("sssssss", $name, $email, $password_hash, $api_key,$country,$avatar,$country_champion);

            $result = $stmt->execute();

            $stmt->close();

            // Check for successful insertion
            if ($result) {
                // User successfully inserted
                return USER_CREATED_SUCCESSFULLY;
            } else {
                // Failed to create user
                return USER_CREATE_FAILED;
            }
        } else {
            // User with same email already existed in the db
            return USER_ALREADY_EXISTED;
        }

        return $response;
    }

    /**
     * Checking user login
     * @param String $email User login email id
     * @param String $password User login password
     * @return boolean User login status success/fail
     */
    public function checkLogin($email, $password) {
        // fetching user by email
        $stmt = $this->conn->prepare("SELECT password FROM logins WHERE email = ?");

        $stmt->bind_param("s", $email);

        $stmt->execute();

        $stmt->bind_result($password_hash);

        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            // Found user with the email
            // Now verify the password

            $stmt->fetch();

            $stmt->close();

            if (PassHash::check_password($password_hash, $password)) {
                // User password is correct
                return TRUE;
            } else {
                // user password is incorrect
                return 'password';
            }
        } else {
            $stmt->close();

            // user not existed with the email
            return 'email';
        }
    }

    public function checkpass($user_id, $password) {
        // fetching user by email
        $stmt = $this->conn->prepare("SELECT password FROM logins WHERE id = ?");

        $stmt->bind_param("i", $user_id);

        $stmt->execute();

        $stmt->bind_result($password_hash);

        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            // Found user with the email
            // Now verify the password

            $stmt->fetch();

            $stmt->close();

            if (PassHash::check_password($password_hash, $password)) {
                // User password is correct
                return TRUE;
            } else {
                // user password is incorrect
                return 'password';
            }
        } else {
            $stmt->close();

            // user not existed with the email
            return 'email';
        }
    }
    /**
     * Checking for duplicate user by email address
     * @param String $email email to check in db
     * @return boolean
     */
    private function isUserExists($email) {
        $stmt = $this->conn->prepare("SELECT id from logins WHERE email = ?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }

    public function checkemailexits($email) {
        require_once 'PassHash.php';
        $stmt = $this->conn->prepare("SELECT id from logins WHERE email = ?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }

    public function updatepassword($password,$email) {
        $password_hash = PassHash::hash($password);
        $stmt = $this->conn->prepare("UPDATE logins SET password = ? WHERE email=?");
        $stmt->bind_param("ss", $password_hash,$email);
        $result = $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        if($result)
            return '1';
        else
            return '0';
    }
    /**
     * Fetching user by email
     * @param String $email User email id
     */
    public function getUserByEmail($email) {
        $stmt = $this->conn->prepare("SELECT user_name, email, api_key, status, created_at,avatar,country,country_champion,quizzestaken,totaltime,totalanswers,correctanswers,incorrectanswers,totalscore FROM logins WHERE email = ?");
        $stmt->bind_param("s", $email);
        if ($stmt->execute()) {
            // $user = $stmt->get_result()->fetch_assoc();
            $stmt->bind_result($name, $email, $api_key, $status, $created_at,$avatar,$country,$country_champion,$quizzestaken,$totaltime,$totalanswers,$correctanswers,$incorrectanswers,$totalscore);
            $stmt->fetch();
            $user = array();
            $user["user_name"] = $name;
            $user["email"] = $email;
            $user["api_key"] = $api_key;
            $user["status"] = $status;
            $user["created_at"] = $created_at;
            $user["avatar"] = $avatar;
            $user["country"] = $country;
            $user["country_champion"] = $country_champion;
            $user["quizzestaken"] = $quizzestaken;
            $user["totaltime"] = $totaltime;
            $user["totalanswers"] = $totalanswers;
            $user["correctanswers"] = $correctanswers;
            $user["incorrectanswers"] = $incorrectanswers;
            $user["totalscore"] = $totalscore;
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }

    public function getUserById($user_id) {
        $stmt = $this->conn->prepare("SELECT user_name, email, api_key, status, created_at,avatar,country,country_champion,quizzestaken,totaltime,totalanswers,correctanswers,incorrectanswers,totalscore FROM logins WHERE id = ?");
        $stmt->bind_param("i", $user_id);
        if ($stmt->execute()) {
            // $user = $stmt->get_result()->fetch_assoc();
            $stmt->bind_result($name, $email, $api_key, $status, $created_at,$avatar,$country,$country_champion,$quizzestaken,$totaltime,$totalanswers,$correctanswers,$incorrectanswers,$totalscore);
            $stmt->fetch();
            $user = array();
            $user["user_name"] = $name;
            $user["email"] = $email;
            $user["api_key"] = $api_key;
            $user["status"] = $status;
            $user["created_at"] = $created_at;
            $user["avatar"] = $avatar;
            $user["country"] = $country;
            $user["country_champion"] = $country_champion;
            $user["quizzestaken"] = $quizzestaken;
            $user["totaltime"] = $totaltime;
            $user["totalanswers"] = $totalanswers;
            $user["correctanswers"] = $correctanswers;
            $user["incorrectanswers"] = $incorrectanswers;
            $user["totalscore"] = $totalscore;
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }
    /**
     * Fetching user api key
     * @param String $user_id user id primary key in user table
     */
    public function getApiKeyById($user_id) {
        $stmt = $this->conn->prepare("SELECT api_key FROM logins WHERE id = ?");
        $stmt->bind_param("i", $user_id);
        if ($stmt->execute()) {
            // $api_key = $stmt->get_result()->fetch_assoc();
            // TODO
            $stmt->bind_result($api_key);
            $stmt->close();
            return $api_key;
        } else {
            return NULL;
        }
    }

     public function getidachivement($name) {
        $stmt = $this->conn->prepare("SELECT id FROM achievement WHERE name = ?");
        $stmt->bind_param("s", $name);
        if ($stmt->execute()) {
            // $api_key = $stmt->get_result()->fetch_assoc();
            // TODO
            $stmt->bind_result($id);
            $stmt->fetch();
            $stmt->close();
            return $id;
        } else {
            return NULL;
        }
    }

    public function checkifidachivementexits($id,$user_id) {
        $stmt = $this->conn->prepare("SELECT id from users_achievement WHERE achievement_id = ? and user_id = ?");
        $stmt->bind_param("ii", $id ,$user_id);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }
    /**
     * Fetching user id by api key
     * @param String $api_key user api key
     */
    public function getUserId($api_key) {
        $stmt = $this->conn->prepare("SELECT id FROM logins WHERE api_key = ?");
        $stmt->bind_param("s", $api_key);
        if ($stmt->execute()) {
            $stmt->bind_result($user_id);
            $stmt->fetch();
            // TODO
            // $user_id = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user_id;
        } else {
            return NULL;
        }
    }

    /**
     * Validating user api key
     * If the api key is there in db, it is a valid key
     * @param String $api_key user api key
     * @return boolean
     */
    public function isValidApiKey($api_key) {
        $stmt = $this->conn->prepare("SELECT id from logins WHERE api_key = ?");
        $stmt->bind_param("s", $api_key);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }

    /**
     * Generating random Unique MD5 String for user Api key
     */
    private function generateApiKey() {
        return md5(uniqid(rand(), true));
    }

    /* ------------- `tasks` table method ------------------ */

    /**
     * Creating new task
     * @param String $user_id user id to whom task belongs to
     * @param String $task task text
     */
    public function createTask($user_id, $task) {
        $stmt = $this->conn->prepare("INSERT INTO tasks(task) VALUES(?)");
        $stmt->bind_param("s", $task);
        $result = $stmt->execute();
        $stmt->close();

        if ($result) {
            // task row created
            // now assign the task to user
            $new_task_id = $this->conn->insert_id;
            $res = $this->createUserTask($user_id, $new_task_id);
            if ($res) {
                // task created successfully
                return $new_task_id;
            } else {
                // task failed to create
                return NULL;
            }
        } else {
            // task failed to create
            return NULL;
        }
    }

    /**
     * Fetching single task
     * @param String $task_id id of the task
     */
    public function getTask($task_id, $user_id) {
        $stmt = $this->conn->prepare("SELECT t.id, t.task, t.status, t.created_at from tasks t, user_tasks ut WHERE t.id = ? AND ut.task_id = t.id AND ut.user_id = ?");
        $stmt->bind_param("ii", $task_id, $user_id);
        if ($stmt->execute()) {
            $res = array();
            $stmt->bind_result($id, $task, $status, $created_at);
            // TODO
            // $task = $stmt->get_result()->fetch_assoc();
            $stmt->fetch();
            $res["id"] = $id;
            $res["task"] = $task;
            $res["status"] = $status;
            $res["created_at"] = $created_at;
            $stmt->close();
            return $res;
        } else {
            return NULL;
        }
    }
    public function getallachievements($user_id){
        $stmt = $this->conn->prepare("SELECT t.name,t.id,t.title,t.type,t.condition,t.description,ut.* FROM achievement t, users_achievement ut WHERE t.id = ut.achievement_id AND ut.user_id = ?");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $achievement = $stmt->get_result();
        $stmt->close();
        return $achievement;
    }

    public function getachievement(){
        $stmt = $this->conn->prepare("SELECT * FROM achievement");
        // $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $achievement = $stmt->get_result();
        $stmt->close();
        return $achievement;
    }
    /**
     * Fetching all user tasks
     * @param String $user_id id of the user
     */
    public function getAllUserTasks($user_id) {
        $stmt = $this->conn->prepare("SELECT t.* FROM tasks t, user_tasks ut WHERE t.id = ut.task_id AND ut.user_id = ?");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }
    public function getalluser(){
        $stmt = $this->conn->prepare("SELECT * FROM logins ORDER BY totalscore desc ");
        // $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $users = $stmt->get_result();
        $stmt->close();
        return $users;
    }
    /**
     * Updating task
     * @param String $task_id id of the task
     * @param String $task task text
     * @param String $status task status
     */
    public function updateTask($user_id, $task_id, $task, $status) {
        $stmt = $this->conn->prepare("UPDATE tasks t, user_tasks ut set t.task = ?, t.status = ? WHERE t.id = ? AND t.id = ut.task_id AND ut.user_id = ?");
        $stmt->bind_param("siii", $task, $status, $task_id, $user_id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }

    public function updateimage($user_id,$avatar){
        $stmt = $this->conn->prepare("UPDATE logins SET avatar = ? WHERE id=?");
        $stmt->bind_param("ss", $avatar,$user_id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }

    public function updateprofile($user_id,$name, $country,$country_champion){
        $stmt = $this->conn->prepare("UPDATE logins SET user_name = ? , country = ? , country_champion = ? WHERE id = ? ");
        $stmt->bind_param("ssss",$name, $country,$country_champion,$user_id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }

    public function updatepassprofile($user_id,$name, $country,$country_champion,$password){
        require_once 'PassHash.php';
        $password_hash = PassHash::hash($password);
        $stmt = $this->conn->prepare("UPDATE logins SET user_name = ? , country = ? , country_champion = ?, password =? WHERE id = ? ");
        $stmt->bind_param("ssssi",$name, $country,$country_champion,$password_hash,$user_id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }

    public function updateTotal($user_id,$quizzestaken,$totaltime,$totalanswers,$correctanswers,$incorrectanswers,$totalscore){
        $stmt = $this->conn->prepare("UPDATE logins SET quizzestaken = ? , totaltime = ?, totalanswers = ?,correctanswers = ?, incorrectanswers = ?, totalscore = ? WHERE id=?");
        $stmt->bind_param("sssssss", $quizzestaken,$totaltime,$totalanswers,$correctanswers,$incorrectanswers,$totalscore,$user_id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }
    /**
     * Deleting a task
     * @param String $task_id id of the task to delete
     */
    public function deleteTask($user_id, $task_id) {
        $stmt = $this->conn->prepare("DELETE t FROM tasks t, user_tasks ut WHERE t.id = ? AND ut.task_id = t.id AND ut.user_id = ?");
        $stmt->bind_param("ii", $task_id, $user_id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }

    /* ------------- `user_tasks` table method ------------------ */

    /**
     * Function to assign a task to user
     * @param String $user_id id of the user
     * @param String $task_id id of the task
     */
    public function createUserTask($user_id, $task_id) {
        $stmt = $this->conn->prepare("INSERT INTO user_tasks(user_id, task_id) values(?, ?)");
        $stmt->bind_param("ii", $user_id, $task_id);
        $result = $stmt->execute();

        if (false === $result) {
            die('execute() failed: ' . htmlspecialchars($stmt->error));
        }
        $stmt->close();
        return $result;
    }

    public function createUserAchievement($user_id, $achievement_id ,$progess ,$awarded) {
        $stmt = $this->conn->prepare("INSERT INTO users_achievement(user_id, achievement_id ,progess,awarded ) values(?,?,?,?)");
        $stmt->bind_param("iiis", $user_id, $achievement_id,$progess,$awarded);
        $result = $stmt->execute();

        if (false === $result) {
            die('execute() failed: ' . htmlspecialchars($stmt->error));
        }
        $stmt->close();
        return $result;
    }
    public function UpdateUserAchievement($user_id, $achievement_id ,$progess ,$awarded) {
        $stmt = $this->conn->prepare("UPDATE  users_achievement SET progess=?,awarded=? WHERE user_id=? and achievement_id=? ");
        $stmt->bind_param("isii",$progess,$awarded,$user_id, $achievement_id);
        $result = $stmt->execute();

        if (false === $result) {
            die('execute() failed: ' . htmlspecialchars($stmt->error));
        }
        $stmt->close();
        return $result;
    }
    public function CreateMatchesBets($user_id, $match_id ) {
        $stmt = $this->conn->prepare("INSERT INTO competition_bets(user_id, question_id) values(?,?)");
        $stmt->bind_param("ii",$user_id, $match_id);
        $result = $stmt->execute();
        if (false === $result) {
            die('execute() failed: ' . htmlspecialchars($stmt->error));
        }
        $stmt->close();
        return $result;
    }
    public function CreateAnswerPredictionBets($user_id, $question_id,$answer_id ) {
        $stmt = $this->conn->prepare("INSERT INTO competition_bets(user_id, question_id,user_answer_id) values(?,?,?)");
        $stmt->bind_param("iii",$user_id, $question_id,$answer_id);
        $result = $stmt->execute();
        if (false === $result) {
            die('execute() failed: ' . htmlspecialchars($stmt->error));
        }
        $stmt->close();
        return $result;
    }
    public function CreateAnswerMatchquestion($user_id, $question_id,$answer_id ) {
        $stmt = $this->conn->prepare("INSERT INTO bets(user_id, question_id,user_answer_id) values(?,?,?)");
        $stmt->bind_param("iii",$user_id, $question_id,$answer_id);
        $result = $stmt->execute();
        if (false === $result) {
            die('execute() failed: ' . htmlspecialchars($stmt->error));
        }
        $stmt->close();
        return $result;
    }
}

?>
