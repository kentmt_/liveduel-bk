<?php session_start(); 
if(!isset($_SESSION['USER_ID'])){ 
	header("Location: index.html");
}
include('db_config.php');
$userId = $_SESSION['USER_ID'];
$userTotalPoints = 0;
$sql = "SELECT 	( SELECT COALESCE(SUM(bets.points),0) FROM bets WHERE bets.user_id = l.id AND bets.is_correct = 'Y')+( SELECT COALESCE(SUM(matches_bets.points),0) FROM matches_bets WHERE matches_bets.user_id = l.id AND matches_bets.is_correct = 'Y') as 'points'
						FROM logins l , bets b, matches_bets mb 
						WHERE l.id = b.user_id AND mb.user_id = l.id AND l.id = $userId ";
foreach ($db_handler->query($sql) as $row) {
	$userTotalPoints = $row[0];
	break;
}

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
<title>LiveDuel</title>
<meta charset="iso-8859-1">
<link rel="stylesheet" href="styles/layout.css" type="text/css">
<script src="scripts/jquery-1.8.2.min.js"></script>
<script src="scripts/jquery-defaultvalue.js"></script>
<script src="scripts/jquery-browsercheck.js"></script>
<!--[if lt IE 9]>
<link rel="stylesheet" href="styles/ie.css" type="text/css">
<script src="scripts/ie/html5shiv.min.js"></script>
<![endif]-->
</head>
<body>
<div class="wrapper row1">
  <header id="header" class="clear">
    <hgroup>
      <h1><a href="home.php"><span>LiveDuel</span></a></h1>
    </hgroup>
    <form action="#" method="post">
      <fieldset>
		<span style="float:right;">Welcome, <?php echo $_SESSION['USER_NAME']; ?>&nbsp;&nbsp;&nbsp;&nbsp;<a href="logout.php">Log Out</a></span>
      </fieldset>
    </form>
	<nav>
	
      <ul class="clear">
        <!-- <li class="first"><a href="home.php">Home</a></li> -->
		<li class="first"><a href="bet.php">Make a Bet!</a></li>
		<?php
		$sql = "SELECT id, name FROM sports";
		foreach ($db_handler->query($sql) as $row) {
		?>
		<li><a href="bet.php?spotId=<?php echo $row["id"];?>"><?php echo $row["name"]; ?></a></li>
		<?php } ?>
		<li class="active"><a href="profile.php">Profile</a></li>
      </ul>
	  
    </nav>
	
  </header>
  
</div>
<!-- content -->
<div class="wrapper row3">
  <div id="container" class="clear">
    <div id="homepage">
		<h1><?php echo $_SESSION['USER_NAME']; ?>'s Profile</h1>
		<div style="float:right;"><h1><?php echo $userTotalPoints; ?> points</h1></div>
		<section style="width:100%;">
			<h2>Past Bets</h2>
			<table>
			  <thead>
				<tr>
				  <th>Match</th>
				  <th>Competition</th>
				  <th>My Answer</th>
				  <th>Correct</th>
				  <th>Points</th>
				</tr>
			  </thead>
			  <tbody>
				<!-- All pending matches-->
				<?php
					$sql = "SELECT  (SELECT name from teams where id = m.team1) as team1 ,
									   (SELECT name from teams where id = m.team2) as team2 ,
									   c.name,
									(SELECT name from teams where id = mb.user_selection) as myAnswer,
									( SELECT CASE
											WHEN winner = team1 THEN team1_points
											WHEN winner = team2 THEN team2_points
											ELSE draw_points
											END AS 'points'  FROM matches WHERE id = m.id ) as points,
											mb.is_correct
							FROM matches m, competitions c, matches_bets mb  WHERE c.id = m.competition_id AND mb.match_id = m.id AND m.is_played = 'Y' AND mb.user_id = $userId ORDER BY m.id ";
					$counter=0;
					foreach ($db_handler->query($sql) as $row) {
						if( ($counter % 2) == 0 ){
							echo "<tr class='light'>";
						}
						else{
							echo "<tr class='dark'>";
						}
						
						echo "<td>".$row["team1"]." VS ".$row["team2"]."</td>";
						echo "<td>".$row["name"]."</td>";
						if($row["myAnswer"] != ""){
							echo "<td>".$row["myAnswer"]."</td>";
						}
						else{
							echo "<td>Match is draw</td>";
						}
						if( $row["is_correct"] == "Y" ){
							echo "<td>Yes</td>";
							echo "<td>".$row["points"]."</td>";
						}
						else{
							echo "<td>No</td>";
							echo "<td>0</td>";
						}
						
						echo "</tr>";
						$counter = $counter + 1;
					}
					if($counter==0){
						echo "<tr class='light'><td colspan='5' align='center'>No data found</td></tr>";
					}
				?>
			  </tbody>
			  </tbody>
			</table>
			
			<table>
			  <thead>
				<tr>
				  <th>Question</th>
				  <th>Match</th>
				  <th>Competition</th>
				  <th>My Answer</th>
				  <th>Correct</th>
				  <th>Points</th>
				</tr>
			  </thead>
			  <tbody>
				<?php
					$sql = "SELECT q.name as 'Question', 
							(SELECT name from teams where id = m.team1) as team1 ,
							(SELECT name from teams where id = m.team2) as team2 ,
							a.answer as 'My Answer', b.points as 'Points' , c.name as 'Competition', b.is_correct as 'is_correct'
							FROM match_questions q, matches m, answers a, bets b, competitions c
							WHERE b.user_id = $userId AND q.is_answered = 'Y'
								AND m.id = q.match_id AND a.question_id = q.id AND b.question_id = q.id AND b.user_answer_id = a.id AND c.id= m.competition_id
							ORDER BY q.id";
					$counter=0;
					foreach ($db_handler->query($sql) as $row) {
						if( ($counter % 2) == 0 ){
							echo "<tr class='light'>";
						}
						else{
							echo "<tr class='dark'>";
						}
						
						echo "<td>".$row["Question"]."</td>";
						echo "<td>".$row["team1"]." VS ".$row["team2"]."</td>";
						echo "<td>".$row["Competition"]."</td>";
						echo "<td>".$row["My Answer"]."</td>";
						if(isset($row["is_correct"]) && $row["is_correct"]!="N"){
							echo "<td>Yes</td>";
							echo "<td>".$row["Points"]."</td>";
						}
						else{
							echo "<td>No</td>";
							echo "<td>0</td>";
						}
						echo "</tr>";
						$counter = $counter + 1;
					}
					if($counter==0){
						echo "<tr class='light'><td colspan='6' align='center'>No data found</td></tr>";
					}
					
				?>
			  </tbody>
			  </tbody>
			</table>
		</section>
		
		<section style="width:100%;">
			<h2>Pending Bets</h2>
			<table>
			  <thead>
				<tr>
				  <th>Match</th>
				  <th>Competition</th>
				  <th>My Answer</th>
				  <th>Points</th>
				</tr>
			  </thead>
			  <tbody>
				<!-- All pending matches-->
				<?php
					$sql = "SELECT  (SELECT name from teams where id = m.team1) as team1 ,
									   (SELECT name from teams where id = m.team2) as team2 ,
									   c.name,
									(SELECT name from teams where id = mb.user_selection) as myAnswer,
									( SELECT CASE
											WHEN mb.user_selection = mm.team1 THEN mm.team1_points
											WHEN mb.user_selection = mm.team2 THEN mm.team2_points
											ELSE mm.draw_points
											END AS 'points'  FROM matches mm WHERE id = m.id ) as points
							FROM matches m, competitions c, matches_bets mb  WHERE c.id = m.competition_id AND mb.match_id = m.id AND m.is_played = 'N' AND mb.user_id = $userId ORDER BY m.id ";
					$counter=0;
					foreach ($db_handler->query($sql) as $row) {
						if( ($counter % 2) == 0 ){
							echo "<tr class='light'>";
						}
						else{
							echo "<tr class='dark'>";
						}
						
						echo "<td>".$row["team1"]." VS ".$row["team2"]."</td>";
						echo "<td>".$row["name"]."</td>";
						if($row["myAnswer"] != ""){
							echo "<td>".$row["myAnswer"]."</td>";
						}
						else{
							echo "<td>Match is draw</td>";
						}
						echo "<td>".$row["points"]."</td>";
						echo "</tr>";
						$counter = $counter + 1;
					}
					if($counter==0){
						echo "<tr class='light'><td colspan='4' align='center'>No data found</td></tr>";
					}
				?>
			  </tbody>
			  </tbody>
			</table>
			
			<table>
			  <thead>
				<tr>
				  <th>Question</th>
				  <th>Match</th>
				  <th>Competition</th>
				  <th>My Answer</th>
				  <th>Points</th>
				</tr>
			  </thead>
			  <tbody>
				<?php
					$sql = "SELECT q.name as 'Question', 
							(SELECT name from teams where id = m.team1) as team1 ,
							(SELECT name from teams where id = m.team2) as team2 ,
							a.answer as 'My Answer', b.points as 'Points' , c.name as 'Competition'
							FROM match_questions q, matches m, answers a, bets b, competitions c
							WHERE b.user_id = $userId AND q.is_answered <> 'Y'
								AND m.id = q.match_id AND a.question_id = q.id AND b.question_id = q.id AND b.user_answer_id = a.id AND c.id= m.competition_id
							ORDER BY q.id";
					$counter=0;
					foreach ($db_handler->query($sql) as $row) {
						if( ($counter % 2) == 0 ){
							echo "<tr class='light'>";
						}
						else{
							echo "<tr class='dark'>";
						}
						
						echo "<td>".$row["Question"]."</td>";
						echo "<td>".$row["team1"]." VS ".$row["team2"]."</td>";
						echo "<td>".$row["Competition"]."</td>";
						echo "<td>".$row["My Answer"]."</td>";
						echo "<td>".$row["Points"]."</td>";
						echo "</tr>";
						$counter = $counter + 1;
					}
					if($counter==0){
						echo "<tr class='light'><td colspan='5' align='center'>No data found</td></tr>";
					}
					
				?>
			  </tbody>
			  </tbody>
			</table>
			
		</section>
		
		<section style="width:50%; float:left;">
			<h2>Leaderboard</h2>
			<table>
			  <thead>
				<tr>
				  <th>Rank</th>
				  <th>Name</th>
				  <th>Score</th>
				</tr>
			  </thead>
			  <tbody>
				<?php
					$sql = "SELECT DISTINCT l.user_name , l.id,  
							( SELECT COALESCE(SUM(bets.points),0) FROM bets WHERE bets.user_id = l.id AND bets.is_correct = 'Y')+( SELECT COALESCE(SUM(matches_bets.points),0) FROM matches_bets WHERE matches_bets.user_id = l.id AND matches_bets.is_correct = 'Y') as 'points'
						FROM logins l , bets b, matches_bets mb 
						WHERE l.id = b.user_id AND mb.user_id = l.id
						ORDER BY points DESC";
					$counter=0;
					foreach ($db_handler->query($sql) as $row) {
						if( ($counter % 2) == 0 ){
							echo "<tr class='light'>";
						}
						else{
							echo "<tr class='dark'>";
						}
						
						echo "<td>".($counter+1)."</td>";
						echo "<td><a href='viewProfile.php?id=".$row["id"]."'>".$row["user_name"]."</a></td>";
						echo "<td>".$row["points"]."</td>";
						echo "</tr>";
						$counter = $counter + 1;
					}
					if($counter==0){
						echo "<tr class='light'><td colspan='3' align='center'>No data found</td></tr>";
					}
				?>
			  </tbody>
			  </tbody>
			</table>
		</section>
		
    </div>
    <!-- / content body -->
  </div>
</div>
<!-- Copyright -->
<div class="wrapper row5">
  <footer id="copyright" class="clear">
    <p class="fl_left">Copyright &copy; 2014 - All Rights Reserved - <a href="home.php">livedule.com</a></p>
  </footer>
</div>
</body>
</html>