<?php session_start(); 
if(!isset($_SESSION['IS_ADMIN'])){ 
	header("Location: index.html");
}
$competitionId = $_REQUEST["id"];
$competitionName = "";
$sportId = "";
include('db_config.php');
$sql = "SELECT id, name, sport_id FROM competitions WHERE id= $competitionId";
foreach ($db_handler->query($sql) as $row) {
	$competitionName = $row["name"];
	$sportId = $row["sport_id"];
	break;
}
$sportName = "";
$sql = "SELECT name FROM sports WHERE id= $sportId";
foreach ($db_handler->query($sql) as $row) {
	$sportName = $row["name"];
	break;
}
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
<title>LiveDuel</title>
<meta charset="iso-8859-1">
<link rel="stylesheet" href="styles/layout.css" type="text/css">
<script src="scripts/jquery-1.8.2.min.js"></script>
<script src="scripts/jquery-defaultvalue.js"></script>
<script src="scripts/jquery-browsercheck.js"></script>
<!--[if lt IE 9]>
<link rel="stylesheet" href="styles/ie.css" type="text/css">
<script src="scripts/ie/html5shiv.min.js"></script>
<![endif]-->

</head>
<body>
<div class="wrapper row1">
  <header id="header" class="clear">
    <hgroup>
      <h1><a href="home.php"><span>LiveDuel</span></a></h1>
    </hgroup>
    <form action="#" method="post">
      <fieldset>
		<span style="float:right;">Welcome, <?php echo $_SESSION['USER_NAME']; ?>&nbsp;&nbsp;&nbsp;&nbsp;<a href="adminLogout.php">Log Out</a></span>
      </fieldset>
    </form>
	<nav>
      <ul class="clear">
        <li class="first active"><a href="adminHome.php">Admin Home</a></li>        
      </ul>
    </nav>
	
  </header>
  <div id="breadcrumb">
	<a href="adminHome.php">Sports</a> > <a href="manageSport.php?id=<?php echo $sportId ;?>"><?php echo $sportName ;?></a> > <a href="manageCompetitionTeams.php?id=<?php echo $competitionId ;?>"><?php echo $competitionName ;?> Teams</a>
  </div> 
</div>
<!-- content -->
<div class="wrapper row3">
  <div id="container" class="clear">
    <div id="homepage">
		<h1>Administration Panel</h1>
		<?php if(!isset($_REQUEST['action']) || $_REQUEST['action']!="edit") {?>
		<section id="comments" style="float:left;">
			<h2>Manage <?php echo $competitionName; ?> Teams</h2>
			<form action="insertTeam.php" method="post" enctype="multipart/form-data">
				<input name="MAX_FILE_SIZE" value="102400" type="hidden">
				<input id="competitionId" name="competitionId" value="<?php echo $competitionId ; ?>" type="hidden">
				<p>
					<input type="text" name="teamName" id="teamName" value="" size="22" />
					<label for="teamName" style="width:120px;"><small>Team Name</small></label>
				</p>
				<p>
					<input type="file" name="teamIcon" id="teamIcon" value="" size="22" />
					<label for="teamIcon" style="width:120px;"><small>Icon</small></label>
				</p>
				<p>
					<input type="file" name="teamHomeJersey" id="teamHomeJersey" value="" size="22" />
					<label for="teamHomeJersey" style="width:120px;"><small>Home Jersey</small></label>
				</p>
				<p>
					<input type="file" name="teamAwayJersey" id="teamAwayJersey" value="" size="22" />
					<label for="teamAwayJersey" style="width:120px;"><small>Away Jersey</small></label>
				</p>
				<p>
					<input type="checkbox" name="display" id="display" checked value="yes" />
					<label for="display" style="width:120px;"><small>Display Icon</small></label>
				</p>
				<p>
					<input name="submit" type="submit" id="submit" value="Add Team" onclick="return validateAddTeam();" />
				</p>
			</form>
		</section>
		<?php }?>
		<?php if(isset($_REQUEST['action']) && $_REQUEST['action']=="edit" && isset($_REQUEST['teamId'])) {
			$teamId = $_REQUEST['teamId'];
			$teamName = "";
			$display = "";
			$sql = "SELECT id, name, display FROM teams WHERE id= $teamId";
			foreach ($db_handler->query($sql) as $row) {
				$teamName = $row["name"];
				$display  = $row["display"];
				break;
			}
		?>
		<section id="comments" style="float:left;">
			<h2>Manage <?php echo $competitionName; ?> Teams</h2>
			<form action="editTeam.php" method="post" enctype="multipart/form-data">
				<input name="MAX_FILE_SIZE" value="102400" type="hidden">
				<input id="competitionId" name="competitionId" value="<?php echo $competitionId ; ?>" type="hidden">
				<input id="teamId" name="teamId" value="<?php echo $teamId ; ?>" type="hidden">
				<p>
					<input type="text" name="teamName" id="teamName" value="<?php echo $teamName ; ?>" size="22" />
					<label for="teamName" style="width:120px;"><small>Team Name</small></label>
				</p>
				<p>
					<input type="file" name="teamIcon" id="teamIcon" value="" size="22" />
					<label for="teamIcon" style="width:120px;"><small>Icon</small></label>
				</p>
				<p>
					<input type="file" name="teamHomeJersey" id="teamHomeJersey" value="" size="22" />
					<label for="teamHomeJersey" style="width:120px;"><small>Home Jersey</small></label>
				</p>
				<p>
					<input type="file" name="teamAwayJersey" id="teamAwayJersey" value="" size="22" />
					<label for="teamAwayJersey" style="width:120px;"><small>Away Jersey</small></label>
				</p>
				<p>
					<input type="checkbox" name="display" id="display" <?php if($display=='Y'){ echo "checked value='yes'" ;} ?> />
					<label for="display" style="width:120px;"><small>Display Icon</small></label>
				</p>
				<p>
					<input name="submit" type="submit" id="submit" value="Update Team" onclick="return validateUpdateTeam();" />
				</p>
			</form>
		</section>
		<?php }?>
		<section style="width:40%;float:right; padding-top:50px;">
			<table class="tableNoBorder" width="100%">
				<tr><td align="center"><a href="manageCompetitionQuestions.php?id=<?php echo $competitionId ; ?>" ><input type="button" style="cursor:pointer;" value="Manage Questions"  /></a></td></tr>
				<tr><td align="center"><a href="manageCompetitionMatches.php?id=<?php echo $competitionId ; ?>" ><input type="button" style="cursor:pointer;" value="Manage Matches"  /></a></td></tr>
			</table>
		</section>
		<br/>
		<section style="width:100%;float:left;">
			<table>
			  <thead>
				<tr>
				  <th>Team Name</th>
				  <th>Team Icon</th>
				  <th>Home Jersey</th>
				  <th>Away Jersey</th>
				  <th>Edit</th>
				  <th>Delete</th>
				</tr>
			  </thead>
			  <tbody>
				<?php
					$sql = "SELECT id, name, icon, home, away FROM teams WHERE competition_id = $competitionId ORDER BY name";
					$counter=0;
					foreach ($db_handler->query($sql) as $row) {
						if( ($counter % 2) == 0 ){
							echo "<tr class='light'>";
						}
						else{
							echo "<tr class='dark'>";
						}
						
						echo "<td>".$row["name"]."</td>";
						echo '<td align="center"><img src="data:image/jpeg;base64,' . base64_encode($row['icon']) . '" width="100" height="100"></td>';
						echo '<td align="center"><img src="data:image/jpeg;base64,' . base64_encode($row['home']) . '" width="100" height="100"></td>';
						echo '<td align="center"><img src="data:image/jpeg;base64,' . base64_encode($row['away']) . '" width="100" height="100"></td>';
						echo "<td align='center'><a title='Edit' href='manageCompetitionTeams.php?action=edit&id=$competitionId&teamId=".$row["id"]."'><img src='images/edit.png' /></a></td>";
						echo '<td align="center"><a title="Delete" href="deleteCompetitionTeam.php?id='.$row["id"].'&competitionId='.$competitionId.'" onclick="return confirm(\'Are you sure you want to delete '.$row["name"].' team?\');"><img src="images/delete.png" /></a></td>';
						echo "</tr>";
						$counter = $counter + 1;
					}
					if($counter==0){
						echo "<tr class='light'><td colspan='6' align='center'>No data found</td></tr>";
					}
				?>
			  </tbody>
			</table>
		<section>
    </div>
    <!-- / content body -->
  </div>
</div>
<!-- Copyright -->
<div class="wrapper row5">
  <footer id="copyright" class="clear">
    <p class="fl_left">Copyright &copy; 2014 - All Rights Reserved - <a href="home.php">livedule.com</a></p>
  </footer>
</div>
<script>
	function getParameterByName(name) {
		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
			results = regex.exec(location.search);
		return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
	
	if(getParameterByName("action")=="found"){
		alert("team name already exists for this competition");
	}
	
	function validateAddTeam(){
		if(document.getElementById("teamName").value==""){
			alert("please enter team name");
			document.getElementById("teamName").focus();
		}
		else if(document.getElementById("teamIcon").value==""){
			alert("please enter team icon");
			document.getElementById("teamIcon").focus();
		}
		else if(!endsWith(document.getElementById("teamIcon").value,".png") && !endsWith(document.getElementById("teamIcon").value,".jpg") && !endsWith(document.getElementById("teamIcon").value,".bmp") && !endsWith(document.getElementById("teamIcon").value,".gif")){
			alert("invalid team icon format");
		}
		else if(document.getElementById("teamHomeJersey").value==""){
			alert("please enter team home jersey");
			document.getElementById("teamIcon").focus();
		}
		else if(!endsWith(document.getElementById("teamHomeJersey").value,".png") && !endsWith(document.getElementById("teamHomeJersey").value,".jpg") && !endsWith(document.getElementById("teamHomeJersey").value,".bmp") && !endsWith(document.getElementById("teamHomeJersey").value,".gif")){
			alert("invalid team home jersey file format");
		}
		else if(document.getElementById("teamAwayJersey").value==""){
			alert("please enter team away jersey");
			document.getElementById("teamAwayJersey").focus();
		}
		else if(!endsWith(document.getElementById("teamAwayJersey").value,".png") && !endsWith(document.getElementById("teamAwayJersey").value,".jpg") && !endsWith(document.getElementById("teamAwayJersey").value,".bmp") && !endsWith(document.getElementById("teamAwayJersey").value,".gif")){
			alert("invalid team away jersey file format");
		}
		else{
			return true;
		}
		return false;
	}
	
	function validateUpdateTeam(){
		if(document.getElementById("teamName").value==""){
			alert("please enter team name");
			document.getElementById("teamName").focus();
			return false;
		}
		if(document.getElementById("teamIcon").value != ""){
			if(!endsWith(document.getElementById("teamIcon").value,".png") && !endsWith(document.getElementById("teamIcon").value,".jpg") && !endsWith(document.getElementById("teamIcon").value,".bmp") && !endsWith(document.getElementById("teamIcon").value,".gif")){
				alert("invalid team icon format");
				return false;
			}
		}
		if(document.getElementById("teamHomeJersey").value != ""){
			if(!endsWith(document.getElementById("teamHomeJersey").value,".png") && !endsWith(document.getElementById("teamHomeJersey").value,".jpg") && !endsWith(document.getElementById("teamHomeJersey").value,".bmp") && !endsWith(document.getElementById("teamHomeJersey").value,".gif")){
				alert("invalid team home jersey file format");
				return false;
			}
		}
		if(document.getElementById("teamAwayJersey").value != ""){
			if(!endsWith(document.getElementById("teamAwayJersey").value,".png") && !endsWith(document.getElementById("teamAwayJersey").value,".jpg") && !endsWith(document.getElementById("teamAwayJersey").value,".bmp") && !endsWith(document.getElementById("teamAwayJersey").value,".gif")){
				alert("invalid team away jersey file format");
				return false;
			}
		}
		return true;
	}
	
	function endsWith(str, suffix) {
		return str.toLowerCase().indexOf(suffix, str.length - suffix.length) !== -1;
	}
	</script>
</body>
</html>