<?php session_start(); 
if(!isset($_SESSION['USER_ID'])){ 
	header("Location: index.html");
}
include('db_config.php');
$sportName;
// if no match or competition, just pick the biggest game 
$url = "bet.php?action=nodata";
if(!isset($_REQUEST["matchId"]) && !isset($_REQUEST["competitionId"]) && !isset($_REQUEST["spotId"]) && !isset($_REQUEST["action"])){
	$sql = "SELECT id, competition_id FROM matches WHERE is_played <> 'Y' AND end_datetime > NOW() HAVING MAX(team1_points+team2_points+draw_points)  LIMIT 1";
	foreach ($db_handler->query($sql) as $row) {
		$url = "bet.php?action=match&matchId=".$row["id"]."&competitionId=".$row["competition_id"];
		break;
	}
	header("Location: $url");
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>LiveDuel</title>
		<!-- JQuery JS -->
		<script src="bootstrap/jquery-1.js"></script>
		<!-- Bootstrap -->
		<link href="bootstrap/bootstrap.css" rel="stylesheet">
		<script src="bootstrap/bootstrap.js"></script>
		<script src="bootstrap/jqBootstrapValidation.js"></script>
		<script src="bootstrap/bootstrap-typeahead.js"></script>
		
		<!-- Liveduel custom styles -->
		<link href="styles/liveduel.css" rel="stylesheet">
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div id="wrap">
			<div class="navbar navbar-default navbar-fixed-top" role="navigation">
			  <div class="container">
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				  <a class="navbar-brand" href="home.php">LiveDuel</a>
				</div>
				<div style="height: 1px;" class="navbar-collapse collapse">
				  <ul class="nav navbar-nav">
					<li class="active"><a href="bet.php">Make a Bet!</a></li>
					<?php
					$sql = "SELECT id, name FROM sports";
					foreach ($db_handler->query($sql) as $row) {
					?>
					<li><a href="bet.php?spotId=<?php echo $row["id"];?>"><?php echo $row["name"]; ?></a></li>
					<?php } ?>
					<li><a href="invite.php">Invite Friends</a></li>
					<li><a href="profile.php">Profile</a></li>
				  </ul>
				  
				</div><!--/.nav-collapse -->
			  </div>
			</div>
			<div class="container">
				<p><span style="float:right;">Welcome, <?php echo $_SESSION['USER_NAME']; ?>&nbsp;&nbsp;&nbsp;&nbsp;<a href="logout.php">Log Out</a></span></p>
				<h3>Make a BET!</h3>
				<div class="accodionMenu">
					<ul class="nav nav-list">
						<?php
						$sql = "SELECT id, name FROM sports";
						foreach ($db_handler->query($sql) as $row) {
						?>
						<li>
							<a class="accordion-heading" data-toggle="collapse" data-target="#sport<?php echo $row['id']; ?>">
							  <b><?php echo $row['name']; ?></b>
							</a>
							<?php 
								$class = "collapse";
								if(isset($_REQUEST["spotId"]) && $_REQUEST["spotId"] == $row["id"]){
									$class= "collapse in";
									$sportName = $row['name'];
								}
								else if(isset($_REQUEST["competitionId"])){
									$sql6 = "SELECT sport_id FROM competitions WHERE id = ".$_REQUEST["competitionId"];
									$theSportId;
									foreach ($db_handler->query($sql6) as $row6){
										if( $row["id"] == $row6["sport_id"]){
											$class= "collapse in";
										}
										break;
									}
								}
							?>
							<ul class="nav nav-list <?php echo $class; ?>" style="padding-left:10px;" id="sport<?php echo $row['id']; ?>">
							
							<?php
							$sql2 = "SELECT id, name FROM competitions WHERE sport_id = ".$row['id'];
							foreach ($db_handler->query($sql2) as $row2){
							?>
								<li>
									<a class="accordion-heading" data-toggle="collapse" data-target="#competition<?php echo $row2['id']; ?>" href="bet.php?action=competition&competitionId=<?php echo $row2["id"];?>">
										<?php echo $row2['name']; ?>
									</a>
									<?php
										$class= "collapse";
										if(isset($_REQUEST["competitionId"]) && $_REQUEST["competitionId"]==$row2["id"]){
											$class= "collapse in";
										}
									?>
									<ul class="nav nav-list <?php echo $class; ?>" style="padding-left:15px;" id="competition<?php echo $row2['id']; ?>">
							
									<?php
										$sql5 = "SELECT id, (SELECT name from teams where id = m.team1) as team1 ,
													   (SELECT name from teams where id = m.team2) as team2 , team1_points , team2_points, draw_points, team1 as team1_id, team2 as team2_id, is_played, winner
											FROM matches m WHERE is_played = 'N' AND end_datetime > NOW() AND competition_id = ".$row2['id'];
										foreach ($db_handler->query($sql5) as $row5){
									?>
										<li><a href="bet.php?action=match&matchId=<?php echo $row5["id"];?>&competitionId=<?php echo $row2["id"];?>"><i><?php echo $row5['team1']." VS ".$row5['team2']; ?></i></a></li>	
									<?php } ?>
									</ul>
								</li>
							<?php } ?>
							</ul>
						</li>
						<?php } ?>
					</ul>
				</div>
				
				
				<?php if( isset($_REQUEST["matchId"]) && isset($_REQUEST["action"]) ) {
						$matchId = $_REQUEST["matchId"];
						$sql = "SELECT id, (SELECT name from teams where id = m.team1) as team1 ,
											   (SELECT name from teams where id = m.team2) as team2 ,
											   (SELECT icon from teams where id = m.team1) as icon1 ,
											   (SELECT icon from teams where id = m.team2) as icon2 ,
											   (SELECT home from teams where id = m.team1) as home1 ,
											   (SELECT home from teams where id = m.team2) as home2 ,
											   (SELECT display from teams where id = m.team1) as display1 ,
											   (SELECT display from teams where id = m.team2) as display2 ,
											   team1_points , team2_points, draw_points, team1 as team1_id, team2 as team2_id, is_played, winner
									FROM matches m WHERE id = $matchId ";
						
						foreach ($db_handler->query($sql) as $row) {
							$team1 = $row["team1"];
							$team2 = $row["team2"];
							$icon1 = $row["icon1"];
							$icon2 = $row["icon2"];
							$home1 = $row["home1"];
							$home2 = $row["home2"];
							$display1 = $row["display1"];
							$display2 = $row["display2"];
							$points1 = $row["team1_points"];
							$points2 = $row["team2_points"];
							$pointsDraw = $row["draw_points"];
							$team1Id = $row["team1_id"];
							$team2Id = $row["team2_id"];
							break;
						}
						
				?>
				<div class="matchesDiv">
					<div class="alert alert-info" id="messageDiv"></div>
					<form action="insertMatchBet.php" name="betForm" id="betForm">
						<input type="hidden" name="matchId" value="<?php echo $matchId; ?>" />
						<input type="hidden" name="userSelection" id="userSelection" value="" />
						<table class='tableNoBorder' width="100%">
							<tr>
								<?php if($display1 == 'Y') {?>
								<td align="center"><img title="<?php echo $team1; ?>" src="data:image/jpeg;base64,<?php echo base64_encode($icon1); ?>" width="30" height="20" /></td>
								<?php } else{ ?>
								<td align="center"><?php echo $team1; ?></td>
								<?php } ?>
								<td align="center"><img src="images/vs.PNG" title="VS"/></td>
								<?php if($display2 == 'Y') {?>
								<td align="center"><img title="<?php echo $team2; ?>" src="data:image/jpeg;base64,<?php echo base64_encode($icon2); ?>" width="30" height="20" /></td>
								<?php } else{ ?>
								<td align="center"><?php echo $team2; ?></td>
								<?php } ?>
							</tr>
							<tr>
								<td align="center">
									<a data-toggle="modal" data-target="#popup1" ><img style="cursor:pointer;" data-target="#popup1" src="data:image/jpeg;base64,<?php echo base64_encode($home1); ?>" width="100" height="100" /></a>
								</td>
								<td align="center" style="vertical-align:middle;">
									<a data-toggle="modal" data-target="#popup3" ><img src="images/draw.png" style="cursor:pointer;" data-target="#popup3" width="100" height="100" /></a>
								</td>
								<td align="center">
									<a data-toggle="modal" data-target="#popup2" ><img style="cursor:pointer;" src="data:image/jpeg;base64,<?php echo base64_encode($home2); ?>" width="100" height="100" /></a>
								</td>
							</tr>
							<tr>
								<td align="center">
									<button type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#popup1">
									  <?php echo $team1; ?> wins <?php echo $points1; ?> Pts
									</button>
								</td>
								<td align="center" style="vertical-align:middle;">
									<button type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#popup3">
									  Draw <?php echo $pointsDraw; ?> Pts
									</button>
								</td>
								<td align="center">
									<button type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#popup2">
									  <?php echo $team2; ?> wins <?php echo $points2; ?> Pts
									</button>
								</td>
							</tr>
						</table>
						
						<!-- Modal Dialogs -->
						<div class="modal fade" id="popup1" tabindex="-1" role="dialog" aria-labelledby="popup1Label" aria-hidden="true">
						  <div class="modal-dialog">
							<div class="modal-content">
							  <div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title" id="popup1Label">Are you sure?</h4>
							  </div>
							  <div class="modal-body">
								The winner is <?php echo $team1; ?> (<?php echo $points1; ?> Pts)!
								<img style="float:right;" src="data:image/jpeg;base64,<?php echo base64_encode($home1); ?>" width="100" height="100" />
								<br/><br/><br/>
								<button type="button" class="btn btn-primary" onclick="submitBet(<?php echo $team1Id ; ?>)">Submit Bet</button>
							  </div>
							</div>
						  </div>
						</div>
						
						<div class="modal fade" id="popup2" tabindex="-1" role="dialog" aria-labelledby="popup2Label" aria-hidden="true">
						  <div class="modal-dialog">
							<div class="modal-content">
							  <div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title" id="popup2Label">Are you sure?</h4>
							  </div>
							  <div class="modal-body">
								The winner is <?php echo $team2; ?> (<?php echo $points2; ?> Pts)!
								<img style="float:right;" src="data:image/jpeg;base64,<?php echo base64_encode($home2); ?>" width="100" height="100" />
								<br/><br/><br/>
								<button type="button" class="btn btn-primary" onclick="submitBet(<?php echo $team2Id ; ?>)">Submit Bet</button>
							  </div>
							</div>
						  </div>
						</div>
						
						<div class="modal fade" id="popup3" tabindex="-1" role="dialog" aria-labelledby="popup3Label" aria-hidden="true">
						  <div class="modal-dialog">
							<div class="modal-content">
							  <div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title" id="popup3Label">Are you sure?</h4>
							  </div>
							  <div class="modal-body">
								The match result is draw (<?php echo $pointsDraw; ?> Pts)!
								<img style="float:right;" src="images/draw.png" width="100" height="100" />
								<br/><br/><br/>
								<button type="button" class="btn btn-primary" onclick="submitBet(0)">Submit Bet</button>
							  </div>
							</div>
						  </div>
						</div>
						
					</form>
					<div class="questionsDiv">
					<?php
						$sql = "SELECT id, name FROM match_questions WHERE match_id = $matchId AND is_answered = 'N' ORDER BY name";
						foreach ($db_handler->query($sql) as $row){
							echo "<h4 style='margin:0px;'>".$row["name"]."</h4>";
							echo "<table class='table'>";
							
							$sql2 = "SELECT id , answer, points, icon FROM match_answers WHERE question_id = ".$row["id"]." ORDER BY answer";
							echo "<tr>";
							foreach ($db_handler->query($sql2) as $row2){
								if(isset($row2["icon"])){
									
									echo '<td align="center"><a href="insertBet.php?competitionId='.$_REQUEST["competitionId"].'&questionId='.$row["id"].'&answerId='.$row2["id"].'&points='.$row2["points"].'" title="Record answer" onclick="return confirmAnswer();">';
									echo '<img src="data:image/jpeg;base64,' . base64_encode($row2['icon']) . '" width="50" height="50">';
									echo '</a></td>';
									
								}
							}
							echo "</tr>";
							echo "<tr>";
							$sql2 = "SELECT id , answer, points, icon FROM match_answers WHERE question_id = ".$row["id"]." ORDER BY answer";
							foreach ($db_handler->query($sql2) as $row2){
								echo "<td align='center'><a href='insertBet.php?competitionId=".$_REQUEST["competitionId"]."&questionId=".$row["id"]."&answerId=".$row2["id"]."&points=".$row2["points"]."' title='Record answer' onclick='return confirmAnswer();' class='linkButton'><b>".$row2["answer"]."</b> <small>".$row2["points"]." Pts</small></a></td>";
							}
							echo "</tr>";
							echo "</table>";
						}
					?>
						<div style="float:right; margin-bottom:10px;"><button type="button" class="btn btn-lg btn-primary" data-toggle="modal" data-target="#challengePopup">Challenge</button></div>
						<div class="modal fade" id="challengePopup" tabindex="-1" role="dialog" aria-labelledby="challengePopupLabel" aria-hidden="true">
						  <div class="modal-dialog">
							<div class="modal-content">
							<form class="form-horizontal" role="form" method="post" action="placeChallenge.php">
								<input type="hidden" name="matchId" value="<?php echo $matchId; ?>" />
								<input type="hidden" name="competitionId" value="<?php echo $_REQUEST["competitionId"]; ?>" />
							  <div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title" id="challengePopupLabel">Challenge a Friend</h4>
							  </div>
							  <div class="modal-body">
								<div class="control-group">
									<label class="control-label">Search Friend</label>
									<div class="controls">
										<input type="text" class="form-control" id="friend" name="friend" required 
												autocomplete="off" spellcheck="false" data-items="8"
												placeholder="Search users..."  />
										<p class="help-block"></p>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Coins</label>
									<div class="controls">
										<input type="number" class="form-control" id="points" name="points" autocomplete="off" placeholder="Coins" required />
									</div>
								</div>
								<div class="control-group">
									<label class='control-label'>Select Question(s)</label>
									<div class="controls form-inline">
										<?php
											$sql = "SELECT id, name FROM match_questions WHERE match_id = $matchId AND is_answered = 'N' ORDER BY name";
											foreach ($db_handler->query($sql) as $row){
												echo "<input id='check".$row["id"]."' value='".$row["id"]."' type='checkbox' name='questions[]' /><label for='check".$row["id"]."'>".$row["name"]."</label>";
												echo "<p></p>";
											}
										?>
									</div>
								</div>
								</div>
								<script>
								  $(function () { $("input,select,textarea").not("[type=submit]").jqBootstrapValidation(); } );
								  $(function () {
									  $('#friend').typeahead({
										source: [
											<?php
											$sql = "SELECT DISTINCT l.user_name , l.id
											FROM logins l
											WHERE id <> ".$_SESSION['USER_ID']." 
											ORDER BY user_name";
											foreach ($db_handler->query($sql) as $row) {
												echo "{ id: ".$row["id"].", name: '".$row["user_name"]."' },";
											}
											?>
											],
											display: 'name',
											val: 'id'
										});
								} );
								$('.selectpicker').selectpicker();
								
								</script>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									<button type="submit" class="btn btn-primary" onclick="return validateChallenge();">Challenge</button>
								</div>
							  </div>
							  
							  </form>
							</div>
						  </div>
						</div>
					</div>
				</div>
				
				<?php } ?>
			  
				<?php if( isset($_REQUEST["competitionId"]) && isset($_REQUEST["action"]) && !isset($_REQUEST["matchId"]) ) {
						$competitionId = $_REQUEST["competitionId"];
				?>
				<div class="matchesDiv">
					<div class="alert alert-info" id="messageDiv"></div>
					<div class="questionsDiv">
					<?php
						$sql = "SELECT id, name FROM questions WHERE competition_id = $competitionId AND is_answered = 'N' ORDER BY name";
						foreach ($db_handler->query($sql) as $row){
							echo "<h4 style='margin:0px;'>".$row["name"]."</h4>";
							echo "<table class='table'>";
							
							$sql2 = "SELECT id , answer, points, icon FROM answers WHERE question_id = ".$row["id"]." ORDER BY answer";
							echo "<tr>";
							foreach ($db_handler->query($sql2) as $row2){
								if(isset($row2["icon"])){
									
									echo '<td align="center"><a href="insertCompetitionBet.php?competitionId='.$_REQUEST["competitionId"].'&questionId='.$row["id"].'&answerId='.$row2["id"].'&points='.$row2["points"].'" title="Record answer" onclick="return confirmAnswer();">';
									echo '<img src="data:image/jpeg;base64,' . base64_encode($row2['icon']) . '" width="50" height="50">';
									echo '</a></td>';
									
								}
							}
							echo "</tr>";
							echo "<tr>";
							$sql2 = "SELECT id , answer, points, icon FROM answers WHERE question_id = ".$row["id"]." ORDER BY answer";
							foreach ($db_handler->query($sql2) as $row2){
								echo "<td align='center'><a href='insertCompetitionBet.php?competitionId=".$_REQUEST["competitionId"]."&questionId=".$row["id"]."&answerId=".$row2["id"]."&points=".$row2["points"]."' title='Record answer' onclick='return confirmAnswer();' class='linkButton'><b>".$row2["answer"]."</b> <small>".$row2["points"]." Pts</small></a></td>";
							}
							echo "</tr>";
							echo "</table>";
						}
					?>
						<div style="float:right; margin-bottom:10px;"><button type="button" class="btn btn-lg btn-primary" data-toggle="modal" data-target="#challengePopup">Challenge</button></div>
						<div class="modal fade" id="challengePopup" tabindex="-1" role="dialog" aria-labelledby="challengePopupLabel" aria-hidden="true">
						  <div class="modal-dialog">
							<div class="modal-content">
							<form class="form-horizontal" role="form" method="post" action="placeCompetitionChallenge.php">
								<input type="hidden" name="competitionId" value="<?php echo $_REQUEST["competitionId"]; ?>" />
							  <div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title" id="challengePopupLabel">Challenge a Friend</h4>
							  </div>
							  <div class="modal-body">
								<div class="control-group">
									<label class="control-label">Search Friend</label>
									<div class="controls">
										<input type="text" class="form-control" id="friend" name="friend" required 
												autocomplete="off" spellcheck="false" data-items="8"
												placeholder="Search users..."  />
										<p class="help-block"></p>
									</div>
								</div>
								<div class="control-group">
									<label class='control-label'>Select Question(s)</label>
									<div class="controls form-inline">
										<?php
											$sql = "SELECT id, name FROM questions WHERE competition_id = $competitionId AND is_answered = 'N' ORDER BY name";
											foreach ($db_handler->query($sql) as $row){
												echo "<input id='check".$row["id"]."' value='".$row["id"]."' type='checkbox' name='questions[]' /><label for='check".$row["id"]."'>".$row["name"]."</label>";
												echo '  <input type="text" class="form-control" id="coins'.$row["id"].'" name="coins'.$row["id"].'" autocomplete="off" placeholder="Coins"	 /><p></p>';
											}
										?>
									</div>
								</div>
								</div>
								<script>
								  $(function () { $("input,select,textarea").not("[type=submit]").jqBootstrapValidation(); } );
								  $(function () {
									  $('#friend').typeahead({
										source: [
											<?php
											$sql = "SELECT DISTINCT l.user_name , l.id
											FROM logins l
											WHERE id <> ".$_SESSION['USER_ID']." 
											ORDER BY user_name";
											foreach ($db_handler->query($sql) as $row) {
												echo "{ id: ".$row["id"].", name: '".$row["user_name"]."' },";
											}
											?>
											],
											display: 'name',
											val: 'id'
										});
								} );
								$('.selectpicker').selectpicker();
								
								</script>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									<button type="submit" class="btn btn-primary" onclick="return validateChallenge();">Challenge</button>
								</div>
							  </div>
							  
							  </form>
							</div>
						  </div>
						</div>
					</div>
				</div>
				
				<?php } ?>
				<?php if( isset($_REQUEST["spotId"]) ) {?>
				<!-- Leaderboard -->
				<h4><?php echo $sportName; ?> Leaderboard</h4>
				<table class="table table-striped" style="width:300px;">
				  <thead>
					<tr>
					  <th>Rank</th>
					  <th>Name</th>
					  <th>Score</th>
					</tr>
				  </thead>
				  <tbody>
					<?php
						$sportId= $_REQUEST["spotId"];
						$sql = "SELECT DISTINCT l.user_name , l.id,  
								( SELECT COALESCE(SUM(bets.points),0) FROM bets, match_questions, matches, competitions, sports 
									WHERE bets.question_id = match_questions.id AND matches.id = match_questions.match_id AND matches.competition_id = competitions.id AND sports.id = competitions.sport_id 
											AND bets.user_id = l.id AND bets.is_correct = 'Y' AND sports.id = $sportId )+
								( SELECT COALESCE(SUM(matches_bets.points),0) FROM matches_bets, matches, competitions, sports 
									WHERE matches_bets.match_id = matches.id AND matches.competition_id = competitions.id AND sports.id = competitions.sport_id
											AND matches_bets.user_id = l.id AND matches_bets.is_correct = 'Y' AND sports.id = $sportId)+
								( SELECT COALESCE(SUM(competition_bets.points),0) FROM competition_bets, questions, competitions, sports 
									WHERE competition_bets.question_id = questions.id AND questions.competition_id = competitions.id AND sports.id = competitions.sport_id 
											AND competition_bets.user_id = l.id AND competition_bets.is_correct = 'Y' AND sports.id = $sportId) 
								as 'points'
							FROM logins l
							ORDER BY points DESC";
						$counter=0;
						foreach ($db_handler->query($sql) as $row) {
							if( ($counter % 2) == 0 ){
								echo "<tr class='light'>";
							}
							else{
								echo "<tr class='dark'>";
							}
							
							echo "<td>".($counter+1)."</td>";
							echo "<td><a href='viewProfile.php?id=".$row["id"]."'>".$row["user_name"]."</a></td>";
							$userChallengesPoints = getUserChallengesPoints($row["id"],$sportId);
							$userChallengesPoints += $row["points"];
							echo "<td>$userChallengesPoints</td>";
							echo "</tr>";
							$counter = $counter + 1;
						}
						if($counter==0){
							echo "<tr class='light'><td colspan='3' align='center'>No data found</td></tr>";
						}
					?>
				  </tbody>
				</table>
				<?php } ?>
			  <div class="alert alert-danger" id="errorDiv"></div>
			</div> <!-- /container -->
		</div>
		<div id="footer">
		  <div class="container">
			<p class="text-muted">Copyright &copy; 2014 - All Rights Reserved - <a href="home.php">livedule.com</a></p>
		  </div>
		</div>
		<script>
			function validateChallenge(){
				var checks = $("input:checkbox");
				var ready = false;
				// make sure user checked at least one question
				checks.each(function(){
					if ($(this).is(":checked")) {
						ready = true;
					}
				});
				if (!ready) {
					alert("Please at least select one question");
					return false;
				}
				// make sure coins is entered for check questions
				/*
				var exists = true;
				checks.each(function(){
					if ($(this).is(":checked")) {
						var coins = $(":input:eq(" + ($(":input").index(this) + 1) + ")").val();
						if( coins.trim() == "" ){
							exists = false;
						}
					}
				});
				if(!exists){
					alert("please enter coins for all selected questions");
					return false;
				}
				
				// make sure coins is valid for check questions
				var valid = true;
				checks.each(function(){
					if ($(this).is(":checked")) {
						var coins = $(":input:eq(" + ($(":input").index(this) + 1) + ")").val();
						if( !isNormalInteger(coins.trim())){
							valid = false;
						}
					}
				});
				*/
				if(!valid){
					alert("please enter valid number for coins for all selected questions");
					return false;
				}
			}
			
			function getParameterByName(name) {
				name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
				var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
					results = regex.exec(location.search);
				return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
			}
			
			function selectTeam(id, team, points, winner){
				if(winner == 1 ){
					$( "#popup1" ).dialog({modal: true,width:400,position: [360,130],show: 'blind', hide: 'explode'});
				}
				else if(winner == 2 ){
					$( "#popup2" ).dialog({modal: true,width:400,position: [360,130],show: 'blind', hide: 'explode'});
				}
				else{
					$( "#popup3" ).dialog({modal: true,width:400,position: [360,130],show: 'blind', hide: 'explode'});
				}
			}
			
			function submitBet(id){
				$("#userSelection").val(id);	
				$("#betForm").submit();
				$('#popup1').dialog('close');
				$('#popup2').dialog('close');
				$('#popup3').dialog('close');
			}
			
			function confirmAnswer(){
				return confirm("Are you sure you want to record this answer?");
			}
			
			if(getParameterByName("action")=="successBet"){
				$("#messageDiv").html("Bet is placed successfully");
				$('#messageDiv').show();
				window.setTimeout("$('#messageDiv').hide('slow');",2000);
			}
			else if(getParameterByName("action")=="successAnswer"){
				$("#messageDiv").html("Answer is recorded successfully");
				$('#messageDiv').show();
				window.setTimeout("$('#messageDiv').hide('slow');",2000);
			}
			else if(getParameterByName("action")=="successChallenge"){
				$("#messageDiv").html("Challenge is recorded successfully");
				$('#messageDiv').show();
				window.setTimeout("$('#messageDiv').hide('slow');",2000);
			}
			else if(getParameterByName("action")=="friendNotFound"){
				$("#messageDiv").html("Friend user name is invalid");
				$('#messageDiv').show();
				window.setTimeout("$('#messageDiv').hide('slow');",2000);
			}
			
			function isNormalInteger(str) {
				var n = ~~Number(str);
				return String(n) === str && n > 0;
			}
		</script>
	</body>
</html>

<?php
	function getChallengeWinnerId($challengeId){
		global $db_handler;
		$sql = "SELECT user1_id, user2_id FROM challenges WHERE challenges.id = $challengeId";
		$user1_id = 0;
		$user2_id = 0;
		$user1_points = 0;
		$user2_points = 0;
		foreach ($db_handler->query($sql) as $row) {
			$user1_id = $row["user1_id"];
			$user2_id = $row["user2_id"];
			break;
		}
		
		$sql = "SELECT SUM(ma.points) as points
				FROM challenges ch, challenge_questions chq, match_questions mq, match_answers ma, bets b
				WHERE ch.id = chq.challenge_id AND chq.question_id = mq.id AND mq.id = ma.question_id AND b.user_answer_id = ma.id
					AND b.user_id = $user1_id AND ma.is_correct = 'Y' AND ch.id = $challengeId";
		foreach ($db_handler->query($sql) as $row) {
			$user1_points = $row["points"];
			break;
		}
		
		$sql = "SELECT SUM(ma.points) as points
				FROM challenges ch, challenge_questions chq, match_questions mq, match_answers ma, bets b
				WHERE ch.id = chq.challenge_id AND chq.question_id = mq.id AND mq.id = ma.question_id AND b.user_answer_id = ma.id
					AND b.user_id = $user2_id AND ma.is_correct = 'Y' AND ch.id = $challengeId";
		foreach ($db_handler->query($sql) as $row) {
			$user2_points = $row["points"];
			break;
		}
		if($user1_points > $user2_points)
			return $user1_id;
		else if($user1_points < $user2_points)
			return $user2_id;
		else
			return 0;
	}
	
	function getUserChallengesPoints($userId, $sportId){
		global $db_handler;
		$sql = "SELECT ch.id FROM challenges ch, matches, competitions, sports 
				WHERE ch.match_id = matches.id AND matches.competition_id = competitions.id AND sports.id = competitions.sport_id
					AND sports.id = $sportId
					AND (ch.user1_id = $userId OR ch.user2_id = $userId )
					AND (SELECT COUNT(*) FROM challenge_questions chq, match_questions mq, challenges
											WHERE challenges.id = chq.challenge_id AND mq.id = chq.question_id
												AND mq.is_answered = 'N'
												AND challenges.id = ch.id
										) = 0";
		$userEarnedPoints = 0;
		foreach ($db_handler->query($sql) as $row) {
			$winnerId = getChallengeWinnerId($row["id"]);
			if($winnerId == $userId){
				$sql2 = "SELECT points
						FROM challenges ch
						WHERE ch.id = ".$row["id"];
				foreach ($db_handler->query($sql2) as $row2) {
					$userEarnedPoints += $row2["points"];
					break;
				}
			}
			else if($winnerId == 0){
				$sql2 = "SELECT points
						FROM challenges ch
						WHERE ch.id = ".$row["id"];
				foreach ($db_handler->query($sql2) as $row2) {
					$userEarnedPoints += 0;
					break;
				}
			}
			else{
				$sql2 = "SELECT points
						FROM challenges ch
						WHERE ch.id = ".$row["id"];
				foreach ($db_handler->query($sql2) as $row2) {
					$userEarnedPoints -= $row2["points"];
					break;
				}
			}
		}
		return $userEarnedPoints;
	}
?>